#!/usr/bin/env bash

find _site -name '*.html' -print0 | xargs -0 perl -i -pwe '
  my $base = "https://www.klimacamp-augsburg.de/";

  sub fixup {
    my $path = shift;
    return $path unless $path =~ /^\Q$base/;

    $path =~ s/^\Q$base//;

    my $level = () = $ARGV =~ /\//g;

    return ("../" x ($level - 1)) . $path;
  }

  s/<a href="(.*?)"/"<a href=\"" . fixup($1) . "\""/eg;
  s/src="(.*?)"/"src=\"" . fixup($1) . "\""/eg;
'
