---
layout: home
title: Start
permalink: /
nav_order: 10
keywords:
  - Augsburg
  - Augsburger
  - Camp
  - CO₂
  - Demonstration
  - Energiewende
  - FFF
  - Fischmarkt
  - Fridays for Future
  - Klimacamp
  - Klimagerechtigkeit
  - Klimaschutz
  - Kohleausstiegsgesetz
  - Kohleeinstiegsgesetz
  - Lokalpolitik
  - Mobilitätswende
  - Moritzplatz
  - Pariser Klimaschutzabkommen
  - Übereinkommen von Paris
  - Protest
  - Rathaus
  - Rathausplatz
  - Restbudget
  - Stadtrat
  - Verkehrswende
  - Versammlung
  - Wissenschaft
  - Workshop
---

<!--div style="background-color: #FFFF20; padding: 1em; text-align:center">
  <strong>
    Wettervorsage:
    Genießen Sie den Sommer.
    Nach den
    <a href="/weltklimaberichte/">Prognosen von Klimawissenschaftler*innen</a>
    ist es einer der kühlsten Sommer für den gesamten Rest Ihres Lebens.
  </strong>
</div-->

# Webseite des Augsburger Klimacamps ⛺📢

Als Ausdruck des Protests
gegen unzureichende Klimagerechtigkeitsmaßnahmen
besteht das Klimacamp seit dem 1. Juli 2020
auf dem Fischmarkt neben dem Augsburger Rathaus¹.
Auf dieser Webseite erklären wir
– das Klimacamp – viel von dem,
was [wir fordern](/forderungen/),
was [wir getan haben](/tagebuch/)
und was [wir planen](/programm/).
Hier dokumentieren wir
[unsere Kommunikation mit der Presse](/presse/)
und geben
[Hintergrundinformationen und Erklärungen](/informationen/)
zu verschiedenen Sachverhalten und Konzepten.

<sup>1 Aufgrund der Gefahr durch herabfallende Steine am Perlachturm
war das Klimacamp vom 9. Dezember 2021 (Tag 527)
bis zum 12. Mai 2022 (Tag 681) zum Moritzplatz umgezogen.
Inzwischen haben wir unseren alten Platz
in Sichtweite der Augsburger Stadtpolitik wieder eingenommen.</sup>


<a href="https://www.speicherleck.de/iblech/stuff/augsburg-klimacamp/">
  <img src="/welcome.small.webp"
       width="960"
       height="326"
       style="width: 100%; height: auto; aspect-ratio: auto"
       alt="Unser Klimacamp am Moritzplatz zu Jahresbeginn 2022: Vor dem Camp haben sich etwa ein dutzend Interessierte versammelt und lesen die zahlreichen Banner und Plakete. Ein Vertreter des Camps erzählt von der Arbeit des Camps.">
</a>


## Aktuell Themen in und um Augsburg:

* 🌳🌲🌳 *Bobinger Auwald bleibt!*:
  [https://www.bobinger-auwald-bleibt.de/](https://www.bobinger-auwald-bleibt.de/)

* 🌳🌲🌳 Rodung des Lohwaldes bei Meitingen:
  [https://www.lohwibleibt.de/](https://www.lohwibleibt.de/)

* 🚲🚋 *Verkehrswendeplan Augsburg*:
  [https://www.verkehrswende-augsburg.de](https://www.verkehrswende-augsburg.de)

* *Pimmelgate Süd*:
  [https://www.pimmelgate-süd.de/](https://www.pimmelgate-süd.de/)
  <br>und<br>
  Hausdurchsuchung wegen Sprühkreide:
  [https://www.pimmelgate-süd.de/kreide/](https://www.pimmelgate-süd.de/kreide/)

* 🗫🗨 Wir stehen für (Online-)Vorträge und Gesprächsrunden,
  auch für Schulklassen, zur Verfügung.

* 🌳 Baumallianz zum Erhalt der Bäume am Bahnhofsvorplatz und an anderen Orten:
  <a href="https://baumallianz-augsburg.de/">https://baumallianz-augsburg.de/</a>

* 🖎✍ Unterschriftensammlungen für Volks- und Bürgerbegehren:
  * 🚲 [Radentscheid Bayern](https://radentscheid-bayern.de/)
  * [Augsburg erdgasfrei](https://augsburg-erdgasfrei.de/)


## Berichterstattung über das Camp

[Bericht in ARD-Tagesthemen](https://www.youtube.com/watch?t=810&v=94LAR0I_BSY&feature=youtu.be){: .btn .btn-purple }

Für weitere Medienberichte über unser Klimacamp
siehe auch unseren [Pressespiegel](/pressespiegel/).


## Eindrücke aus dem Camp

<style>
  .carousel-container { position: relative; }
  .carousel { overflow-x: scroll; scroll-snap-type: x mandatory; scroll-behavior: smooth; display: flex; }
  .carousel a { flex-shrink: 0; scroll-snap-align: start; }
</style>

<div class="carousel-container" style="padding-top: 1em">
  <div class="carousel">
    <a href="https://www.speicherleck.de/iblech/stuff/klimacamp-2021-auswahl/verschneit.jpeg"><img loading="lazy" alt="" src="https://www.speicherleck.de/iblech/stuff/klimacamp-2021-auswahl/verschneit.webp" width="538" height="302" style="display: block; width: auto; height: 20em; aspect-ratio: auto" alt="Das verschneite Klimacamp am Standort neben dem Rathaus"></a>
    <a href="https://www.speicherleck.de/iblech/stuff/klimacamp-2021-auswahl/photo_2021-02-08_14-45-41.jpg"><img loading="lazy" alt="" src="https://www.speicherleck.de/iblech/stuff/klimacamp-2021-auswahl/photo_2021-02-08_14-45-41.small.webp" width="768" height="576" style="display: block; width: auto; height: 20em; aspect-ratio: auto"></a>
    <a href="https://www.speicherleck.de/iblech/stuff/klimacamp-2021-auswahl/photo_2021-02-20_17-21-18.jpg"><img loading="lazy" alt="" src="https://www.speicherleck.de/iblech/stuff/klimacamp-2021-auswahl/photo_2021-02-20_17-21-18.jpg" width="606" height="960" style="display: block; width: auto; height: 20em; aspect-ratio: auto"></a>
    <a href="https://www.speicherleck.de/iblech/stuff/klimacamp-2021-auswahl/photo_2021-02-21_12-30-51.jpg"><img loading="lazy" alt="" src="https://www.speicherleck.de/iblech/stuff/klimacamp-2021-auswahl/photo_2021-02-21_12-30-51.jpg" width="1280" height="960" style="display: block; width: auto; height: 20em; aspect-ratio: auto"></a>
    <a href="https://www.speicherleck.de/iblech/stuff/klimacamp-2021-auswahl/photo_2021-02-21_12-37-57.jpg"><img loading="lazy" alt="" src="https://www.speicherleck.de/iblech/stuff/klimacamp-2021-auswahl/photo_2021-02-21_12-37-57.jpg" width="960" height="1280" style="display: block; width: auto; height: 20em; aspect-ratio: auto"></a>
    <video controls width="848" height="464" preload="none" poster="https://www.speicherleck.de/iblech/stuff/klimacamp-2021-auswahl/video_2021-02-21_23-55-34.webp" style="display: block; width: auto; height: 20em; aspect-ratio: auto">
      <source src="https://www.speicherleck.de/iblech/stuff/klimacamp-2021-auswahl/video_2021-02-21_23-55-34.mp4" type="video/mp4">
    </video>
  </div>
</div>

<div style="width: 100%; height: auto; display: block; aspect-ratio: 17 / 10">
<a href="https://www.youtube.com/embed/KGXvJnUI1hw?origin=https://www.klimacamp-augsburg.de">
<img src="/youtube.webp" width="736" height="433" alt="Klimacamp Augsburg seit Juli 2020" style="width: 100%"></a></div>


## Über die Gründung des Augsburger Klimacamps im Jahr 2020

Der fünfte Jahrestag des Pariser Klimaabkommens ist verstrichen und wir
befinden uns noch immer weit davon entfernt Politik zu führen, die mit der
1,5-Grad-Grenze übereinstimmt.

Der Bundestag sollte am 3. Juli 2020 das Kohleeinstiegsgesetz beschließen. Das
war unser Anlass, am 1. Juli das Klimacamp zu errichten: 1½ Jahre
Demonstrationszüge von Fridays for Future waren unzureichend, um diesen
klimapolitischen Wahnsinn zu verhindern. Das Gesetz sieht vor, dass
durch Zahlungen an Kohlekraftwerksbetreiber die Kohlekraft mit Steuergeldern
bis 2038 künstlich am Leben gehalten wird. Nach
[Berechnungen von Wissenschaftler\*innen](https://web.archive.org/web/20201221085324/https://info-de.scientists4future.org/wesentliche-defizite-kohleausstiegsgesetz-kvbg-e/)
kann Deutschland das demokratisch beschlossene Pariser Klimaabkommen nicht mehr
einhalten, wenn dieses Gesetz tatsächlich umgesetzt wird.

Aber auch unsere Augsburger Stadtregierung muss Verantwortung übernehmen --
tut sie aber nicht. Sie ist der Hauptadressat unseres Protests. Der
schwarz-grüne Koalitionsvertrag sieht vor, drei Mal so viel CO₂ in der
Atmosphäre zu deponieren, als Augsburg selbst bei einer sehr
[wohlwollenden Rechnung](/co2-budget/) noch zusteht.
Wir fragen: Mit welchem Recht nimmt sich die Stadt Augsburg
heraus, ihr CO₂-Restbudget so sehr zu überschreiten? Wir formulieren
[konkrete Forderungen](/forderungen/) an unsere Stadtregierung.

Das Klimacamp wurde von Fridays for Future Augsburg errichtet und ist das erste
seiner Art -- mittlerweile gibt es [viele weitere Klimacamps](https://www.klimacamp.eu/)
in anderen Städten. Es ist aber kein reines FFF-Camp, schon an
Tag 1 waren wir Klimagerechtigkeitsaktivist\*innen diverser weiterer Initiativen wie
Ende Gelände, Extinction Rebellion, Greenpeace, Grüne Jugend und
Aja. Auch Einzelpersonen ohne Heimatinitiative machen bei uns mit.
Und wir bleiben. Bis es die Situation zulässt.
#nichtmituns #fightfor1point5 #wircampenbisihrhandelt


## Was wir konkret machen (2020 bis heute)

Jeden Tag sprechen wir mit Passant\*innen,
die am Camp vorbeikommen oder uns besuchen.
Wir verlesen Reden, organisieren Workshops
und wir machen mit Sprechchören und Liedern
auf das klimapolitische Totalversagen der Regierung aufmerksam.
Anstehende Events wie auch Workshops
versuchen wir frühzeitig in unserem [Programm](/programm/) anzukündigen.
Zahlreiche Vorträge und Workshops fanden bereits am Klimacamp statt.
Im [Sommer 2020](/tagebuch/2020/) konnten Tage ohne besonderes Programm
an einer Hand abgezählt werden.
Sofern es Wetter oder Corona-Inzidenz nötig machen,
pausieren wir unser Vortragsprogramm.

Über [offene Briefe](/offeneBriefe/),
Stellungnahmen zu aktuellen Themen und
[Pressemitteilung](/pressemitteilungen/)
beteiligen wir uns an der öffentlichen Meinungsbildung.
Unter *[Artikel & Medien](/informationen/)*
veröffentlichen wir selbstgeschriebene Artikel
zu verschiedenen Aspekten von Klimagerechtigkeit.

Daneben führen wir zahlreiche Gespräche mit Lokalpolitiker\*innen --
im Sommer auch oft öffentlich in Form von Podiumsdiskussionen.
Dabei erlangten wir den Eindruck,
dass die Opposition im Augsburger Stadtrat die nötige Eile in der
Bekämpfung der Klimakrise weitgehend verstanden hat,
während die schwarz-grüne Regierungskoalition weiterhin
das Wohlergehen der Wirtschaft im kommenden Quartal
über die Bedürfnisse der Menschen (und der Wirtschaft in langfristiger
Perspektive) stellt.

Besonders wichtig ist es uns,
Stimmen aus der Wissenschaft Gehör zu verschaffen
und damit uns sowie die stets eingeladene
[Lokalpolitik](/lokalpolitik/) weiterzubilden.
Auch eine [Übersicht über die Weltklimaberichte](/weltklimaberichte/)
verdient einen prominenten Platz auf unserer Webseite.

Unser [Programm](/programm/),
unser [Tagebuch](/tagebuch/),
unsere [Pressemitteilungen](/pressemitteilungen/)
sowie unser [Pressespiegel](/pressespiegel/) sind,
zusammen mit unserer [Telegram-Gruppe](https://t.me/klimacamp_augsburg),
die besten Möglichkeiten,
bezüglich unserer politischer Aktivitäten auf dem Laufenden zu bleiben.


## Soziale Medien

<img alt="das Telegram-Logo" src="/icons/telegram.svg" style="width: 1em; height: 1em">
**[Infokanal auf Telegram](https://t.me/klimacamp_augsburg)** (gleichzeitig
Veranstaltungskalender für diverse Initiativen) <br>
<img alt="das Instagram-Logo" src="/icons/instagram.svg" style="width: 1em; height: 1em">
**[Instagram: @klimacamp](https://www.instagram.com/klimacamp/)**<br>
<img alt="das Facebook-Logo" src="/icons/facebook.svg" style="width: 1em; height: 1em">
**[Facebook](https://www.facebook.com/Augsburger-Klimacamp-100721742596025)**<br>
<img alt="das Twitter-Logo" src="/icons/twitter.svg" style="width: 1em; height: 1em">
**[Twitter: @Klimacamp_aux](https://twitter.com/Klimacamp_aux)**

### Andere Klimacamps

[https://www.klimacamp.eu/](https://www.klimacamp.eu/)


## Unterstützung

Du möchtest das Klimacamp aktiv unterstützen, weißt aber nicht wie?
Dann komm gerne in unsere
[Unterstützungsgruppe auf Telegram](https://t.me/joinchat/GebYOlhhZMoAtCQQ).

Wenn es eine Aufgabe gibt, die zu erledigen ist,
wird sie in diese Gruppe gestellt.
Wenn du die Aufgabe dann übernehmen möchtest,
kannst du dich dann per Direktnachricht melden.
Wenn du ein Angebot hast, wie du uns unterstützen möchtest,
kannst du das auch gerne in den Chat oder als Privatnachicht schreiben.

<script data-no-instant>
  {% include instantclick.min.js %}
  InstantClick.init();
</script>
