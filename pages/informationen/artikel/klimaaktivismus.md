---
layout: page
title: Klimaaktivismus
permalink: /informationen/artikel/klimaaktivismus/
parent: "Artikel & Medien"
nav_order: 20
keywords:
  - Aktionsform
  - Aktivismus
  - Autobahnblockade
  - Demokratie
  - Demonstration
  - Erregungskorridor
  - Fahrraddemo
  - Klimagerechtigkeit
  - Klimaschutz
  - Politik
  - Protest
  - Versammlung
  - Waldbesetzung
  - Wissenschaft
  - Workshop
---

# (Klima-)Aktivismus

*veröffentlicht am 17. März 2022*

In jeder Regierungsform besteht eine Notwendigkeit für den Souverän
über Gefahren für das Land informiert zu sein.
In einer Demokratie besteht somit die Notwendigkeit,
dass die Wähler\*innen informiert sind.
Es besteht aber keine Pflicht für die Wähler\*innen sich zu informieren.

Der Mechanismus, wie dieser Aspekt in unserer Demokratie trotzdem funktionieren kann,
ist ein starkes Versammlungs- und Demonstrationsrecht.
Wenn sich zwei Personen gefunden haben, die gemeinsam einen Missstand
anprangern wollen, können sie eine Versammlung bilden und eine
Demonstration anmelden.
Je mehr Menschen sie überzeugen, umso größer kann der Protest werden.
Bei den globalen Klimastreiks streikten weltweit Millionen von Menschen
und allein schon in Deutschland mehrere hundert Tausend.
Beim dritten globalen Klimastreik am 20. September 2019
waren es sogar allein in Deutschland etwa 1,4 Millionen Menschen.
Das sind mehr als 1,5% der Bevölkerung.

Demonstrationen und Protest richten sich an den Souverän,
also die Bevölkerung, und seine Vertreter\*innen –
die Politiker\*innen und sonstigen Entscheider\*innen.

Protest schafft einen Erregungskorridor.
Er ruft wichtige Themen in das Bewusstsein der Menschen.
Dann heißt es abends beim Tischgespräch in manchen Familien,
dass man zwar ihre Methoden nicht gut finde,
wohl aber die Ziele der Bewegung teile.
Beispielsweise heiße man es nicht gut, dass die
Kinder Schulunterricht verpassten, stimme aber zu,
dass die Klimakrise doch ein wichtiges Anliegen sei.

Auch wenn Protest nur in wenigen Fällen sein langfristiges Ziel sofort erreicht,
so schafft er es in den meisten Fällen zumindest,
dass über das Thema des Protests gesprochen wird.
Damit das funktioniert, muss Protest auch stören.
Er muss mediale Aufmerksamkeit erlangen.
Dazu muss der Protest kontrovers sein.
Wenn Klimademos bei Straßenbahnen oder Bussen zu Verspätungen führen,
dann tut das Teilnehmer\*innen der Demos ernsthaft Leid.
Aber dieses Stören ist genau der Bestandteil, der dafür sorgt,
dass dieser Informationsmechanismus unserer Gesellschaft funktioniert.

Der durch Protest geschaffene Erregungskorridor kann dann durch andere
Akteur\*innen gefüllt werden.
Häufig sind dies Bürger\*inneninitiativen, Bürger\*innenbegehren,
Journalist\*innen, Wissenschaftler\*innen oder auch Politiker\*innen,
die von der Aufmerksamkeit profitieren
und die inhaltliche Debatte weiterführen können.

Beispielsweise sind Medienberichte über unsere Protestaktionen
gelegentlich wie folgt strukturiert:
1. Aktivist\*innen haben eine gewisse Aktion durchgeführt.
2. Mit der Aktion wollen sie auf einen Sachverhalt aufmerksam machen.
3. Inhaltlich recht gibt ihnen dabei der Weltklimabeirat und/oder
   die folgende Organisation, Studie oder Gerichtsentscheidung.

Nur der dritte Punkt für sich allein,
also die (wissenschaftliche) Erkenntnis über einen Missstand,
würde keine Veränderung bewirken.
Sie würde es nicht in die Nachrichten schaffen
und wäre allenfalls eine Randbemerkung in der Zeitung.
Die Information würde lediglich
innerhalb der wissenschaftlichen Gemeinschaft Bekanntheit erlangen.
Nicht die Wissenschaft,
sondern die Bevölkerung als Souverän ist aber zum Handeln aufgerufen.
Diese muss informiert werden.
Hierfür ist der Protest wichtig.

Manche Klimagerechtigkeitsaktivist\*innen beschreiben ihre Aufgabe auch wie folgt:
„Wir generieren den Wind, in den die Politik ihre Fahne hängt.“

Es ist übrigens nicht unüblich, dass der Enthusiasmus für Protest
nur einen kleinen Teil derjenigen ergreift,
die eine gewisse Veränderung befürworten.
Sei es beim Kampf um ein Ende der Rassentrennung in den USA
oder bei der Gleichberechtigung von Frauen.
Es gab immer eine große Gruppe von Menschen,
die zwar mit den Zielen sympathisierte,
aber Protestmethoden zu deren Herbeiführung kritisierten.
Das Portal [crmvet.org](https://www.crmvet.org/) pflegt eine Sammlung von
[Umfrageergebnissen](https://www.crmvet.org/docs/60s_crm_public-opinion.pdf)
zur öffentlichen Meinung über die amerikanische Bürgerrechtsbewegung.
Danach äußerte sich 1961 die Mehrheit der Befragten eher ablehnend
über die Proteste, Demonstrationen und
[Freedom Rides](https://de.wikipedia.org/wiki/Freedom_Ride).
Sie meinten, dass diese Aktionen dem Ziel der Abschaffung der Rassentrennung eher schadeten.
Es waren letztendlich aber genau diese Proteste,
die dann den Weg für die gewünschten Veränderungen frei machten.
Sie schufen den Erregungskorridor,
durch den die Veränderungen herbei geführt werden konnten.
Dementsprechend sehen die Umfragen von 1969
die Proteste in einem deutlich positiveren Licht.

Aktivismus muss einen Punkt finden,
an dem die Aktionsform kontrovers genug ist,
um Aufmerksamkeit zu generieren,
aber nicht so kontrovers,
dass sie vom eigentlichen Thema und Inhalt des Protests ablenkt.
Dabei gibt es keinen einen perfekten Punkt, keine perfekte Aktionsform.
Mit verschiedenen Arten von Aktionen
erreicht man verschiedene Zielgruppen.
Daher gibt es Laufdemos, Fahrraddemos,
kinderfreundliche Fahrraddemos (Kidical Mass),
Blockade von Kohlekraftwerken (Ende Gelände),
Waldbesetzungen (Altdorfer Wald, Dannröder Forst,
Forst Kasten, Hambacher Forst, Ulmer Uniwald usw.),
Stellen von Anträgen in der Bürgerversammlung,
Autobahnblockaden (Aufstand der letzten Generation),
Klagen und Verfassungsbeschwerden,
Podiumsdiskussionen, Vorträge und Workshops
und viele andere Arten von Aktionen.
Letztendlich will die Klimagerechtigkeitsbewegung alle Zielgruppen erreichen.
Am Augsburger Klimacamp und an vielen anderen Orten auf der Welt
versuchen Menschen mit ihren Aktionen genau das zu tun.

-- Klimacamp Augsburg (https://augsburg.klimacamp.eu alias https://www.klimacamp-augsburg.de/)

Dieser Artikel steht unter der Lizenz
[Creative Commons BY 4.0](https://creativecommons.org/licenses/by/4.0/).

---

*Dieser Artikel fasst die gesellschaftliche Bedeutung
von Aktivismus und Protest zusammen.
Viele der Inhalte dieses Artikels sind aus den Workshops,
welche Januar 2022 am Klimacamp abgehalten worden waren,
(insbesondere der Workshops vom 10.01.2022 und 11.01.2022) entnommen.
Der Artikel spiegelt aber nicht die volle Vielfalt
an Ansichten zu Aktivismus
wie ein zweistündiger Workshop
mit einem halben bis einem Dutzend Teilnehmer wieder.
Wer neugierig geworden ist,
kann Workshops zu diesem oder verwandten Themen am Klimacamp besuchen.
Weitere Aspekte von Aktivismus sind:*
 * *Welche Funktion erfüllt Aktivismus für die Aktivist\*innen?
   Ist es Dauerbeschäftigung mit einer deprimierenden Thematik
   und eine sinnstiftende Betätigung?
   Was motiviert Klimagerechtigkeitsaktivist\*innen?*
 * *How To: Welche Protestformen gibt es?
   Wie wird effektiver Protest organisiert?
   Was kann jede\*r Einzelne tun?
   Welche Protestformen passen zu mir persönlich?*
 * *Demonstrations- und Versammlungsrecht: Was ist erlaubt?*
