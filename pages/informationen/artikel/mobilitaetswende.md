---
layout: page
title: "Mobilitätswende"
permalink: /informationen/artikel/mobilitaetswende/
parent: "Artikel & Medien"
nav_order: 30
keywords:
  - Akkus
  - Aktivismus
  - Antriebswende
  - Augsburg
  - Elektrifizierung
  - Fahrradstadt Jetzt
  - Klimaschutzprogramm
  - Mobilitätswende
  - ÖPNV
  - Personennahverkehr
  - Straßenbahn
  - Verkehrswende
  - Verkehrswendeplan
  - Wasserstoff
  - Workshop
---

# Mobilitätswende

*veröffentlicht am 7. April 2022*

Der Mobilitätssektor ist eines der Problemkinder im Klimaschutz.
In anderen bedeutenden Sektoren wie der Energiewirtschaft,
der Industrie und den Haushalten
sind die Treibhausgasemissionen Deutschlands
in den letzten dreißig Jahren um 30% bis knapp über 40% gefallen.
Das ist zu wenig und erst recht nicht ausreichend,
um der 1,5°C-Grenze gerecht zu werden,
aber wenigstens stimmt schon einmal die grobe Richtung der Entwicklung.
Im deutschen Mobilitätssektor sind die Treibhausgasemissionen
über die letzten dreißig Jahre dagegen in etwa gleich geblieben
und in den letzten Jahren sogar gestiegen.[1]
Das hat nichts damit zu tun, dass es inhärent schwieriger wäre,
im Mobilitätssektor Emissionen einzusparen als in anderen Bereichen.
Der Grund liegt vielmehr darin, dass konsequent
die falschen Anreize gesetzt werden.
Was es an Effizienzsteigerung bei Motoren gab,
wurde unter anderem dadurch negiert,
dass es nun deutlich mehr Autos gibt
und diese größer und schwerer wurden
und mehr Motorleistung bedürfen.

Das birgt einen ganzen Strauß von Problemen.
Die Lösung ist eine Mobilitätswende.

Viele verwenden den Begriff „Verkehrswende“.
Es gibt zwei verschiedene verbreitete Vorstellungen davon,
was Verkehrswende bedeutet,
die jedoch alles andere als gleichwertig sind.


## Reine Antriebswende

Die schwächere Bedeutung von „Verkehrswende“
ist die einer reinen „Antriebswende“.
Der Grundgedanke ist, dass man fast alles so lässt wie es ist
und nur die fossilen Antriebe in den Verkehrsmitteln
durch nachhaltige Antriebsformen ersetzt.
Es gibt verschiedene Varianten der Vorstellungen davon,
wie so etwas funktionieren soll.
Viele davon werden letztendlich unwirtschaftlich sein.

* Eine Variante sieht die verbreitete Benutzung
  batteriebetriebener Elektrofahrzeuge vor.
  Sie ist die realistischste der Varianten einer reinen Antriebswende.
  Gerade im ländlichen Bereich ergibt sie auch einen gewissen Sinn.
  Allerdings adressiert sie neben den Treibhausgasen
  nicht die anderen Probleme des Autoverkehrs,
  also die Rohstoff- und Platzverschwendung, vor allem in Städten.
  In dichtbesiedelten Regionen wie Augsburg
  greift die Antriebswende daher zu kurz.
* Eine andere Variante besagt,
  dass man Verbrennungsmotoren beibehalten könnte,
  solange man von fossilen Kraftstoffen zu Biokraftstoffen wechselt.
  Dafür gibt es schlicht nicht genügend landwirtschaftliche Flächen.
  Da wir mit dem Klimaschutz sehr spät dran sind,
  wird die Menschheit auch noch einige dieser Flächen verlieren.
  Für die verbleibenden Flächen muss Nahrungsmittelproduktion
  Priorität haben, um die Ernährung der Menschheit
  auch in den kommenden Jahrzehnten sicherzustellen.<br>
  Überhaupt ist die Nutzung von Fläche zum Anbau von Pflanzen
  als Energieträgern relativ ineffizient.
  Man kann in Bayern deutlich mehr Energie
  durch einen Windradpark gewinnen
  als durch den Anbau von Pflanzen auf der landwirtschaftlichen Fläche,
  auf der der Windpark gebaut wurde.
* Eine weitere Variante besagt, dass man Wasserstoff
  oder einen anderen synthetischen Treibstoff nutzt,
  entweder indem man ihn verbrennt oder in Brennstoffzellen nutzt.
  Auch hier ist der Wirkungsgrad das Problem.
  Umwandlungsverluste treten sowohl bei der Produktion
  des synthetischen Kraftsstoffs als auch bei der Rückgewinnung
  von Energie aus dem synthetischen Kraftstoff auf.
  Letztendlich bleibt nur 30% bis 45% der ursprünglichen Energie
  für die Fortbewegung des Fahrzeuges übrig.
  Bei batteriebetriebenen Elektrofahrzeugen
  beträgt der Wirkungsgrad über 70%.
  Neue Technologien, die den Wirkungsgrad von synthetischen Kraftstoffen
  konkurrenzfähig zu Batterien machen könnten,
  sind uns derzeit nicht bekannt.<br>
  Man kann sich sehr gut vorstellen, wie wenig wirtschaftlich
  synthetische Kraftstoffe sind,
  wenn man für die gleiche Bewegungsenergie zwei Mal so viel
  Primärenergie bezahlen muss wie bei Akkus.
  Auch gesamtgesellschaftlich gilt,
  solange wir nicht unseren gesamten Strom
  durch erneuerbare Energie produzieren,
  können wir es uns nicht leisten,
  Strom durch die massenhafte Verwendung von Technologien
  mit einem dermaßen geringen Wirkungsgrad zu verschwenden.

Batterien sind also im Individualverkehr
die vielversprechenste Technologie.
Allerdings haben synthetische Kraftstoffe ihre Nischen.
Klimaneutral produzierter Wasserstoff ist
in einer klimaneutralen Stahl- und Ammoniakindustrie unabdingbar.
Wasserstoff oder andere synthetische Kraftstoffe
werden noch eine ganze Weile in der Luftfahrtindustrie
benötigt werden.
Insofern ist der Hype um Wasserstoff nicht nur negativ zu sehen.


## Echte Mobilitätswende

Die echte Mobilitätswende beinhaltet auch eine Antriebswende,
aber sie ist weit mehr als das.
Das Ziel einer Mobilitätswende besteht darin,
die Umwelt zu schonen und Energie, Rohstoffe und Fläche einzusparen.
Das geschieht, indem man Verkehr vom mobilen Individualverkehr (MIV)
zu Verkehrsmitteln des sogenannten Umweltverbunds verlagert.
Umweltverbund ist ein Sammelbegriff für Fußverkehr, Radverkehr
und öffentliche Verkehrsmittel.
Die öffentlichen Verkehrsmittel sind selbstverständlich
mit klimaneutralen Antrieben zu betreiben.

Die frei werdenden Verkehrs- und Parkflächen
können zum Teil als Grünanlagen, Außenflächen für Cafés
oder andere Formen von öffentlichen Aufenthaltsflächen
dem Wohl der Allgemeinheit dienen und
zur Steigerung der Lebensqualität in der Stadt beitragen.

Eine weitere Maßnahme der Mobilitätswende
ist die Vermeidung von Verkehr,
indem man Stadtteilzentren so gestaltet,
dass lange Wege, beispielsweise zum Einkaufen,
unnötig werden.

Auch nach einer echten Mobilitätswende
wird es noch Autos geben.
Carsharing kann seinen Beitrag leisten,
um Lücken im Nahverkehrskonzept zu schließen.
Außerdem würde es in ländlichen Regionen ohne Autos schwierig werden.


## Lokalpolitische Bedeutung

Das Klimacamp setzt sich für eine echte Mobilitätswende ein.
Argumentativ unterstützt wird die Mobilitätswende von der Studie
„[Klimaschutz 2030: Studie für ein Augsburger Klimaschutzprogramm](https://www.augsburg.de/fileadmin/user_upload/umwelt_soziales/umwelt/klima%20und%20energie/Studie_Klimaschutz_2030_mit_allen_anlagen.pdf)“.
Die Studie wurde von der Stadt selbst in Auftrag gegeben
und empfiehlt über die nächsten Jahre
eine Halbierung des motorisierten Individualverkehrs in Augsburg.

Damit das gelingen kann, ist ein deutlicher Ausbau
des öffentlichen Personenverkehrs und des Radverkehrs notwendig,
denn wir alle haben auch weiterhin Mobilitätsbedürfnisse,
die befriedigt werden müssen.
Der Autoverkehr muss unattraktiver werden.
Parallel dazu müssen die Alternativen günstiger,
einfacher zugänglich und bequemer werden.
Außerdem muss Fläche, die derzeit für Autos vorgesehen ist,
umgewidmet werden.

Wichtig ist auch,
dass eine günstige und bedarfsunabhängige Grundversorgung
mit öffentlichem Personennahverkehr existiert.
Das Angebot muss dem Bedarf vorangehen.
Derzeit ist das noch nicht der Fall.
Allzu häufig wird zuerst ein Neubaugebiet bebaut
und erst nachträglich durch den öffentlichen Nahverkehr erschlossen.
Haunstetten Südwest ist hier die absolute Ausnahme.
Es ist aber eher Zufall, dass die Straßenbahnlinie 3
hier vor dem Baubeginn erweitert wurde.

Außerdem gibt es Uhrzeiten und Strecken,
die durch den öffentlichen Nahverkehr nicht bedient werden.
Derartiges muss weniger werden.
Die Notwendigkeit zum Besitz eines Autos
muss für viele Menschen wegfallen.
Sonst werden sich die Menschen für die Strecken,
für die sie ein Auto brauchen, ein Auto anschaffen
und die Strecken, auf denen ein gut ausgebauter ÖPNV existiert,
aus purer Bequemlichkeit auch mit dem Auto fahren.

Inzwischen existieren auch einige andere Städte,
die erfolgreich eine Mobilitätswende vollzogen haben
und als Vorbild für Augsburg dienen könnten.
So hat auch schon Morten Kabell,
der ehemaliger Umweltbürgermeister von Kopenhagen,
hier in Augsburg über die Transformation in Kopenhagen gesprochen.[2]
Weitere Informationen dazu, was Kopenhagen gemacht hat,
kann man auch dem Vortrag
[Mobilität und Klimaschutz - Mit Morten Kabell](https://www.youtube.com/watch?v=9U7VHDfNTCg&ab_channel=CenterforAppliedEuropeanStudies%28CAES%29)
entnehmen.

Die Realität in Augsburg besteht leider noch aus Rückschritten.

* Die Zahl der Autos in Augsburg steigt.
  2007 waren es pro 1000 Einwohnern 371 angemeldete Autos,
  2020 waren es schon 406.[3][4]
* Im Jahr 2021 gab es bei den swa im Abstand von nur wenigen Monaten
  zwei Preiserhöhungen.
* Es gibt sogar Bestrebungen die Taktung der Straßenbahnen
  nicht wieder auf das Vorpandemieniveau zu erhöhen.

Gegen diese Rückschritte gehen wir vor,
indem wir Alternativen aufzeigen und
die Stadt an ihre verkehrspolitische Verantwortung erinnern.


## Vorzüge der Mobilitätswende

Eine erfolgreiche Mobilitätswende

* reduziert den Verkehrslärm,
* erhöht die Luftqualität,
* macht den öffentlichen Raum unfallärmer,
* erhöht die Mobilität für Kinder und Senior\*innen,
* schafft Platz für Grünflächen und Sitzgelegenheiten und
* erhöht die Lebensqualität und Lebenserwartung der Anwohner\*innen.


## Links / Referenzen

[1] [Umweltbundesamt: Entwicklung der Treibhausgasemissionen in verschiedenen Sektoren von 1990 bis 2019](https://www.umweltbundesamt.de/sites/default/files/medien/366/bilder/rolle_des_verkehrssektors_bei_den_treibhausgasemissionen_in_deutschland_1.jpg)<br>
[2] [Augsburger Radlwoche vom 4. bis 11. Juli 2020](https://www.augsburg.de/buergerservice-rathaus/verkehr/radverkehr/news-termine/detail?tx_news_pi1%5Baction%5D=detail&tx_news_pi1%5Bcontroller%5D=News&tx_news_pi1%5Bnews%5D=9580&cHash=d261f8895bc6d07f98e99265e2de81f5)<br>
[3] [Die Entwicklung des Kfz-Bestands in der Stadt Augsburg](https://www.augsburg.de/fileadmin/user_upload/buergerservice_rathaus/rathaus/statisiken_und_geodaten/statistiken/kurzmitteilungen/KM_2021_04_Entwicklung_des_Kfz-Bestands_in_Augsburg.pdf)<br>
[4] [So viele Autos gibt es in Augsburg](https://www.augsburg.de/aktuelles-aus-der-stadt/detail/so-viele-autos-gibt-es-in-augsburg)

## Siehe auch:

* [Kurzes Video zur Veranschaulichung der Mobilitätswende](/vortrag-staatstheater/ohne_autos.mp4)
* [This Brilliant Illustration Shows How Much Public Space We’ve Surrendered to Cars](https://smartgrowth.org/this-brilliant-illustration-shows-how-much-public-space-weve-surrendered-to-cars/)
* [Krautreporter: So können Städte einen kostenlosen Nahverkehr einführen](https://krautreporter.de/2482-so-konnen-stadte-einen-kostenlosen-nahverkehr-einfuhren)

-- Klimacamp Augsburg (https://augsburg.klimacamp.eu alias https://www.klimacamp-augsburg.de/)

Dieser Artikel steht unter der Lizenz
[Creative Commons BY 4.0](https://creativecommons.org/licenses/by/4.0/).


---

*Dieser Artikel beschreibt die Mobilitätswende sehr allgemein.
Doch was bedeutet die Mobilitätswende konkret für Augsburg?
Welche Finanzierungskonzepte gibt es für den öffentlichen Nahverkehr?
Themen wie diese werden immer mal wieder am Klimacamp diskutiert.*

*Das Klimacamp fordert den Ausbau des öffentlichen Nahverkehrs,
sowohl in Qualität als auch in Quantität.
Viele Entwicklungen gerade im Mobilitssektor
verlaufen leider noch immer in die falsche Richtung.
Kostenloser öffentlicher Personennahverkehr
wäre eine effiziente Möglichkeit,
um die Treibhausgasemissionen des Verkehrssektors zu senken.*

*Neben dem Klimacamp gibt es in Augsburg noch weitere Akteure,
die eine Mobilitätswende anstreben.*

* *[Aktionsbündnis „Fahrradstadt Jetzt“](https://www.fahrradstadt-jetzt.de/)*
  <br>
  *Das Aktionsbündnis wird vom* Allgemeinen deutschen Fahrrad-Club (ADFC)*,*
  Forum Augsburg lebenswert e.V. *und* Fridays for Future Augsburg
  *getragen und hat einen großen Unterstützerkreis.
  Zu den größten Erfolgendes Aktionsbündnisses
  zählt das Augsburger Radbegehren,
  welches die Stadt an den Verhandlungstisch zwang
  und zu einem Vertrag zwischen der Stadt und dem Aktionsbündnis führte.*
* *[Verkehr 4.0 für den Ballungsraum Augsburg](https://www.verkehr4x0.de/)*
  <br>
  *„Verkehr 4.0“ ist ein Vorschlag
  für ein verbessertes Nahverkehrskonzept
  für Augsburg sowie das Umland von Augsburg.*
* *[Verkehrswendeplan Augsburg](https://www.verkehrswende-augsburg.de/)*
  <br>
  *Der* Verkehrswendeplan Augsburg *ist ein
  von Klimagerechtigkeitsaktivist\*innen
  erarbeitetes Konzept für eine Mobilitätswende
  der Stadt Augsburg.*
