---
layout: page
title: "Konsumkritikkritik – Kritik an Konsumkritik"
permalink: /informationen/artikel/konsumkritikkritik/
parent: "Artikel & Medien"
nav_order: 40
keywords:
  - Augsburg
  - Augsburger
  - bewusster Konsum
  - Bank
  - BP
  - Bürgerenergiegenossenschaft
  - CO₂
  - Fleisch
  - Fußabdruck
  - Industrie
  - Konsum
  - Konsumkritik
  - Konsumkritikkritik
  - Klimacamp
  - Klimagerechtigkeit
  - Klimaschutz
  - nachhaltig
  - Ökostrom
  - Politik
  - Rebound-Effekt
  - Täter-Opfer-Umkehr
  - vegan
  - vegetarisch
  - Verbraucher
  - Workshop
---

# Konsumkritikkritik – Kritik an Konsumkritik

*veröffentlicht am 01. Oktober 2022*

Manchmal hört man Sprüche wie
„Dein Kassenbon ist ein Stimmzettel“
oder „der Verbraucher entscheidet“.

Ist das wahr?
Oder ist das Augenwischerei?

In diesem Artikel möchten wir erörtern,
wann und wo Konsumkritik sinnvoll ist,
und gleichzeitig Kritik an schädlicher Konsumkritik,
sprich *Konsumkritikkritik*, üben.
Denn unserer Erfahrung aus verschiedenen Gesprächen nach
ist Konsumkritik einer der Aspekte von Klimagerechtigkeit
mit dem größten Wissensdefizit in der Politik
und auch allgemein.


![Das Bild zeigt eine Meme. Auf der Meme ist zuerst eine freundlich dargestellte, weibliche Wissenschaftlicherin zu sehen, betitelt mit "KLIMAAKTIVISTI", die einen Topf rührt, auf dem durcheinander die folgenden Begriffe abgebildet sind: "Mobilitätswende, Energierevolution, Soziale Gerechtigkeit, Agrarreform, Transparenz & Beteiligung, Systemwandel".
Darunter sind drei kleinere Abbildungen mit jeweils einer bösartig wirkenden dunklen Silhouette. Die Silhouetten sind jeweils beschriftet mit "LOBBYISTEN, POLITIKER, PÖBLER" und diese fügen jeweils, ähnlich wie beim Kochen, böswillig Zutaten hinzu; abgebildet wie eine große Menge Zucker aus einem Karton, Pfeffer und Salz aus Streuern, und eine bunte Mischung Kram aus einem Eimer. Beschriftet sind diese hinzugefügten Zutaten mit "Konsumkritik, CO2-Fußabdruck, Werbetricks, Verantwortungsabgabe, E-Autos, Biosiegel, Konsumkritik, Schuldzuweisung"](/pages/material/images/kkkpowerpuff-nolink.png)


## Inhalt
* [Der CO₂-Fußabdruck](#der-co-fußabdruck)
* [Beschränkter Einfluss der Verbraucher\*innen](#beschränkter-einfluss-der-verbraucherinnen)
  * [Bewusster Konsum](#bewusster-konsum)
    * [...erfordert Erfahrung und Wissen](#bewusster-konsum-erfordert-erfahrung-und-wissen)
      * [Einschub *„Ökosiegel“*](#einschub-ökosiegel)
    * [...erfordert Immunität gegen Werbung](#bewusster-konsum-erfordert-immunität-gegen-werbung)
    * [...erfordert Zeit](#bewusster-konsum-erfordert-zeit)
    * [Rebound-Effekte](#rebound-effekte)
    * [...erfordert Geld](#bewusster-konsum-erfordert-geld)
      * [Einschub *„Externalisierte Kosten“*](#einschub-externalisierte-kosten)
    * [Fazit zum *Bewussten Konsum*](#fazit-zum-bewussten-konsum)
  * [Bereiche ohne Einfluss der Verbraucher](#bereiche-ohne-einfluss-der-verbraucher)
* [Die Schattenseiten der Konsumkritik](#die-schattenseiten-der-konsumkritik)
  * [Vielschichtige Täter-Opfer-Umkehr](#vielschichtige-täter-opfer-umkehr)
  * [Ein Beispiel aus Augsburg](#ein-beispiel-aus-augsburg)
* [Sinnvolle Konsumentscheidungen](#sinnvolle-konsumentscheidungen)
  * [Nachhaltige Banken](#nachhaltige-banken)
  * [Fleischkonsum](#fleischkonsum)
  * [Ökostrom](#ökostrom)
* [Fazit](#fazit)
* [Bedeutung für die Arbeit des Augsburger Klimacamps](#bedeutung-für-die-arbeit-des-augsburger-klimacamps)


## Der CO₂-Fußabdruck

Heutzutage sind die meisten Menschen
mit dem Begriff „CO₂-Fußabdruck“ (engl. *carbon footprint*) vertraut.
Der CO₂-Fußabdruck soll in gewisser Weise
eine Maßeinheit für die Schädlichkeit des eigenen Konsums sein.
Er ist ein Versuch, den Treibhausgasauststoß
auf das Individuum und sogar das individuelle Verhalten
herunter zu rechnen.
So soll man beispielsweise bewerten können,
wie sehr individuelle Verhaltensweisen
wie Fleischkonsum, Autofahren oder Fliegen
dem Klima schaden würden.

Deutlich weniger Menschen wissen,
wer den Begriff des CO₂-Fußabdruck populär gemacht hat.
Es war der britische Öl- und Gasgigant BP
(ehemals „British Petroleum“, zu deutsch „Britisches Erdöl“)
und er hat sich das richtig viel Geld kosten lassen.
250 Millionen Dollar hat der Konzern dafür ausgegeben,
das Konzept *CO₂-Fußabdruck* weltbekannt zu machen.
Welchen Vorteil hat das fossile Unternehmen davon?

Die Absicht dahinter ist,
dass gesamtgesellschaftliche Verantwortung
auf einzelne Personen abgewälzt wird.
Schuld ist plötzlich die autofahrende Person
– nicht der Staat,
der weder gute Radwege noch einen angemessenen
öffentlichen Nahverkehr zur Verfügung stellt.
Der weder erneuerbare Energien vorausschauend ausbaut,
um Gaskrisen zu verhindern,
noch Investitionen in Ausbeutung und Klimazerstörung unterlässt.
Anstatt gemeinsam richtungsweisende Entscheidungen
und Gesetzesänderungen zu fordern,
kann man sich Dank des CO₂-Fußabdrucks nun streiten,
welcher Mitmensch ein schädlicheres Verhalten an den Tag legt.
Das Konzept lädt ein zum Streiten und
kann zivile Bewegungen mit Handlungsunfähigkeit bedrohen.


## Beschränkter Einfluss der Verbraucher\*innen

Der CO₂-Fußabdruck erweckt den Eindruck,
dass die wesentliche Verantwortung beim Verbraucher läge.
In der Realität haben Verbraucher\*innen
in vielen Bereichen vielfach kaum Einfluss.

Als Mieter\*in habe ich keinen Einfluss darauf,
wie groß das Angebot an Wohnungen ist,
die mindestens dem Passivhausstandard entsprechen.
Wenn ich in Augsburg eine Wohnung habe,
wird diese mit hoher Wahrscheinlichkeit mit Erdgas beheizt
oder aber mit Fernwärme,
die wiederum auch mit hohem Erdgasanteil produziert wird.
Als Einzelne\*r kann ich keinen Einfluss darauf nehmen,
wie die Augsburger Stadtwerke (swa) ihre Fernwärme produzieren.
Oft gibt es leider keine Alternativen
und Verbraucher\*innen sind in ihrer Rolle machtlos
oder müssten enorme Einschränkungen auf sich nehmen,
wie beispielsweise ausschließlich kalt zu duschen.

Auch in anderen Bereichen ist es keine Selbstverständlichkeit,
dass die Macht bei den Verbraucher\*innen liegt.
Wichtig wäre die Befähigung zum bewussten Konsum.


### Bewusster Konsum

Wir sprechen von bewusstem Konsum,
wenn Kaufentscheidungen basierend auf
selbstgesteckten Werten getroffen werden.
Bei sozial- und umweltbewussten Menschen
geht es dann oft um Werte wie Menschenrechte,
Tier- und Artenschutz, Umweltschutz,
Klimagerechtigkeit, Bodenerhaltung usw.
Die Kriterien stehen dabei manchmal im Widerspruch zu einander.
Höheres Tierwohl kann beispielsweise
mit stärkeren negativen Auswirkungen
auf Umwelt und Klima einhergehen.

Damit bewusster Konsum überhaupt möglich und sinnvoll ist,
müssen mehrere Voraussetzungen vorliegen.


#### Bewusster Konsum erfordert Erfahrung und Wissen.

Damit man Kaufentscheidungen basierend auf
ethischen und ökologischen Gesichtspunkten treffen kann,
muss man erst einmal die Auswirkungen
der Produktion, des Transports, der Verwendung und der Entsorgung
der jeweiligen Produkte kennen.
Das ist ein enormer Schatz an Erfahrung und Wissen,
den man aufbauen muss, bevor man zuverlässig
bewusste Konsumentscheidungen treffen kann.

Gleiches gilt nicht nur für den Kauf von Produkten oder Dienstleistungen.
Auch wenn man in umweltfreundliche Geldanlagen investieren will,
steht man vor dem gleichen Bewertungsproblem.

Es gibt so viele Details, die man über Prozesse
und ihre Produktionsketten wissen muss,
um einschätzen zu können,
wie die Auswirkungen einer Konsumentscheidung
auf das Klima sind und worin bessere Alternativen bestehen.
Oft kann man die notwendigen Informationen
auch gar nicht in Erfahrung bringen,
beispielsweise weil Transparenz fehlt
und man gar nicht weiß,
wie die jeweiligen Lieferketten und Produktionsbedingungen aussehen.


##### Einschub *„Ökosiegel“*

Ökosiegel sollen hier Abhilfe schaffen.
Das funktioniert in der Praxis aber nur mäßig.
Selbst wir am Klimacamp haben keinen Überblick darüber,
welche Ökosiegel gut sind, welche schlecht sind
und welche Versprechungen und Zusagen
hinter den verschiedenen Ökosiegeln stehen.

Ökosiegel sind an sich ein komplexes Thema.
Sich für ein Ökosiegel zertifizieren zu lassen kostet Geld.
Vor allem kleinere Betriebe können sich das oft nicht leisten.
Wer also ausschließlich Produkte mit Ökosiegel kauft,
der schadet mitunter dem lokal produzierenden,
umweltfreundlichen Familienbetrieb
zum Vorteil eines großen internationalen Konzerns,
der seine Produkte vielleicht umweltfreundlich herstellt,
aber dann klimaschädlich über große Strecken transportieren lässt.


#### Bewusster Konsum erfordert Immunität gegen Werbung.

Der Spruch die „Nachfrage bestimmt das Angebot“
spiegelt nicht die ganze Wahrheit wieder.
„Das Angebot bestimmt die Werbung“ und
„die Werbung generiert Nachfrage“
stimmen mindestens genauso.

Moderne Werbung ist hochmanipulativ.
So werden Bedürfnisse generiert, die vorher nicht da waren.
Oder Bedürfnisse werden zweckentfremdet.
Das Bedürfnis nach Mobilität wird so zu einem Bedürfnis
für ein Auto umgebogen.
Das Bedürfnis nach Freiheit wird in perverser Weise
zu einem Bedürfnis nach einem Suchtmittel
und so zu Zwang und Abhängigkeit umgewandelt.
Es gibt zahlreiche weitere Beispiele.

Hinter der Werbung stehen gut ausbildete Menschen,
die über Jahrzehnte an Forschungserkenntnissen
aus der Psychologie, Soziologie und Neurologie nutzen,
um über Emotionen und sozialen Druck
Menschen zum Kauf des beworbenen Produkts zu manipulieren.
Als Folge davon tätigt nahezu jede\*r
von uns immer mal eine unbewusste Kaufentscheidung.

Um sich dagegen zu verteidigen,
kann man die Techniken der Werbeindustrie lernen und
sich dann darin trainieren diese im Alltag zu erkennen.
Das Erkennen allein reicht dann aber noch nicht.
Man muss sich noch eine Abwehrreaktion aneignen.
Man kann beispielsweise
Zorn auf Manipulierungsversuche entwickeln.
Dann empfindet man anstelle des Gefühls,
das die Werbung in einem auszulösen versucht,
Zorn auf den erkannten Manipulationsversuch.
Das hält einen von einem unbedarften Kauf des beworbenen Produkts ab.


#### Bewusster Konsum erfordert Zeit.

Bewusster Konsum erfordert deshalb Zeit,
weil die umweltfreundliche Alternative
nicht immer die am nächsten Gelegene ist.
Man muss Umwege in Kauf nehmen.
Der nächste Bioladen ist für die meisten Menschen
weiter entfernt als der nächste Supermarkt.
Wenn man zu Gunsten eines umweltfreundlichen Verkehrsmittels
auf ein persönliches Auto verzichtet,
benötigt man oft auch mehr Zeit.
(Das stimmt nicht immer.
Für viele Strecken in Augsburg,
insbesondere solche in die Innenstadt,
ist man schon heute mit dem Fahrrad
ähnlich schnell oder sogar schneller.
Ein Grund ist, dass man mit Autos
bei der Suche nach Parkplätzen
und dem Fußweg vom Parkplatz zum Zielort
den kleinen Geschwindigkeitsvorteil des Autos verliert.)


#### Rebound-Effekte

Ein weiteres Problem sind Rebound-Effekte.
Menschen tendieren dazu sich bewusst oder unbewusst zu belohnen.

* Energiesparsame Elektrogeräte werden häufiger genutzt
  oder länger laufen gelassen.
* Viele Autos haben heute effizientere Motoren
  als noch vor wenigen Jahrzehnten.
  Die Kraftstoffersparnis wird aber
  durch größere und schwerere Autos wieder wettgemacht.
* Wer sich ein Elektroauto kauft,
  wird dieses vielleicht eher auch für kurze Strecken nutzen
  als einen dreckigen Benziner.
* Oder allgemein:
  Da ich immer *X* machte, darf ich mir auch mal *Y* leisten.

Die „Belohnung“ macht den Effekt der bewussten Konsumentscheidung
mindestens in Teilen zunichte oder schadet sogar mehr.


#### Bewusster Konsum erfordert Geld.

Die tatsächlich höheren Kosten des weniger umweltfreundlichen Konsums
trägt die Allgemeinheit.
Für den Konsumenten sind umweltfreundliche, tierfreundliche
oder klimafreundliche Alternativen dagegen häufig teurer.
Viele Menschen können sich das nicht leisten.

Neben den genannten gibt noch weitere mögliche Hemmnisse,
die bewussten Konsum erschweren
und beispielsweise im sozialen Umfeld liegen können.


##### Einschub *„Externalisierte Kosten“*

Man spricht von „externalisierten Kosten“,
wenn willentlich in Kauf genommen wird,
dass ein Produkt nur deswegen billig sein kann,
weil jemand anders zusätzliche Kosten hat.
Dieser „jemand anders“ sind dann die Menschen,
die mit den Schäden an Umwelt,
Menschenrechtsverletzungen usw. auskommen müssen.
Häufig sind das Menschen, denen es schlechter geht
als uns, oder auch Menschen, die noch gar nicht leben.
Diese aufgeschobenen Kosten werden
durch verschiedene Effekte größer ausfallen
als die jetzt erlangten, kurzfristigen Ersparnisse.
Oft sind es auch wir selbst,
die Teile dieser externalisierten Kosten
über unsere Steuern, über unsere Versicherungsgebühren
oder mit unserer Gesundheit bezahlen müssen.


#### Fazit zum *Bewussten Konsum*

**Fazit: Bewusster Konsum ist ein Privileg.**
Wie können wir von allen Menschen erwarten,
dass sie als bewusste Verbraucher\*innen auftreten
und auschließlich nach ökologischen Kriterien konsumieren?

Das können wir nicht.
Bewusster Konsum ist nicht für alle Menschen eine Option.
Wir brauchen aber keine Möglichkeiten,
durch die privilegierte Menschen ihr Gewissen
etwas weniger belasten müssen.
Wir brauchen Klimagerechtigkeit und CO₂-Neutralität,
die für alle Menschen funktioniert –
auch für die überarbeitete und schlecht bezahlte Pflegekraft
mit wenig Freizeit.
Wie können Politiker\*innen und Konzerne es wagen,
ihre Verantwortung auf diese Menschen abzuschieben?


### Bereiche ohne Einfluss der Verbraucher

Hinzu kommt, dass auch der Einfluss von bewusstem Konsum begrenzt ist.
Selbst, wenn man als Einsiedler\*in und Selbstversorger\*in
CO₂-neutral im Wald lebt,
so ist man immer noch Teil der Gesellschaft
und trägt mit Verantwortung dafür,
dass Steuergeld für klimaschädliche Subventionen
verwendet wird
und dass die Dienstwägen der Regierung mit Benzin,
die Panzer der Bundeswehr mit Diesel,
die Hubschrauber der Bergwacht mit Kerosin und
die Ampeln und Krankenhäuser
mit Strom aus Kohlekraftwerken betrieben werden.

Es gibt Schätzungen,
dass man von den etwa 10 Tonnen CO₂-Äquivalenten,
die pro Einwohner\*in in Deutschland durchschnittlich jährlich anfallen,
im Extremfall mit allen möglichen persönlichen Einschränkungen
maximal etwa 6 Tonnen einsparen könnte
und die restlichen 4 Tonnen
einem nur deswegen persönlich angerechnet werden,
weil man Teil der deutschen Gesellschaft ist.
Vier Tonnen pro Jahr und Person sind immer noch zu viel
und deutlich mehr als der Pro-Kopf-Ausstoß in Ländern
wie Ägypten, Brasilien, Indien, Pakistan oder gar Uganda.
Kein Konsum ist also auch noch keine Lösung.

Die Ohnmacht der Verbraucher\*innen
zeigt sich noch an vielen anderen Stellen.

Verbraucher\*innen würden sich wünschen,
dass ihre Geräte untereinander kompatibel sind.
Im Fall von Netzteilen brauchte es erst die Europäische Union,
um USB-C als Standard für kleine Elektrogeräte festzulegen.
Druckerhersteller bauen auch heute noch
alle möglichen Tricks in ihre Drucker,
um die Benutzung von Tintenpatronen anderer Hersteller zu unterbinden.

Ebenfalls im Interesse der Verbraucher\*innen
und im Sinne des Umweltschutzes
wäre die lange Haltbarkeit von Produkten.
Viele Hersteller bevorzugen dagegen sich verschleißende Produkte.
Wenn der Hersteller Schritte
zur Begrenzung der Haltbarkeit des Produkts unternimmt,
spricht man von *geplanter Obsoleszenz*.
Auch hier hat der Wunsch der Verbraucher\*innen nicht gereicht.
Es brauchte Verbraucherschutzgesetze,
die Gewährleistungsgarantien und Mindesthaltbarkeiten
gesetzlich vorschreiben.
Frankreich ging noch einen Schritt weiter.
Das absichtliche Verkürzen der Lebensdauer von Produkten
ist dort eine Straftat.


## Die Schattenseiten der Konsumkritik

### Vielschichtige Täter-Opfer-Umkehr

Der Plan von BP ist aufgegangen.
Der persönliche CO₂-Fußabdruck ist inzwischen
in den Köpfen vieler Menschen fest verankert.
„Wenn du Klimaschutz willst,
dann fang doch erstmal bei dir selbst an
und höre auf ...“, und so ähnlich lauten Sprüche,
die Klimaaktivist\*innen an den Kopf geworfen werden.

Das Narrativ, das Klimaschutz vor allem in der Verantwortung
des Einzelnen läge, ist eines der gefährlichsten Narrative,
mit denen derzeit wirksame Klimaschutzmaßnahmen bekämpft werden
und versucht wird, die Klimagerechtigkeitsbewegung
als antifreiheitlich zu diskreditieren.
Dabei sind es die Folgen der Erderhitzung,
die Menschen ihrer Freiheit in Form
ihrer Umwelt, ihrer Lebensgrundlagen und ihres Lebens berauben.


### Ein Beispiel aus Augsburg

Zuletzt sahen wir das am 16. und 17. September 2022 am Rathausplatz,
als die bayerische Staatsregierung
dort in ihren „Klimawandel meistern“-Zelten
versuchte, durch Tipps an Verbraucher\*innen
über ihre eigene Untätigkeit hinwegzutäuschen.
Dabei sprachen die Verantwortlichen vor Ort den Bürger\*innen
sogar das Interesse an echter politischer Veränderung ab.
Von einem sich als interessiertem Bürger ausgebenden Klimacamper
auf die Verantwortung von Politik und Konzerne angesprochen,
gab es an einem „Klimawandel meistern“-Zelt die Antwort,
dass das die Bürger gar nicht interessiere,
sondern diese bei sich selbst anfangen wollten.
Eine Meinungsumfrage mit einem derartigen Ergebnis
ist uns aber nicht bekannt.

„Klimawandel meistern“ ist ein Projekt des
Bayerischen Staatsministeriums für Umwelt und Verbraucherschutz,
welches gerade unter Leitung von Thorsten Glauber (Freie Wähler) steht.
Auf der Webseite der Veranstaltung heißt es bespielsweise:

* „Fahre einen Monat lang Alltagswege mit dem Fahrrad.“
* „Heimliche Stromfresser entlarven ... Schalter aus, Stecker raus.“

Die Tipps sind nicht grundsätzlich schlecht,
allerdings muss man genau darauf achten,
welches Ziel mit ihnen verfolgt wird.
Aufgabe der bayerischen Staatsregierung wäre es,
Fahrradwege und öffentlichen Personennahverkehr (ÖPNV) zu verbessern
und den Ausbau von erneuerbaren Energiequellen
und Energiespeichern voranzutreiben.
Stattdessen werden Bürger\*innen
von der bayerischen Staatsregierung aufgefordert,
Strom zu sparen und mehr Rad zu fahren.

Neben der Renaturierung von Mooren
wäre eigentlich auch der Ausbau von erneuerbarer Energieproduktion
eine explizite Aufgabe von Glaubers Ministerium.
Die bayerische Staatsregierung blockiert
mit der 10H-Regelung den Windkraftausbau und trägt so dazu bei,
dass wir im nächsten Winter nicht kostengünstig
mit billigem und klimafreundlichem Strom aus Windkraftanlagen
unsere Wohnungen heizen können,
sondern länger auf teures Erdgas angewiesen bleiben.
Glaubers Koalitionspartner CSU
stellte viel zu lange den Bundesverkehrsminister
und ist maßgeblich für den schlechten Zustand der Radwege
und des ÖPNV und der Bahninfrastruktur verantwortlich.
Weiter erzählt selbst Augsburgs Oberbürgermeisterin ständig,
dass das Land Bayern nicht genügend Geld zur Finanzierung
des ÖPNV zur Verfügung stelle.

Das Ministerium für Verbraucherschutz überträgt
seine Verantwortlichkeiten auf die Verbraucher\*innen.
Der Zynismus hinter der Aktion „Klimawandel meistern“
ist schwer erträglich.
Dabei sind die Verkehrspolitik und die 10H-Regelung
vielleicht die bekanntesten,
aber bei weitem nicht die einzigen Beispiele für das Versagen derer,
die nun das Thema Klimagerechtigkeit
auf die Bürger\*innen abwälzen wollen.

![Das Bild zeigt eine Zeichnung.
Die Zeichnung ist betitelt mit dem Wort „Konsumkritik“.
In der Zeichnung sieht man eine kleine Insel im Ozean,
auf der sich mehrere Personen befinden,
welche die Verbraucher\*innen darstellen sollen.
Über den Personen befindet sich eine mit „Politik“ beschriftet Plattform.
Von dort kippt die Karikatur eines Politikers
Fässer mit Öl auf die Personen.
Ausweichen ist aufgrund des umgebenen Ozeans nicht möglich
Die Sprechblase des Politikers sagt „Hört doch auf im Öl zu duschen!“.
Unter dem Bild steht:
‚Die Politik „weiß“, dass Verbraucher\*innen Dreck an den Händen haben.‘](/pages/material/images/karikatur_von_konsumkritik.svg)


## Sinnvolle Konsumentscheidungen

Es gibt ein paar Bereiche,
in denen sinnvolle Konsumentscheidungen
ohne lange Abwägung getroffen werden können.
Dazu zählen der Verzicht auf Fleischkonsum
und der Bezug von Ökostrom,
aber auch der Wechsel zu einer nachhaltigen Bank und
Abzug aller eigenen Gelder aus klimaschädlichen Subventionen.


### Nachhaltige Banken

Wer Guthaben auf einem Konto bei einer Bank besitzt,
überlässt damit dieser Bank
das Geld zu ziemlich günstigen Konditionen.
Die Bank kann das Geld dann weiter anlegen.
Die meisten Banken investieren auch in klimaschädliche Industrien.
Selbst als nachhaltig beworbene Finanzprodukte
sind davon nicht gänzlich ausgenommen,
denn der Begriff „nachhaltig“ ist nicht ausreichend geschützt.
Auch andere Formen von unmoralischen Investitionen,
wie beispielsweise Investitionen in Rüstungsfirmen,
werden so finanziert.

Ein paar Banken haben es sich zum Ziel gesetzt,
Investitionen nach sozialen und ökologischen Kriterien zu tätigen.
Man spricht dann auch von einer *nachhaltigen Bank* oder *Ökobank*.

Das Klimacamp berät allerdings nicht bei Finanzinvestitionen.
Für weitere Informationen siehe daher
[https://www.urgewald.org/bankwechsel](https://www.urgewald.org/bankwechsel)
oder Verbraucher\*inneninformationsseiten im Internet.
<br>
Siehe auch Wikipedia: [Nachhaltiges Banking](https://de.wikipedia.org/wiki/Nachhaltiges_Banking)
<br>
Und wer einen etwas unterhaltsameren Einstieg in die Thematik möchte: [Spar Wars – Angriff der Fondskrieger (YouTube)](https://www.youtube.com/watch?v=_91Gmb7HN7U)


### Fleischkonsum

Fleischkonsum ist nahezu immer mit negativen Folgen
für Tierwohl, die Umwelt oder das Klima verbunden.
Der Verzicht auf Fleischkonsum
zwingt Bereiche der Wirtschaft dazu sich anzupassen.
Beispielsweise sehen sich Restaurants und Supermärkte gezwungen
gute vegetarische und vegane Alternativen bereit zu stellen.
Da hat sich in den vergangenen 20 Jahren schon sehr viel getan.
Je besser das Angebot an gesunden und abwechslungsreichen
pflanzenbasierten Speisen ist,
umso leichter fällt es dann anderen Menschen
ihren eigenen Fleischkonsum auch zu reduzieren.


### Ökostrom

Auch der Bezug von Ökostrom hat
eine Lenkungswirkung auf die Energiewirtschaft.
Selbst falls ein Ökostromtarif
nur einen Cent pro Kilowattstunde mehr kostet,
ist die Wirkung enorm.
Denn der Ökostromanbieter erhält nicht nur den einen Cent,
sondern etliche Cent pro Kilowattstunde,
die man sowieso zahlen müsste.
Gleichzeitig erhält ein konventioneller Stromanbieter dieses Geld nicht.
Der Bezug von Ökostrom hat daher eine schwer zu überschätzende Wirkung.

Viele Personen in der Klimagerechtigkeitsbewegung
empfehlen den Bezug von Strom
von einem reinen Ökostromanbieter,
der keinen Nichtökostromtarif im Angebot hat.
Dieser kann sich auch nicht durch Buchhaltungstricks herausschummeln,
indem er bei erhöhtem Ökostrombedarf den Ökostromanteil
seines konventionellen Stromtarifs herunterrechnet.
Es gibt mehrere reine Ökostromanbieter auf dem Markt.
In alphabetischer Reihenfolge kennen wir:

* bavariastrom<br>
  https://bavariastrom.de/
* EWS (ElektrizitätsWerke Schönau)<br>
  https://www.ews-schoenau.de/
* Green Planet Energy<br>
  https://green-planet-energy.de/
* LichtBlick<br>
  https://www.lichtblick.de/
* NATURSTROM<br>
  https://www.naturstrom.de/
* Polarstern<br>
  https://www.polarstern-energie.de/
* ...

Die Liste ist nicht zwingend vollständig oder korrekt.
Am Besten erkundigt man sich
auf den Webseiten der jeweiligen Anbieter
oder auf Verbraucher\*inneninformationsseiten.

Einige dieser Ökostromanbieter beziehen ihren Strom
von Bürgerenergiegenossenschaften
oder sind selbst Energiegenossenschaften.
An diesen kann man als Privatperson Anteile erwerben.

Die swa sind kein reiner Ökostromanbieter.
Viele der Firmenkunden der swa beziehen
weiterhin einen Strommix mit fossilen Anteilen,
ja sogar einen besonders dreckigen Strommix.
Der Unternehmenseinkaufsmix der swa hatte im Jahr 2020
438 g an CO₂-Emissionen pro kWh verursacht.
Das ist deutlich schlechter als der deutsche Strommix
des gleichen Jahres mit 310 g/kWh an CO₂-Emissionen.


## Fazit

Bewusster Konsum ist punktuell sinnvoll und zumutbar.
Allerdings ist er kein Ersatz für tiefgreifende politische Veränderungen.
Keinesfalls sollte zugelassen werden,
dass Konsumkritik zur Täter-Opfer-Umkehr missbraucht wird
oder konkrete politische Entscheidungen mit Verweis
auf eine abstrakte individuelle Verantwortung
unterlassen werden.

Wir kritisieren Konsumkritik dann,
wenn sie von Regierungen,
Politiker\*innen oder Konzernen kommt.
Denn derartige Aktionen sind fast immer Ablenkungsmaßnahmen,
die Untätigkeit verschleiern oder Zwiespalt säen sollen.
Die Bürger\*innen sollen sich im Kleinklein verlieren,
anstatt geeint effektive Klimagerechtigkeitsmaßnahmen
zu fordern und durchzusetzen.

Die wirklich wichtigen Stellschrauben liegen in der Politik.
Konzerne können auch viel tun,
sind aber in ihren Möglichkeiten durch die Konkurrenzsituation,
in der sie sich mit anderen Firmen befinden, beschränkt.
Sie müssen „wirtschaftlich“ bleiben.
Die Politik hat dafür Sorge zu tragen,
dass Klimagerechtigkeit wirtschaftlich Sinn ergibt.
Die Profite aus klimaschädlichem Verhalten
dürfen nicht länger privatisiert werden,
während die Kosten der Schäden
von der Allgemeinheit getragen werden.

Die Verantwortung der Bürger\*innen ist es wiederum,
zum einen mit Druck politische Maßnahmen
für effektive Klimagerechtigkeit
von den Politiker\*innen einzufordern
und zum anderen diese Maßnahmen dann zu akzeptieren
und mit zu tragen.
Dazu leisten wir als Klimacamp unseren Beitrag,
indem wir Bürger\*innen informieren
und auch selbst mit Nachdruck
die notwendigen Entscheidungen einfordern.


## Bedeutung für die Arbeit des Augsburger Klimacamps

Aufgrund verschiedener Erfahrung wird Konsumkritik
bei uns am Klimacamp sehr kritisch gesehen.
Am Klimacamp beteiligen sich Veganer genauso wie Menschen,
die gelegentlich Fleisch essen.
Es beteiligen sich Autofahrer\*innen genauso wie Personen,
die trotz Besitz eines Führerscheins seit über einem halben Jahrzehnt
nicht mehr am Steuer eines Autos saßen.

Dass man nicht nur weiß,
was die Folgen des eigenen Verhaltens auf das Klima sind,
sondern sich der Bedeutung dessen wirklich bewusst wird,
beeinflusst natürlich auch das eigene Verhalten.
Daher haben viele von uns von sich aus
ihren Fleischkonsum reduziert und
ihr Mobilitätsverhalten geändert.

Die wesentlichen Veränderungen,
das ist uns allen bewusst,
müssen politischer Natur sein.
Die Politik muss die Voraussetzungen dafür schaffen,
dass klimagerechtes Verhalten auch ökonomisch Sinn ergibt.
Klimafreundliche Varianten müssen billiger,
schneller, bequemer und zuverlässiger
als ihre nicht klimafreundliche Konkurrenz sein.
Klimafreundliches Verhalten muss der Standard sein.


-- Klimacamp Augsburg (https://augsburg.klimacamp.eu alias https://www.klimacamp-augsburg.de/)

Dieser Artikel steht unter der Lizenz
[Creative Commons BY 4.0](https://creativecommons.org/licenses/by/4.0/).

---

*Dieser Artikel basiert auf dem Workshop
„Konsumkritik-Kritik: von der Mär
der angeblichen Macht der Verbraucher\*innen“,
der am 26. Januar 2022 und am 1. Juli 2022
im Augsburger Klimacamp abgehalten wurde.
Der Artikel spiegelt aber nicht die volle Vielfalt
an Ansichten und Meinungen zu Konsumkritikkritik wieder,
wie es ein zweistündiger Workshop
mit einem halben bis einem Dutzend Teilnehmer\*innen kann.
Wer neugierig geworden ist,
kann Workshops zu diesem oder verwandten Themen am Klimacamp besuchen.*
