---
layout: page
title: Bodenbanner Korrupte Union (2021)
permalink: /bodenbanner/2021-csu-korrupt/
nav_exclude: true
---

# Bodenbanner Korrupte Union (2021)

![TODO Beschreibung Bodenbanner Korrupte Union](/bodenbanner/png-vorschau/2021-07-bodenbanner-csu-korrupt-mittel.png)


- [PDF-Druckdatei](/bodenbanner/pdf-druck/2021-07-bodenbanner-csu-korrupt-v2.pdf) (38,8 MB)
- [Inkscape-SVG (bearbeitbar)](/bodenbanner/bearbeitbar/2021-07-bodenbanner-csu-korrupt.svg) (56,8 MB)
- [Große PNG-Vorschau (1920 × 738 Pixel)](/bodenbanner/png-vorschau/2021-07-bodenbanner-csu-korrupt.png) (2,1 MB)
- [Mittlere PNG-Vorschau (768 × 295 Pixel)](/bodenbanner/png-vorschau/2021-07-bodenbanner-csu-korrupt-mittel.png) (390 KB)
- [Kleine PNG-Vorschau (320 × 123 Pixel)](/bodenbanner/png-vorschau/2021-07-bodenbanner-csu-korrupt-klein.png) (82 KB)


Wenn du diesen Text siehst, dann musst du dich noch ein paar wenige Tage gedulden, bevor hier mehr Inhalte stehen :-)

## Originale Grafiken

TODO