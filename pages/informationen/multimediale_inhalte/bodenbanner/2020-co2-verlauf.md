---
layout: page
title: Bodenbanner Verlauf CO₂ (2020)
permalink: /bodenbanner/2020-co2-verlauf/
nav_exclude: true
---

# Bodenbanner Verlauf CO₂ (2020)

![TODO Beschreibung Bodenbanner Verlauf CO₂](/bodenbanner/png-vorschau/2020-rav-banner-3-co2-verlauf22-mittel.png)


- [PDF-Druckdatei](/bodenbanner/pdf-druck/2020-rav-banner-3-co2-verlauf22.pdf) (130 KB)
- [Inkscape-SVG (bearbeitbar)](/bodenbanner/bearbeitbar/2020-rav-banner-3-co2-verlauf22.svg) (255 KB)
- [Große PNG-Vorschau (1920 × 738 Pixel)](/bodenbanner/png-vorschau/2020-rav-banner-3-co2-verlauf22.png) (220 KB)
- [Mittlere PNG-Vorschau (768 × 295 Pixel)](/bodenbanner/png-vorschau/2020-rav-banner-3-co2-verlauf22-mittel.png) (115 KB)
- [Kleine PNG-Vorschau (320 × 123 Pixel)](/bodenbanner/png-vorschau/2020-rav-banner-3-co2-verlauf22-klein.png) (30 KB)


Wenn du diesen Text siehst, dann musst du dich noch ein paar wenige Tage gedulden, bevor hier mehr Inhalte stehen :-)

## Originale Grafiken

TODO