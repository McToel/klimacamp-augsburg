---
layout: page
title: Bodenbanner Facts (2020)
permalink: /bodenbanner/2020-catchy/
nav_exclude: true
---

# Bodenbanner Facts (2020)

![TODO Beschreibung Bodenbanner Facts](/bodenbanner/png-vorschau/2020-banner-2-catchy-mittel.png)


- [PDF-Druckdatei](/bodenbanner/pdf-druck/2020-banner-2-catchy.pdf) (850 KB)
- [Inkscape-SVG (bearbeitbar)](/bodenbanner/bearbeitbar/2020-banner-2-catchy.svg) (865 KB)
- [Große PNG-Vorschau (1920 × 738 Pixel)](/bodenbanner/png-vorschau/2020-banner-2-catchy.png) (320 KB)
- [Mittlere PNG-Vorschau (768 × 295 Pixel)](/bodenbanner/png-vorschau/2020-banner-2-catchy-mittel.png) (125 KB)
- [Kleine PNG-Vorschau (320 × 123 Pixel)](/bodenbanner/png-vorschau/2020-banner-2-catchy-klein.png) (28 KB)


Wenn du diesen Text siehst, dann musst du dich noch ein paar wenige Tage gedulden, bevor hier mehr Inhalte stehen :-)

## Originale Grafiken

TODO