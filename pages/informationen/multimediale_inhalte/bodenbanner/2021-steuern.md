---
layout: page
title: Bodenbanner Spenden & Steuern (2021)
permalink: /bodenbanner/2021-steuern/
nav_exclude: true
---

# Bodenbanner Spenden & Steuern (2021)

![TODO Beschreibung Bodenbanner Spenden & Steuern](/bodenbanner/png-vorschau/2021-07-bodenbanner-steuern-mittel.png)


- [PDF-Druckdatei](/bodenbanner/pdf-druck/2021-07-bodenbanner-steuern-v3.pdf) (390 KB)
- [Inkscape-SVG (bearbeitbar)](/bodenbanner/bearbeitbar/2021-07-bodenbanner-steuern.svg) (5,0 MB)
- [Große PNG-Vorschau (1920 × 738 Pixel)](/bodenbanner/png-vorschau/2021-07-bodenbanner-steuern.png) (460 KB)
- [Mittlere PNG-Vorschau (768 × 295 Pixel)](/bodenbanner/png-vorschau/2021-07-bodenbanner-steuern-mittel.png) (180 KB)
- [Kleine PNG-Vorschau (320 × 123 Pixel)](/bodenbanner/png-vorschau/2021-07-bodenbanner-steuern-klein.png) (40 KB)


Wenn du diesen Text siehst, dann musst du dich noch ein paar wenige Tage gedulden, bevor hier mehr Inhalte stehen :-)

## Originale Grafiken

TODO