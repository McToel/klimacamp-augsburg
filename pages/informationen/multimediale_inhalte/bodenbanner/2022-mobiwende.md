---
layout: page
title: Bodenbanner Mobilitätswende (2022)
permalink: /bodenbanner/2022-mobiwende/
nav_exclude: true
---

# Bodenbanner Aufheizung (2022)

![TODO Beschreibung Bodenbanner Aufheizung Stripes](/bodenbanner/png-vorschau/2022-10-bodenbanner-mobiwende-mittel.png)


- [PDF-Druckdatei](/bodenbanner/pdf-druck/2022-10-bodenbanner-mobiwende.pdf) (1,1 MB)
- [Inkscape-SVG (bearbeitbar)](/bodenbanner/bearbeitbar/2022-10-bodenbanner-mobiwende.svg) (2,7 MB)
- [Große PNG-Vorschau (1920 × 738 Pixel)](/bodenbanner/png-vorschau/2022-10-bodenbanner-mobiwende.png) (550 KB)
- [Mittlere PNG-Vorschau (768 × 295 Pixel)](/bodenbanner/png-vorschau/2022-10-bodenbanner-mobiwende-mittel.png) (180 KB)
- [Kleine PNG-Vorschau (320 × 123 Pixel)](/bodenbanner/png-vorschau/2022-10-bodenbanner-mobiwende-klein.png) (40 KB)


Wenn du diesen Text siehst, dann musst du dich noch ein paar wenige Tage gedulden, bevor hier mehr Inhalte stehen :-)

## Originale Grafiken

TODO