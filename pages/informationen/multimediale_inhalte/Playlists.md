---
layout: page
title: Playlists
permalink: /playlists/
parent: "Artikel & Medien"
nav_order: 1030
---

# Playlists für Demos

Nicht von uns komponiert, aber von uns zusammengestellt:

### Fahrrad
* [Kidical Mass Aux (Spotify)](https://open.spotify.com/playlist/1QmxxMuT6UuWZybX9O7Cbu) – mit vielen Liedern für die ganz kleinen, aber auch für die mitradelnden Eltern
* [Taulex Radelparade (YouTube)](https://www.youtube.com/playlist?list=PLwsCJhMSOdZzfNEsjULzZWFDEEdTr5D2m) – neben zwei Kultklassikern von Queen ausschließlich deutschsprachige Hits für jung und alt (vier Extrasongs in der Playlistbeschreibung!)
* [Fahrradhits (Spotify)](https://open.spotify.com/playlist/0xeyTCRjJBPBQfkdRAScqq) – Kultige verschiedensprachige Demolieder rund ums Rebellieren und Fahrradfahren

### Allgemeine Demo
* [Demoknaller (Spotify)](https://open.spotify.com/playlist/7kPtl439qKKPtWhKfpjlsD) – fetzige Stimmung zu treffenden Inhalten
