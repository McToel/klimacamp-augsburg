---
layout: page
title: Bodenbanner
permalink: /bodenbanner/
parent: "Artikel & Medien"
nav_order: 1010
---

# Bodenbanner

Hier sind alle unsere bisher fertiggestellten Bodenbanner zu sehen. Neben dem Download der Bannerdateien selbst, gibt es zu jedem Banner auch einen Link zu jeweils einer extra Seite mit Erläuterungen von uns.

Die meisten Bodenbanner werden zusammengefügt aus bereits bestehenden Grafiken. Dabei benutzen wir häufig tolle Grafiken vom [Katapult-Magazin](https://katapult-magazin.de/de). Viele der benutzten Grafiken werden von uns angepasst. Oft werden nur die Inhalte anders platziert, manchmal fügen wir aber auch Infos hinzu oder fügen sie zusammen.


## Vier neue Bodenbanner (2022)

Wir haben eure Anregungen gesammelt und gehört, worüber ihr reden wollt, und daraus die folgenden neuen Bodenbanner erstellt. Diese liegen seit Oktober 2022 beim Klimacamp aus. Viel Spaß und vielen Dank für alle Ideen und Wünsche bisher!

### Aufheizung

![Vorschau Bodenbanner Aufheizung Stripes](/bodenbanner/png-vorschau/2022-10-bodenbanner-aufheizung-stripes-klein.png)

- [Klicke hier](/bodenbanner/2022-aufheizung/) für Erläuterungen
- [PDF-Druckdatei](/bodenbanner/pdf-druck/2022-10-bodenbanner-aufheizung-stripes.pdf) (1,9 MB)
- [Inkscape-SVG (bearbeitbar)](/bodenbanner/bearbeitbar/2022-10-bodenbanner-aufheizung-stripes.svg) (6,4 MB)
- [Große PNG-Vorschau (1920 × 738 Pixel)](/bodenbanner/png-vorschau/2022-10-bodenbanner-aufheizung-stripes.png) (600 KB)
- [Mittlere PNG-Vorschau (768 × 295 Pixel)](/bodenbanner/png-vorschau/2022-10-bodenbanner-aufheizung-stripes-mittel.png) (235 KB)
- [Kleine PNG-Vorschau (320 × 123 Pixel)](/bodenbanner/png-vorschau/2022-10-bodenbanner-aufheizung-stripes-klein.png) (50 KB)


### Mobilitätswende

![Vorschau Bodenbanner Mobilitätswende](/bodenbanner/png-vorschau/2022-10-bodenbanner-mobiwende-klein.png)

- [Klicke hier](/bodenbanner/2022-mobiwende/) für Erläuterungen
- [PDF-Druckdatei](/bodenbanner/pdf-druck/2022-10-bodenbanner-mobiwende.pdf) (1,1 MB)
- [Inkscape-SVG (bearbeitbar)](/bodenbanner/bearbeitbar/2022-10-bodenbanner-mobiwende.svg) (2,7 MB)
- [Große PNG-Vorschau (1920 × 738 Pixel)](/bodenbanner/png-vorschau/2022-10-bodenbanner-mobiwende.png) (550 KB)
- [Mittlere PNG-Vorschau (768 × 295 Pixel)](/bodenbanner/png-vorschau/2022-10-bodenbanner-mobiwende-mittel.png) (180 KB)
- [Kleine PNG-Vorschau (320 × 123 Pixel)](/bodenbanner/png-vorschau/2022-10-bodenbanner-mobiwende-klein.png) (40 KB)


### Strom 1

![Vorschau Bodenbanner Strom 1](/bodenbanner/png-vorschau/2022-10-bodenbanner-strom1-klein.png)

- [Klicke hier](/bodenbanner/2022-strom1/) für Erläuterungen
- [PDF-Druckdatei](/bodenbanner/pdf-druck/2022-10-bodenbanner-strom1.pdf) (1,7 MB)
- [Inkscape-SVG (bearbeitbar)](/bodenbanner/bearbeitbar/2022-10-bodenbanner-strom1.svg) (2,4 MB)
- [Große PNG-Vorschau (1920 × 738 Pixel)](/bodenbanner/png-vorschau/2022-10-bodenbanner-strom1.png) (860 KB)
- [Mittlere PNG-Vorschau (768 × 295 Pixel)](/bodenbanner/png-vorschau/2022-10-bodenbanner-strom1-mittel.png) (195 KB)
- [Kleine PNG-Vorschau (320 × 123 Pixel)](/bodenbanner/png-vorschau/2022-10-bodenbanner-strom1-klein.png) (40 KB)


### Strom 2

![Vorschau Bodenbanner Strom 2](/bodenbanner/png-vorschau/2022-10-bodenbanner-strom2-klein.png)

- [Klicke hier](/bodenbanner/2022-strom2/) für Erläuterungen
- [PDF-Druckdatei](/bodenbanner/pdf-druck/2022-10-bodenbanner-strom2.pdf) (1,1 MB)
- [Inkscape-SVG (bearbeitbar)](/bodenbanner/bearbeitbar/2022-10-bodenbanner-strom2.svg) (4,2 MB)
- [Große PNG-Vorschau (1920 × 738 Pixel)](/bodenbanner/png-vorschau/2022-10-bodenbanner-strom2.png) (530 KB)
- [Mittlere PNG-Vorschau (768 × 295 Pixel)](/bodenbanner/png-vorschau/2022-10-bodenbanner-strom2-mittel.png) (205 KB)
- [Kleine PNG-Vorschau (320 × 123 Pixel)](/bodenbanner/png-vorschau/2022-10-bodenbanner-strom2-klein.png) (45 KB)



## Zwei neue Bodenbanner zur Bundestagswahl (2021)

Zur Bundestagswahl am 26. September 2021, lagen ab Anfang August 2021 diese zwei neuen Bodenbanner am Klimacamp aus.


### Spenden & Steuern

![Vorschau Bodenbanner Spenden & Steuern](/bodenbanner/png-vorschau/2021-07-bodenbanner-steuern-klein.png)

- [Klicke hier](/bodenbanner/2021-steuern/) für Erläuterungen
- [PDF-Druckdatei](/bodenbanner/pdf-druck/2021-07-bodenbanner-steuern-v3.pdf) (390 KB)
- [Inkscape-SVG (bearbeitbar)](/bodenbanner/bearbeitbar/2021-07-bodenbanner-steuern.svg) (5,0 MB)
- [Große PNG-Vorschau (1920 × 738 Pixel)](/bodenbanner/png-vorschau/2021-07-bodenbanner-steuern.png) (460 KB)
- [Mittlere PNG-Vorschau (768 × 295 Pixel)](/bodenbanner/png-vorschau/2021-07-bodenbanner-steuern-mittel.png) (180 KB)
- [Kleine PNG-Vorschau (320 × 123 Pixel)](/bodenbanner/png-vorschau/2021-07-bodenbanner-steuern-klein.png) (40 KB)


### Korrupte Union

![Vorschau Bodenbanner Korrupte Union](/bodenbanner/png-vorschau/2021-07-bodenbanner-csu-korrupt-klein.png)

- [Klicke hier](/bodenbanner/2021-csu-korrupt/) für Erläuterungen
- [PDF-Druckdatei](/bodenbanner/pdf-druck/2021-07-bodenbanner-csu-korrupt-v2.pdf) (38,8 MB)
- [Inkscape-SVG (bearbeitbar)](/bodenbanner/bearbeitbar/2021-07-bodenbanner-csu-korrupt.svg) (56,8 MB)
- [Große PNG-Vorschau (1920 × 738 Pixel)](/bodenbanner/png-vorschau/2021-07-bodenbanner-csu-korrupt.png) (2,1 MB)
- [Mittlere PNG-Vorschau (768 × 295 Pixel)](/bodenbanner/png-vorschau/2021-07-bodenbanner-csu-korrupt-mittel.png) (390 KB)
- [Kleine PNG-Vorschau (320 × 123 Pixel)](/bodenbanner/png-vorschau/2021-07-bodenbanner-csu-korrupt-klein.png) (82 KB)



## Erste drei Bodenbanner (2020)

Unsere allerersten Bodenbanner. Die heute ausliegenden und hier rgezeigten Banner wurden 2022 aktualisiert. So wurde die Auflistung der fünf heißesten Jahre erweitert zu den sieben heißesten Jahren, die zu denen auch 2020 und 2021 gehören, und es wurden die 'Warming Stripes' eingefügt. Beim Graphen zur CO₂-Konzentration wurde das Ende bis 2022 erweitert.

Als Blickfang und Veranschaulichungshilfe haben die Bodenbanner schon seit 2020 viele belebte Diskussionen gestartet. Wir danken euch für euer bestehendes Interesse, unsere Bodenbanner anzuschauen!


### Facts

![Vorschau Bodenbanner Facts](/bodenbanner/png-vorschau/2020-banner-2-catchy-klein.png)

- [Klicke hier](/bodenbanner/2020-catchy/) für Erläuterungen
- [PDF-Druckdatei](/bodenbanner/pdf-druck/2020-banner-2-catchy.pdf) (850 KB)
- [Inkscape-SVG (bearbeitbar)](/bodenbanner/bearbeitbar/2020-banner-2-catchy.svg) (865 KB)
- [Große PNG-Vorschau (1920 × 738 Pixel)](/bodenbanner/png-vorschau/2020-banner-2-catchy.png) (320 KB)
- [Mittlere PNG-Vorschau (768 × 295 Pixel)](/bodenbanner/png-vorschau/2020-banner-2-catchy-mittel.png) (125 KB)
- [Kleine PNG-Vorschau (320 × 123 Pixel)](/bodenbanner/png-vorschau/2020-banner-2-catchy-klein.png) (28 KB)


### Verlauf CO₂

![Vorschau Bodenbanner Verlauf CO₂](/bodenbanner/png-vorschau/2020-rav-banner-3-co2-verlauf22-klein.png)

- [Klicke hier](/bodenbanner/2020-co2-verlauf/) für Erläuterungen
- [PDF-Druckdatei](/bodenbanner/pdf-druck/2020-rav-banner-3-co2-verlauf22.pdf) (130 KB)
- [Inkscape-SVG (bearbeitbar)](/bodenbanner/bearbeitbar/2020-rav-banner-3-co2-verlauf22.svg) (255 KB)
- [Große PNG-Vorschau (1920 × 738 Pixel)](/bodenbanner/png-vorschau/2020-rav-banner-3-co2-verlauf22.png) (220 KB)
- [Mittlere PNG-Vorschau (768 × 295 Pixel)](/bodenbanner/png-vorschau/2020-rav-banner-3-co2-verlauf22-mittel.png) (115 KB)
- [Kleine PNG-Vorschau (320 × 123 Pixel)](/bodenbanner/png-vorschau/2020-rav-banner-3-co2-verlauf22-klein.png) (30 KB)


### Vergleich größte Emittenten

![Vorschau Bodenbanner Verlauf CO₂](/bodenbanner/png-vorschau/2020-rav-banner-1-vergleich-laender-klein.png)

- [Klicke hier](/bodenbanner/2020-vergleich-laender/) für Erläuterungen
- [PDF-Druckdatei](/bodenbanner/pdf-druck/2020-banner-1-vergleich-laender.pdf) (185 KB)
- [Inkscape-SVG (bearbeitbar)](/bodenbanner/bearbeitbar/2020-rav-banner-1-vergleich-laender.svg) (4,2 MB)
- [Große PNG-Vorschau (1920 × 738 Pixel)](/bodenbanner/png-vorschau/2020-rav-banner-1-vergleich-laender.png) (105 KB)
- [Mittlere PNG-Vorschau (768 × 295 Pixel)](/bodenbanner/png-vorschau/2020-rav-banner-1-vergleich-laender-mittel.png) (60 KB)
- [Kleine PNG-Vorschau (320 × 123 Pixel)](/bodenbanner/png-vorschau/2020-rav-banner-1-vergleich-laender-klein.png) (15 KB)

