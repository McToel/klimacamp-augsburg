---
layout: page
title: CSU-Korruption
permalink: /csu-korruption/
nav_exclude: true
---

# €SU = Korruption ?

## Übertreibung? Nein!

Wir haben heute ein 13m langes Banner mit dem Slogan „€SU = Korruption“ an den Baukran auf dem Grundstück Georg Nüßleins am Klingeberg in Billenhausen gehängt. Hier hat (immer noch) Bundestagsabgeordneter Nüßlein, bekannt durch die Maskenaffäre, ein Bauobjekt, wo schon etliche halblegale Auffälligkeiten auftraten, die bei normalen Bürgern im Vorhinein verboten oder im Nachhinein nicht genehmigt worden wären. Eine wahrscheinlich unvollständige Liste befindet sich weiter unten. Doch der Skandal ist weitaus größer.

Amigo-Affäre, CDU-Spendenskandal, Aserbaidschan-Affäre, gefälschte Doktortitel, Abgeordneten-Nebeneinkünfte (oder besser Haupteinkünfte), einfach nur alles was Andi Scheuer tut, Armin Laschets gewürfelte Noten oder kürzlich die Maskendeals um Georg Nüßlein. Egal wo man hinschaut, die Unionsparteien sind in dermaßen vielen Fällen von Korruption, Betrug und Rechtsbeugung verwickelt, dass selbst alle Politiker der Parteien zusammen nicht auf eine so große Zahl an moralisch verwerflichem Verhalten kommen [1]. Gleichzeitig setzen sie sich gegen jede Form der Transparenz und Kontrolle zur Wehr.

Da fragt man sich schon, ob sie ohne die Millionen Spendengelder, die ihnen große Wirtschaftsverbände und Unternehmen vor jeder Wahl überweisen [2], überhaupt in der Lage wären, sich an der Macht zu halten und so vergessene Skandale, wie das bayerische Atombombenbau-Programm [3] (Strauß-Idee) oder die Ernennung von CSU-Größe Otto Wiesheu zum bayerischen Verkehrsminister (obwohl er zuvor jemanden besoffen tot gefahren hat) weiter in Vergessenheit zu halten und sich deshalb als glorreiche Traditionspartei inszenieren können.

Wenn die Politik schon so durchsetzt ist von CSU-Seilschaften, dann braucht es uns, die Zivilbevölkerung, die bei jeder Gelegenheit darauf aufmerksam macht, was hier vor sich geht. Darum bitten wir Sie, Georg Nüßlein (der trotz Arbeitsverweigerung weiterhin sein üppiges Bundestagsgehalt bezieht) und die CSU (die sich weiterhin gegen ein Lobbyregister und Transparenz bei Spendengelder ausspricht [4]) wo es nur geht damit zu konfrontieren, was sie für eine Sauerei abziehen. Das 13m-Banner kann dafür nur ein Mahnmal sein. Handeln muss jeder und jede von uns!


![Spenden an Bundestagsparteien bei letzten Bundestagswahljahren, von Lobby Control](/pages/Pressemitteilungen/A-Gesamtspenden-2017.png)


## Liste mit halblegalen baurechtlichen Machenschaften bei den Nüßlein-Immobilien in Billenhausen

### Reihenhaus-Immobilien am Klingeberg
Über einen Tipp aus der Bevölkerung haben wir erfahren, dass Nüßlein über die KfW-Bank eine Sanierungs-Förderung beantragt hat, die doppelt so hoch ist, wie üblich. Diese Sanierung wird pro Hauseingang beantragt. Normalerweise bedeutet dies für ein zweistöckiges Gebäude mit zwei Wohneinheiten also eine Förderung. Die Nüßlein-Immobilie hat an den Seiten für die oberen Geschosse relativ unbrauchbare Wendeltreppen angebracht, die man kaum nutzen kann, wenn man etwas mehr als eine kleine Einkaufstasche trägt, die aber deshalb als zweiter Haupteingang klassifiziert werden. Ob das gerade noch legal ist, ist auch unter Baurechtlern umstritten. Auf jeden Fall läuft es dem Förderzweck entgegen und für einen Repräsentanten des Staates mit Vorbildfunktion daher nicht würdig. Wir begannen daraufhin zu recherchieren und es fielen uns noch mehr Merkwürdigkeiten auf. Laut Aussage von Billenhausenern waren urplötzlich jedes mal, wenn Nüßlein aufgrund der Maskendeals negativ in der Presse war, seine ausschließlich rumänischen Bauarbeiter für Wochen von der Baustelle verschwunden. Wurde es wieder ruhig um ihn, kamen sie zurück. Georg Nüßlein besitzt außerdem eine Pferdekoppel am östlichen Rand von Billenhausen, kurz vor dem Wald. Es würde uns nicht wundern, wenn auch hier der Anbau mit Unstimmigkeiten in Verbindung zu bringen ist.

Alle diese Punkte sind nicht eindeutig illegal, sondern ein Stück weit Auslegungssache, aber darauf sollte man es nicht sitzen lassen. Denn jeder normale Bürger, der einen Bauantrag stellt, wird auf hunderte Vorschriften hingewiesen, wovon etliche völlig unpassend sind. Aber nur mit guten Kontakten oder viel mühsamer Überzeugungsarbeit werden Ausnahmen genehmigt. Georg Nüßlein, der immer noch 14.000€ pro Monat für sein Bundestagsmandat erhält und darüber hinaus ein Vielfaches davon an Nebeneinkünften einstreicht, ist das Paradebeispiel für die korrupten Machenschaften der CSU-Seilschaften. Aber er ist nur die Spitze des Eisbergs und das ist der eigentliche Skandal!


## Quellen 
1. Die CDU/CSU ist korrupter als alle anderen Parteien: [https://de.wikipedia.org/wiki/Liste_von_Korruptionsaffären_um_Politiker_in_der_Bundesrepublik_Deutschland](https://de.wikipedia.org/wiki/Liste_von_Korruptionsaff%C3%A4ren_um_Politiker_in_der_Bundesrepublik_Deutschland), Siehe dazu auch: [https://www.youtube.com/watch?v=1x3QwB7bVNg](https://www.youtube.com/watch?v=1x3QwB7bVNg)
2. Spenden an die deutschen Parteien [https://www.abgeordnetenwatch.de/blog/parteispenden/neue-liste-von-diesen-konzernen-und-verbaenden-bekamen-parteien-das-meiste-geld](https://www.abgeordnetenwatch.de/blog/parteispenden/neue-liste-von-diesen-konzernen-und-verbaenden-bekamen-parteien-das-meiste-geld)
3. „Die bayerische Atombombe“ [https://taz.de/Franz-Josef-Strauss-und-die-Gelueste-auf-eine-Atombombe/!1876587/](https://taz.de/Franz-Josef-Strauss-und-die-Gelueste-auf-eine-Atombombe/!1876587/)
4. Otto Wiesheu, der CSU-Verkehrsminister, der zuvor jemanden besoffen tot gefahren hat [https://de.wikipedia.org/wiki/Otto_Wiesheu](https://de.wikipedia.org/wiki/Otto_Wiesheu)

Die Angaben zu Nüßleins Immobilien sind durch Recherchen entstanden von verschiedenen ihm politisch bzw. wirtschaftlich nahestehenden Personen in und um Krumbach. Wir würden die Quellen gerne öffentlich machen. Sie haben aber Angst vor politischer und juristischer Repression, insbesondere durch die Hintertür, was wir respektieren.
