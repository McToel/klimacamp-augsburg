---
layout: page
title: Mitmachen
permalink: /mitmachen/
has_children: true
nav_order: 40
---


# Nicht länger warten – einfach anrufen

Du willst lieber direkt mit einer Person aus dem Klimacamp sprechen?
[Schreib einfach dem Klimacamp oder ruf an.](/kontakt/)
Gerne direkt anrufen bei Ingo: [+49 176 95110311](tel:+4917695110311)


# Vorbeikommen & mitmachen

Es gibt verschiedene Möglichkeiten sich bei uns einzubringen,
beispielsweise indem man Schichten im Camp übernimmt,
bei der Erstellung von Inhalten für Bodenbanner oder die Webseite hilft,
eigene Vorträge oder Workshops anbietet
oder bei der Organisation von Events unterstützt.

Des Weiteren kann man einfach so vorbeikommen,
um sich mit uns zu unterhalten oder an unseren Workshops teilzunehmen.
Wir haben auch immer wieder einstiegsfreundliche Veranstaltungen
in unserem [Programm](/programm/),
wie beispielsweise die Teerunde am 25. November 2022.


## Adresse

Wenn du Lust hast vorbeizukommen, ob für ein kurzes Gespräch oder mehrere Tage
-- du findest uns direkt neben dem Augsburger Rathaus (Adresse: "Am Fischmarkt").


## Du bist willkommen!

Zum Klimacamp (und unseren digitalen Strukturen) kommen ständig neue Menschen
hinzu, die sich für Klimagerechtigkeit engagieren möchten. Du kannst dich
laufenden Projekten anschließen, neue vorantreiben (wir haben
mittlerweile viel Know-how angesammelt, um dich dabei möglichst gut
unterstützen zu können) oder auch einfach Zeit im Camp verbringen, um uns
kennenzulernen und dich in die lange Reihe von Menschen, die das Camp seit
Gründung stabil hielten, einzureihen. :-)

Wir sind eine öffentliche Versammlung. Es gibt keine Mitgliederliste oder
Verpflichtungen. Engagiere dich so, wie du Zeit hast, und nimm dich zurück,
wenn du dich auf andere Lebensbereiche konzentrieren möchtest oder musst.

WLAN und Strom zum Arbeiten sind vorhanden.


## Schreib uns oder ruf an

Wenn du weitere Infos möchtest, schreib uns auf Insta (@klimacamp) oder Telegram oder ruf an :-) Aktuelle Kontaktmöglichkeiten unter [Kontakt](/kontakt/)
