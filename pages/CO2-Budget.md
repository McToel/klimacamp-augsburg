---
layout: page
title: CO₂-Budget
permalink: /co2-budget/
parent: "Forderungen"
nav_order: 10
---

# CO₂-Budget und -Emissionen der Stadt Augsburg

Das Pariser Klimaabkommen aus dem Jahr 2015 sieht eine Reduktion
klimaschädlicher Treibhausgasemissionen für die Weltgemeinschaft vor, damit die
globale Erhitzung deutlich unter 1,5 °C bleibt.

Dieses Ziel wurde beschlossen, da sonst zu befürchten ist, dass Kippelemente
des Öko- und Klimasystems ausgelöst werden. Überschreiten wir diese 1,5 °C
Erhitzung, kann eine Spirale des sich selbst verstärkenden Klimawandels
entstehen. Die Bundesrepublik hat dieses Papier unterschrieben.

Vor der Wahl bekannte sich die Augsburger Oberbürgermeisterin Eva Weber (CSU)
im [Interview mit der
DAZ](https://www.daz-augsburg.de/interview-zur-stichwahl-weber-und-wurm-ueber-klimaschutz-mobilitaet-integration-und-wohnen/) zu den „Klimazielen der Weltgemeinschaft“. In einem
Gespräch im Klimacamp betonte sie am 10. Juli 2020 ebenfalls wiederholt die
Wichtigkeit des Pariser Klimaschutzabkommens.

Die Klimaschutzziele des schwarz-grünen Koalitionsvertrages sind jedoch völlig
unzureichend, um innerhalb des CO₂-Budgets zu bleiben, welches uns laut dem
IPCC-Bericht anteilig noch als Stadtgemeinschaft zustehen würde. Bis zum Jahr
2030 plant die Stadt Augsburg fast 20 Millionen Tonnen CO₂ zu emittieren.
Insgesamt sieht sie einen CO₂-Ausstoß bis 2050 von **34 Millionen Tonnen** vor. Ihr
steht insgesamt aber nur noch eine Menge von ca. **11 Millionen Tonnen** zu. Diese
Menge wird laut Koalitionsplan bis 2025 emittiert sein! **Bis zum Jahr 2050
werden wir nach den Zielen der schwarz-grünen Koalition ungefähr dreimal so
viel emittieren, wie wir dürften, um unseren Teil zum 1,5-°C-Ziel
beizutragen.** Dies ist Unrecht, und deswegen sind wir hier.


## Grafik

![CO₂-Budget und -Emissionen der Stadt Augsburg](/pages/material/CO2-Budget/Grafik.png)

[Grafik in groß](/pages/material/CO2-Budget/Grafik.pdf) --
[mögliche Emissionspfade](/pages/material/CO2-Budget/Emissionspfade.pdf)

[Wie kommt diese Berechung zustande?](/pages/material/CO2-Budget/Rechnung.pdf)


## Kritik

1. Augsburg hat Industrie und stellt Güter für andere Städte her. Es wird
   gelegentlich argumentiert, dass die bei der Produktion anfallenden
   CO₂-Emissionen auf das Konto derjenigen Städte, in denen die Produkte
   konsumiert werden, geschlagen werden sollten. Für Augsburg sind
   diesbezüglich leider keine Zahlen bekannt, wohl aber für Deutschland als
   Ganzes. Diese Betrachtungsweise führt aufgrund der Vielzahl CO₂-intensiver
   Importe zu einem *geringeren Restbudget*.
2. Die Rechnung basiert auf dem IPCC-Report. *Dieser ist weichgewaschen.* Alle
   UN-Nationen mussten ihm zustimmen, auch etwa Saudi-Arabien. Viele
   europäische Studien kamen zum Schluss, dass das Welt-CO2-Budget viel
   geringer ist als in dem Sonderbericht von 2018 angegeben. Es wird auch
   erwartet, dass der nächste IPCC-Report viel geringere Margen angeben wird.
3. Die Rechnung beginnt willkürlich mit 2020 als Referenzdatum. Wir könnten auch
   1850 als Referenz nehmen — denn für den Treibhauseffekt kommt es auf die
   Gesamtmenge CO₂ an. Dann kommt aber heraus: Deutschland überschritt sein
   Budget schon vor vielen Jahrzehnten. Das ist eine Erkenntnis, die nur zu Lähmung
   führt: Im Klimacamp haben wir die Position: Klar, in der Vergangenheit wurde
   deutlich zu wenig eingespart. Aber Vergangenes ist Vergangenes. Es ist
   wichtig, jetzt entschlossen zu handeln. Kritik an früherer Politik lenkt nur ab.
