---
layout: page
title: "Artikel & Medien"
has_children: true
has_toc: false
permalink: /informationen/
nav_order: 90
---

# Artikel

Hier sammeln wir Beiträge für Personen,
die sich inhaltlich tiefer mit dem Thema *Klimagerechtigkeit*
auseinander setzen möchten.
*Was sind unsere Quellen?*
*Welche Funktion erfüllt Klimaaktivismus?*

* [Quellen – Woher nehmen wir unsere Informationen?](/informationen/artikel/quellen/)<br>
  Hier geben wir eine Auflistung
  einiger unserer wichtigsten inhaltlichen Quellen
  inklusive einer kurzen Beschreibung an.
  Wo es uns möglich ist,
  verlinken wir auch direkt auf die Originalquelle.
* [(Klima-)Aktivismus](/informationen/artikel/klimaaktivismus/)<br>
  Welche Funktion hat Aktivismus in unserer Gesellschaft?
* [Mobilitätswende](/informationen/artikel/mobilitaetswende/)<br>
  Was verstehen wir unter einer Mobilitätswende und
  wie unterscheidet sie sich von einer reinen Antriebswende?
* [Klimauhren](/informationen/artikel/klimauhren/)<br>
  Was ist eine Klimauhr?
  Wie berechnet sich ihr Wert?
  Warum gibt es verschiedene Klimauhren?
* [Konsumkritikkritik – Kritik an Konsumkritik](/informationen/artikel/konsumkritikkritik/)<br>
  Hier erklären wir, was es mit *Konsumkritik* auf sich hat,
  wo Konsumkritik sinnvoll ist,
  in welchen Fällen sie schädlich ist
  und warum Konsumkritik nicht wesentlich
  zur Lösung der Klimakrise beiträgt.

# Multimediale Inhalte

* [Bodenbanner](/bodenbanner/) (in Arbeit)
* Flyer (demnächst)
* [Musik-Playlists](/playlists/)
* [Podcasts vom Dezember 2020](/podcast/)
* [Videologs vom Dezember 2020](/videolog/)
