---
layout: page
title: Programm
permalink: /programm/
nav_order: 50
has_children: false
has_toc: false
keywords:
  - Augsburg
  - Augsburger
  - Camp
  - Demonstration
  - Fahrrad
  - FFF
  - Fridays for Future
  - Klimacamp
  - Klimagerechtigkeit
  - Klimaschutz
  - Lokalpolitik
  - Programm
  - Protest
  - Rathaus
  - Rathausplatz
  - Streik
  - Versammlung
  - Vortrag
  - Workshop
---

# Anstehendes Programm

<details>
  <summary>Allgemeine Informationen ein-/ausblenden</summary>
  <div>
    Hier findest du unser Workshop- und Vortragsprogramm im Klimacamp.
    Daneben gibt es rund um die Uhr die Möglichkeit,
    mit uns ins Gespräch zu kommen und Aktionen zu planen.
  </div>
  <div>
    Nicht das gesamte Programm wird über diese Webseite angekündigt.
    Wenn du Zeit hast und in der Nähe des Camps bist,
    schau doch an unserem Aushang vorbei.
    Eine ganze Reihe von Workshops
    wird mit relativ kurzer Vorlaufzeit spontan organisiert.
    Andere Aktionen sollen aus strategischen Gründen
    eine Überraschung bleiben.
  </div>
  <div style="font-style: italic;">
    Wir gestalten das Camp alle gemeinsam.
    Deine Workshopidee ist willkommen!
  </div>
  <div>
    ℹ️ Aktuelle Informationen auch über Telegram: <a href="https://t.me/klimacamp_augsburg">https://t.me/klimacamp_augsburg</a>
  </div>
  <hr>
</details>


## Sonntag 13.11.2022

In München findet an diesem Tag eine Demonstration
gegen das bayerische Polizeiaufgabengesetz statt.
Die weitreichenden Repressionsmaßnahmen dieses Gesetzes
waren einst mit islamistischen Gefährdern und Terrorismus
begründet worden,
werden inzwischen aber auch regelmäßig gegen Menschen eingesetzt,
die einfach nur unbequem sind –
friedliche Klimagerechtigkeitsaktivist\*innen und Asylbewerber.
An mehreren Stellen musste das Gesetz bereits aufgrund
seiner Verfassungswidrigkeit zurückgerollt werden.
Vor dem bayerischen Verfassungsgerichtshof
liegen zahlreiche weitere Klagen gegen das Polizeiaufgabengesetz vor.

**Ort:** Wettersteinplatz, München
<br>
**Zeit:** 14:00 Uhr
<br>
**Link zur Veranstaltung:** [https://www.nopagby.de/2022/11/11/demo-fuer-die-abschaffung-des-praeventivgewahrsam/](https://www.nopagby.de/2022/11/11/demo-fuer-die-abschaffung-des-praeventivgewahrsam/)


## Freitag 25.11.2022

### 18 Uhr Critical Mass

Wie gewohnt wird es am letzten Freitag des Monats um 18 Uhr
wieder eine Critical Mass am Rathausplatz geben.
Für weitere Eindrücke und Erfahrungsberichte gibt es
in [unserem Tagebuch für 2022](/tagebuch/) mehrere Berichte
über den Ablauf von vergangenen Critical Masses.

Link zur Veranstaltung: [https://criticalmass-augsburg.de/termine](https://criticalmass-augsburg.de/termine)

**Ort:** Rathausplatz
<br>
**Zeit:** 18:00 Uhr


### <span style="background-color: #80FF80">ab etwa 19:30 Tee, Kinderpunsch und Crêpes am Klimacamp</span>

![Das Bild zeigt eine krakelige aber kunstvolle Zeichnung
einer zu einer Teekanne verformten Erdkugel.
Rund herum stehen Datum und Zeit sowie die Stichpunkte
„Tee“, „Punsch“, „Crepes“ und „Freunde“
und die Sätze „Tee Plausch im Klimacamp“ und
„komm nicht zu spät zum Tee!“](/pages/material/images/sharepics/2022-11-25_teerunde.png)

Wir wollen uns zum gemütlichen Kennenlernen,
Austausch und direkten Kontakt bei warmen Getränken und Crêpes treffen.
Die Veranstaltung ist als Kennenlernveranstaltung ausgelegt.
Wer mehr über die Klimagerechtigkeitsbewegung erfahren möchte und
verschiedene Klimagerechtigkeitsaktivist\*innen kennen lernen möchte,
hat hier die Gelegenheit dazu.
Auch die Teilnehmer\*innen der Critical Mass sind eingeladen,
den Abend mit uns am Klimacamp ausklingen zu lassen.

**Ort:** Klimacamp
<br>
**Zeit:** 19:30 Uhr


## Frühere Veranstaltungen

Verweise auf zurückliegende Veranstaltungen
stehen im *[Tagebuch](/tagebuch/)*
des aktuellen Jahres verschoben.

Für das letztes Jahr siehe auch *[Tagebuch 2021](/tagebuch/2021/)*
und für die Einträge des Gründungsjahres 2020
*[Tagebuch 2020](/tagebuch/2020/)*.


---

👋 Herzliche Einladung zu allen Veranstaltungen, insbesondere an alle,
die mit dem Gedanken spielen, klimaaktivistisch aktiv zu werden. Ihr
seid willkommen und trefft im Klimacamp und in den anderen Augsburger
Strukturen auf motivierte Menschen, mit denen Veränderung möglich wird!

---

