---
layout: page
title: Weltklimaberichte
permalink: /weltklimaberichte/
has_children: true
nav_order: 80
has_toc: false
keywords:
  - AR6
  - CO₂
  - Erderhitzung
  - Intergovernmental Panel on Climate Change
  - IPCC
  - IPCC-Report
  - Klimaerwärmung
  - Klimagerechtigkeit
  - Klimaschutz
  - Klimawandel
  - Restbudget
  - Sechster Sachstandsbericht
  - SR15
  - SPM
  - Summary for Policymakers
  - Technical Summary
  - TS
  - Weltklimarat
  - Wissenschaft
  - 1,5°C-Grenze
  - 2°C-Grenze
---

# Weltklimaberichte

Der Weltklimarat (IPCC) arbeitet seit seiner Gründung daran,
den weltweiten wissenschaftlichen Kenntnisstand zum Weltklima
und die es beeinflussenden Faktoren
in Berichten zusammen zu fassen.
Diese Berichte sind frei zugänglich.


## Downloadlinks aktueller Weltklimaberichte

(Stand: November 2022)

| Bericht      | Jahr | Summary for Policymakers | Technical Summary | Volle Version |
|:-------------|------|:-------------------------|:------------------|:--------------|
| AR5          | 2014 | [AR5-SPM-EN](https://archive.ipcc.ch/pdf/assessment-report/ar5/syr/AR5_SYR_FINAL_SPM.pdf)<br>Deutsche Übersetzung: [AR5-SPM-DE](https://archive.ipcc.ch/pdf/reports-nonUN-translations/deutch/IPCC-AR5_SYR_SPM_deutsch.pdf) | --- | [AR5-FULL-EN](https://archive.ipcc.ch/pdf/assessment-report/ar5/syr/SYR_AR5_FINAL_full_wcover.pdf)<br>Deutsche Übersetzung: [AR5-FULL-DE](https://archive.ipcc.ch/pdf/reports-nonUN-translations/deutch/IPCC-AR5_SYR_barrierefrei.pdf) |
| SR15         | 2018 | [SR15-SPM-EN](https://www.ipcc.ch/site/assets/uploads/sites/2/2019/05/SR15_SPM_version_report_LR.pdf)<br>Deutsche Übersetzung: [SR15-SPM-DE](https://www.de-ipcc.de/media/content/SR1.5-SPM_de_barrierefrei.pdf) | [SR15-TS-EN](https://www.ipcc.ch/site/assets/uploads/sites/2/2019/05/SR15_TS_High_Res.pdf) | [SR15-FULL-EN](https://www.ipcc.ch/site/assets/uploads/sites/2/2019/06/SR15_Full_Report_High_Res.pdf) |
| SRCCL        | 2019 | [SRCCL-SPM-EN](https://www.ipcc.ch/site/assets/uploads/sites/4/2020/02/SPM_Updated-Jan20.pdf)<br>Deutsche Übersetzung: [SRCCL-SPM-DE](https://www.ipcc.ch/site/assets/uploads/sites/4/2020/08/SRCCL-SPM_de_barrierefrei.pdf) | [SRCCL-TS-EN](https://www.ipcc.ch/site/assets/uploads/sites/4/2020/07/03_Technical-Summary-TS_V2.pdf) | [SRCCL-FULL-EN](https://www.ipcc.ch/site/assets/uploads/sites/4/2021/07/210714-IPCCJ7230-SRCCL-Complete-BOOK-HRES.pdf) |
| SROCC        | 2019 | [SROCC-SPM-EN](https://www.ipcc.ch/site/assets/uploads/sites/3/2019/11/03_SROCC_SPM_FINAL.pdf)<br>Deutsche Übersetzung: [SROCC-SPM-DE](https://www.de-ipcc.de/media/content/SROCC-SPM_de_barrierefrei.pdf) | [SROCC-TS-EN](https://www.ipcc.ch/site/assets/uploads/sites/3/2019/11/04_SROCC_TS_FINAL.pdf) | [SROCC-FULL-EN](https://www.ipcc.ch/site/assets/uploads/sites/3/2019/12/SROCC_FullReport_FINAL.pdf) |
| [AR6 – Teil 1](https://www.ipcc.ch/report/ar6/wg1/) | 2021 | [AR6-WGI-SPM-EN](https://www.ipcc.ch/report/ar6/wg1/downloads/report/IPCC_AR6_WGI_SPM.pdf)<br>Deutsche Übersetzung: [AR6-WGI-SPM-DE](https://www.de-ipcc.de/media/content/AR6-WGI-SPM_deutsch_barrierefrei.pdf) | [AR6-WGI-TS-EN](https://www.ipcc.ch/report/ar6/wg1/downloads/report/IPCC_AR6_WGI_TS.pdf) | [AR6-WGI-FULL-EN¹](https://www.ipcc.ch/report/ar6/wg1/downloads/report/IPCC_AR6_WGI_Full_Report.pdf) |
| [AR6 – Teil 2](https://www.ipcc.ch/report/ar6/wg2/) | 2022 | [AR6-WGII-SPM-EN](https://www.ipcc.ch/report/ar6/wg2/downloads/report/IPCC_AR6_WGII_SummaryForPolicymakers.pdf)² | [AR6-WGII-TS-EN](https://www.ipcc.ch/report/ar6/wg2/downloads/report/IPCC_AR6_WGII_TechnicalSummary.pdf) | [AR6-WGII-FULL-EN](https://report.ipcc.ch/ar6/wg2/IPCC_AR6_WGII_FullReport.pdf) |
| [AR6 – Teil 3](https://www.ipcc.ch/report/ar6/wg3/) | 2022 | [AR6-WGIII-SPM-EN](https://www.ipcc.ch/report/ar6/wg3/downloads/report/IPCC_AR6_WGIII_SPM.pdf)² | [AR6-WGIII-TS-EN¹](https://www.ipcc.ch/report/ar6/wg3/downloads/report/IPCC_AR6_WGIII_TS.pdf) | [AR6-WGIII-FULL-EN¹](https://report.ipcc.ch/ar6/wg3/IPCC_AR6_WGIII_Full_Report.pdf) |

<small>1 Final editing in progress. Es kann sein,
dass hier noch kleinere Änderungen vorgenommen werden
und als Folge davon Bilder oder Kapitel auf eine andere Seite
verrutschen.</small>
<br>
<small>2 Übersetzungen der Zusammenfassungen
für politische Entscheidungsträger
ins Deutsche sollen noch veröffentlicht werden.
Übersetzungen der Hauptaussagen des Berichts
finden sich bereits jetzt auf
<a href="https://www.de-ipcc.de/270.php">https://www.de-ipcc.de/270.php</a>.</small>

Diese tabellarische Übersicht soll einen schnellen Zugang zu den aktuellsten Klimaberichten bieten.
<br>
Die Links verweisen direkt auf den jeweils offiziellen Download von der [Webseite des IPCC](https://www.ipcc.ch/)
beziehungsweise für deutsche Übersetzungen auf die [Deutsche IPCC-Koordinierungsstelle](https://www.de-ipcc.de/).
*(Die Links brechen leider manchmal,
wenn die Dokumente innerhalb der Webseite des IPCC
an einen anderen Ort verschoben werden.
Sobald uns das auffällt, suchen wir die Dokumente neu heraus
und passen die Links auf dieser Seite an.)*

Aktuelle Diskussionen möchten wir sachlich und
auf dem Boden wissenschaftlicher Erkenntnisse führen können.
<br>
Wir hoffen, dass jede\*r für das Thema relevante\*r Politiker\*in
zumindest die „Zusammenfassung für politische Entscheidungsträger“
(engl. „Summary for Policymakers“, abgekürzt auch SPM)
der aktuellen Berichte gelesen hat.
Gespräche mit politischen Beratern und Informationen aus den Medien
kann ein eigenes Lesen der (Zusammenfassungen der) Berichte
nicht vollständig ersetzen.
Jede Art von Medium nimmt immer noch eine Vorselektierung
und Fokussierung auf einzelne Aspekte vor.
Um ein möglichst umfängliches Bild der Situation zu erhalten,
muss man die Berichte oder ihre Zusammenfassungen selbst lesen.


## Berichte / Themen

### AR5 – Der fünfte Sachstandsbericht (Assessment Report) zum Weltklima
Der Bericht fasst den weltweiten Kenntnisstand seiner Zeit zusammen.

### SR15 – Special Report on Global Warming of 1.5 °C
Der Bericht führt detailliert die weitreichenderen Folgen
einer Erwärmung um 2°C gegenüber einer Erwärmung von 1,5°C aus
und erläutert, was zu tun wäre, um die Erwärmung auf 1,5°C zu begrenzen.

### SRCCL – Special Report on Climate Change and Land
Der Bericht konzentiert sich auf Folgen der Klimaerwärmung
für die Landmassen und deren Ökosysteme.

### SROCC – Special Report on the Ocean and Cryosphere in a Changing Climate
Der Bericht konzentiert sich auf die Folgen der Klimaerwärmung
für Ozeane und Eisflächen.

### AR6 – Der sechste Sachstandsbericht (Assessment Report) zum Weltklima
Dieser Bericht stellt eine Neuauflage des letzten Sachstandsberichts dar.
Auch die Ergebnisse der in der Zwischenzeit erschienenen Sonderberichte
sind in diesen eingeflossen.
Der Bericht besteht aus drei Teilen,
die nach und nach über die Jahre 2021 und 2022 veröffentlicht wurden.

* Teil 1 (WGI) fasst die physikalischen Grundlagen der Klimaerwärmung zusammen.
  Er beinhaltet auch die aktuellen Restbudgets an Treibhausgasen,
  die die Menschheit noch ausstoßen kann, um eine gewisse Temperaturgrenze
  mit einer gewissen Wahrscheinlichkeit einzuhalten.<br>
  Teil 1 wurde bereits 2021 veröffentlicht.<br>
  *Die Klimauhren in unserem
  [Artikel über Klimauhren](/informationen/artikel/klimauhren/)
  benutzen die in diesem Bericht genannten Restbudgets.*
* Teil 2 (WGII) beschreibt die Auswirkungen und notwendigen Anpassungen
  an verschiedene mögliche Grade der Erwärmung.
  Weite Teile des Berichts beschäftigen sich mit Auswirkungen,
  die bereits heute das Leben vieler Menschen betreffen.<br>
  Teil 2 wurde am 28. Februar 2022 veröffentlicht.
* Teil 3 (WGIII) beschreibt Maßnahmen zur Minderung der Klimakrise.<br>
  Teil 3 erschien am 04. April 2022.<br>
  Bereits im Jahr 2021 war eine vorläufige Fassung von Teil 3
  aus Sorge um eine Verwässerung
  nach Abschluss der wissenschaftlichen Arbeit daran
  auf [https://scientistrebellion.com](https://scientistrebellion.com/we-leaked-the-upcoming-ipcc-report/)
  geleakt worden.
  Eine Verwässerung fand dann tatsächlich auch statt.³


## Zum Aufbau

Die Berichte des IPCC bestehen üblicherweise aus verschiedenen Teilen.

 * Die **Summary for Policymakers (SPM)**,
   auf Deutsch die „**Zusammenfassung für politische Entscheidungsträger**“,
   ist die kürzeste und reich bebilderte Zusammenfassung des Berichts.
   Gewöhnlich umfasst sie einige dutzend Seiten.<br>
   Die SPMs unterliegen zu einem gewissen Grad politischer Einflussnahme.³
   Wissenschaftler haben ein Vetorecht und können Falschdarstellungen
   verhindern, nicht aber Verwässerungen.

 * Die **Technical Summary (TS)**,
   auf Deutsch „technische Zusammenfassung“,
   ist eine deutlich detailliertere Zusammenfassung.
   Sie enthält mehr Informationen und
   erklärt komplexe Mechanismen besser.

Manchmal ist der Bericht ergänzt um zusätzliches Material, Supplementary Material (SM), und Anhänge.
Die Anhänge können Tabellen, einen Glossar zur Erklärung der verwendeten Fachbegriffe und vieles mehr enthalten.

<small>
  3 Quelle:
  <a href="https://www.spiegel.de/wissenschaft/mensch/neuer-weltklimabericht-wie-saudi-arabien-den-ipcc-report-verwaesserte-a-4cce6dd2-5271-48c9-a997-9087abda9940">https://www.spiegel.de/wissenschaft/mensch/neuer-weltklimabericht-wie-saudi-arabien-den-ipcc-report-verwaesserte-a-4cce6dd2-5271-48c9-a997-9087abda9940</a>
</small>
