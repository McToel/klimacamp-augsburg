---
layout: page
title: "Lokalpolitik"
has_children: true
has_toc: false
permalink: /lokalpolitik/
nav_order: 100
---

# [Dokumente](/lokalpolitik/dokumente/)

KlimaKom-Studie?
Vertrag zum Radentscheid?
Es gibt viele Dokumente,
die in der Berichterstattung erwähnt werden.
Hier zeigen wir euch, wo man diese findet.

# [Stadtratsprotokolle](/protokolle/)

Hier findet ihr unsere Mitschriften aus den Sitzungen des Stadtrats.

# [Ausreden](/lokalpolitik/ausreden/)
