---
layout: page
title: Kontakt
permalink: /kontakt/
nav_order: 120
---

# Kontakt
Bei allen Anliegen (inkl. **Presseanfragen**) zum Klimacamp schreibt bitte an [klimacamp@systemli.org](mailto:klimacamp@systemli.org).

Direkter Kontakt zur rechtlich verantwortlichen Person: [+49 176 95110311](tel:+4917695110311) (Ingo Blechschmidt, Arberstr. 5, 86179 Augsburg, [iblech@web.de](mailto:iblech@web.de)). 

Weiterer Kontakt: [+49 161 6696097](tel:+491616696097) (Alexander Mai, [alex.mai@posteo.net](mailto:alex.mai@posteo.net)).


