---
layout: page
title:  "21.08.2020: Wald statt Asphalt – Aktion für den Erhalt der Schutzgebiete Dannenröder Wald und Lechtal"
date:   2020-08-21 07:00:00 +0200
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
nav_order: 13
---

*Pressemitteilung vom Augsburger Klimacamp am 21. August 2020*

# „Wald statt Asphalt“ – Aktion für den Erhalt der Schutzgebiete Dannenröder Wald und Lechtal

Am heutigen Freitagnachmittag demonstrieren Aktivist\*innen des
Augsburger Klimacamps auf der Karlstraße für den Erhalt der
Schutzgebiete Dannenröder Wald und Lechtal im Osten Augsburgs. Durch
beide Gebiete sollen neue Autobahnen gebaut werden. Die Demonstration
findet im Rahmen eines deutschlandweiten Aktionstags zum Erhalt des
Dannenröder Walds statt.

Seit 40 Jahren setzen sich Bürger\*inneninitiativen gegen den geplanten
Bau der A49 durch den Dannenröder Wald ein. Der Dannenröder Wald ist
etwa 300 Jahre alt und einer der wenigen Laub- und Mischwälder
Deutschlands. Wie beim Hambacher Forst entschlossen sich
Klimagerechtigkeitsaktivist\*innen als letztes Mittel zur Bewahrung des
Walds im Oktober 2019, den Wald friedlich zu besetzen.

Auch das lokale Lechtal-Schutzgebiet im Osten Augsburgs soll in Teilen
zerstört werden, durch den Bau einer autobahnähnlichen Osttangente um
Augsburg. Gegen diesen verkehrspolitischen Irrsinn setzt sich seit 2015
die Bürger\*inneninitiative „Aktionsbündnis keine Osttangente“ zur Wehr.

„Diese Bauprojekte belasten massiv Klima, Umwelt und Gesundheit und sind
völlig aus der Zeit gefallen“, erklärt Lennart Dröge (21) sein
Engagement. „Wir benötigen dringend eine für alle Bürger\*innen gerechte
Mobilitätswende, die weder Natur noch Mensch in den Hintergrund stellt.
Die Finanzmittel sollten stattdessen in den Ausbau und die Vergünstigung
des öffentlichen Nahverkehrs investiert werden.“

„In Augsburg entschlossen wir uns, auf der Karlstraße zu demonstrieren.
Die vierspurige Karlstraße durch das Herz Augsburgs ist ein Symbolbild
für Augsburgs antiquierte Verkehrspolitik, welche seit Jahrzehnten des
Auto einseitig bevorzugt und zahlreiche Unorte schafft.“, bekräftigt
Sarah Bauer (16).

Die Protestaktion findet an der Ecke Karolinenstraße/Karlstraße von
17:00 Uhr bis 17:30 Uhr statt. Wir laden alle interessierten
Medienvertreter\*innen herzlich zu der Aktion ein. Im Anhang gibt es
Hintergrundinformationen zum Dannenröder Wald.
