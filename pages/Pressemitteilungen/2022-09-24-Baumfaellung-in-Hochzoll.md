---
layout: page
title:  "24.09.2022: Klimacamp Augsburg lässt Baumfällungen in Hochzoll zu"
date:   2022-09-24 08:30:00 +0100
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
permalink: /pressemitteilungen/2022-09-24-baumfaellung-in-hochzoll/
nav_order: 324
---

*Pressemitteilung vom Augsburger Klimacamp am 24.9.2022*

# Klimacamp Augsburg lässt Baumfällungen in Hochzoll zu

Die Aktivist\*innen vom Augsburger Klimacamp werden bei den angekündigten
Fällungen von bis zu 80 Bäumen in Hochzoll nicht intervenieren. Dies
gaben die Klimacamper\*innen in einer Mitteilung am Samstag bekannt,
nachdem zuvor die Augsburger Allgemeine über die Fällungspläne
berichtete [1]. Die DB Netz AG behauptet, dass die Fällungen aufgrund
sich in den Bäumen ausbreitendem Eschentriebsterben notwendig seien.

„Pilzkrankheiten in Bäumen, wie das Eschentriebsterben, werden gerade
durch die Erdaufheizung immer unaufhaltsamere Schneisen der Verwüstung
durch unsere Wälder und Grünbereiche ziehen“, so Charlotte Lauter (19)
vom Klimacamp. Wie etwa das Ministerium für Klimaschutz, Umwelt,
Landwirtschaft, Natur- und Verbraucherschutz des Landes NRW bestätigt,
beschleunigen ungewöhnlich warme Winter das Pilzwachstum in Rinde und
Holz. Nasse Standorte und eine Zunahme der Sommerniederschläge fördern
außerdem die Ausbreitung von Pilzkrankheiten [2]. So gehört das
Eschentriebsterben zu denjenigen Folgen der Erdaufheizung, von denen
Augsburger\*innen aktuell betroffen sind.

Begrüßenswert sei, dass damit der störungsfreie Betrieb des
Schienennetzes ermöglicht werde, dessen Rolle im Zuge der
Klimakatastrophe von zentraler Bedeutung ist. „Es ist zwar schade, dass
die Bäume wohl nicht mehr zu retten sind“, bedauert Elias Wasner (22).
„Aber es ist auch schön zu sehen, dass Baumfällungen nachvollziehbar und
für das Gemeinwohl durchgeführt werden. Sonst kennen wir Baumfällungen
eigentlich nur von aus der Zeit gefallenen Expansionsplänen von
Industrieflächen oder Autobahnen.“ Wasner ergänzt, dass eine Anfrage bei
der DB Netz AG, ob sie Ersatzpflanzungsexperimente vorzunehmen gedenke,
bislang unbeantwortet blieb.


NEUE WENDUNG BEI DEN UMSTRITTENEN BAHNHOFSVORPLATZFÄLLUNGEN

„Nicht zulassen werden wir dagegen die umstrittene Baumfällungsfantasie
des CSU-geführten Baureferats am Bahnhofsvorplatz“, so Wasner weiter.
Dieser Skandal erreichte sogar überregionale Medien [3]. Die Stadt hatte
2015 einen Architekturwettbewerb zur Umgestaltung des Platzes ausgelobt.
„Weitsichtige Klimapolitik? Keine Spur!“, kommentiert Wasner und
bezeichnet es als „Versäumnis“, dass die Stadt es bei der
Wettbewerbsausschreibung unterließ, den Baumerhalt als offensichtliches
Plankriterium festzulegen (BSV/15/02778). Der Siegerentwurf von
Architekturwettbewerb Loidl sah aber trotzdem einen nahezu vollständigen
Erhalt der Bäume vor (BSV/15/03789). Das CSU-geführte Baureferat
steuerte federführend die im Nachgang solcher Wettbewerbe übliche
Detailausgestaltung des Plans. Erst dadurch kamen die umstrittenen
Baumfällungen auf den Tisch.

Lauter betont, dass sie und ihre Kolleg\*innen diese und andere
Fällungspläne gesunder Bäume genau beobachten und bei Bedarf durch
juristische und aktivistische Mittel verhindern, sollte es keine
Einsicht seitens des Baureferats geben. „Wir Klimaaktivist\*innen sind es
gewohnt, bei Bedarf zur unfreiwilligen Feuerwehr zu werden, und haben
für kurzfristig nötige Kletterinventionen im gesamten Stadtgebiet Depots
mit Klettermaterialien angelegt“. Ein Anfang August von den
Klimacamper\*innen durchgeführter Kletterworkshop am Bahnhofsvorplatz,
der das Ziel hatte, Anwohner\*innen das nötige Handwerkszeug zum
Baumschutz beizubringen, wurde von einem Aufgebot der Abteilung
‚Staatsschutz‘ der Augsburger Polizei verhindert [4,5].

[1] [https://www.augsburger-allgemeine.de/augsburg/augsburg-entlang-der-bahnstrecke-in-hochzoll-muessen-bis-zu-80-baeume-gefaellt-werden-id64017921.html](https://www.augsburger-allgemeine.de/augsburg/augsburg-entlang-der-bahnstrecke-in-hochzoll-muessen-bis-zu-80-baeume-gefaellt-werden-id64017921.html)
<br>
[2] [https://www.wald-und-holz.nrw.de/fileadmin/Publikationen/Broschueren/Waldzustandsbericht_2015_Langfassung.pdf](https://www.wald-und-holz.nrw.de/fileadmin/Publikationen/Broschueren/Waldzustandsbericht_2015_Langfassung.pdf),
Seiten 44–46
<br>
[3] [https://www.br.de/nachrichten/bayern/schattenlos-streit-um-baumfaellungen-vor-augsburger-hauptbahnhof,TCr2opE](https://www.br.de/nachrichten/bayern/schattenlos-streit-um-baumfaellungen-vor-augsburger-hauptbahnhof,TCr2opE)
<br>
[4] [https://www.augsburger-allgemeine.de/augsburg/augsburg-baeume-am-bahnhofsvorplatz-sollen-gefaellt-werden-kritik-vom-klimacamp-id63507481.html](https://www.augsburger-allgemeine.de/augsburg/augsburg-baeume-am-bahnhofsvorplatz-sollen-gefaellt-werden-kritik-vom-klimacamp-id63507481.html)
<br>
[5] [https://www.jungewelt.de/artikel/432407.polizeiaufgabengesetz-hausdurchsuchungen-sind-bei-uns-auf-der-tagesordnung.html](https://www.jungewelt.de/artikel/432407.polizeiaufgabengesetz-hausdurchsuchungen-sind-bei-uns-auf-der-tagesordnung.html)


## *Nachtrag*

Die *Baumallianz Augsburg* geht in einer
[eigenen Stellungnahme](https://baumallianz-augsburg.de/archive/2750)
noch mehr ins Detail.
Darin beschreibt sie,
dass die erkrankten Eschen leider gefällt werden müssen,
besteht aber darauf,
dass ein radikaler Kahlschlag ausbleibt und die vor Ort befindlichen
Spitzahorne, Ulmen sowie eine schöne Linde erhalten bleiben.
