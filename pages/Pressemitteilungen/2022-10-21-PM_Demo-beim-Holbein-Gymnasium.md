---
layout: page
title: "21.10.2022: Nach Untätigkeit der Stadt: Demo in Hallstraße"
date: 2022-10-21 23:00:00 +0100
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
permalink: /pressemitteilungen/2022-10-21-demo-beim-holbein-gymnasium/
nav_order: 344
---

*Pressemitteilung am 21.10.2022*

# Nach Untätigkeit der Stadt: Verkehrswendeaktivist\*innen lassen wieder Straße beim Holbein-Gymnasium sperren

Vergangenen Sommer nutzten immer wieder unabhängige
Verkehrswendeaktivist\*innen wiederholt das Versammlungsrecht, um die
Straße zwischen den beiden Gebäudeteilen des Holbein-Gymnasiums für die
dort oft schnell fahrenden Autos polizeilich sperren zu lassen. Die
letzte Aktion dieser Art ist nun mehr als einen Monat her -- man wollte
der Stadt auch Gelegenheit geben, sich erneut mit der von der
Schulfamilie vielfach gewünschten Verkehrsberuhigung zu befassen,
so Florian Lenz (21), einer der an der Aktion Beteiligten.
 
Nachdem es seit der letzten Demonstration am 16.9.2022 aber zu keiner
Verbesserung der Situation gekommen sei, setzten die Aktivist\*innen am
heutigen Freitag (21.10.) zur Zeit der zweiten Pause (11:20 bis 11:40 Uhr)
ihre Aktionsserie fort. Wie bei den früheren Sperrungen konnten die
Kinder den öffentlichen Raum zwischen den beiden Schulgebäuden für eine
ausgelassene Pause nutzen ohne Gefahr laufen zu müssen, von Autos
erfasst zu werden. Zuletzt ereignete sich ein derartiger Vorfall am
16.5.2022 [1].

Gelegenheit zum Beschluss geeigneter Maßnahmen zur Beruhigung der
angespannten Situation auf der Hallstraße gab es indes einige: Nicht nur
der Augsburger Stadtrat tagte, sondern auch mehrere relevante
Fachausschüsse [2].

Die Stadt gab zuletzt an, aus Sorge vor möglichen Klagen von
Anwohner\*innen [3] keinen Verkehrsberuhigungsversuch unternehmen zu
wollen. Ute Grathwohl (49) überzeugt das nicht: „Die Schulfamilie
wünscht sich eine Verkehrsberuhigung schon seit Jahrzehnten. Die
Situation ist akut. Hier könnte effektiv und kostengünstig sofort
gehandelt werden.“ Lenz ergänzt, dass die Stadt auf Kosten der
Schüler\*innen mit zweierlei Maß messe. Denn in Klima- und
Umweltschutzbelangen lasse sich die Stadt von möglichen Klageandrohungen
von ihrem Kurs, die Empfehlungen der von der Stadt in Auftrag gegebeben
KlimaKom-Studie in den Wind zu schlagen, nicht abbringen. Beispielhaft
führt Lenz die geplante Erhöhung der ÖPNV-Preise an, auf die die Stadt
einen großen Einfluss habe. Diese steht im Widerspruch mit den
Erkenntnissen aus der KlimaKom-Studie.

[1] https://www.klimacamp-augsburg.de/pressemitteilungen/2022-05-17-protest-nach-vorfall-in-schulstrasse/ (nicht zu verwechseln mit dem anderem Unfall ohne Fremdverschulden am
selben Tag)
<br>
[2] https://ratsinfo.augsburg.de/bi/si010.asp?MM=10&YY=2022
<br>
[3] https://www.augsburger-allgemeine.de/augsburg/augsburg-nicht-nur-das-geld-fehlt-verkehrsberuhigung-der-hallstrasse-liegt-auf-eis-id63122111.html


## HINWEIS

Es kam in der Vergangenheit vereinzelt zu Zwischenfällen, als die
versammlungsanmeldenden Personen nicht erfahrene
Verkehrswendeaktivist\*innen, die der Polizei als rechtskundige Personen
bekannt sind, sondern Schüler\*innen des Gymnasiums waren: Die Straße
wurde in diesen Fällen erst verspätet, deutlich nach Pausenbeginn
gesperrt. Eine solche Verzögerung der Straßensperrung ist gefährlich, da
sich insbesondere kleine Kinder auf die Durchsage der Schulleitung
verlassen und von einer gesperrten Straße ausgehen. Aus diesem Grund
wurde die Versammlung am 21.10.2022 wieder von einer Aktivist\*in
angezeigt.
