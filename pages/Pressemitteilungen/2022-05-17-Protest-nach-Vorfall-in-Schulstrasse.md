---
layout: page
title:  "17.05.2022: Protest nach Vorfall in Schulstraße"
date:   2022-05-11 10:00:00 +0100
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
permalink: /pressemitteilungen/2022-05-17-protest-nach-vorfall-in-schulstrasse/
nav_order: 314
---

*Pressemitteilung von Augsburger Verkehrswendeaktivist\*innen am 17.5.2022*

# Nach Verkehrsunfall: Aktivist\*innen sperren als Notmaßnahme Hallstraße vor dem Holbein-Gymnasium

Nachdem ein Auto eine Schüler anfuhr: Aktivist\*innen sperren als
Notmaßnahme Hallstraße vor dem Holbein-Gymnasium und fordern autofreie
Zonen rund um Schulen – „Die Zeit des ungezügelten Autoverkehrs in
Schulstraßen ist vorbei“

Am gestrigen Montag (16.5.) wurde in der Hallstraße vor dem Augsburger
Holbein-Gymnasium ein Schüler von einem Auto angefahren. Als Notmaßnahme
ergriffen die unabhängigen Aktivist\*innen hinter dem Augsburger
Verkehrswendeplan am heutigen Dienstag (17.5.) das Versammlungsrecht, um
einen Modellversuch durchzusetzen: Für die Zeitdauer der ersten Pause
des Holbein-Gymnasiums (9:30 Uhr bis 9:50 Uhr) wird die Hallstraße durch
eine Demonstration autofrei.

Eine solche fand auch schon vergangenen Freitag (13.5.2022) statt,
damals als „fröhlicher Versuch einer gelebten und greifbaren
Verkehrsutopie“, so Organisator Florian Lenz (21). „Heute war es dagegen
eine Notmaßnahme als Mahnung, um weitere Unfälle zu verhindern und um
aufzuzeigen, dass eine Sperrung der Hallstraße für Autos möglich und
notwendig ist.“ Zwar sei jeder Verkehrsunfall einzigartig und Produkt
verschiedener Faktoren, doch trage Augsburgs autozentrierte
Verkehrsstruktur die größte Schuld. Diese begünstigte Unfälle
systematisch und erschwere es, unfallfrei von A nach zu B kommen.

„Die Zeit des ungezügelten Autoverkehrs in Schulstraßen ist vorbei“,
erklärt Ute Grathwohl (49), die sich beim Verkehrswendeplan insbesondere
zum Thema sichere Schulwege einbrachte. „Als Mutter ist mir dieses Thema
ein besonderes Herzensanliegen.“ Auch bei der Roten-Tor-Schule kam es
erst letzte Woche zu einem Unfall, bei dem ein Grundschüler vor der
Schule von einem Auto erfasst und verletzt wurde. Die Aktivist\*innen
kündigten an, fortan regelmäßig Schulstraßen mittels Versammlungsrecht
temporär autofrei umzugestalten. „Dazu ermächtigen wir auch die
Schüler\*innen selbst durch Demoanmelde-Workshops. Solange die Stadt
nicht die Mobilitätsfreiheit erhöht und einseitig das
Partikularinteresse Auto fördert, bleibt Verkehrswende Handarbeit“,
erklärt Grathwohl.

Sie bezieht sich damit auch auf die seit Jahren kontinuierlich
ansteigenden Autozahlen in Augsburg [1]. Vor allem die für
Fußgänger\*innen mit geringer Körpergröße - klassischerweise Kinder im
Kita- und Schulalter - besonders gefährlichen großen SUV sind in immer
größerer Zahl auf den Straßen unterwegs. „Hier wäre die Politik gefragt,
durch einen günstigen und ausgebauten öffentlichen Personennahverkehr
attraktive Alternativen zu schaffen.“ Die Stadt sei dabei in der
Pflicht, öffentlichkeitswirksam entsprechende Fördermittel bei Bund und
Land einzufordern, sowie übergangsweise von Augsburgs Unternehmen eine
Nahverkehrsabgabe zu erheben. „Das ist nur fair, schließlich profitieren
unsere Unternehmen von einer guten Mobilität ihrer Kund\*innen und
Beschäftigen.“

[1] [https://www.augsburg.de/fileadmin/user_upload/buergerservice_rathaus/rathaus/statisiken_und_geodaten/statistiken/2020Jahrbuch_Internet.pdf, Abschnitt 7.02](https://www.augsburg.de/fileadmin/user_upload/buergerservice_rathaus/rathaus/statisiken_und_geodaten/statistiken/2020Jahrbuch_Internet.pdf, Abschnitt 7.02)


ÜBER DEN VERKEHRSWENDEPLAN

Der Verkehrswendeplan ist der erste umfassende Konzeptentwurf für eine
Umgestaltung des öffentlichen Raums im Sinne einer Verkehrswende. Neben
fünf übergeordneten Zielen formuliert er zahlreiche konkrete
Diskussionsvorschläge für Augsburgs Straßenraum.

Ausführliche Homepage zum Verkehrswendeplan:<br>
[https://www.verkehrswende-augsburg.de](https://www.verkehrswende-augsburg.de)

Flyer zum Verkehrswendeplan (1. Auflage, Stand Mai 2022):<br>
[https://www.verkehrswende-augsburg.de/assets/Flyer%20Verkehrswendeplan%20Augsburg.pdf](https://www.verkehrswende-augsburg.de/assets/Flyer%20Verkehrswendeplan%20Augsburg.pdf)


ÜBER DAS KONZEPT DER NAHVERKEHRSABGABE
(Quelle: https://projektwerkstatt.de/index.php?domain_id=40&p=14668)

Bei der Nahverkehrsabgabe (versement transport) wird die französische
Taxe Versement de Transport (VT) als Vorbild genommen, die Kommunen ab
20.000 Einwohnern zweckgebunden zur ÖPNV-Finanzierung erheben können.
Das Steueraufkommen kann investiv und konsumtiv verwendet werden. Die
französische Nahverkehrsabgabe ist von Arbeitgebern mit mehr als zehn
Mitarbeitern und vom Einzelhandel als Nutznießer des ÖPNV-Angebots zu
entrichten. Die Nahverkehrsabgabe ist an die Lohnsumme gekoppelt und der
Steuer-Höchstsatz orientiert sich an der Einwohnerzahl der Kommune.

* Abgaben entweder pauschal für alle Gewerbetreibenden, die vom
  kostenlosen Personenverkehr profitieren (Handel, Dienstleister,
  Hotels, Gaststätten, Touristik usw.) oder speziell für die Anlieger an
  Linien, wenn Haltestellen z.B. an öffentlichen Einrichtungen,
  Geschäften und anderen Vielfachzielen eingerichtet werden. So wird in
  Frankreich der kostenlose Nahverkehr finanziert. Dafür nötig ist eine
  Ermächtigung für Kommunen, solche Nahverkehrsabgaben erheben zu
  können.

* Integration bisheriger Kund\*innen- und Sonderlinien in die
  Freifahrnetze. So bringen z.B. im ländlichen Bereich Supermärkte
  ihre Kund\*innen 1-2x pro Woche von den Dörfern zu ihrem Laden.
  Solche Linien können in den Nulltarifs-Fahrplan integriert und der
  Handel an den Kosten beteiligt werden.
