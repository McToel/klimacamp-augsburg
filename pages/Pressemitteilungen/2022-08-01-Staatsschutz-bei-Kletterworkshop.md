---
layout: page
title:  "01.08.2022: Staatsschutz bei Kletterworkshop"
date:   2022-08-01 23:59:00 +0100
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
permalink: /pressemitteilungen/2022-08-01-staatsschutz-bei-kletterworkshop/
nav_order: 319
---

*Pressemitteilung vom Augsburger Klimacamp am 1.8.2022*

# Polizeiaufgebot bei Kletterworkshop vom Augsburger Klimacamp: Fällen erlaubt, Beklettern verboten

![Einsatzkräfte vom Staatsschutz verhindern Kletterworkshop vom Augsburger
Klimacamp](/pages/Pressemitteilungen/2022-08-01-Staatsschutz-bei-Kletterworkshop-1.jpeg)

![Damit hatte das Klimacamp für den Workshop geworben](/pages/Pressemitteilungen/2022-08-01-Staatsschutz-bei-Kletterworkshop-2.jpeg)

Am gestrigen Montag (1.8.2022) verhinderte am frühen Abend ein Aufgebot
der Polizei einen Kletterworkshop vom Augsburger Klimacamp. Wie die
Aktivist\*innen vergangene Woche ankündigten, hätte dieser am
Bahnhofsvorplatz stattfinden sollen. Das Ziel: Anwohner\*innen und
weiteren Bürger\*innen ermöglichen, im Notfall die Bäume zu ihrem Schutz
zu besetzen. Hintergrund dieser Vorbereitung sind die umstrittenen [Pläne der
Stadt](https://www.br.de/nachrichten/bayern/schattenlos-streit-um-baumfaellungen-vor-augsburger-hauptbahnhof,TCr2opE),
die dortigen Bäume zur Umsetzung eines Umgestaltungsvorschlags aus dem
Jahr 2015 zu fällen.


## Auch Kräfte der Abteilung "Staatsschutz" der Augsburger Polizei vor Ort

Ein Aufgebot aus etwa 10 Polizeibeamt\*innen samt Einsatzkräften der
Abteilung "Staatsschutz" der Augsburger Polizei verhinderte den
Workshop, zu dem sich etwa 15 Interessenten einfanden. "Die Polizei
bestätigte vor Ort unsere Rechtseinschätzung, dass das Beklettern weder
Ordnungswidrigkeit noch Straftat darstellt", so Charlotte Lauter (19)
vom Klimacamp. Lauter zu Folge gab der Einsatzleiter an, den
Kletterworkshop "zur Gefahrenabwehr nach Polizeiaufgabengesetz" zu
verhindern, primär mit Blick auf etwaige Baumschädigungen und sekundär
mit Blick auf potenzielle Verletzungen. "Fällen erlaubt, Beklettern
verboten", interpretiert Lauter das Aufgebot. "Ist bloßes Beklettern
wirklich eine von der Polizei abzuwehrende Gefahr? Der Widerspruch –
Klettersteige und Risikosportarten sind in Deutschland erlaubt – blieb
auch den Beamt\*innen vor Ort nicht verborgen, sie verwiesen jedoch auf
eindeutige Anweisungen von weiter oben", so Lauter. Die Aktivist\*innen
kritisieren, dass sich die Polizei trotz Überstunden und Personalmangel
für diese Aktion einspannen ließ.

Die Abteilung "Staatsschutz" der Augsburger Polizei machte zuletzt
Schlagzeilen, als bekannt wurde, dass sie den Klimaaktivisten und
früheren Bundestagskandidat Alexander Mai hausdurchsuchte, nachdem
dieser auf Facebook unter seinem vollen Namen [einen Link zu einem
Zeitungsfoto des "Pimmelgate"-Skandals
einstellte](https://www.pimmelgate-süd.de/) sowie als die
Abteilung "Staatsschutz" [ins Kinderzimmer der damals 16-Jährigen Janika
Pondorf eindrang](https://www.pimmelgate-süd.de/kreide/).


## Platz jahrelang vernachlässigt, nun unter Beobachtung – Ersatzpflanzungsexperimente riskant

Die Aktivist\*innen des Augsburger Klimacamps teilen die Einschätzung von
anderen Stimmen aus der Zivilgesellschaft, dass die Stadt den
Bahnhofsvorplatz über Jahre vernachlässigte. Aktuell sei es eine
ungepflegte Fläche und Lagerplatz für Schrottfahrräder, so Lauter. Ihre
Forderung: eine ansprechende Bepflanzung und Sitzgelegenheiten, sodass
der Platz zu einem schönen Verweilort für Menschen werde. Auf die von
der Stadt ins Spiel gebrachten Ersatzplanzungsexperimente möchte sich
Lauter nicht verlassen und bezeichnet sie als "riskant". Die
Klimacamper\*innen schließen sich vielmehr der Einschätzung der
Expert\*innen der Augsburger Baum-Allianz an, dass der aktuelle
Baumbestand gepflegt und beibehalten werden sollte.

"Nach außen hin wird die Stadt nicht müde, den Wunsch nach mehr
Bürgerbeteilung zu beteuern", so Lauter. "Offenbar ist damit aber nur
passive Beteiligung gemeint."


## Auch Verfahren gegen Baum-Allianz eingeleitet

Neben Vertreter\*innen des Klimacamps waren auch Expert\*innen der Augsburger
Baum-Allianz vor Ort, die den "Planungsdinosaurier" der Stadt [als erstes
öffentlich machten](https://baumallianz-augsburg.de/archive/2571). Um über den
drohenden Kahlschlag zu informieren, hatten diese kleine Hinweisschilder an den
Bäumen befestigt. Da die Kontaktdaten einer Ansprechperson nur auf ihrer
Webseite, nicht allerdings auf den Schildern selbst verzeichnet waren, wurde
ein Verfahren gegen die Baum-Allianz eingeleitet. Es droht nun ein Bußgeld im
mittleren dreistelligen Bereich.


## Mangelndes Gespür für Größenordnungen

Im Hinblick darauf, dass die Klimakrise und seine schon jetzt erheblich
spürbaren Folgen eine große Gefahr für die Gesellschaft darstellt,
resümiert Lauter: "Die Polizei verwendete den Begriff 'Gefahrenabwehr'.
Eine Baumkletterei als Gefahr zu bezeichnen, die ähnlich wie bei
Kreidemalereien den Staatsschutz auf den Plan ruft, zeichnet ein gutes
Bild des noch nicht vorhandenen Bewusstseins für die Schwere der
Klimakrise."
