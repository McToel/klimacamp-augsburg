---
layout: page
title: "20.10.2022: Klimacamp sieht Rüge der ECRI nach Verurteilung von Ingo Blechschmidt bestätigt"
date: 2022-10-20 23:00:00 +0100
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
permalink: /pressemitteilungen/2022-10-20-kritik-an-polizei-mit-abschreckung-begegnet/
nav_order: 343
---

*Pressemitteilung vom Augsburger Klimacamp am 20.10.2022*

# Klimacamp sieht Rüge der ECRI nach Verurteilung von Ingo Blechschmidt bestätigt

Deutschland geht Vorwürfen des Racial Profilings nicht ausreichend nach.
Dies befand der Europäische Gerichtshof für Menschenrechte in einem
jüngst veröffentlichten Urteil [1]. Zuvor hatte auch die
Anti-Rassismus-Kommission des Europarates (ECRI) Deutschland wegen
Untätigkeit gegen Racial Profiling durch die Polizei gerügt, speziell in
Augsburg aber Fortschritte prognostiziert [1, letzter Absatz]. Diese
Hoffnung wurde enttäuscht, meint Charlotte Lauter (20) vom Augsburger
Klimacamp in Zusammenhang mit der Verurteilung von Aktivist Ingo
Blechschmidt (34).

„Mit Racial Profiling wird die Methode bezeichnet, das physische
Erscheinungsbild wie die Hautfarbe oder Gesichtszüge eines Menschen als
Entscheidungsgrundlage für polizeiliche Maßnahmen wie
Personenkontrollen, Ermittlungen und Überwachungen heranzuziehen“, fasst
der Bayerische Rundfunk in seiner Berichterstattung über das Urteil
zusammen. Einen solchen Fall von Racial Profiling meinten Aktivist\*innen
im Klimacamp am 26. Juni 2021 erkannt zu haben, als die Polizei
ausschließlich schwarze Menschen auf dem Rathausplatz kontrollierte [2,
erster Absatz]. Nachdem die Polizei auf Nachfrage keine Gründe für ihre
Kontrolle angab und widersprüchlicherweise behauptete, dass alle
Menschen auf dem Rathausplatz kontrolliert werden würden, äußerte
Blechschmidt die Vermutung, dass es sich bei der Kontrolle um Racial
Profiling handeln könne, über Lautsprecher und rief Passant\*innen auf,
die Situation zu dokumentieren.

Wie AZ und Augsburger Stadtzeitung berichteten [2,3], wurde Blechschmidt
für diese Durchsage am 13. Juli 2022 wegen übler Nachrede zu einer
Geldstrafe von 1200 Euro verurteilt. Solche Vermutungen dürfe man nicht
laut aussprechen, erklärte die zuständige Richterin ihr Urteil [2,
vorletzter Absatz]; allenfalls könne man in einer solchen Situation
rechtliche Schritte einleiten, führte die Staatsanwältin an [2,
vorletzter Absatz].

Mit diesem Urteil rechneten die Klimacamper\*innen nicht. Zumal die
Polizei bei ihren Kontrollen am Rathausplatz, bei dem sie ausschließlich
Schwarze kontrollierte, später als Rechtfertigung angab, dass sie nach
einem schwarzen Tatverdächtigen bei der „Krawallnacht“ fahnde [2, erster
Absatz; sowie Gespräch der polizeilichen Einsatzleitung mit Blechschmidt
im Nachgang der Durchsage] – als sähe die Polizei bei ihren Ermittlungen
nichts anderes als die Hautfarbe. Dabei gab es von den Tatverdächtigen
der „Krawallnacht“ gestochen scharfe Bilder [7]. Daher könne man eine
bessere Personenbeschreibung als „er ist schwarz“ erwarten.

„In einer akuten Situation hilft auch der Verweis auf rechtliche
Schritte wenig“, erklärt Lauter, denn einerseits könne dadurch nicht die
dringend benötigte Öffentlichkeit hergestellt werden, und andererseits
würden Ermittlungen gegen Polizeikräfte oft nicht mit dem nötigen
Verantwortungsbewusstsein verfolgt werden. Diese Auffassung sieht Lauter
in dem EuGH-Urteil bestätigt – und verweist als konkretes Beispiel auf
die verschleppten Ermittlungen gegen die Beamten der Abteilung
„Staatsschutz“ der Augsburger Polizei [4,5,6] im „Pimmelgate
Süd“-Skandal. Stattdessen ernte man in solchen Fällen eher eine
Gegenanzeige, so Lauter; eine solche traf auch Alexander Mai, den
Betroffenen dieses Skandals [4,6]. Dabei wünschten sich viele
Polizist\*innen in Fällen wie diesen Zivilcourage aus der Bevölkerung,
damit auch für den Job weniger geeignete Kolleg\*innen gewisse Standards
erfüllen.

„Solange man wegen übler Nachrede verurteilt wird, wenn man Vermutungen
über Racial Profiling öffentlich anspricht, hat Augsburg noch einen
langen Weg vor sich“, resümiert Lauter. Lauter ist dabei wichtig, dass
in der Gerichtsverhandlung gegen Blechschmidt von keiner Seite
angezweifelt wurde, dass Blechschmidt seine Vermutung, dass es sich bei
der Kontrolle um Racial Profiling handele, deutlich als Vermutung
gekennzeichnet war. „Die Hoffnung, die die Anti-Rassismus-Kommission des
Europarates in Augsburg legte, aufgrund einer Amtsstelle gegen
Diskriminierung, sehen wir bitter enttäuscht.“

[1] https://www.br.de/nachrichten/deutschland-welt/racial-profiling-kritik-an-entscheidungen-deutscher-gerichte,TKcXaPR
<br>
[2] https://www.staz.de/region/augsburg/blaulicht/fuehrender-augsburger-klimaaktivist-zahlt-strafe-wegen-uebler-nachrede-id241244.html
<br>
[3] https://www.augsburger-allgemeine.de/augsburg/prozess-in-augsburg-protest-gegen-polizei-kontrolle-klimaaktivist-akzeptiert-geldstrafe-id63431366.html
<br>
[4] https://www.klimacamp-augsburg.de/pressemitteilungen/2022-08-02-so-reagiert-die-polizei-auf-pimmelgate-sued/
<br>
[5] https://www.klimacamp-augsburg.de/pressemitteilungen/2022-10-06-polizei-kann-widersprueche-nicht-aufloesen/
<br>
[6] https://netzpolitik.org/2022/pimmelgate-sued-augsburger-polizei-ueberzieht-klimaaktivisten-mit-weiterem-verfahren/
<br>
[7] https://www.br.de/nachrichten/bayern/augsburger-krawallnacht-zwei-jahre-bewaehrung-fuer-angeklagten,SpCWr1A
