---
layout: page
title: "07.11.2022: Besorgnis wegen radikalen bürgerlichen Mitte"
date: 2022-11-07 12:30:00 +0100
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
permalink: /pressemitteilungen/2022-11-07-besorgnis-wegen-radikaler-buergerlichen-mitte/
nav_order: 351
---

*Pressemitteilung vom Augsburger Klimacamp am 7.11.2022*

# Klimagerechtigkeitsaktivist\*innen besorgt wegen der radikalen sogenannten „Bürgerlichen Mitte“

Die sogenannte Augsburger Stadtratsfraktion „Bürgerliche Mitte“ bemüht
sich in Augsburg seit langem darum, außerparlamentarlichen Einsatz für
Klimagerechtigkeit und den Erhalt der Lebensgrundlagen als radikal zu
diffamieren. So geschah es vor Kurzem auch durch Peter Hummel gegenüber
presse-augsburg.de [1].

Zuletzt hatte die Bürgerliche Mitte im September für Aufsehen gesorgt,
als sie sich im Stadtrat zusammen mit der AfD gegen eine neue
Stellplatzsatzung aussprach, welche Fahrrädern deutlich mehr Raum
einräumen würde. Erschwerend hinzu kommt, dass die Änderung der
Stellplatzsatzung eigentlich bereits im Vertrag zwischen der Stadt und
den Initiator\*innen von „Fahrradstadt jetzt“ festgelegt worden war. Den
Unwillen sich an geschlossene Verträge zu halten und diese inhaltliche
Nähe zur AfD sind es auch, die man am Klimacamp mit großer Sorge
beobachtet.

Ein Klimacamper erinnert sich noch deutlich an eine Podiumsdiskussion im
Mai 2022, an der auch Beate Schabert-Zeidler als Vertreterin der
bürgerlichen Mitte teilnahm. Sie sagte, dass bei ihr die Devise „Jute
statt Plastik“ gelte und sie auch immer ihre Kinder aufforderte, dass
Licht auszumachen, wenn sie es nicht brauchten. Damit war
Klimagerechtigkeit als Thema für sie aber auch schon abgeschlossen. Den
Rest des Abends kritisierte sie dann die
Klimagerechtigkeitsaktivist\*innen. Dabei bietet sie selbst bis heute
keine tragfähige Alternative zu den Forderungen der Aktivist\*innen.

Sie gestand sogar ein, dass sie im Gegensatz zu vielen anderen
Stadträt\*innen noch nie zu einem Gespräch am Klimacamp war und schien
lieber aus der Ferne über die Menschen dort urteilen zu wollen.

„Sie reduziert Klimagerechtigkeit auf plumpe und rein symbolische
Konsumkritik und versucht so die Verantwortlichkeit der Politik klein zu
reden. Ich habe mich einfach nur für sie fremdgeschämt.“, erzählte ein
damals im Publikum anwesender Klimacamper unter der Zusicherung von
Anonymität.

Am Klimacamp weiß man schon lange, wie Konsumkritik für derartige Zwecke
missbraucht wird. Die Details dazu hat man in einem eigenen Artikel mit
dem Titel „Konsumkritikkritik – Kritik an Konsumkritik“ [2]
veröffentlicht.

Ebenfalls bei der Bürgerlichen Mitte von der Partie sind auch die
sogenannten „Freien Wähler“, denen auch der oben genannte Peter Hummel
angehört. Die Freien Wähler stehen zur Zeit massiv in der Kritik, weil
sie sowohl die bereits erfolgte Teilrodungen des Lohwaldes bei
Meitingen, eines besonders schützenswerten Bannwaldes, als auch die
geplante Rodung des restlichen Lohwaldes unterstützen.

Die Bürgerliche Mitte behauptet, sie fürchte die Radikalisierung des
Klimacamps. Damit geht sie auf Stimmenfang. Dabei sind es ihre Politik,
die eine radikale Verschlechterung der Lebensumstände der Menschen auch
in Deutschland zur Folge hat.

Keine moderne Demokratie hat die Not, die wir aufgrund der Erderhitzung
in den kommenden Jahren erleben werden, in ihrer Geschichte jemals
erlebt oder überlebt. Eine Erderhitzung um 2°C gilt unter Experten als
Katastrophe. Eine Erderhitzung um 4°C gilt unter Experten als
wahrscheinlich zivilisationsendend. Das ist die Welt, auf die die
radikale Bürgerliche Mitte und andere Parteien mit ihrer Politik der
Untätigkeit hinarbeiten.[3] Sie nehmen die Zerstörung unser aller
Zukunft in Kauf, nur um noch ein paar Jahre der Illusion ihrer
Vorstellung von Ordnung nachhängen zu können.

Bei der Bürgerlichen Mitte wird weiter über das Kind geschimpft, welches
es immer wieder wagt auszusprechen, dass der Kaiser keine Kleider anhat.
Sie treiben erst mit ihrer Politik junge Menschen in eine Sackgasse.
Dann beschweren sie sich, dass diese jungen Menschen in Panik mit dem
Rücken zur Wand laut und unangenehm werden.

Nur sehr engagierter und sozialer Klimaschutz kann Gerechtigkeit bringen
und Wohlstand erhalten. Die bürgerliche Mitte will lediglich symbolische
Klimapolitik, um das eigene Gewissen zu beruhigen, nicht effektive
Klimapolitik, die dazu geeignet wäre, eine weitere Eskalation der
Erderhitzung aufzuhalten und den jungen Menschen eine lebenswerte
Zukunft ermöglichen könnte.

Mehr Informationen über Klimagerechtigkeitsaktivismus, dazu wie er
funktioniert und welche Bedeutung er für die Demokratie hat, finden sich
auch in einem Artikel des Klimacamps [4].

[1] [https://presse-augsburg.de/enge-verbindung-von-letzte-generation-zum-augsburger-klimacamp/829380/](https://presse-augsburg.de/enge-verbindung-von-letzte-generation-zum-augsburger-klimacamp/829380/)

[2] [https://www.klimacamp-augsburg.de/informationen/artikel/konsumkritikkritik/](https://www.klimacamp-augsburg.de/informationen/artikel/konsumkritikkritik/)

[3] [https://www.augsburger-allgemeine.de/politik/bericht-deutschland-verfehlt-seine-klimaziele-wenn-sich-nichts-aendert-id64453621.html](https://www.augsburger-allgemeine.de/politik/bericht-deutschland-verfehlt-seine-klimaziele-wenn-sich-nichts-aendert-id64453621.html)

[4] [https://www.klimacamp-augsburg.de/informationen/artikel/klimaaktivismus/](https://www.klimacamp-augsburg.de/informationen/artikel/klimaaktivismus/)


## Anmerkungen

Mehr Informationen zur Entstehung dieser Pressemitteilung
finden sich auch in unserem
[Tagebucheintrag vom 07.11.2022](/tagebuch/#montag-07112022--tag-860).
