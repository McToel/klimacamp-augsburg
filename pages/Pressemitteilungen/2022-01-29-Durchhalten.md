---
layout: page
title:  "29.01.2022: Antwort an Herrn Hörmann auf seine Frage „Wie lange hält das Klimacamp in Augsburg noch durch?“"
date:   2022-01-29 22:00:00 +0200
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
nav_order: 301
---

Am 29.01.2022 fragte Michael Hörmann
in einem Artikel in der Augsburger Allgemeinen:
„[Wie lange hält das Klimacamp in Augsburg noch durch?](https://www.augsburger-allgemeine.de/augsburg/debatte-bewundert-und-beschimpft-wie-lange-haelt-das-klimacamp-in-augsburg-noch-durch-id61582291.html)“
Hier antwortet Ingo Blechschmidt auf diese Frage.

--

Sehr geehrter Herr Hörmann,

vielen Dank für Ihren ausführlichen Bericht „Wie lange hält das
Klimacamp noch durch?“. Insbesondere die zeitliche Einordnung und
aktuelle Einschätzung nahmen wir Aktivist\*innen vom Klimacamp mit großem
Interesse auf.

Wenn Sie uns näher und aus erster Hand kennenlernen möchten, laden wir
Sie gerne zu einer losen Austauschrunde zu uns ins Klimacamp ein.
Vielleicht möchten Sie auch einen unserer Abendvorträge besuchen, gerade
für unsere Presseworkshops wären Sie sicherlich eine tolle Bereicherung!

Das Klimacamp wird von einem großen Unterstützer\*innenkreis getragen.
Die Altersspanne umfasst nicht nur Schüler\*innen und Studierende:
Augsburg\*innen aller Altersgruppen übernehmen freiwillig Aufgaben wie
die Betreuung der Webseite oder das Verfassen unserer
Pressemitteilungen. Mit professionellen Kommunikationsstrateg\*innen
stehen wir indes nicht in Kontakt. Dass Sie diesen Eindruck gewinnen
konnten, empfinden wir als großes Kompliment für die Qualität unserer
Arbeit. Die über 19 Monate hinweg gesammelte Erfahrung hat sicherlich
dazu beigetragen. Kompetente juristische Beratung nehmen wir tatsächlich
in Anspruch, um uns gegen die Nachstellungen der Stadt zu behaupten. Pro
Monat erreichen uns nämlich zahlreiche Bescheide von Ordnungsamt und
Staatsschutz, deren Bearbeitung viel Zeit und Energie in Anspruch nimmt,
bevor sie im Regelfall vor Gericht dann doch wieder kassiert werden.

Auf die in der Artikelüberschrift gestellte Frage ist unsere Antwort:
Noch lange!

Als Mitinitiator des Klimacamps, der schon viele Phasen des Protests
miterlebte, kann ich nämlich feststellen: Die
Klimagerechtigkeitsbewegung in Augsburg ist so stark wie nie zuvor. Sie
vermochte es, über zwei Winter und mehr als 570 Tage hinweg das
Klimacamp als zentralen Protest-, Einfluss- und Freiraum zu betreiben.
Mit der Wiederaufnahme der universitären Präsenzlehre wurden auch die
Students for Future wieder aktiv und zählen jetzt mehr Mitglieder zu
ihren Reihen als vor der Pandemie. Mit einem Minimum an Vorlaufzeit
organisierten diese mit der Public Climate School eine öffentliche
Vortragsreihe, zu der sie zahlreiche namhafte Wissenschaftler\*innen
gewinnen konnten, und haben für das kommende Jahr viele Projekte in
Aussicht. Es waren auch wir von der Augsburger
Klimagerechtigkeitsbewegung, die zusammen mit anderen das Bündnis
„Augsburg Solidarisch“ gründeten, um den Corona-„Sparziergängen“
Gegendemonstrationen unter dem Motto "geradeaus denken – hört auf die
Wissenschaft!" entgegenzusetzen.

Früher kannten wir nur eine Aktionsform: die großen Schulstreiks fürs
Klima. Die sind pandemiebedingt aktuell nicht das Mittel der Wahl,
weswegen wir ihre Anzahl auch deutlich reduzierten. Aber immer noch
nutzen zahlreiche Augsburger\*innen diese Streiks, um ihren Protest an
der Klimapolitik des Verschleppens und Abwartens auszudrücken: Beim
letzten Globalstreik, am Freitag vor der Bundestagswahl, folgten 5.000
Augsburger\*innen unserem Aufruf.

Der große Zeitdruck, den die physikalischen Gegebenheiten der Klimakrise
herstellen, erfordert aber, dass wir über die Demonstrationszüge hinaus
weitere Aktionsformen einsetzen. Die gab es in der Anfangszeit von
Fridays for Future kaum und sind der beste Indikator dafür, wie sehr wir
uns weiterentwickelten.

Wenn in Augsburg und Umgebung mal wieder Bäume oder ganze Wälder der
Zerstörungswut der Politiker\*innen und Konzerne weichen sollen und uns
diesbezüglich Bürger\*inneninitiativen um Hilfe bitten, sind wir
diejenigen, die diese nach dem Vorbild des Hambacher Forsts besetzen und
so mit unseren Körpern verteidigen. Die mehrmonatige Besetzung des
Forsts Kasten bei München ging auf unsere Initiative zurück. [1,2,3,4,5]

Wenn – auch aufgrund des Augsburger Kohlestrombezugs – Menschen
enteignet, Kirchen entweiht und Dörfer abgerissen werden, um wie aktuell
in Lützerath Platz für eine Kohlegrube zu machen, die jetzt schon einer
Marslandschaft der Größe von fast halb Augsburg gleicht, sind wir
diejenigen, die friedlich die dortige Kohleinfrastruktur blockieren.

Wenn die CSU ihren Wähler\*innen christliche Werte vorgaukelt, zugleich
aber eine illegale Maskenprovision nach der anderen einstreicht, sind
wir es, die den Balkon ihres Wahlkreisbüros oder die Kräne der illegalen
Baustellen von Georg Nüßlein erklettern und mit aufklärenden Bannern
versehen. [6,7]

Und wenn die Stadt die ausführlichen Protokolle unseres wichtigsten
kommunalen Gremiums, dem Augsburger Stadtrat, unter Verschluss hält und
sich nicht nur über die Empfehlungen der Bürger\*innenversammlung,
sondern auch dem dringenden Rat der von ihr selbst in Auftrag gegebenen
KlimaKom-Studie hinwegsetzt, sind wir es, die die Sitzungen besuchen,
eigene Protokolle anfertigen, diese der Öffentlichkeit zur Verfügung
stellen und vor allem: immer wieder nachhaken. Sowohl wenn die Stadt
weiter an fatalen Fehlinvestitionen in klimaschädliche Konzerne wie
Bayerngas festhält, die trotz Klimakrise aktiv die Nordsee nach neuen
Erdgasquellen absuchen, als auch, wenn sie alle sechs Monate Bus und
Tram durch weitere Preiserhöhungen stetig unattraktiver macht. [8]

Der nächste Globalstreik, auf den wir wieder umfassend aufmerksam machen
werden, findet am 25. März statt. Die Jubiläumskundgebung von Fridays
for Future bewarben wir dagegen kaum. Natürlich ist es immer schön, wenn
mehr Gäste zu einer Geburtstagsfeier kommen, aber die zentrale
politische Veranstaltung war eben nicht die Feier, sondern der
vorausgehende Globalstreik. Der Entschluss zur sonntäglichen Fahrraddemo
fiel übrigens äußerst kurzfristig am Donnerstag. Nötig wurde sie nur, da
unser im Vergleich zu anderen Kommunen besonders versammlungsfeindliches
Ordnungsamt an eben jenem Donnerstag eine lange im Voraus geplante
Abseilversammlung für den Sonntag untersagte.

Sie haben ganz recht: Wir müssen so schnell wie möglich unseren alten
Platz in Sichtweite der Augsburger Stadtpolitik wieder einnehmen. Es ist
einfach etwas anderes, ob wir um die Ecke stehen oder vor jeder
Stadtratssitzung und jedem Ausschuss den Politiker\*innen auf dem Weg ins
Rathaus ins Auge stechen. Denn unser Protest ist nötig. Das zeigt nicht
zuletzt der letzte Bericht vom Weltklimabeirat oder das
Abstimmungsverhalten der Stadtregierung. Die Erdaufheizung ist schon
jetzt spürbar und verursacht in zahlreichen Gebieten großes Leid. Für
unseren Aktivismus verzichten wir auf Freizeit und vernachlässigen Beruf
und Freundeskreis, gewinnen aber auch neue Freund\*innen – und kämpfen:
So vieles, das wir heute für selbstverständlich halten, wird es in
Zukunft nicht mehr geben.

Wir freuen uns, Sie persönlich kennenzulernen!

Viele Grüße
Ingo Blechschmidt

[1] [https://www.sueddeutsche.de/muenchen/muenchen-forst-kasten-umweltschuetzer-1.5331295](https://www.sueddeutsche.de/muenchen/muenchen-forst-kasten-umweltschuetzer-1.5331295)<br>
[2] [https://www.sueddeutsche.de/muenchen/muenchen-forst-kasten-baeume-aktivisten-polizei-1.5327554](https://www.sueddeutsche.de/muenchen/muenchen-forst-kasten-baeume-aktivisten-polizei-1.5327554)<br>
[3] [https://www.sueddeutsche.de/muenchen/muenchen-forst-kasten-baumbesetzer-1.5297147](https://www.sueddeutsche.de/muenchen/muenchen-forst-kasten-baumbesetzer-1.5297147)<br>
[4] [https://www.br.de/nachrichten/bayern/klimaaktivisten-besetzen-waldstueck-im-sueden-von-muenchen,SXmRlda](https://www.br.de/nachrichten/bayern/klimaaktivisten-besetzen-waldstueck-im-sueden-von-muenchen,SXmRlda)<br>
[5] [https://www.br.de/nachrichten/bayern/aktivisten-bauen-baumhaeuser-in-forst-kasten-polizei-vor-ort,SZpMd7c](https://www.br.de/nachrichten/bayern/aktivisten-bauen-baumhaeuser-in-forst-kasten-polizei-vor-ort,SZpMd7c)<br>
[6] [https://www.augsburger-allgemeine.de/krumbach/Landkreis-Guenzburg-Aktivisten-greifen-mit-Banner-Nuesslein-und-CSU-an-Was-steckt-dahinter-id60323906.html](https://www.augsburger-allgemeine.de/krumbach/Landkreis-Guenzburg-Aktivisten-greifen-mit-Banner-Nuesslein-und-CSU-an-Was-steckt-dahinter-id60323906.html)<br>
[7] [https://www.augsburger-allgemeine.de/augsburg/Augsburg-Feuerwehr-rueckt-wegen-Klimacamp-Aktivisten-mit-Drehleiter-an-id59878656.html](https://www.augsburger-allgemeine.de/augsburg/Augsburg-Feuerwehr-rueckt-wegen-Klimacamp-Aktivisten-mit-Drehleiter-an-id59878656.html)<br>
[8] [https://augsburg.klimacamp.eu/protokolle/](https://augsburg.klimacamp.eu/protokolle/)
