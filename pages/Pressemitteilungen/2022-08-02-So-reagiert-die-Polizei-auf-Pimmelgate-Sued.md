---
layout: page
title:  "02.08.2022: So reagiert die Polizei auf die Veröffentlichung der umstrittenen Hausdurchsuchung beim Klimacamper Alexander Mai"
date:   2022-08-02 23:59:00 +0100
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
permalink: /pressemitteilungen/2022-08-02-so-reagiert-die-polizei-auf-pimmelgate-sued/
nav_order: 320
---

*Pressemitteilung vom Augsburger Klimacamp am 2.8.2022*

# So reagiert die Polizei auf die Veröffentlichung der umstrittenen Hausdurchsuchung bei Klimacamper Alexander Mai

Im April dieses Jahres geriet die Abteilung „Staatsschutz“ der
Augsburger Polizei [in die Schlagzeilen](https://www.pimmelgate-süd.de/), als sie in aller Früh mit
einer Hausdurchsuchung in den intimen Schutzbereich des ehemaligen
Bundestagskandidaten und Klimaaktivisten Alexander Mai (26) eingriff,
seine privaten technischen Geräte sowie seinen Arbeitslaptop
konfiszierte und sich so Zugang zu großen Teilen der vertraulichen
digitalen Kommunikation der Augsburger Klimagerechtigkeitsbewegung
verschaffte. Sechs Monate zuvor hatte Mai auf der Facebook-Seite der
Augsburger AfD unter einem ihrer sexistischen und fremdenfeindlichen
Beiträge ein Zeitungsfoto des Hamburger Pimmelgate-Skandals verlinkt,
worin die Abteilung „Staatsschutz“ eine verfolgenswerte Beleidigung sah.
Die Abteilung „Staatsschutz“ begründete ihren Übergriff damit, Mais
Urheberschaft am Kommentar nachweisen zu wollen – obwohl daran kein
Zweifel bestand, da Mai den Kommentar unter seinem vollen Namen
veröffentlichte und dies auch nicht bestritt.

Besonders brisant an dem Vorstoß: [Wie das polizeiliche
Durchsuchungsprotokoll bestätigt](https://www.pimmelgate-süd.de/pressemappe/), verweigerte die Abteilung
„Staatsschutz“ Mai während der Durchsuchung sein Recht, seine Anwältin
hinzuziehen. Dieses Recht steht in Deutschland allen Betroffenen von
Hausdurchsuchungen zu.[^1] „Ich wusste, dass mir das Recht auf einen
Anruf zustand“, so Mai, „aber nicht nur untersagten mir die vier
Polizisten den Anruf, sie schüchterten mich auch mit ihrer Sprache und
ihrer Körperhaltung entsprechend ein, dass es unmittelbare, schmerzhafte
Konsequenzen hätte, wenn ich zum Telefon greife.“

[^1]: § 137 StPO ohne Aufhebung durch § 24 BayPAG; Kanzlei Röttig: „Die Beamten müssen den Betroffenen eine gewisse Zeit einräumen, um einen Anwalt kontaktieren zu können. Es ist der Polizei nicht gestattet, Ihnen ein Telefonat mit dem Anwalt zu untersagen.“, Strafverteidiger Thomas Penneke: „Die Polizei spricht oft ein Telefonverbot während der Hausdurchsuchung aus. Sollten Sie Ihren Anwalt anrufen wollen, ist das Verbot rechtswidrig. Lassen Sie sich hiervon nicht abbringen.“, Baumann Mayer Seidel & Partner mbB: „Am Anruf [des Anwalts] hindern darf man Sie nicht. Sie haben das Recht, auch während der Durchsuchung zu telefonieren.“

Nachdem Mai und seine Mitstreiter\*innen vom Augsburger Klimacamp den
Vorstoß mit geschwärzten persönlichen Daten öffentlich machten, wurde
nun ein zweites, zusätzliches Verfahren gegen Mai eröffnet. Die
transparente öffentliche Information des Klimacamps über das laufende
Verfahren sei ein strafbahres Vergehen, Mai ist für den morgigen
Mittwoch (3.8.2022) vorgeladen. „Der Staatsschutz merkt selbst, dass er
nichts gegen mich in der Hand hat. Deswegen versucht er, uns mit langer
und harter Repression unterzukriegen. Dass das nichts als kurze,
schlaffe Einschüchterungsversuche sind, ist uns aber klar“, so Mai.

"Wir sind nicht wütend, wir sind enttäuscht", schätzt Klimacamperin Charlotte
Lauter (19) den neuen Repressionsversuch ein. Denn schon die Ermittlungen im
ursprünglichen Hamburger ‚Pimmelgate‘-Fall wurden von der Hamburger
Generalstaatsanwaltschaft bereits im März 2022 eingestellt
„‚Pimmelgate‘-Verfahren eingestellt – Zu klein für öffentliches Interesse“
[titelte die taz
dazu](https://taz.de/Pimmelgate-Verfahren-eingestellt/!5871270/) vergangene
Woche ([Artikel bei
Netzpolitik.org](https://netzpolitik.org/2022/-eingestellt-hamburger-staatsanwaltschaft-erklaert-pimmelgate-affaere-fuer-beendet/)).
Damals richtete ein Twitter-Nutzer in Kritik von dessen doppelten Standards in
der Corona-Pandemie die Äußerung "Du bist so 1 Pimmel" an den Hamburger
Innensenator. Im Augsburger „Pimmelgate Süd“ gab es eine solch explizite
Äußerung nie, Mai verlinkte nur ein Titelbild eines Online-Zeitungsartikels zu
jenem Fall. „Die Bürger\*innen Augsburgs werden sich natürlich fragen, warum
der ‚Staatsschutz‘ sich mit solchen Pimmeleien abgibt statt Verbrechen
nachzugehen“, konstatiert Mai.
