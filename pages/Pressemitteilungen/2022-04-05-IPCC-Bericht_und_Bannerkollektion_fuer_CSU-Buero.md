---
layout: page
title:  "05.04.2022: Klimacamp Augsburg: Kommentar zum neuen IPCC-Bericht, neue Bannerkollektion an CSU-Büro"
date:   2022-04-05 10:00:00 +0100
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
nav_order: 304
---

*Pressemitteilung des Augsburger Klimacamps am 5.4.2022*

# Klimacamp Augsburg: Kommentar zum neuen IPCC-Bericht, neue Bannerkollektion an CSU-Büro

Sehr geehrte Pressevertreter\*innen,

uns fehlen angesichts des neuen bitteren Berichts des Weltklimarats die
Worte.

Zum Glück für Sie, steht alles, was Sie dazu wissen müssen, direkt in
dem IPCC-Bericht und größtenteils auch in seinen Zusammenfassungen.
Bitte werden Sie Ihrer Aufgabe als Presse gerecht. Berichten Sie
darüber.

### Zusammenfassung für Entscheidungsträger\*innen
[https://report.ipcc.ch/ar6wg3/pdf/IPCC_AR6_WGIII_SummaryForPolicymakers.pdf](https://report.ipcc.ch/ar6wg3/pdf/IPCC_AR6_WGIII_SummaryForPolicymakers.pdf)

### Folien zur IPCC-Pressekonferenz
[https://report.ipcc.ch/ar6wg3/pdf/IPCC_AR6_WGIII_PressConferenceSlides.pdf](https://report.ipcc.ch/ar6wg3/pdf/IPCC_AR6_WGIII_PressConferenceSlides.pdf)

Schlüsselzitate

* „Wir sind nicht auf einem Weg, die Erdaufheizung auf 1,5 Grad zu
  begrenzen.“ (Folie 4)
* „Wenn nicht quer über alle Sektoren sofort und substanziell Emissionen
  eingespart werden, sind 1,5 Grad außer Reichweite.“ (Folie 6)
* „Mit heutiger Technologie könnte jeder Sektor bis 2030 seine Emissionen
  halbieren“ (Folie 12)

Die Strategien einiger Politiker und Unternehmen bezeichnet der UN-Chef
als „moralischen und wirtschaftlichen Wahnsinn“. „Es ist ein Dokument
der Schande, ein Katalog der leeren Versprechen, die die Weichen klar in
Richtung einer unbewohnbaren Erde stellen“, sagte er in einer
Videobotschaft.“<br>
Quelle: [Antonio Guterres bezeichnet neuen Klimabericht als „Dokument der Schande“](https://www.rnd.de/politik/antonio-guterres-bezeichnet-neuen-klimabericht-als-dokument-der-schande-XLA2Z4M2HPT7WCMKNPVAYSNUMM.html)


## Augsburg direkt betroffen

„Sogar die von der Stadt selbst in Auftrag gegebene KlimaKom-Studie
zeigt auf, dass die bisherige Klimapolitik des Abwartens und Nichtstuns
ein großer Fehler war“, ordnet Mathematiker Dr. Ingo Blechschmidt (33)
vom Klimacamp die Ergebnisse des neuen Berichts ein. „Abwarten und
Nichtstun sind in Klimafragen infantil und riskant. Abwarten und
Nichtstun können wir uns nicht mehr leisten, große kluge Maßnahmen sind
nun erforderlich. Wann werden Busse und Trams in Augsburg endlich mal
günstiger?“, fragt Blechschmidts Mitstreiterin Charlotte Lauter (19).
„Wann kommen neue Buslinien? Wie sollen denn wir Augsburger\*innen auf
den ÖPNV umsteigen, wenn er so teuer ist?“


## Klimacamper\*innen kündigen Bannerkollektion an CSU-Büro an

„Die CSU tut sich unter allen Parteien mit wissenschaftlichen
Erkenntnissen besonders schwer“, erklärt Lauter. „Der CSU müssen wir
Informationen immer direkt vor die Haustür liefern.“ Lauter bezieht sich
damit sowohl auf die CSU Augsburg als auch die Union insgesamt.

Aus diesem Grund kündigen die Klimacamper\*innen eine spezielle
10-teilige Bannerkollektion an. Die Banner werden je einen
Themenschwerpunkt des neuen IPCC-Berichts zum Inhalt haben und in
nächtlichen Kletteraktionen direkt am Balkon des Augsburger CSU-Büros
befestigt werden.

„Im Klimacamp können alle interessierten Augsburger\*innen lernen, wie
man Banner am CSU-Büro befestigt, oder uns nachts begleiten.“
Journalist\*innen können telefonisch oder per Mail Termine für die
Banneraktionen erfragen.


KONTAKT<br>
Ingo Blechschmidt (+49 176 95110311)
