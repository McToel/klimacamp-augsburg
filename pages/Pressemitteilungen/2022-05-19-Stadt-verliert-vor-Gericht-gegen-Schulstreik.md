---
layout: page
title:  "19.05.2022: Stadt verliert vor Gericht gegen Schulstreik"
date:   2022-05-19 10:00:00 +0100
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
permalink: /pressemitteilungen/2022-05-19-stadt-verliert-vor-gericht-gegen-schulstreik/
nav_order: 316
---

*Pressemitteilung von Fridays for Future Augsburg am 19.5.2022*

# Fridays for Future Augsburg: Stadt Augsburg kassiert erneut Niederlage vor Gericht – Schulstreik auf Rathausplatz

Am morgigen Freitag, dem 20. Mai, veranstaltet Fridays for Future um
11:00 Uhr den ersten Schulstreik seit mehr als einem Jahr, also die
erste Demonstration, die zur Schulzeit stattfindet.

Als Kundgebungsort zeigte Fridays for Future dazu der Stadt Augsburg
frühzeitig der Rathausplatz an. Nach Wochen der Wartezeit großes
Unverständnis: Das Ordnungsamt untersagt die Aufstellung am Rathausplatz
und ordnet stattdessen den publikumsarmen Elias-Holl-Platz auf der
Rückseite des Rathauses an.

Als Grund für diesen substanziellen Eingriff in die Versammlungsfreiheit
gab das Ordnungsamt im Bescheid (angehängt) an, dass die zeitgleich zum
kurzen anfänglichen Redeblock des Schulstreiks stattfindende
Veranstaltung beim Fugger-Pavillion gestört werde. Laut
Veranstaltungsprogramm handelt es sich dabei um „Frühschoppen“
(Duden: „geselliger Trunk am Vormittag“).

Das Verwaltungsgericht Augsburg entschied heute:
Dieser Bescheid war illegal. „Gemessen an [...] verfassungsrechtlichen
[...] Vorgaben erweist sich [...] die Verlegung des Auftaktplatzes vom
Rathausplatz auf den Elias-Holl-Platz als unangemessener Eingriff in die
Versammlungsfreiheit [...]“ (Seite 12, Zeile 18). Gemäß BayVersG Art. 15
müsste „die öffentliche Sicherheit oder Ordnung bei Durchführung der
Versammlung unmittelbar gefährdet“ sein, damit die freie Ortswahl
eingeschränkt werden darf. Die Rechtfertigungsversuche der Stadt
überzeugten das Gericht bei den friedlichen Schulstreiks von Fridays for
Future nicht.

Janika Pondorf (17) von Fridays for Future kommentiert: „Sogar für uns
Laien war vollkommen offensichtlich, dass der Rechtfertigungsversuch der
Stadt grober Unfug war. Deshalb reichten wir einen Eilantrag ein. Busse
und Trams werden in Augsburg immer teurer, von einer Verkehrswende ist
nichts zu spüren. Unser Protest ist wichtiger denn je – zum Glück
untersagte das Gericht den städtischen Versuch, unseren Protest
unsichtbar zu machen!“

Pondorfs Mitstreiterin Charlotte Lauter (19) ergänzt: „Nach so vielen
gewonnen Gerichtsverfahren wundern wir uns schon, warum die Stadt
Augsburg immer noch offensichtlich rechtswidrige Versammlungsbescheide
schreibt. Könnte sie ihre offenkundig im Überfluss vorhandene
Bürokratiekapazität nicht für vereinfachte Windparkzulassungsverfahren
nutzen?“ Die Aktivist\*innen gewannen zuvor drei Mal in Sachen
Versammlungsstatus des Klimacamps (Eilverfahren am Verwaltungsgericht,
Hauptverfahren am Verwaltungsgericht, Hauptverfahren am Bayerischen
Verwaltungsgerichtshof) sowie zwei Mal in Sachen Fahrraddemo über B17
(Eilverfahren an VG und VGH). Aufgrund der großen Unverhältnismäßigkeit
des städtischen Bescheids prüfen Lauter zufolge die Aktivist\*innen, ob
weitere rechtliche Schritte gegen das Ordnungsamt eingeleitet werden
können. Dieses sei als staatliche Institution eigentlich
grundrechtsgebunden.

*Anmerkung:
Im Original enthielt die Presse geleitete Pressemitteilung
noch verschiedene Anhänge.
Darunter war auch eine Kopie der Klage
gegen die Versammlungsauflagen.*
