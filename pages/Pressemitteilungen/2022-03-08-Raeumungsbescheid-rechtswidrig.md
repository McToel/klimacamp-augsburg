---
layout: page
title:  "08.03.2022: Räumungsbescheid rechtswidrig – Stadt Augsburg kassiert zum fünften Mal Schlappe vor Gericht gegen Klimacamper*innen"
date:   2022-03-08 10:00:00 +0100
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
nav_order: 302
---

*Pressemitteilung des Augsburger Klimacamps am 8.3.2022*

# Räumungsbescheid rechtswidrig – Stadt Augsburg kassiert zum fünften Mal Schlappe vor Gericht gegen Klimacamper\*innen

Am gestrigen Montag, den 7.03.2022 und 615-ten Tag des Klimacamps, wurde vor dem Bayerischen Verwaltungsgerichtshof (VGH) in München über das von der Stadt Augsburg eingereichte Berufungsverfahren gegen das Augsburger Klimacamp (Az. 10 B 21.1694) verhandelt. Dabei ging es um die Frage, ob das Camp in den ersten zehn Tagen – vom 1. bis zum 10. Juli 2020 – eine Versammlung im Sinne von Art. 8 GG war. Die Entscheidung des VGH, dass der städtische Räumungsbescheid an das Klimacamp vom 10. Juli 2020 illegal war, wurde den Klimacamper\*innen am heutigen Dienstagmorgen telefonisch mitgeteilt. Sowohl das Eil- als auch das Hauptverfahren vor dem Verwaltungsgericht Augsburg gegen den Räumungsbescheid hatte das Klimacamp bereits im Jahr 2020 gewonnen. Auf dieser Grundlage findet die Dauerversammlung der Klimaaktivist\*innen bis zum heutigen Tag und auch weiterhin ununterbrochen statt.

Die Ausgangstendenz gab der Vorsitzende Richter Reinhard Senftl bereits in der gestrigen mündlichen Verhandlung bekannt. Nachdem das zentrale Element der städtischen Ausführungen laut Senftl „in sich zusammengefallen“ war, fragte er die Vertreter der Stadt, ob sie die Revision nicht doch zurückziehen wollen. Damit hätte die Stadt die noch größere Blamage in Form einer ausführlichen Urteilsbegründung, die nun doch kommen wird, noch abwenden können. „Die fünfköpfige städtische Entourage sollte lieber Rechtsgrundlagen für den Bau von Windrädern abklären anstatt sich für die infantile Klimapolitik des Abwartens und Nichtstuns blamieren zu lassen“, so Klimacamperin Charlotte Lauter (19).


### STADT IRRT BEI VERHANDLUNGSGEGENSTAND

Richter Senftl stellte gleich zu Verhandlungsbeginn klar, dass er kein Grundsatzurteil zur grundsätzlichen Zulässigkeit von dauerhaften Versammlungen fällen werde. Dies hatte zuletzt Ordnungsreferent Pintsch fälschlicherweise so gegenüber der Augsburger Allgemeinen behauptet. [Quelle: [augsburger-allgemeine.de](https://www.augsburger-allgemeine.de/augsburg/klimacamp-augsburg-vor-dem-aus-gericht-entscheidet-id61924561.html)] Es ging in der Verhandlung tatsächlich nur um den Räumungsbescheid vom 10.7.2020, den zuvor bereits das Augsburger Verwaltungsgericht als rechtswidrig erklärt hatte.

Schülerin und Klimacamp-Mitinitiatiorin Janika Pondorf (17) dazu: „Wir sind schon überrascht, dass die Stadt Augsburg ganz offensichtlich nicht genau wusste, um was es bei dem Prozess geht, den sie nun schon so lange mit uns führt. Wir freuen uns auf die Strahlwirkung, die dieses Urteil auf die Politik entfalten wird: Das Urteil gibt dem politischen Druck für Klimagerechtigkeit starken Rückenwind.“


### DER PROZESS VOR DEM VGH HAT PIONIERCHARAKTER

Üblicherweise halten Versammlungen nicht so lange an, dass ein derartiges Verfahren im Hauptverfahren in einer obergerichtlichen Instanz möglich wird.

„Man kann der Stadt Augsburg zwar dankbar sein, dass sie durch ihre Klage so viel mediale Aufmerksamkeit für Klimagerechtigkeit schuf. Lieber wäre es uns allerdings gewesen, wenn die Stadtregierung endlich sozial gerechte Lösungen gegen den Treibhausgasausstoß umsetzen würde“, so Pondorf.

Stattdessen betreibe die Stadt unablässlich eine „Täter-Opfer-Umkehr“, so Pondorf, und schiebe ihre Verantwortung für Klimaschutz auf Augsburgs Privatpersonen ab. Die städtische PR-Kampagne „Klimawette“ demonstriere dies besonders eindrucksvoll. Dabei wurden Augsburgs Privatpersonen aufgefordert, tunlichst CO₂ zu vermeiden. „Jedoch ohne sie dabei in irgendeiner Form zu unterstützen und ohne selbst gestalterisch aktiv zu werden: ÖPNV ticketfrei machen, Fernwärmeausbau vorantreiben, Unterstützung vom Land einklagen. Sogar einfache Balkonsolaranlagen verhindert die Stadt.“


### RECHTSWIDRIGE AUFTRAGSVERGABE UND STÄDTISCHE STEUERVERSCHWENDUNG?

Die Oberbürgermeisterin der Stadt Augsburg, Eva Weber, stellte wiederholt fest, dass dauerhafte Versammlungen im Sinne des Art. 8 GG nicht ihrer Rechtsauffassung entsprächen. Schon vor Aushändigung des Räumungsbescheids am 10.7.2020 gab sie den Schüler\*innen gegenüber an, dass sie sich als Juristin sicher sei, dass vom Klimacamp kein Kundgebungscharakter ausgehe und es deshalb nicht von der Versammlungsfreiheit geschützt sei.

„Das Urteil in erster Instanz war eindeutig. War es wirklich nötig, für diesen Prozess Augsburger Steuergelder zu verschwenden?“, fragt Klimacamperin Lutz Reng (22). Das Verwaltungsgericht Augsburg hatte nämlich nach seiner Entscheidung im November 2020 keine Berufung zugelassen – diese musste die Stadt Augsburg erst vor dem VGH beantragen.

Reng fordert auch endlich eine Aufklärung, wie es zur Auftragsvergabe an die Kanzlei kam, welche die Stadt vertreten hat. „Einen Informationsantrag über 'Frag den Staat' wies die Stadt mit Verweis auf den Datenschutz zurück“, erklärt Reng. „Nirgendwo konnten wir Spuren einer öffentlichen Ausschreibung finden. Wohl aber den Nachweis, dass Ordnungsreferent Pintsch früher bei der nun beauftragten Kanzlei arbeitete.“ [Abgelehnte Anfragen auf [fragdenstaat.de](https://fragdenstaat.de/anfrage/notwendigkeit-offentlicher-ausschreibungen/) und auf [fragdenstaat.de](https://fragdenstaat.de/anfrage/kosten-des-neuen-prozesses-gegen-das-klimacamp/), Information über frühere Beschäftigung bei Linkedin-Profil von Pintsch. Die Kanzlei hieß früher „Puhle und Kollegen“, mittlerweile „pdrei Rechtswälte“.]


### RÜCKHALT IN DER BEVÖLKERUNG

Als Begründung für die Fortsetzung des Rechtsstreits hatte Oberbürgermeisterin Eva Weber mehrfach angeführt, dass die Zulässigkeit der dauerhaften Protestform grundsätzlich rechtlich geklärt werden müsse. Denn Weber sei vor Gruppen mit weniger sympathietragenden Anliegen besorgt, die sich ebenfalls über ein dauerhaftes Protestcamp in der Stadt Gehör verschaffen könnten.

Diese Sorge ist nach Ansicht der Klimacamper\*innen von Anfang an unbegründet und vorgeschoben. „Egal ob Kälte, Nässe oder kritische Leserbriefe: Um eine Dauerversammlung über so lange Zeit Tag und Nacht aufrecht zu erhalten, muss sie von einer vielfältigen Bewegung getragen werden, die viel Rückhalt in der Bevölkerung genießt“, so Pondorf. Die Aktiven im Klimacamp umfassen Schüler\*innen, Auszubildende, Student\*innen, Berufstätige und Rentner\*innen. „Wir sind keinesfalls ein seit Jahren eingeschworener Freundeskreis. Ständig kommen neue Personen zum Camp dazu. Wer sich für Klimagerechtigkeit engagieren möchte, ist im Klimacamp jederzeit willkommen und findet bei uns eine nette Gemeinschaft ohne Chef\*innen.“


### AUGSBURGER STADTREGIERUNG IM VERMEIDUNGSMODUS

„Ein Dilletantenstück! Die Stadt lenkt von den drängendsten Themen ab. Sie beschäftigt sich stattdessen lieber mit Zuständigkeiten, Rechts- und Verfahrensfragen. Wenn unsere Stadtregierung hofft, dass diese schlecht gemachte Farce unbemerkt bleibt, hat sie die Rechnung ohne das Klimacamp gemacht!“, so Lauter.

Nicht zuletzt sind die finanziellen und zeitlichen Investitionen der Stadtregierung in Gerichtsverfahren, die der Prüfung der Rechtmäßigkeit von verfassungskonformer Versammlungsfreiheit dienen, ein unmissverständliches Zeichen. Die Stadtregierung zieht dieses Verhalten vor, statt in die ausreichende Umsetzung des Grundrechts auf verfassungsrechtlichen Klimaschutz zu investieren, wie ihn das Bundesverfassungsgericht am 29. April 2021 bestätigte. (1 BvR 2656/18, 1 BvR 288/20, 1 BvR 96/20, 1 BvR 78/20) ([bundesverfassungsgericht.de](https://www.bundesverfassungsgericht.de/SharedDocs/Pressemitteilungen/DE/2021/bvg21-031.html))

Lauter fährt fort: „Wir fordern die Stadtregierung dazu auf, jetzt endlich inhaltlich in Sachen Klimaschutz aktiv zu werden!“

Pondorf schließt sich ihrer Mitstreiterin an: „'Wir campen bis ihr handelt!' war, ist und bleibt unser Motto. Die Stadt Augsburg bekannte sich zwar dazu, ihren Teil zur Einhaltung der 1,5-Grad-Grenze beizutragen und beschloss auf unseren Druck hin ein CO₂-Restbudget. Wenig überraschend bestätigte die von der Stadt in Auftrag gegebene KlimaKom-Studie aber, dass die wenigen bislang ergriffenen Maßnahmen unzureichend sind. Zuletzt wurden die Bus- und Trampreise zu Neujahr angehoben. Wie sollen Augsburger\*innen so auf den ÖPNV umsteigen? Das zeigt uns, dass politischer Druck für Klimagerechtigkeit wichtiger ist denn je.“

„Um die Forderungen und die Rechtmäßigkeit des Camps besonders zu betonen, werden wir in den nächsten Monaten eine ganze Bandbreite an kreativen Ausgestaltungsmöglichkeiten des Grundrechts auf Versammlungsfreiheit zeigen“, so Lauter. „Wir sind dankbar, mit Martina Sulzberger eine so kompetente und engagierte Anwältin an unserer Seite zu haben. Wir empfahlen sie daher schon oft an die vielen von uns inspirierten Klimacamps in anderen Städten weiter.“


### GLOBALSTREIK: „KEINE LEEREN VERSPRECHEN MEHR“

Das Klimacamp Augsburg lädt in diesem Sinne alle dazu ein, beim Fridays-for-Future-Globalstreik mit dem Motto „Keine leeren Versprechen mehr“ eine gerechte Klimapolitik zu fordern:

**Fridays-for-Future-Globalstreik am 25. März 2022. 16:00 Uhr, Ulrichsplatz**


#### ÜBERSICHT ÜBER FRÜHERE GERICHTSVERFAHREN DES KLIMACAMPS GEGEN DIE STADT

Vom Klimacamp gewonnen:

1. Eilverfahren gegen Räumungsbescheid am Verwaltungsgericht
2. Hauptverfahren gegen Räumungsbescheid am Verwaltungsgericht
3. Eilverfahren gegen Untersagung der ersten Fahrraddemo auf der B17 am Verwaltungsgericht
4. Eilverfahren gegen Untersagung der ersten Fahrraddemo auf der B17 am Verwaltungsgerichtshof
5. Hauptverfahren gegen Räumungsbescheid am Verwaltungsgerichtshof

Von der Stadt vorläufig gewonnen, weitere rechtliche Entwicklung noch ausstehend:

1. Eilverfahren gegen Abseilversammlung am Verwaltungsgericht
