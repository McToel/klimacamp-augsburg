---
layout: page
title: "28.10.2022: Besetzung der Regierung von Schwaben"
date: 2022-10-28 7:30:00 +0100
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
permalink: /pressemitteilungen/2022-10-28-sek-einsatz-bei-regierung-von-schwaben/
nav_order: 349
---

*Pressemitteilung vom Augsburger Klimacamp am 28.10.2022*

# Besetzung und SEK-Räumung bei der Regierung von Schwaben: Klimacamper ziehen Bilanz

Am Donnerstagvormittag (27.10.2022) besetzten Aktivist\*innen des
Augsburger Klimacamps für über drei Stunden das Vorzimmer des
Präsidialbüros der Regierung von Schwaben, „Klima-Aktivisten nehmen
Regierung von Schwaben aufs Korn“ titelte die AZ [1]. Anlass war die
vergangenen Samstag erfolgte Rodung eines Teils des Lohwaldes bei
Meitingen. Die Klimaaktivist\*innen werfen der Regierung von Schwaben
vor, den Bürgerinitiativen in und um Meitingen durch ihre im Verborgenen
ausgestellte Sondergenehmigung für die Rodung der Möglichkeit
einstweiligen Rechtsschutzes beraubt zu haben. Damit habe sich die
Regierung von Schwaben mit ihrem Präsidenten Erwin Lohner über den
Bayerischen Verwaltungsgerichtshof in München gestellt, an dem Klagen
zum Erhalt des Lohwalds anhängig waren und sind.

Beendet wurde die Aktion zur Mittagszeit durch das
Spezialeinsatzkommando (SEK) der Polizei, nachdem die Klimacamper\*innen
anders als bei der Fällung der 57 Bäume auf dem Reese-Areal nicht
freiwillig aufgeben wollten, bevor sich nicht Regierungspräsident Lohner
gegenüber der anwesenden Presse erklärte: „Ziel der Aktion war es, dass
Regierungspräsident Lohner erklärt, wieso er die Sondergenehmigung im
Geheimen ausstellte“, so Ingo Blechschmidt (34) vom Klimacamp. Das SEK
brachte zunächst die Aktivist\*in Charlie Kiehne (20), welche an der
Fassade protestierte, zu Boden, womit die Aktion des Klimacamps beendet
war. Dann verwendete das SEK aber auch noch Zwangsmittel, um
Blechschmidt und Samuel Bosch (19) aus dem Vorzimmer zu entfernen.
Blechschmidt und Bosch wurden in Handschellen hinter dem Rücken
abgeführt und zur Polizeiinspektion Süd gebracht. Dort wurde
Blechschmidt ähnlich wie bei seinem Einsatz für den Erhalt des Forst
Kastens bei München [2] eine Gewahrsamnahme unbekannter Dauer nach
Polizeiaufgabengesetz in Aussicht gestellt. „Es reicht nicht ganz“, habe
dann aber der zuständige Sachbearbeiter Blechschmidt verkündet. Nach
etwa drei Stunden kamen alle drei Aktivist\*innen wieder frei.

„Der SEK-Einsatz im Büro war unverhältnismäßig. Nachdem die Aktivist\*in
am Seil geräumt war, packten wir unsere Sachen um freiwillig zu gehen.
Komplett aus dem nichts wurden wir dann von hinten gepackt, auf den
Boden gepresst und mit dem Fuß auf dem Kopf und im Rücken fixiert. Wir
waren völlig perplex, weil dieses Maß an Gewalt unerwartet kam“,
so Blechschmidt nach dem Vorfall. Den gesamten Vormittag über
sei der Polizeieinsatz dagegen professionell verlaufen.

„Es ist eine Frechheit, Bürger\*innen von Meitingen und der angrenzenden
Gemeinden, die auf juristischem Wege versuchen, den Lohwald zu erhalten,
auf eine solche Art und Weise vor vollendete Tatsachen zu stellen. Das
ist undemokratisch. Das Gespann aus Stahlwerksbesitzer Max Aicher und
Regierungspräsident Erwin Lohner hebelte die Gewaltenteilung aus. Diese
Rechtswidrigkeit möchten wir auch gerichtlich feststellen lassen“, so
Blechschmidt weiter. Diese weitere Klage werden die Aktivist\*innen
zusammen mit BN-Anwältin Lisa Eberlein kommende Woche auf den Weg
bringen.

Sie kündigen außerdem weitere Aktionen zum Erhalt des Bannwaldes an.
„Dass ein Drittel das Waldes jetzt zerstört ist, heißt auch, dass wir
zwei Drittel noch retten können. Wälder sind unsere natürlichen
Verbündeten im Kampf gegen die Erdaufheizung. Um die Erdaufheizung zu
stoppen, brauchen wir jeden Baum dringend“, erklärt Kletteraktivist\*in
Kiehne, welche an der Fassade der Regierung von Schaben demonstrierte.

[1] [https://www.augsburger-allgemeine.de/augsburg-land/augsburg-meitingen-klima-aktivisten-nehmen-regierung-von-schwaben-aufs-korn-id64382891.html](https://www.augsburger-allgemeine.de/augsburg-land/augsburg-meitingen-klima-aktivisten-nehmen-regierung-von-schwaben-aufs-korn-id64382891.html)
[2] [https://www.sueddeutsche.de/muenchen/muenchen-forst-kasten-umweltschuetzer-1.5331295](https://www.sueddeutsche.de/muenchen/muenchen-forst-kasten-umweltschuetzer-1.5331295)

## HINWEIS ZU STELLUNGNAHME GEGENÜBER AZ

„Die Rodung des Lohwalds in Meitingen bedurfte keiner Genehmigung der
Regierung von Schwaben“, gab die Regierung von Schwaben gegenüber der AZ
an [1]. Diese Aussage ist wörtlich genommen korrekt, doch irreführend.
Das Rodungsvorhaben insgesamt wurde vergangenes Jahr durch den
Marktgemeinderat Meitingen bestätigt und Einzelheiten in einem Vertrag
mit den Stahlwerken festgehalten. Dass die Rodung aber schon vergangenen
Samstag stattfinden konnte, wurde erst durch die Sondergenehmigung
möglich. Wenn dem nicht so wäre, hätte sich das Stahlwerk ja nicht um
die Sondergenehmigung bemüht.

## FOTOS UND VIDEOS DER RÄUMUNG

[https://www.speicherleck.de/iblech/stuff/.rvs/messages.html](https://www.speicherleck.de/iblech/stuff/.rvs/messages.html)


## Anmerkungen

Angekündigt wurde die Aktion in der
[Pressemitteilung vom 27. Oktober 2022](/pressemitteilungen/2022-10-27-besetzung-der-regierung-von-schwaben/).

Der
[Tagebucheintrag vom 27.10.2022](/tagebuch/#donnerstag-27102022--tag-849)
erwähnt noch die Personalienkontrollen,
welche die Polizei unter den Personen vornahm,
die sich gegen 10 Uhr auf dem Fronplatz befanden.
Diese hatten es leider nicht in die Pressemitteilung geschafft.
