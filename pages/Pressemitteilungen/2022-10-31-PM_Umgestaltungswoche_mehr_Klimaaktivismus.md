---
layout: page
title: "31.10.2022: Umgestaltungswoche für mehr Klimaaktivismus"
date: 2022-10-31 13:00:00 +0100
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
permalink: /pressemitteilungen/2022-10-31-umgestaltungswoche-mehr-klimaaktivismus/
nav_order: 350
---

*Pressemitteilung vom Augsburger Klimacamp am 31.10.2022*

# Klimacamp beginnt professionelle Umgestaltungswoche für mehr Klimaaktivismus

- einwöchiges professionelles Gemeinschaftsprojekt aus der Alten Schmiede
  Augsburg, dem Architekten Jan Glasmeier und Studierenden der Hochschule Augsburg
- Camp soll für den Winter funktional gemacht und strukturiert werden
- Klimacamper bauen eigene öffentliche Fahrradständer nachdem die Stadt ihre entfernte
- Beliebte Selbsthilfe-Fahrradwerkstatt, über die sich CSU-Baureferent Gerd
  Merkle echauffierte, kann leider nicht erhalten werden

Am heutigen Montag (31.10.2022) begann das einwöchige Gemeinschaftsprojekt aus
der Alten Schmiede Augsburg, dem Architekten Jan Glasmeier und Studierenden des
Studiengangs Energieeffizientes Bauen und Planen (E2D) der Hochschule Augsburg
zur Umgestaltung des Augsburger Klimacamps. Das Klimacamp, das in den letzten
Wochen durch den Protest gegen die im Geheimen erteilte Sondergenehmigung der
Regierung von Schwaben zur Teilrodung des Meitinger Lohwaldes [1] und das
Engagement gegen die Baumfällungen der WBG auf dem Reese-Areal im öffentlichen
Fokus stand [2], soll in Zukunft noch bessere Bedingungen bieten für effektives
Engagement für eine klimagerechte Politik in Augsburg und darüber hinaus.


## KLIMACAMP INSTALLIERT EIGENE ÖFFENTLICHE FAHRRADABSTELLBÜGEL

Die Aktivist\*innen des Klimacamps möchten dabei auch eigene öffentliche
Fahrradabstellbügel installieren, und zwar direkt im vorderen Bereich, der vom
Versammlungsrecht geschützt ist. Der hintere Bereich mit den städtischen
Fahrradabstellbügeln und Parkplätzen zur Anlieferung und für Behinderte seien
oft durch Handwerker fürs Rathaus und andere Fahrzeuge im städtischen Auftrag
zugeparkt, so die Klimacamper\*innen. Bei Veranstaltungen im Rathaus stehen oft
mehr als zwei Fahrzeuge für Stunden auf dem Fischmarkt und behindern
Fußgänger\*innen den Zugang zu Gebäuden und Fahrradabstellbügeln; die
Verantwortlichen der Stadt unterbinden das nicht. Vivian Führer (18) vom
Klimacamp freut sich über das Timing des Gemeinschaftsprojekts, denn genau am
heutigen Montagmorgen wurden in Vorbereitung des Toilettenhäuschens für den
Weihnachtsmarkt die städtischen Fahrradabstellbügel entfernt.

Die beliebte Selbsthilfe-Fahrradwerkstatt des Klimacamps, über die sich
CSU-Baureferent Gerd Merkle kürzlich echauffierte [3] kann bei der Umgestaltung
leider nicht erhalten werden. Es gab sie seit mehr als 1½ Jahren. In vielen
anderen Städten gibt es öffentliche Fahrradreparaturstationen, die speziell für
den Außeneinsatz konzipiert sind, robust und funktionell. Dort ist es möglich,
sein Fahrrad aufzuhängen und mit dem Werkzeug, das vor Ort vorhanden ist, zu
reparieren. Aktuell bietet die Hochschule Augsburg zwei Stationen dieser Art an
[4]. Weitere, von der Stadt aufgestellte Stationen, könnten folgen, etwa neben
der öffentlichen Fahrradluftpumpe am Königsplatz.


## AUGSBURG ALS STADT DES WANDELS POSITIONIEREN

Dem Vorwurf, das Klimacamp sehe wie eine Müllhalde aus, entgegneten die
Klimacamper\*innen bisher die Aussage „warum sollte etwas schön sein, wenn man
gegen etwas demonstriert das nicht schön ist, nämlich die Klimapolitik“ [5].
Und auch jetzt wollen sich die Aktivist\*innen nicht einfach an das Stadtbild
anpassen, sondern umgekehrt im Klimacamp ihre Visionen für eine schönere und
bessere Stadt verwirklichen. „Augsburg kann durch das Klimacamp zu einem neuen
Ort gestaltet werden, das haben wir in der Vergangenheit gesehen. Wir möchten
keine Stadt, in der die Lebensqualität wegen zunehmender Hitze und zu wenig
Wasser- und Grünflächen sinkt, sondern wir möchten einen schönen Ort mit
nachhaltiger Infrastruktur, Stadtbegrünung und Platz für alle Menschen. Uns ist
es wichtig, dass Menschen sich in Augsburg wohlfühlen und Augsburg als Stadt
des Wandels gesehen wird. Es wird Zeit, das Augsburg sich anpasst und sich eine
nachhaltige Stadtkultur entwickelt“, so Vivian Führer (18) vom Klimacamp.

Nach wie vor erschwert die im Vergleich mit anderen Städten
versammlungsfeindliche Auslegung des Versammlungsrechts durch die
Ordnungsbehörde in Augsburg ansprechende Bauten auf dem Fischmarkt. Viele
Gestaltungsmöglichkeiten werden behördlich untersagt. So könne nur mit leichten
Materalien gebaut werden. Die von der Stadt ins Spiel gebrachte Idee eines
„Raum für Klimaaktivismus“ [6] scheint indes in der Versenkung verschwunden zu
sein. Nach Auskunft von Führer wurden weder das Klimacamp noch andere
Augsburger Initiativen für Klimagerechtigkeit diesbezüglich kontaktiert.

Darüber hinaus sorgen Starkwetterereignisse mit Sturm und Starkregen regelmäßig
für Schäden. Bereits vor einiger Zeit ersetzte das Klimacamp daher
Planendächer, die von Passant\*innen als störend genannt wurden, durch
Metalldächer. Diese Starkwetterereignisse treten bedingt durch die
Klimakatastrophe immer häufiger auf und werden auch bei vielen Augsburger\*innen
zum Beispiel in Kleingärten zunehmend für Schäden sorgen.


## PRIVATMEINUNG VON CSU-BAUREFERENT GERD MERKLE

Den Toilettencontainer, der für den Weihnachtsmarkt auf dem Fischmarkt
aufgestellt werden wird, stufen die Klimacamper\*innen für das Stadtbild als
weniger ansprechend ein als das Klimacamp in seiner neuen Gestaltung. „Ein
nüchterner Container trägt nicht zum dringend notwendigen öffentlichen Diskurs
über den Umgang der Stadt Augsburg mit der Klimakatastrophe und ihren Anteil
daran bei“, so Führers Mitstreiterin Charlotte Lauter (20). „Dass Herr Merkle
sich privat zu seinen Ansichten über das Klimacamp äußert, freut uns sehr. Dass
er in seiner Funktion als noch amtierender Baureferent die städtischen
Verantwortlichen in ihren Überlegungen beeinflussen möchte, halten wir dagegen
für einen nicht akzeptablen Interessenkonflikt.“ Merkle war durch seine
Forderung nach Überstundenauszahlung in sechsstelliger Höhe in die Kritik
geraten.

[1] [https://www.augsburger-allgemeine.de/augsburg-land/augsburg-meitingen-klima-aktivisten-nehmen-regierung-von-schwaben-aufs-korn-id64382891.html](https://www.augsburger-allgemeine.de/augsburg-land/augsburg-meitingen-klima-aktivisten-nehmen-regierung-von-schwaben-aufs-korn-id64382891.html)
<br>
[2] [https://www.br.de/nachrichten/bayern/klimacamp-aktivisten-besetzen-baeume-in-augsburg,TJprUnj](https://www.br.de/nachrichten/bayern/klimacamp-aktivisten-besetzen-baeume-in-augsburg,TJprUnj)
<br>
[3] [https://www.augsburger-allgemeine.de/augsburg/augsburg-christkindlesmarkt-klimacamp-soll-fuer-toiletten-platz-machen-id64397656.html](https://www.augsburger-allgemeine.de/augsburg/augsburg-christkindlesmarkt-klimacamp-soll-fuer-toiletten-platz-machen-id64397656.html)
<br>
[4] [https://www.hs-augsburg.de/Fahrradfreundliche-Hochschule.html](https://www.hs-augsburg.de/Fahrradfreundliche-Hochschule.html)
<br>
[5] [https://www.augsburger-allgemeine.de/bayern/augsburger-klimacamp-zwei-jahre-augsburger-klimacamp-aber-wo-sind-die-anderen-aktivisten-id63129941.html](https://www.augsburger-allgemeine.de/bayern/augsburger-klimacamp-zwei-jahre-augsburger-klimacamp-aber-wo-sind-die-anderen-aktivisten-id63129941.html)
<br>
[6] [https://ratsinfo.augsburg.de/bi/to020.asp?TOLFDNR=34341](https://ratsinfo.augsburg.de/bi/to020.asp?TOLFDNR=34341)
