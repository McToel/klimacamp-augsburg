---
layout: page
title: "27.10.2022: Besetzung der Regierung von Schwaben"
date: 2022-10-27 9:30:00 +0100
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
permalink: /pressemitteilungen/2022-10-27-besetzung-der-regierung-von-schwaben/
nav_order: 348
---

*Pressemitteilung vom Augsburger Klimacamp am 27.10.2022*

Nach umstrittener Ausnahmegenehmigung für Teilrodung vom Lohwald:
Klimacamper besetzen Regierung von Schwaben

- Regierungspräsident Erwin Lohner ließ Ausnahmegenehmigung für
  Teilrodung des Lohwalds erteilen
- Dabei war und ist eine Normenkontrollklage am VGH anhängig
- Die Klageparteien wurden der Möglichkeit geraubt, einstweiligen
  Rechtsschutz zu beantragen, da diese nicht informiert wurden und die
  Rodung im Verborgenen stattfand
- Klimacamper\*innen bringen in Kletteraktion Banner an der Regierung von
  Schwaben an, besetzen das Büro von Lohner und fordern, dass er sich
  für seine Missachtung des VGH verantwortet

„Lohwald-Rodung genehmigen trotz laufender Gerichtsverfahren? Frech!“
Diesen Ausspruch zierte am heutigen Donnerstagmorgen (27.10.2022) ein
Banner am Torbogen der Regierung von Schwaben (Fronhof 10, 86152
Augsburg). Dazu seilte sich eine Aktivist\*in des Augsburger Klimacamps
aus dem Präsidiumsbüro der Regierung von Schwaben ab, während andere das
Büro von Regierungspräsident Erwin Lohner besetzten. Von ihm fordern
sie, dass er sich für eine Ausnahmegenehmigung zur Teilrodung des
Meitinger Lohwalds verantwortet. Diese ließ Lohner trotz anhängiger
Klagen vor Bayerns höchstem Verwaltungsgericht erteilen. Wie sich der
ergebende Polizeieinsatz entwickelt, ist aktuell nicht absehbar.

Wie berichtet [1,2,3], erteilte die Regierung von Schwaben am 17.
Oktober 2022 den Lech-Stahlwerken eine Ausnahmegenehmigung (im Volltext
auf [lohwibleibt.de](https://www.lohwibleibt.de/)
veröffentlicht), damit diese am vergangenen Samstag
(22.10.2022) den ersten Teilabschnitt vom Lohwald roden konnten. Thomas
Frey, der Regionalreferent des BUND Naturschutz, sprach gegenüber BR von
einer „perfiden Aktion“ und einer „Watschen für die engagierte
Bürgerschaft“ [1]. Denn im über mehr als ein Jahr ausgehandelten Vertrag
zwischen der Gemeinde Meitingen und den Stahlwerken war vereinbart, dass
die Rodung erst erfolgen darf, wenn gewisse artenschutzrechtliche
Maßnahmen erfolgreich durchgeführt worden sind. Damit hätte, so die
Naturschützer, die Rodung nicht vor Herbst 2023 stattfinden können [*].

Die Regierung von Schwaben setzte sich mit ihrem Alleingang nicht nur
über die Gemeinde Meitingen hinweg, sondern auch über den Bayerischen
Verwaltungsgerichtshof in München. Bei diesem ist nämlich vom
„Bannwald-Bündnis Unterer Lech“, einem Bündnis aus mehreren Initiativen
(u.a. BUND Naturschutz, Bürgerinitiative Lech-Schmuttertal e.V.,
Aktionsgemeinschaft zum Erhalt des Bannwalds bei Meitingen e.V.,
Augsburger Klimacamp) eine Normenkontrollklage gegen den Vertrag
anhängig. Aufgrund der besonderen Bedeutung des Lohwalds und seinem
Status als rechtlich geschützter Bannwald rechnen sich die
Umweltschützer gute Chancen für ihre Klage aus: Der Lohwald sei für die
Anwohner\*innen als Lärm- und Emissionsschutz vor dem Stahlwerk
unbezahlbar, wichtig fürs Mikroklima und zur CO₂-Bindung, und Heimat
zahlreicher geschützter Arten. „Auf spekulative
Ersatzpflanzungsexperimente ist kein Verlass“, erklärt Ingo Blechschmidt
(34) vom Klimacamp. „Zudem vermag ein Feld kleiner Setzlinge für die
Anwohner\*innen keine der Schutzfunktionen des Bannwalds zu leisten.“

Die anhängige Normenkontrollklage entfaltet zwar keinen einstweiligen
Rechtsschutz gegen die Rodung. Dieser wäre nach Auffassung der
Aktivist\*innen auch nicht nötig gewesen, da nach dem abgeschlossenen
Vertrag die Rodung erst im Herbst 2023, nach Abschluss des Verfahrens am
VGH, hätte stattfinden können. Die Regierung von Schwaben erteilte ihre
Ausnahmegenehmigung ohne Anhörung oder Inkenntnissetzung der vor dem VGH
klageführenden Parteien. Dies war vermutlich rechtswidrig, so
Bündnis-Rechtsanwältin Lisa Eberlein nach einer erste Prüfung des
Sachverhalts, da das Bannwald-Bündnis so der Möglichkeit beraubt wurde,
gegen die Ausnahmegenehmigung einstweiligen Rechtsschutz zu beantragen.
Ein solcher Antrag hätte vermutlich Erfolg gehabt. Für die
Ausnahmegenehmigung bezahlten die Stahlwerke 250 Euro.

Die politische Verantwortung für diese Missachtung des höchsten
bayerischen Verwaltungsgerichts, wie die Aktivist\*innen vom Augsburger
Klimacamp die Genehmigungserteilung bezeichnen, trägt der Präsident der
Regierung von Schwaben, Erwin Lohner. 2010 würdigte die CSU ihn für
seine 50-jährige Mitgliedschaft [4]. Er war schon in Judikative,
Exekutive und Legislative tätig [5] und gilt als bestens vernetzt [6].
Stahlwerksbesitzer Max Aicher ist zusammen mit dem Lobbyverband VBM der
größte Spender der CSU Bayern [7]. Er spendet fast sieben Mal mehr als
der zweitgrößte Spender.

„Regierungspräsident Erwin Lohner verhökerte den Lohwald für 250 Euro
und setzte sich damit über das höchste bayerische Verwaltungsgericht
hinweg. Das ist undemokratisch und frech!“, so Ingo Blechschmidt (34).
„Der gesamte Vorgang lässt nur einen Schluss zu: Regierungspräsident
Erwin Lohner ließ nur deswegen die Ausnahmegenehmigung zur
Lohwald-Rodung erteilen, weil er aufgrund langjähriger Sponsoringgelder
von Stahlswerksbesitzer Max Aicher mit diesem freundschaftlich
verbandelt ist. Von Rechtsstaatlichkeit keine Spur“, so Blechschmidt.
Blechschmidt vermutet: „Erwin Lohner ist korrupt, auch wenn hier
vermutlich nur 250 Euro flossen und Stahlwerkschef Max Aicher den Rest
in Form freundschaftlicher Gefallen zahlen wird.“ Andere hochrangige
Manager vom Stahlwerk sitzen bereits wegen Bestechung in Haft [8].

Die Aktivist\*innen vom Augsburger Klimacamp hatten zuletzt mit einer
Notbesetzung von Bäumen auf dem Reese-Areal dem Thema Baumschutz
Aufmerksamkeit verschafft [9,10,11]. Nach knapp zweistündigen
Verhandlungen mit der Polizei hatten sie aufgegeben [12].

[1] [https://www.br.de/nachrichten/bayern/lech-stahlwerke-sorgen-mit-bannwald-rodung-fuer-kritik,TLBZdnV](https://www.br.de/nachrichten/bayern/lech-stahlwerke-sorgen-mit-bannwald-rodung-fuer-kritik,TLBZdnV)
<br>
[2] [https://www.augsburger-allgemeine.de/augsburg-land/meitingen-lechstahlwerke-roden-lohwald-bei-meitingen-trotz-anhaengiger-klagen-id64346931.html](https://www.augsburger-allgemeine.de/augsburg-land/meitingen-lechstahlwerke-roden-lohwald-bei-meitingen-trotz-anhaengiger-klagen-id64346931.html)
<br>
[3] [https://www.daz-augsburg.de/90948-2/](https://www.daz-augsburg.de/90948-2/)
<br>
[4] [https://www.wochenanzeiger-muenchen.de/m%c3%bcnchen/langjaehrige-mitglieder-geehrt,20429.html](https://www.wochenanzeiger-muenchen.de/m%c3%bcnchen/langjaehrige-mitglieder-geehrt,20429.html)
<br>
[5] [https://www.regierung.schwaben.bayern.de/ueber_uns/praesidium/rp/index.html](https://www.regierung.schwaben.bayern.de/ueber_uns/praesidium/rp/index.html)
<br>
[6] [https://www.augsburger-allgemeine.de/bayern/Neuer-Regierungspraesident-Ein-Mann-mit-besten-Beziehungen-id50987216.html](https://www.augsburger-allgemeine.de/bayern/Neuer-Regierungspraesident-Ein-Mann-mit-besten-Beziehungen-id50987216.html)
<br>
[7] [https://lobbypedia.de/wiki/CSU](https://lobbypedia.de/wiki/CSU) und dort verlinkte Rechenschaftsberichte der CSU
<br>
[8] [https://www.sueddeutsche.de/bayern/augsburg-prozess-lech-stahlwerke-urteil-meitingen-1.5323933](https://www.sueddeutsche.de/bayern/augsburg-prozess-lech-stahlwerke-urteil-meitingen-1.5323933)
<br>
[9] [https://www.daz-augsburg.de/klimacamp-stemmt-sich-gegen-baumfaellungen-auf-reese-gelaende/](https://www.daz-augsburg.de/klimacamp-stemmt-sich-gegen-baumfaellungen-auf-reese-gelaende/)
<br>
[10] [https://www.br.de/nachrichten/bayern/klimacamp-aktivisten-besetzen-baeume-in-augsburg,TJprUnj](https://www.br.de/nachrichten/bayern/klimacamp-aktivisten-besetzen-baeume-in-augsburg,TJprUnj)
<br>
[11] [https://www.augsburger-allgemeine.de/augsburg/augsburg-baumbesetzung-auf-reese-areal-beendet-id64200876.html](https://www.augsburger-allgemeine.de/augsburg/augsburg-baumbesetzung-auf-reese-areal-beendet-id64200876.html)
<br>
[12] [https://www.augsburger-allgemeine.de/augsburg/augsburg-widerstand-gegen-faellungen-deshalb-haben-die-baum-besetzer-aufgegeben-id64205311.html](https://www.augsburger-allgemeine.de/augsburg/augsburg-widerstand-gegen-faellungen-deshalb-haben-die-baum-besetzer-aufgegeben-id64205311.html)

[*] MdL Fabian Mehring (FW) gab auf sozialen Medien an, dass die
Stahlwerke die Rodung gemäß des geltenden Vertrags vorgenommen hätten.
Diese Aussage steht im Widerspruch dazu, dass die Stahlwerke bei der
Regierung von Schwaben eine Ausnahmegenehmigung beantragten. Diese wäre
nicht nötig gewesen, wenn die Rodung schon nach den Vertragsbestimmungen
möglich gewesen wäre.

HINWEIS
<br>
Die Aktion beginnt wenige Minuten nach 9:20 Uhr direkt bei der Regierung
von Schwaben. Polizeiliche Intervention, bis hin zu Räumung durch SEK,
ist wahrscheinlich. Falls Blechschmidt oder Kiehne nicht erreichbar sein
sollten, liegt das daran, dass die Handys konfisziert wurden. In diesem
Fall ist der Kontakt nur direkt vor Ort möglich. Zudem werden wir
versuchen, über den Infokanal vom Augsburger Klimacamp
(https://t.me/klimacamp_augsburg) Statusmeldungen durchzugeben, aber ob
das gelingt, ist auch nicht sicher.

Zur Vermeidung von Beschädigungen des Fenstersims verwenden wir einen
Kantenschoner. Zur Vermeidung eines Hängetraumas bei der abseilenden
Aktivist\*in kommt ein Holzbrett zum Einsatz. Zur Vermeidung von
Beschädigung der Fassade ist dieses mit Vlies ausgekleidet. Charlie
Kiehne und Samuel Bosch besitzen beide eine abgeschlossene
SKT-A-Ausbildung.


## Nachtrag

[Fotos und Videos zur freien Verwendung](https://www.speicherleck.de/iblech/stuff/.rvs/messages.html)

„Leider entschloss sich Regierungspräsident Lohner heute dafür, sich
nicht dafür zu verantworten, dass er sich mit seiner Rodungsgenehmigung
über das Bayerische Verwaltungsgericht hinwegsetzte. Er sprach nicht
einmal mit der anwesenden Presse“, so Blechschmidt.

Zum Ablauf der Aktion gibt es auch einen
[Bericht in unserem Tagebuch](/tagebuch/#die-erste-besetzung-der-regierung-von-schwaben)
sowie die nachfolgende
[Pressemitteilung vom 28. Oktober 2022](/pressemitteilungen/2022-10-28-sek-einsatz-bei-regierung-von-schwaben/).
