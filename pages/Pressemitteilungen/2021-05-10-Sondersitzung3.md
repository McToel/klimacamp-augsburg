---
layout: page
title:  "10.05.2021: Klimacamper*innen stellen mit Kunstaktion den klimapolitischen Tiefschlaf der Stadtregierung dar"
date:   2021-05-10 23:30:00 +0200
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
nav_order: 216
---

*Presseinformation vom Augsburger Klimacamp am 10.5.2021*

# Klimacamper\*innen stellen mit Kunstaktion den klimapolitischen Tiefschlaf der Stadtregierung dar

Am Montagnachmittag (10.5.2021, 12 Uhr) stellen
Klimagerechtigkeitsaktivist\*innen des Augsburger Klimacamps drei Schlafbetten
vor dem Rathaus auf, verkleiden sich als gerade aufwachende Politiker\*innen und
geben künstlerische Interviews, die auf realen Zitaten basieren. Damit wollen
sie auf das zu unzureichende Tempo der Augsburger Klimapolitik aufmerksam
machen. Für den 3. April hatte die Stadtregierung ursprünglich eine
Sonderstadtratssitzung zum Thema Klima angesetzt, dann aber auf den 11. Mai
verschoben.

"Unsere Stadtregierung, allen voran Oberbürgermeisterin Weber und
CSU-Fraktionschef Leo Dietz, befindet sich in einem klimapolitischen
Dauertiefschlaf!", erklärt Fabian Theenhaus (17) die Aktion. "Bei der bereits
verschobenen Sondersitzung werden nur kleine Maßnahmen beschlossen, die
wichtigen werden, wenn überhaupt, erst im Herbst behandelt."

Bei der Pressekonferenz am 7. Mai erklärte Oberbürgermeisterin Weber
wiederholt, dass Privatpersonen die größte Schuld an der Klimakrise treffe.
Etwa behauptete sie, Mieter\*innen ließen ihre Heizung unnötig laufen. Theenhaus
erwidert dazu: "Privatpersonen klimafreundliches Verhalten nahelegen ohne die
passenden Rahmenbedingungen hierfür zu schaffen, ist anscheinend der erste
Schritt unserer Stadtpolitik zu Klimagerechtigkeit. Das ist jedoch reine
Abwälzung der eigenen Verantwortung, kein ernstzunehmender Klimaschutz!
Verantwortung abgeben ist das einzige, das unsere Stadtregierung gut kann. Sie
verschläft die Klimakrise!"

Die drei inszenierten Politiker\*innen sind Oberbürgermeisterin Weber,
CSU-Fraktionschef Leo Dietz sowie Grünen-Fraktionsvorsitzende Verena von
Mutius-Bartholy. "Die CSU konstruiert permanent einen Scheinwiderspruch
zwischen Klima und Wirtschaft", erklärt Stefanie Bauer (18) diese
Personenauswahl. "Aber auch von der Augsburger Grünen-Stadtratsfraktion sind
wir maßlos enttäuscht. Klimagerechtigkeitsaktivist\*innen aus Ravensburg und
Passau schwärmen davon, wie sie mit den dortigen Grünen gemeinsam Aktionen des
zivilen Ungehorsams planen. In Augsburg treten die Grünen gemeinsam mit
der CSU aufs Klima-Bremspedal."

## Fotos und Videos zur freien Verwendung
https://www.speicherleck.de/iblech/stuff/.schlafmuetze
(wird im Laufe des Tages gefüllt)

## Kontakt
Ingo Blechschmidt (+49 176 95110311)
