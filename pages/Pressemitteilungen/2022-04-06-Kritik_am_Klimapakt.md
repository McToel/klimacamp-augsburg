---
layout: page
title:  "06.04.2022: Klimacamp Augsburg kritisiert den „Klimapakt“ als neue Greenwashing-Kampagne der Stadt Augsburg"
date:   2022-04-06 10:00:00 +0100
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
nav_order: 305
---

*Pressemitteilung des Augsburger Klimacamps am 6.4.2022*

# Klimacamp Augsburg kritisiert den „Klimapakt“ als neue Greenwashing-Kampagne der Stadt Augsburg


Am heutigen Mittwoch (6.4.) findet im Goldenen Saal die feierliche
Unterzeichnung des städtischen „Klimapakts“ statt. Die Stadtregierung
möchte damit Voraussetzungen für mehr Klimaschutz in Augsburg schaffen.
Aktivist\*innen des Augsburger Klimacamps stehen indes dem Vorhaben indes
mit großer Skepsis gegenüber. Die Stadt würde von ihrer eigenen
Verantwortung ablenken, eine Bühne für Greenwashing bieten und die
großen Rahmenbedingungsverbesserungen für mehr Klimagerechtigkeit
weiterhin den Bürger\*innen vorenthalten.

Die Klimacamper\*innen wüssten es zu schätzen, dass das Thema beim
OB-Referat so präsent sei. Beratungsangebote für Unternehmen und
Akteure, die es ernst meinen, könnten eine wertvolle Ressource werden.
Es sei richtig, Unternehmen einzubeziehen, nicht zuletzt weil der
Konkurrenzdruck sie zu oft nötigt, auf Kosten von Umwelt oder ihrer
Arbeiter\*innen und Angestellten zu wirtschaften.


## STÄDTISCHES VORHABEN UNGLAUBWÜRDIG

Doch primär hagelt es Kritik: „Der Klimapakt ist leider nur das nächste
Element der städtischen Verzögerungspolitik in Sachen Klimaschutz: große
Töne klopfen, im gleichen Atemzug aber Verantwortung abgeben und selbst
weiterhin keine Klimaschutzmaßnahmen ergreifen“, so Franziska Pux (27)
vom Klimacamp.

Die Aktiven nehmen in ihrer Kritik auf eine Äußerung von
Oberbürgermeisterin Weber Bezug [1]. „Mit ‚wir alle‘ und ‚nur gemeinsam‘
meint Oberbürgermeisterin Weber immer andere, aber nicht die Stadt und
keinesfalls sich selbst“, erklärt Pux. „Oder bekamen wir die
weitreichenden städtischen Beschlüsse zur Vergünstigung von Bus und
Tram, zur Einrichtung neuer Buslinien sowie zum Ausbau des
Fernwärmenetzes nur nicht mit? Um die Klimakatastophe abzumildern,
benötigt es aber ein sofortiges Handeln der Stadt!“

Der „Klimapakt“ biete nach Ansicht der Klimacamper\*innen vor allem eine
Fläche für die Stadt und die unterzeichnenden Unternehmen, Greenwashing
zu betreiben, also sich ein umweltfreundliches und Image zu verleihen,
ohne dass es dafür eine hinreichende Grundlage gäbe. Tatsächlich
durchzieht diese Doppelmoral viele der Paktunternehmen: Zum „Klimapakt“
gehören unter anderem MT Aerosprace und ein Automobilzuliefer. Das mache
das städtische Vorhaben unglaubwürdig.

Der motorisierte Individualverkehr werde in kleinen Teilen immer nötig
bleiben, gerade auch auf dem Land, habe nach Meinung des Klimacamps aber
in einem Zusammenschluss namens ‚Klimapakt‘ nichts verloren. „Da sollte
es um Mobilität der Zukunft gehen!“ Zum Pakt gehört auch MT Aerospace,
ein Konzern, der unter anderem Komponenten für verschiedene militärische
Raketen herstellt [2]. „Klimagerechtigkeit und Frieden gehen Hand in
Hand und benötigen sich gegenseitig, Rüstungskonzerne sind in einem
‚Klimapakt‘ fehl am Platz“, so Pux.


## LECHWERKE, STAHLWERKE UND STADTSPARKASSE NUTZEN GREENWASHING-PLATTFORM

Den Kern des „Klimapakt“ machen aber Konzerne wie die Lechwerke, die
Stadtwerke und die Stadtsparkasse aus, die sich nach Aussage der
Klimaschützer bisher alle eher durch Greenwashing-Kampagnen statt mit
echtem Klimaschutz einen Namen machten. Über ihr Wertpapierhaus „Deka
Invest“ investiert die Sparkasse sogar ganz direkt in Kohle- und
Ölunternehmen und ermöglicht so, dass diese die Klimakrise weiter
befeuern [3]; Stadt- und Lechwerke beziehen weit über dem
deutschlandweiten Durchschnitt Kohlestrom [4].

Auch Augsburgs Wohnbaugruppe, ebenfalls Unterzeichnerin des Klimapakts,
bekleckert sich in Klimafragen nicht mit Ruhm. „Mit viel Elan verhindert
sie, dass Mieter\*innen kleine Balkonsolaranlagen installieren [5], sogar
wenn diese sie auf eigene Kosten einrichten möchten, und im städtischen
Klimabeirat treten sie immer wieder bremsend auf“, so Pux.


## REFERENZEN

[1] Weber: „Klimaschutz ist keine Haltung. Klimaschutz ist die Aufgabe
unserer Zeit. Und wir alle müssen einen Beitrag dazu leisten. Die
Augsburger Unternehmen stellen sich dieser Aufgabe zum Teil schon seit
vielen Jahren und sind mit Innovation und Technologie wichtige Partner,
wenn es darum geht, den CO₂-Ausstoß in unserem Stadtgebiet zu
verringern. Nur gemeinsam können wir unsere ambitionierten
Klimaschutzziele erreichen.“<br>
--
[https://www.augsburg.de/aktuelles-aus-der-stadt/detail/klimapakt-augsburger-wirtschaft-wird-am-6-april-unterzeichnet](https://www.augsburg.de/aktuelles-aus-der-stadt/detail/klimapakt-augsburger-wirtschaft-wird-am-6-april-unterzeichnet)

[2] [https://www.forumaugsburg.de/s_2kommunal/Friedensstadt/200815_blockade-von-premium-aerotec-werk-iii-zum-friedensfest/index.htm](https://www.forumaugsburg.de/s_2kommunal/Friedensstadt/200815_blockade-von-premium-aerotec-werk-iii-zum-friedensfest/index.htm)

[3] [https://www.urgewald.org/medien/klimaschutz-selbstverpflichtung-sparkassen-laesst-konkreten-fahrplan-vermissen](https://www.urgewald.org/medien/klimaschutz-selbstverpflichtung-sparkassen-laesst-konkreten-fahrplan-vermissen)

[4] [https://www.sw-augsburg.de/fileadmin/content/6_pdf_Downloadcenter/1_Energie/swa_Strom-Mix.pdf](https://www.sw-augsburg.de/fileadmin/content/6_pdf_Downloadcenter/1_Energie/swa_Strom-Mix.pdf)
<br>
(438 Gramm CO2 pro kWh bei den Stadtwerken, 310 Gramm im bundesweiten
Durchschnitt)

[5] Das hat eine lange Geschichte und ist ein eigener Skandal in sich.
Wir vermitteln gerne Kontakt zu den relevanten Personen, etwa
Geschäftsführer Mark Hoppe von der WBG (diese Klimaschutz- und
Geldeinsparungsmaßnahme ausbremsend) oder Xenia Drost (eine Mieterin der
WBG, die sich bei den Parents for Future engagiert, gerne eine eigene
Balkonsolaranlage installieren würde und das Projekt vorantreibt).


KONTAKT<br>
Ingo Blechschmidt (+49 176 95110311)
