---
layout: page
title: "06.10.2022: Nach Dienstaufsichtsbeschwerde: Polizei kann Widersprüche im Fehlverhalten der Abteilung 'Staatsschutz' nicht auflösen"
date: 2022-10-06 23:00:00 +0100
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
permalink: /pressemitteilungen/2022-10-06-polizei-kann-widersprueche-nicht-aufloesen/
nav_order: 341
---

*Pressemitteilung vom Augsburger Klimacamp am 6.10.2022*

# Nach Dienstaufsichtsbeschwerde: Polizei kann Widersprüche im Fehlverhalten der Abteilung 'Staatsschutz' nicht auflösen

In dem überregional als „Pimmelgate Süd“ bekannt gewordenen Skandal
[1,2,3] um den Eingriff der Abteilung ‚Staatsschutz‘ der Augsburger
Polizei in den intimen Schutzbereich des ehemaligen Bundestagskandidaten
und Klimaaktivisten Alexander Mai (26), über den mittlerweile sogar die
New York Times berichtete [4], liegt nun die polizeiliche Stellungnahme
zu der von Mai gestellten Dienstaufsichtsbehörde vor. Sie vermag
Widersprüche im Fehlverhalten der Abteilung 'Staatsschutz' nicht
aufzulösen.

Im April diesen Jahres hatte die Abteilung „Staatsschutz“ die Wohnung
von Mai durchsucht, seine privaten technischen Geräte sowie seinen
Arbeitslaptop konfisziert und sich so Zugang zu großen Teilen der
vertraulichen digitalen Kommunikation der Augsburger
Klimagerechtigkeitsbewegung verschafft. Die Abteilung „Staatsschutz“
begründete ihren Übergriff damit, Mais Urheberschaft an einem
Facebook-Link zu einem Zeitungsfoto nachweisen zu wollen – obwohl daran
kein Zweifel bestand, da Mai den Kommentar bereits ein halbes Jahr zuvor
unter seinem vollen Namen veröffentlichte und dies auch nicht bestritt.
Wie im Nachgang des Vorstoßes bekannt wurde [5,6,7,8], geht die
Abteilung „Staatsschutz“ regelmäßig mit Hausdurchsuchungen gegen
friedlichen Klimagerechtigkeitsaktivismus in Augsburg vor. Die
Aktivist\*innen und zahlreiche Medien sehen darin einen Versuch der
Einschüchterung.

In seiner Dienstaufsichtsbeschwerde bat Mai um Aufklärung, wie es dazu
kommen konnte, dass er während der umstrittenen Hausdurchsuchung zu
keinem Zeitpunkt seine Anwältin anrufen durfte, obwohl er die ganze Zeit
über auf diesem Recht bestand. Das Brisante dabei: Das von zwei der
anwesenden Polizisten unterschriebene polizeiliche
Durchsuchungsprotokoll [9] bestätigt Mais Aussage, dass ihm das
Telefonat mit seinem Rechtsbeistand verwehrt wurde. Im Widerspruch dazu
behauptete die Pressestelle der Polizei gegenüber der Süddeutschen
Zeitung und Netzpolitik.org, Mai sei ein Diensthandy für den Anruf zur
Verfügung gestellt worden. Die der Redaktion vorliegende Stellungnahme
der Polizei zu Mais Dienstaufsichtsbeschwerde wiederholt diese
Behauptung wörtlich. Sie vermag daher nicht, den Widerspruch aufzulösen.
Für die geschwärzte Veröffentlichung des Durchsuchungsprotokolls
handelte sich Mai dagegen ein zweites Ermittlungsverfahren gegen ihn ein
[10].


## Erst verschleppt, dann ungründlich ermittelt?

Mai und seine Mitstreiter\*innen vom Augsburger Klimacamp reagieren auf
die Stellungnahme mit einem förmlichen Widerspruch, den sie auch online
auf der dafür eingerichteten Webseite „pimmelgate-süd.de“ [transparent
zur Verfügung stellen](https://www.pimmelgate-süd.de/pressemappe/alex-durchsuchung/reaktion-auf-polizeiliche-stellungnahme-zu-dab.pdf).
Darin fordern sie auch Auskunft über die Aussage der städtischen Zeugin, die
der Hausdurchsuchung beiwohnte. Sie – da ist sich Mai sicher – könne
bestätigen, dass ihm sein Recht, seine Anwältin zu kontaktieren,
rechtswidrigerweise verwehrt wurde. Die Stellungnahme enthält indes keinen
Hinweis auf eine Befragung der städtischen Zeugin im Rahmen der
dienstaufsichtlichen Ermittlungen gegen die vier Beamten.

Nachdem Mais ursprüngliche Dienstaufsichtsbeschwerde nach der
persönlichen Übergabe innerhalb der Polizei „verloren ging“, wie Mai
zwei Wochen später auf telefonische Nachfrage erfuhr, hofft er nun im
zweiten Anlauf auf gründliche Ermittlungstätigkeit der Polizei. Viel
Hoffnung hat er dabei nicht: „Unser aktuelles Polizeisystem legt allen
Polizeibeamten, die ihren Dienst am Gemeinwohl gewissenhaft verfolgen
möchten, Steine in den Weg. An der Stelle einer gesunden Fehlerkultur
stehen stattdessen strenge Hierarchien und Gruppendruck. Andernfalls
kann ich mir nicht erklären, wie es sein kann, dass von vier anwesenden
Beamten kein einziger seine Kollegen zur Seite nimmt und sie darauf
hinweist, dass Betroffenen von Hausdurchsuchungen ein Anruf bei der
Anwältin zusteht? Zusätzlich ermittelt im Nachgang keine unabhängige
Behörde, sondern die Polizei gegen die Polizei. Diese wiederholt dabei
sogar dann noch widersprüchliche Aussagen, wenn deren Unwahrheit
schriftlich, öffentlich und polizeilich dokumentiert ist.“

Mai weist auch daraufhin, dass viele Bürger\*innen bei rechtswidrigem
Verhalten der Polizei weit weniger Glück im Unglück haben als er. Ein
Gutachten bestätigte vor wenigen Tagen [11], dass die Polizeigewalt
ursächlich für den Tod eines in Mannheim betrauerten 47-jährigen
Patienten war. Der Fall sorgte bundesweit für Entsetzen und führte auch
in Augsburg zu Demonstrationen für ein besseres Polizeisystem [12]. Nur
acht Tage später starb in Mannheim ein weiterer Hilfebedürftiger durch
die Hand des Polizeisystems [13].

[1] [https://netzpolitik.org/2022/augsburg-pimmel-kommentar-fuehrt-zu-razzia-bei-klimaaktivisten/](https://netzpolitik.org/2022/augsburg-pimmel-kommentar-fuehrt-zu-razzia-bei-klimaaktivisten/)
<br>
[2] [https://www.sueddeutsche.de/bayern/augsburg-pimmelgate-klimacamp-polizei-1.5566562](https://www.sueddeutsche.de/bayern/augsburg-pimmelgate-klimacamp-polizei-1.5566562)
<br>
[3] [https://www.augsburg.tv/mediathek/video/wirbel-um-hausdurchsuchung-klimacamp-aktivisten-kritisieren-polizeieinsatz/](https://www.augsburg.tv/mediathek/video/wirbel-um-hausdurchsuchung-klimacamp-aktivisten-kritisieren-polizeieinsatz/)
<br>
[4] [https://www.nytimes.com/2022/09/23/technology/germany-internet-speech-arrest.html](https://www.nytimes.com/2022/09/23/technology/germany-internet-speech-arrest.html)
<br>
[5] [https://www.br.de/nachrichten/bayern/wenn-der-augsburger-staatsschutz-im-kinderzimmer-steht,T6NGbJ1](https://www.br.de/nachrichten/bayern/wenn-der-augsburger-staatsschutz-im-kinderzimmer-steht,T6NGbJ1)
<br>
[6] [https://www.stern.de/gesellschaft/janika-pondorf---ich-war-wegen-dieses-polizeieinsatzes-in-der-klinik--31895656.html](https://www.stern.de/gesellschaft/janika-pondorf---ich-war-wegen-dieses-polizeieinsatzes-in-der-klinik--31895656.html)
<br>
[7] [https://www.daz-augsburg.de/jkjlkj/](https://www.daz-augsburg.de/jkjlkj/)
<br>
[8] [https://forumaugsburg.de/s\_1aktuelles/2022/06/22\_wenn-der-staat-sich-schuetzt.htm](https://forumaugsburg.de/s\_1aktuelles/2022/06/22\_wenn-der-staat-sich-schuetzt.htm)
<br>
[9] [https://www.pimmelgate-süd.de/pressemappe/alex-durchsuchung/durchsuchungsprotokoll.pdf](https://www.pimmelgate-süd.de/pressemappe/alex-durchsuchung/durchsuchungsprotokoll.pdf)
<br>
[10] [https://netzpolitik.org/2022/pimmelgate-sued-augsburger-polizei-ueberzieht-klimaaktivisten-mit-weiterem-verfahren/](https://netzpolitik.org/2022/pimmelgate-sued-augsburger-polizei-ueberzieht-klimaaktivisten-mit-weiterem-verfahren/)
<br>
[11] [https://www.swp.de/baden-wuerttemberg/tod-nach-polizeieinsatz-ermittler\_-mann-starb-an-atembehinderung-nach-polizeikontrolle-in-mannheim-66544665.html](https://www.swp.de/baden-wuerttemberg/tod-nach-polizeieinsatz-ermittler\_-mann-starb-an-atembehinderung-nach-polizeikontrolle-in-mannheim-66544665.html)
<br>
[12] [https://www.forumaugsburg.de/s\_3themen/Repression/220509\_47-jaehriger-verstirbt-in-mannheim-nach-polizeigewalt-kundgebung-in-augsburg/index.htm](https://www.forumaugsburg.de/s\_3themen/Repression/220509\_47-jaehriger-verstirbt-in-mannheim-nach-polizeigewalt-kundgebung-in-augsburg/index.htm)
<br>
[13] [https://www.zeit.de/gesellschaft/zeitgeschehen/2022-05/mannheim-polizeigewalt-psychisch-kranke](https://www.zeit.de/gesellschaft/zeitgeschehen/2022-05/mannheim-polizeigewalt-psychisch-kranke)
