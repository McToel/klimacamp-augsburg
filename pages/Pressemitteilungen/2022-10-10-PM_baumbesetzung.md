---
layout: page
title: "10.10.2022: Notbesetzung für einen Erhalt der Bäume auf dem Reese-Areal"
date: 2022-10-10 23:00:00 +0100
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
permalink: /pressemitteilungen/2022-10-10-baumbesetzung/
nav_order: 342
---

*Pressemitteilung vom Augsburger Klimacamp am 9.10.2022*

# Klimacamper\*innen folgen Hilferuf einer Anwohnerin und protestieren mit einer Notbesetzung für einen Erhalt der Bäume auf dem Reese-Areal

* Sanierung nach Studien klimafreundlicher als Abriss und Neubau [1]
* Besondere kulturelle Bedeutung des Altbestands
* Nach Überzeugung der Aktivist\*innen sollte dringend bezahlbarer
  Wohnraum geschaffen werden, und zwar primär durch Nutzung von Leerstand
  und Umwidmung von Bestandsgebäuden

„Leerstand verwenden, Rodungswahn beenden! Bestandsbauten sanieren,
Altbaumbestand integrieren“. Unter diesem Motto besetzten am heutigen
Montagmorgen Aktivist\*innen des Augsburger Klimacamps die
fällungsbedrohten Bäume auf dem Reese-Areal, nachdem sie am
Sonntagmorgen durch eine Anwohnerin auf die bevorstehende Fällungen
aufmerksam gemacht wurden. Wie die Augsburger Allgemeine am Freitag
berichtete [2], hätten dort heute die Fällarbeiten beginnen sollen, um
den Weg für den umstrittenen Abriss der letzten verbliebenen
Kasernengebäude an der Sommestraße freizumachen. Nun konnten die Bäume
vorerst gerettet werden. Wie sich die Situation in den nächsten Tagen
entwickelt, ist derzeit nicht absehbar. Aufgrund der Kürze der
Vorbereitungszeit baten die Augsburger Klimacamper\*innen auch erfahrene
Kletteraktivist\*innen aus anderen Städten zur Hilfe.


## FÄLLUNGEN ZWAR MIT GRÜNAMT, NICHT JEDOCH MIT KLIMACAMP ABGESTIMMT

Nach dem Vorbild einer früheren Baumschutzaktion in Gersthofen [3]
errichteten die Klimacamper\*innen teils provisorische Baumhäuser, teils
besetzten sie die Bäume mit einfachen Hängematten. Mit einem Banner,
dass das zentrale Aktionsmotto ziert, machen sie darauf aufmerksam, dass
neu zu schaffender Wohnraum nicht primär durch Neubau, sondern durch
Nutzung der zahlreichen leerstehenden Gebäude in der Innenstadt und
durch Umwidmung existierender Flächen bereitgestellt werden kann, und
dass dafür in Zeiten der Klimakrise erst recht kein alter Baumbestand
abgeholzt werden dürfe. Eine Studie des renommierten Wuppertal-Instituts
bestätigt ein Argument der Klimaschützer: Sanierung ist
klimafreundlicher als Neubau [1]. Die Aktivist\*innen stellen sich dem
„Neubau-Wahnsinn“, wie sie ihn nennen, sowie der „Phantasie von
CSU-Baureferent Gerd Merkle“ entgegen, dass gesunde, alte und ökologisch
wertvolle Bäume lediglich banale Designelemente seien.


## Umstrittenes Neubauprojekt

Auf dem Gebiet der ehemaligen Reese-Kaserne möchte die Wohnbaugruppe die
letzten verbliebenen Gebäude abreißen und an ihrer Stelle später neue
bauen. „Bislang hat die WBG nie nachvollziehbar erklärt, warum überhaupt
eine Fällung der Bäume erforderlich sei“, so Ingo Blechschmidt (34) vom
Klimacamp. Die von der WBG in Aussicht gestellten
Neupflanzungsexperimente überzeugen Blechschmidt nicht: Falls solche
Experimente gedeihen, dauere es 80 bis 100 Jahre, bis die anfangs
winzigen Setzlinge dieselben Funktionen übernehmen können wie die
wertvollen Bestandsbäume. Wie das Handelsblatt berichtete [4],
bestätigt, was die Schutzfunktion der CO₂-Bindung angeht, eine
vielzitierte Studie der Universität Hamburg diese Aussage. Blechschmidt
betont, dass das Klimacamp keineswegs pauschal gegen alle geplanten
Baumfällungen vorgehe. Die Fällungen längs der Bahntrasse etwa ließ das
Klimacamp zu [5].

Der bereits abgerissene Gebäudebestand wurde umfangreich kulturell
genutzt; gegen den Abriss stemmten sich große Teile der Kulturszene und
mehrere Bürger\*innenbewegungen. „Leider blieb ein Kurswechsel der Stadt
seit mehreren Jahren aus. Wir nutzen die letzte Chance, den Bäumen und
dem ehemaligen Kulturort die Aufmerksamkeit zukommen zu lassen, die sie
verdienen“, so Blechschmidt. „Gewissermaßen ein Aufbäumen der
Verdammten.“


## Umstrittene Äußerungen von WBG-Geschäftsführer Mark Hoppe

WBG-Geschäftsführer Mark Hoppe steht im Klimacamp nicht nur wegen seiner
Abrisspläne in der Kritik. Wie ein Mitglied der *Parents for Future*
Augsburg auch bei der Bürgerversammlung im Oktober 2021 ausführte, legt
die WBG Mieter\*innen diverse Hürden in den Weg, wenn diese auf eigene
Kosten Balkonsolarkraftwerke installieren möchten. Aus Sicht der
Aktivist\*innen sei das in Zeiten der Klimakrise und steigender
Energiekosten unverständlich. Vielmehr sollte die WBG ihre Mieter*innen
zur Installation solcher Solarmodule ermuntern und sie darin auch
finanziell unterstützen.

[1] [https://wupperinst.org/a/wi/a/s/ad/7671](https://wupperinst.org/a/wi/a/s/ad/7671)
<br>
[2] [https://www.augsburger-allgemeine.de/augsburg/augsburg-platz-fuer-neue-wohnungen-die-rodungsarbeiten-im-kulturpark-west-starten-id64181716.html](https://www.augsburger-allgemeine.de/augsburg/augsburg-platz-fuer-neue-wohnungen-die-rodungsarbeiten-im-kulturpark-west-starten-id64181716.html)
<br>
[3] [https://www.augsburger-allgemeine.de/augsburg-land/Gersthofen-Klima-Aktivisten-kaempfen-um-Baeume-am-Gersthofer-Festplatz-id59011896.html](https://www.augsburger-allgemeine.de/augsburg-land/Gersthofen-Klima-Aktivisten-kaempfen-um-Baeume-am-Gersthofer-Festplatz-id59011896.html)
<br>
[4] [https://www.swp.de/lokales/ulm/protest-in-ulm-umweltaktivisten-besetzen-uniwald_-im-baumhaus-gegen-bettenhaus-62807819.html](https://www.swp.de/lokales/ulm/protest-in-ulm-umweltaktivisten-besetzen-uniwald_-im-baumhaus-gegen-bettenhaus-62807819.html)
<br>
[5] [https://www.klimacamp-augsburg.de/pressemitteilungen/2022-09-24-baumfaellung-in-hochzoll/](https://www.klimacamp-augsburg.de/pressemitteilungen/2022-09-24-baumfaellung-in-hochzoll/)


## Fotos

![](/pages/material/images/2022-10-10_baumbesetzung/2022-10-10_baumbesetzung_1_medium.jpg)
![](/pages/material/images/2022-10-10_baumbesetzung/2022-10-10_baumbesetzung_2_medium.jpg)
![](/pages/material/images/2022-10-10_baumbesetzung/2022-10-10_baumbesetzung_3_medium.jpg)
![](/pages/material/images/2022-10-10_baumbesetzung/2022-10-10_baumbesetzung_4_medium.jpg)
![](/pages/material/images/2022-10-10_baumbesetzung/2022-10-10_baumbesetzung_5_medium.jpg)
![](/pages/material/images/2022-10-10_baumbesetzung/2022-10-10_baumbesetzung_6_medium.jpg)
