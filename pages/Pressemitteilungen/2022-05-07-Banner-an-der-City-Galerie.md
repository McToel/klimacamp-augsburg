---
layout: page
title:  "07.05.2022: Augsburger Klimacamp fordert von City-Galerie Hausverbot für klimaschädliche Produktion"
date:   2022-05-07 10:00:00 +0100
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
permalink: /pressemitteilungen/2022-05-07-banner-an-city-galerie/
nav_order: 311
---

*Pressemitteilung vom Augsburger Klimacamp am 7.5.2022*

# Augsburger Klimacamp fordert von City-Galerie Hausverbot für klimaschädliche Produktion

Am heutigen Samstag (7.5.) gab es um 17:00 Uhr es vom Klimacamp eine
Kletteraktion zum Thema Klimagerechtigkeit an der City-Galerie
(angehängtes Foto zur freien Verwendung). Beteiligt waren Kim Schulz
(25), der vergangenen Mittwoch wegen eines Banners am Rathaus vor
Gericht stand, sowie Charlie Kiehne (20) und Samuel Bosch (19), die das
Brandenburger Tor besetzten.

"Der City-Galerie ist die tödliche Klimakrise egal", so Charlie Kiehne
(20). "Solange der Gesetzgeber Lieferkettengesetze verwässert, müsste
die City-Galerie selbst auf Lieferketten achten. CO2-intensive
Produktion gehört verboten, insbesondere wenn sie durch Kinderarbeit
zustande kommt."

"Die City-Galerie sollte der Klimakrise ein Hausverbot erteilen!", sind
sich Samuel Bosch (19) und Kim Schulz (25) einig, die sich beide mit der
heutigen Aktion über ein aufgrund früherer Klimaproteste an der
City-Galerie erteiltes Hausverbot hinwegsetzen.
