---
layout: page
title:  "27.08.2021: Bürgerbündnis Mobilitätswende – Raddemo für klimagerechten kostenfreien ÖPNV anstatt Tariferhöhungen"
date:   2022-03-08 22:00:00 +0200
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
nav_order: 223
---

*Pressemitteilung vom „Bürgerbündnis Mobilitätswende“ am 27. August 2021*

# Raddemo für klimagerechten kostenfreien ÖPNV anstatt Tariferhöhungen
 
Das Bürgerbündnis Mobilitätswende veranstaltet am Sonntag
den 29. August 2021 einen Fahrrad-Demonstrationszug
durch das Augsburger Stadtgebiet.
Die Veranstalter\*innen fordern vom Augsburger Verkehrs- und
Tarifverbund (AVV), sowie von Landkreis und Stadt Augsburg
eine Reduzierung der Ticketpreise im Augsburger Nahverkehr
stufenweise bis hin zu einem kostenfreien ÖPNV (auch genannt Nulltarif).
Die Kundgebung beginnt um 14 Uhr am Ulrichsplatz.
 
"Für eine klimagerechte Mobilitätswende brauchen wir
einen attraktiven und sozial gerechten ÖPNV.
Dazu gehört neben dem schnellstmöglichen Ausbau des Streckennetzes
auch der Nulltarif
um allen Menschen klimafreundliche Mobilität zu ermöglichen.
Tariferhöhungen sind ein Schritt in die falsche Richtung und
befördern den klimaschädlichen motorisierten Individualverkehr"
so Stefanie Bauer (19) .
 
Zuletzt wurden die Preise für den Augsburger Nahverkehr zu 1. Juli 2021
um insgesamt etwa  3,2 Prozent erhöht.
Dies geschah trotz des coronabedingt reduzierten Taktes der Tramlinien [1]. 
 
"Natürlich ist uns klar, dass kostenfreier ÖPNV
nicht von heute auf morgen geht,
das würde die räumlichen und finanziellen Kapazitäten sprengen.
Deshalb schlagen wir ein Stufenmodell vor,
das sofort mit dem 365€ Ticket für alle beginnt.
In den kommenden Jahren wird stufenweise der Nulltarif
für verschiedene Bevölkerungsgruppen eingeführt.
Dies könnten sozial schwache Menschen, Personen die ihr Auto abwracken
(allgemeine Abwrackprämie: Freifahrschein), Senioren, Pendler*innen
sowie Kinder und Jugendliche sein" so Bauers Mitstreiter Samuel Bosch (18).
Ute Grathwohl (48) ergänzt: "So haben auch Stadt und Tarifverbund
genug Zeit, um den Ausbau des ÖPNV voranzutreiben
um den steigenden Fahrgastzahlen gerecht zu werden,
zum Beispiel mit Schnellbuslinien wie im Verkehrskonzept 4.0 vorgeschlagen,
um Lücken im Netz zu schließen während das Tramnetz erweitert wird.
Schnellbusse sind unkompliziert und erfordern nur wenig Umbaumaßnahmen.
Deshalb fordern wir den AVV auf, eine Verdopplung der Fahrgastzahlen
als Grundlage für die Erstellung des Nahverkehrsplans 2025 herzunehmen." [2] .
 
"Stadt und Verband argumentieren immer
mit der fehlenden Finanzierbarkeit des Nulltarif.
Ein deutschlandweiter Nulltarif würde laut Branchenverband VDV
den Staat jährlich 12 Milliarden Euro kosten [3].
Diese Kosten können leicht durch den Wegfall der Pendlerpauschale
in Höhe von rund 5,2 Milliarden Euro [4]
sowie Teileinsparungen der jährlichen 7,8 Milliarden Euro Kosten
für das Dieselprivileg  [5],
der Steuerbefreiung von Dienstwagen in Höhe von 4,6 Milliarden Euro [6]
und die staatliche Subvention der privaten Nutzung von Dienstwagen
mit 3 Milliarden Euro [7] ausgeglichen werden.
Mit anderen Worten: Der Nulltarif finanziert sich durch den Wegfall
anderer staatlicher Subventionen von selbst,
ganz zu Schweigen vom Wegfall des Verwaltungsaufwandes
und der Kosten im Fahrscheinwesen" so Nico Kleitsch (21).
 
Der etwa 10 km lange familienfreundliche Streckenverlauf der Kundgebung
führt vom Ulrichsplatz über Rathausplatz und Jakobertor
zur Berliner Allee und von dort über Amagasaki und Nagahama Allee
und B300 zur Schertlinstraße und anschließend über Gögginger Straße
und Herrmanstraße zum Bahnhof und anschließend wieder über die Karlstraße
zurück zum Ulrichsplatz.
 
 
Quellen:<br>
[1] https://www.augsburger-allgemeine.de/augsburg/Augsburg-AVV-erhoeht-die-Preise-Fahrgaeste-in-Bus-und-Tram-muessen-ab-Juli-mehr-zahlen-id59839626.html<br>
[2] https://www.verkehr4x0.de/<br>
[3] https://www.berliner-zeitung.de/mensch-metropole/gratis-nahverkehr-kostenlos-busfahren-in-dieser-brandenburger-stadt-gibts-das-laengst-li.19447?pid=true<br>
[4] https://www.wiwo.de/politik/deutschland/berufspendler-pendler-kosten-den-fiskus-fuenf-milliarden-euro/20654060.html<br>
[5] http://www.klimaretter.info/mobilitaet/nachricht/23512-umweltbundesamt-fordert-ende-der-diesel-subvention<br>
[6] https://www.klima-allianz.de/fileadmin/user_upload/Dateien/Daten/Publikationen/Positionen/2011_06_Positionspapier_Firmenwagen.pdf<br>
[7] https://www.nachhaltig-links.de/images/DateienJ2/1_Mobilitaet/2_PDF/2018/7307_LinkeBTF_Nulltarif_A5_4c_Web.pdf<br>
 
Informationen zur Route:<br>
Ulrichsplatz, Maximilianstraße, Karolinenstraße, Leonhardsberg, Pilgerhausstraße, Bei der Jakobskirche,
Jakoberstraße, Lechhausener Straße, Berliner Allee, Amagasaki Allee, Nagahama
Allee, Inverness Allee, Haunstetter Straße, Schertlinstraße, Hochfeldstraße, Von-Der-
Tann-Straße, Morellstraße, Gögginger Straße, Hermanstraße,
Halderstraße, Viktoriastraße, Prinzregentenstraße, Am Alten Einlaß, Grottenau, Ludwigstraße,
Karlstraße, Karolinenstraße, Maximilianstraße, Ulrichsplatz<br>
Mehrspurige Straßen (z.B. Inverness Allee, Gögginger Straße, Karlstraße, ...) werden auf beiden Fahrbahnen einer Richtung befahren.
