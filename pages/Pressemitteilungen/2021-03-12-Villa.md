---
layout: page
title:  "12.03.2021: Klimagerechtigkeitsaktivist*innen demonstrieren mit Banneraktion bei Diesel-Villa für bezahlbaren und zukunftsgerechten Wohnraum"
date:   2021-03-12 01:00:00 +0200
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
nav_order: 207
---

*Pressemitteilung vom Augsburger Klimacamp am 12. März 2021*

# Klimagerechtigkeitsaktivist\*innen demonstrieren mit Banneraktion bei Diesel-Villa für bezahlbaren und zukunftsgerechten Wohnraum

Sperrfrist Samstag (13.3.2021) 6:00 Uhr
{: .label .label-red }

<img src="https://www.speicherleck.de/iblech/stuff/villa.jpeg" style="width: 100%">

In der Nacht vom Freitag (12.3.) auf den Samstag (13.3.) brachten
Klimagerechtigkeitsaktivist\*innen ein Banner an einem Baum auf dem Grundstück
der von Abbruchplänen bedrohten Diesel-Villa
(Hochfeldstraße 15) an: "Solidarische Wohnräume und lebendige Stadtbäume
statt Profit-Albträume". Der neue Eigentümer und Immobilieninvestor
Maximilian Wolf plant den Abriss der Diesel-Villa und einen
CO2-intensiven Neubau mit teuren Mietwohnungen und einer Tiefgarage [1].
Dieser Neubau würde, so Augsburgs Klimacamper\*innen, den Charme des
Viertels verletzen und trotzdem keinen Beitrag zu sozialer Gerechtigkeit
leisten.

Die Klimacamper\*innen fordern nicht nur den Erhalt der mehr als 100 Jahre
alten Rotbuche (nicht auf dem Foto abgebildet), die
nach Aussage der Expert\*innen der Baum-Allianz Augsburg "zusammen mit
den anderen Bäumen auf dem Grundstück Grundlage für ein reichhaltiges
und diverses Ökosystem in diesem Bereich des Stadtviertels bildet" [2].

"Der Kampf für Klimagerechtigkeit berührt auch Themen wie
Wohnungspolitik. Gesellschaftliche Veränderungen, wie sie zur
Abmilderung der Klimakrise notwendig sind, gehen nur Hand in Hand mit
sozialer Gerechtigkeit!", so Klimacamperin Gwendolyn Rautenberg (19).
"Eigentum verpflichtet. Herr Wolf sollte die vielfältig nutzbare
Diesel-Villa sanieren und in ein soziales Wohnprojekt wandeln. So könnte
die Villa die Wohnungsnot in Augsburg lindern. Die Stadt sollte das
Projekt bezuschussen und künftig im gesamten Stadtgebiet Baum- und
Umweltschutz mit erschwinglichen grünen Wohnungsprojekten kombinieren."

[1] https://www.augsburger-allgemeine.de/augsburg/Umkaempfte-Villa-in-Augsburg-wird-zum-Fall-fuer-die-Feuerwehr-id59161661.html<br>
[2] https://baumallianz-augsburg.de/archive/1944
