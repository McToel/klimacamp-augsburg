---
layout: page
title:  "11.05.2022: Verkehrswendeplan für Augsburg"
date:   2022-05-11 10:00:00 +0100
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
permalink: /pressemitteilungen/2022-05-11-verkehrswende-augsburg/
nav_order: 312
---

*Pressemitteilung von Augsburger Verkehrswendeaktivist\*innen am 11.05.2022*

# Verkehrswendeaktivist\*innen enthüllen während Verkehrsmodellversuch eigenen Verkehrswendeplan für Augsburg

* Pressekonferenz am Freitag (13.05.2022) um 10:00 Uhr in der Hallstraße
* umfassendes integratives Verkehrsneuordnungskonzept verschiedener Akteure
* nicht nur abstrakte übergeordnete Ziele, sondern Vielzahl an konkreten Maßnahmen
* Vor und nach der Pressekonferenz über Versammlungsrecht durchgesetzter
  Verkehrsmodellversuch: autofreie Hallstraße während der Pause des
  Holbein-Gymnasiums (9:30 Uhr)

Aktivist\*innen verschiedener Initiativen (zahlreiche freie
Verkehrswendeaktivist\*innen, Menschen aus dem Umfeld des Klimacamps und
anderen Initiativen) arbeiteten in den vergangenen Monaten einen
umfassenden Verkehrswendeplan aus, der den Autoverkehr deutlich
verringert und Fuß-, Fahrrad- und ÖPNV-Verbindungen erheblich ausbaut.
Diesen Freitag (13.05.2022) wird der Verkehrswendeplan erstmals der
Öffentlichkeit vorgestellt.

„Unser Verkehrswendeplan zieht verschiedene bekannte Konzepte und
Maßnahmen zur Verbesserung und Steigerung der Attraktivität von Stadt,
ÖPNV und klimafreundlicher Mobilität mit ein“, so Ute Grathwohl (49).
„So ist der Plan nicht nur eine breite Ideensammlung, sondern vor allem
ein umfassendes Konzept zur Umsetzung der Empfehlungen der von der Stadt
in Auftrag gegebenen KlimaKom-Studie, zur Umsetzung der vom Stadtrat
beschlossenen Emissionsziele für den Klimaschutz und zur Reduktion des
Automobilverkehrs um 50%. Unser Verkehrswendeplan zeigt, wie attraktive
klimafreundliche Mobilität wirklich aussehen kann.“ Die Gefahren des
Autoverkehrs sowie Stau und Feinstaubbelastung durch Reifen- und
Bremsenabrieb könnten durch eine reine Antriebswende nicht gelöst
werden, „ganz abgesehen vom hohen Energie- und Ressourcenverbrauch durch
diese Fortbewegungsart“.

Florian Lenz (21) ergänzt: „Zu Fuß, mit dem Rad und einem guten ÖPNV
kämen wir entspannter, oft schneller und klimafreundlich ans Ziel. Für
die Mobilitätswende ist ein gut ausgebauter, schneller und gut
vernetzter Nahverkehr und ein gut ausgebautes Radwegenetz unabdingbar.
Ein Umstieg vom eigenen Auto auf den öffentlichen Nahverkehr ist aktuell
für viele keine echte Alternative, da zu oft Orte nicht schnell und
direkt verbunden sind.“

Zentral ist für die Aktivist\*innen ein Grundnetz miteinander verbundener
echter Fahrradstraßen quer durch die Innenstadt mit weiterführenden
Achsen von dort in alle Ortsteile sowie um die Altstadt herum – soweit
möglich auf vierspurigen Straßen vom Autoverkehr abgetrennt auf zwei
eigenen Spuren, dies sei etwa möglich zwischen Schertlinstraße, Rotes
Tor und Theodor-Heuss-Platz.

Dabei sei keinerlei weitere Flächenversiegelung nötig, stattdessen werde
der vorhandene Verkehrsraum umverteilt. Weiter vom Plan vorgesehen sei
eine echte autofreie Innenstadt mit attraktiven Flaniermeilen zur
Stärkung von Aufenthaltsqualität und Einzelhandel, ein Ausbau des
Tramliniennetzes, etwa nach Hochzoll Süd und Richtung Hammerschmiede
durch die Karlstraße und von Göggingen über Messe zum Innovationspark
oder, wie schon lange geplant, nach Haunstetten Süd.

Außerdem brauche es die Reaktivierung und den Neubau von Bahnstrecken
und Bahnhalten, so die Aktivist\*innen, etwa an der Hirblinger Straße
oder an der Fußball-Arena, und die Einführung der S-Bahn im Taktverkehr
in den direkten Umlandgemeinden sowie den Nulltarif, fahrscheinloses
Fahren für alle. Da nicht alle Maßnahmen zeitgleich umgesetzt werden
können, geben die Aktivist\*innen in ihrem Verkehrswendeplan dabei auch
einen zeitlichen Fahrplan an. „Kurzfristig möglich ist die Etablierung
von Schnellbussen auf den Bundesstraßen mit enger Taktung im AVV-Gebiet
zur Verknüpfung von DB-Bahnhöfen, Straßenbahnästen und Randzielen.“


VERKEHRSMODELLVERSUCH WÄHREND PRESSEKONFERENZ: AUTOFREIE HALLSTRASSE

Seit mittlerweile mehreren Jahrzehnten ist eine autofreie Hallstraße im
Gespräch; es ist ein lang gehegtes Begehren der Schulfamilie ums
Holbein-Gymnasium. „Daher zeigen wir direkt während der Pausen des
Holbein-Gymnasiums, wie die autofreie Hallstraße aussehen könnte: als
sicherer Schulweg, als Freiraum für Schüler\*innen und als Bestandteil
einer zentralen autofreien Fahrradstraßenverbindung durch die
Innenstadt“, so Grathwohl. (Die erste Pause beginnt um 09:30.)

Der Plan werde in den nächsten Monaten unter Einbeziehung vieler
Menschen und Stimmen weiter ergänzt, so Lenz. Auf Basis des
Verkehrswendeplans sind zahlreiche Aktionen geplant, um die
Verkehrswende in Augsburg voran zu bringen.

Insbesondere wird es in den einzelnen Stadtvierteln
Bürger\*innenkonferenzen geben. Der Verkehrswendeplan wird mit
zahlreichen Aktionen einen Arbeitsschwerpunkt der Augsburger
Verkehrswendeszene bilden. Dazu gehören vielfältige angemeldete
Demonstrationen, Verkehrsversuche, Vorträge und Workshops sowie weitere
Aktionen.


INFORMATIONEN ZUM VERKEHRSWENDEPLAN

ausführliche Homepage zum Verkehrswendeplan:
[https://www.verkehrswende-augsburg.de](https://www.verkehrswende-augsburg.de)

Flyer zum Verkehrswendeplan (1. Auflage, Stand Mai 2022):
[https://www.verkehrswende-augsburg.de/assets/Flyer%20Verkehrswendeplan%20Augsburg.pdf](https://www.verkehrswende-augsburg.de/assets/Flyer%20Verkehrswendeplan%20Augsburg.pdf)


QUELLEN

Zum Bahnhalt an der Fußball Arena, AZ Artikel von 2008:
[https://www.augsburger-allgemeine.de/augsburg/Plaene-fuer-neues-FCA-Stadion-Mit-der-Bahn-zur-impuls-arena-id3953381.html](https://www.augsburger-allgemeine.de/augsburg/Plaene-fuer-neues-FCA-Stadion-Mit-der-Bahn-zur-impuls-arena-id3953381.html)
<br>
Zum Bahnhalt an der Hirblinger Straße:
[https://www.augsburger-allgemeine.de/augsburg/augsburg-bahnausbau-haelt-der-zug-bald-in-der-hirblinger-strasse-id62490491.html](https://www.augsburger-allgemeine.de/augsburg/augsburg-bahnausbau-haelt-der-zug-bald-in-der-hirblinger-strasse-id62490491.html)
<br>
Zur Tram nach Haunstetten Süd und zur Reduktion von 4 auf 2 Autospuren in Haunstetten:
[https://www.augsburger-allgemeine.de/augsburg/Augsburg-Alte-B17-Nur-noch-zwei-statt-vier-Spuren-in-Haunstetten-id52713511.html](https://www.augsburger-allgemeine.de/augsburg/Augsburg-Alte-B17-Nur-noch-zwei-statt-vier-Spuren-in-Haunstetten-id52713511.html)
<br>
Was ist eine Fahradstraße (offizielles Erklärvideo vom Hamburger Senat):
[https://youtu.be/YS_WC21Zims](https://youtu.be/YS_WC21Zims)
