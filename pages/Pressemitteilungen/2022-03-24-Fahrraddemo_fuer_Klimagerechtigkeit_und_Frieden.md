---
layout: page
title:  "24.03.2022: Fahrraddemonstration über B17 für autofreien Sonntag und Frieden -- bundesweiter Aktionstag"
date:   2022-03-24 10:00:00 +0100
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
nav_order: 303
---

*Pressemitteilung von unabhängigen Klimagerechtigkeitsaktivist*innen am 24.3.2022*

# Fahrraddemonstration über B17 für autofreien Sonntag und Frieden -- bundesweiter Aktionstag

Diesen Sonntag (27. März) gibt es eine Fahrrad-Demo über die B17. Es ist
Augsburgs erste kombinierte Demonstration zu den verknüpften Themen
Frieden und Klimagerechtigkeit. Auch für die Einführung eines
Tempolimits sowie autofreier Sonntage wird demonstriert. Start der
Demonstration ist um 14:00 Uhr am Augsburger Königsplatz.

Eingebettet ist die Aktion in einen dezentralen Aktionstag für Frieden
und Klimagerechtigkeit [2]. An diesem Tag finden in ganz Deutschland
verschiedene Events, Aktionen und Informationsveranstaltungen statt.

„Klimagerechtigkeit und Frieden sind untrennbar verknüpft“, erklärt
Charlotte Lauter (19) die Demonstration. „Die Erdaufheizung wird in
Zukunft zahlreiche Ressourcenkriege befeuern. Hätten wir die
Energiewende nicht so sehr verschleppt, wären wir heute nicht abhängig
von Putins Gas. Energie könnten wir dann direkt in der Region erzeugen,
anstatt Putin jeden Tag Millionen für seine Kriegsindustrie zu
überweisen“. Die täglichen Gesamtexporteinnahmen Russlands für Öl und
Gas belaufen sich auf 700 Millionen Euro [1].

„Gerade Augsburg sollte sich als Friedensstadt stärker für ein
Tempolimit auf A8 und B17 einsetzten. Den Fuß vom Gaspedal zu nehmen,
freut nicht nur den privaten Geldbeutel bei den aktuellen Spritpreisen.
Auch die Umwelt, Mensch und Tier danken“, so
Klimagerechtigkeitsaktivistin Charlotte Lauter (19). Deshalb ist eine
der zentralen Forderungen, auf die am Sonntag verwiesen wird:

**Verkehrswende jetzt! Tempo 100 auf der A8 zwischen Ulm und München und
Tempo 50 im Stadtgebiet Augsburg auf der B17! Autofreier Sonntag jetzt!**

Weitere Demostrecken über die Bundestraße hatten in den vergangenen
Monaten bereits für Aufsehen gesorgt und zahlreichen Augsburger\*innen
eine Plattform für ihre Kritik an der „infantilen Klimapolitik des
Aufschiebens und Abwartens“ gegeben. „Weil die Regierung bei wirksamem
Klimaschutz und der Ausbremsung von Kriegen durch den Kauf von fossilen
Brennstoffen versagt, sorgen wir selbst dafür, dass die B17 am Sonntag
autofrei ist und zeigen die – hoffentlich bald reale – Utopie des
autofreien Sonntags auf.“

Die Demonstration richtet sich auch direkt an Augsburgs Stadtregierung,
die Stadtwerke sowie die Lechwerke. „Dass die Stadt immer wieder die
Bus- und Trampreise erhöht, ist sowas von kontraproduktiv! Wie sollen
Menschen denn da auf die Öffis umsteigen, wenn sich das Auto noch
rentabel anfühlt?“, fragt Lauter. „Der Ausbau von erneuerbaren Energien
ist überfällig. Warum beziehen Stadtwerke und Lechwerke immer noch
Kohle- und Gasstrom?“

Wer sich mit einem verkehrssicher befestigten Demoschild auf dem Fahrrad
der Radldemo anschließen möchte, ist am Sonntag herzlich willkommen. Um
14:00 Uhr fährt der Radlzug am Augsburger Königsplatz los (Route: Kö -
Hermanstr. - Gögginger Str. - Eichleiterstr - B17 - Bürgermeister
Ackermann Straße, Rosenaustraße, Pferseer Unterführung, Fröhlichstr. und
zurück zum Königsplatz).

[1] [https://www.spiegel.de/ausland/putin-sichert-energielieferungen-zu-a-e1718411-aa5d-4f3c-90c2-b7ae8f6a047a](https://www.spiegel.de/ausland/putin-sichert-energielieferungen-zu-a-e1718411-aa5d-4f3c-90c2-b7ae8f6a047a)<br>
[2] [https://www.ende-gelaende.org/aktionstag-27-03-aufruf/](https://www.ende-gelaende.org/aktionstag-27-03-aufruf/)


FOTOS ZUR FREIEN VERWENDUNG<br>
[https://www.speicherleck.de/iblech/stuff/.b17](https://www.speicherleck.de/iblech/stuff/.b17)
(aktuell nur mit einem Foto einer vergangenen B17-Fahrraddemo gefüllt)


WEITERE TERMINE

1. Freitag 25.3. 14:00 vor der Patrizia AG in der Fuggerstraße:
   24-Stunden-Mahnwache
   [https://augsburgfueralle.noblogs.org/](https://augsburgfueralle.noblogs.org/)
   für fairen Wohnraum und gegen Immobilienspekulation

2. Freitag 25.3. 16:00 Ulrichsplatz: Globaler Klimastreik für
   Klimagerechtigkeit

3. Freitag 25.3. 18:10 Rathausplatz: Critical Mass — entspannt
   Fahrrad fahren durch die Stadt

4. Samstag 26.3. 13:00 Rathausplatz: Demonstrationszug: Wohnraum für
   alle mit verschiedenen sinnbildlichen Orten Augsburgs als
   Zwischenstationen

5. Sonntag 27.3. 14:00 Königsplatz: Die erste kombinierte Fahrraddemo
   über die B17 für die eng verknüpften Themen Frieden und
   Klimagerechtigkeit. Tempolimit auf A8 und B17 und Investitionen in
   Pflege, Bildung und öffentlichen Nahverkehr und den Ausbau von
   Erneuerbaren statt Aufrüstung und fossile Energien. Mobilitätswende
   jetzt! Autofreier Sonntag jetzt!


KONTAKT
Ingo Blechschmidt (+49 176 95110311)
