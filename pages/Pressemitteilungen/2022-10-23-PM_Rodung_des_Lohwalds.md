---
layout: page
title: "23.10.2022: Rodung des Lohwalds bei Meitingen"
date: 2022-10-23 23:00:00 +0100
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
permalink: /pressemitteilungen/2022-10-23-rodung-des-lohwalds/
nav_order: 345
---

*Pressemitteilung vom Augsburger Klimacamp am 23.10.2022*

# Trotz schwebenden Verfahrens: Stahlwerke roden ein Drittel des rechtlich geschützten Lohwalds bei Meitingen

Als Aktivist\*innen des Augsburger Klimacamps am vergangenen Samstag
(22.10.2022) einem Hilferuf eines Meitinger Bürgers folgten und den
nächsten Zug zum Lohwald bei Meitingen nahmen, wartete dort schon ein
Großaufgebot der Polizei auf sie. Der Bürger hatte die Aktivist\*innen
auf den Plan gerufen, weil er samstagfrüh Rodungsarbeiten im Lohwald
entdeckte – obwohl gegen den von CSU und FW unterstützten Bebauungsplan
der Lech-Stahlwerke am Bayerischen Verwaltungsgerichtshof in München
gleich drei Klagen anhängig sind, eine davon von einem Bündnis zum
Schutz des Bannwalds [1].

Die Stahlwerke möchten durch die Bannwaldrodung den wirtschaftlichen
Wert des Areals erhöhen. Auf einem kleinen Teil der Rodungsfläche soll
eine weitere Anlage entstehen, für die restliche Fläche sind Pläne in
Arbeit, aber noch nicht öffentlich vorgelegt. Für die Anwohner\*innen
bedeutet der Lohwald einen Schutz vor dem Lärm und den Emissionen des
Stahlwerks und einen wichtigen Beitrag zum Mikroklima, die spekulative
Anpflanzungsexperimente aus kleinen Setzlingen nicht zu leisten
vermögen.

Die Polizei erklärte ihr Aufgebot damit, „konrete Hinweise auf die
Begehung von Straftaten“ zu haben. „Für einen Moment hofften wir, dass
die Polizei angesichts der schwebenden Gerichtsverfahren gekommen war,
um den Wald zu schützen“, so Ingo Blechschmidt (34) vom Klimacamp. „Wir
mussten dann aber schnell feststellen, dass die Polizei uns meinte statt
die Rodungsarbeiten. Stahlwerksbesitzer Max Aicher schafft Fakten. Von
Respekt vor dem Bayerischen Verwaltungsgericht keine Spur!“ Da die CSU
sonst immer auf Ordnung poche, ging man bei den Aktivist\*innen davon
aus, dass vor der Entscheidung des Bayerischen Verwaltungsgerichtshof
nicht mit einer Rodung zu rechnen sei. Daher kritisieren die
Aktivist\*innen auch CSU-Bürgermeister Michael Higl: „Als demokratisch
gewählter Bürgermeister sollte man auch demokratisch legitimierte
Gerichte respektieren“, so Blechschmidt.


RECHTLICH GESCHÜTZTER BANNWALD

Der Lohwald bei Meitingen ist eigentlich ein nach dem Bayerischen
Waldgesetz rechtlich besonders geschützter Bannwald; im Herbst 2021
beschloss der Meitinger Marktgemeinderat dennoch seine Rodung. Im
Vorfeld dieser Entscheidung gab es vielfältige Proteste, etwa eine
Menschenkette, an der sich hunderte Bürger\*innen beteiligten [2,3], oder
von Förster\*innen angebotene Waldspaziergänge [4] und diverse Kletter-
[5,6] und Laseraktionen [7] von einer Aktivistengruppe namens „Wald
statt Stahl“, zu der mehrere Einzelpersonen des Augsburger Klimacamps
gehören.

Der Rodungsbeschluss gilt als umstritten, nicht nur weil Anwohner\*innen
mit Verweis auf spekulative Ersatzpflanzungsexperimente vertröstet
werden, sondern auch weil Stahlwerksbesitzer Max Aicher zusammen mit dem
Lobbyverband VBM der größte Spender der CSU ist [8,9]. „Higl ist
unchristlich, unsozial und frech“, resümiert Blechschmidt. Higl stelle
das Parteisponsoring durch Aicher über christliche Werte wie den Erhalt
der Schöpfung.

Details zum rechtlichen Status der am Samstagmorgen vorgenommenen Rodung
sind derzeit nicht bekannt. Das Bannwald-Bündnis kündigte eine
Stellungnahme im Laufe der Woche an, nachdem ihre Anwältin Lisa Eberlein
Gelegenheit hatte, bei Gerichten relevante Informationen einzuholen.

[1] [https://www.br.de/nachrichten/bayern/naturschuetzer-klagen-gegen-bannwaldrodung-bei-lech-stahlwerken,TJa04mR](https://www.br.de/nachrichten/bayern/naturschuetzer-klagen-gegen-bannwaldrodung-bei-lech-stahlwerken,TJa04mR)
Zum Bannwald-Bündnis gehören der BUND Naturschutz,
die Bürgerinitiative Lech-Schmuttertal e.V.,
die Aktionsgemeinschaft zum Erhalt des Bannwalds bei Meitingen e.V.,
das Augsburger Klimacamp und weitere Initiativen.
<br>
[2] [https://www.augsburger-allgemeine.de/augsburg-land/Meitingen-Hunderte-protestieren-lautstark-fuer-den-Erhalt-des-Lohwalds-bei-Meitingen-id59211951.html](https://www.augsburger-allgemeine.de/augsburg-land/Meitingen-Hunderte-protestieren-lautstark-fuer-den-Erhalt-des-Lohwalds-bei-Meitingen-id59211951.html)
<br>
[3] [https://web.archive.org/web/20210302042933/https://www.br.de/nachrichten/bayern/bannwaldrodung-fuer-lech-stahlwerke-umweltschuetzer-demonstrieren,SQK6YTp](https://web.archive.org/web/20210302042933/https://www.br.de/nachrichten/bayern/bannwaldrodung-fuer-lech-stahlwerke-umweltschuetzer-demonstrieren,SQK6YTp)
<br>
[4] [https://www.augsburger-allgemeine.de/augsburg-land/Meitingen-Meitinger-Klima-Aktivisten-bleiben-diesmal-mit-beiden-Beinen-am-Boden-id59668431.html](https://www.augsburger-allgemeine.de/augsburg-land/Meitingen-Meitinger-Klima-Aktivisten-bleiben-diesmal-mit-beiden-Beinen-am-Boden-id59668431.html)
<br>
[5] [https://www.augsburger-allgemeine.de/augsburg-land/Meitingen-Aktivisten-setzen-erneut-Zeichen-gegen-Lohwald-Rodung-id59472581.html](https://www.augsburger-allgemeine.de/augsburg-land/Meitingen-Aktivisten-setzen-erneut-Zeichen-gegen-Lohwald-Rodung-id59472581.html)
<br>
[6] [https://www.augsburger-allgemeine.de/augsburg-land/Meitingen-So-bereiten-sich-die-Klima-Aktivisten-auf-Aktionen-im-Meitinger-Lohwald-vor-id59477346.html](https://www.augsburger-allgemeine.de/augsburg-land/Meitingen-So-bereiten-sich-die-Klima-Aktivisten-auf-Aktionen-im-Meitinger-Lohwald-vor-id59477346.html)
<br>
[7] [https://www.augsburger-allgemeine.de/bilder/Laser-Laseraktion-Wald-statt-Stahl-Lohwald-Bannwald-LWS-id59799181.html?aid=59798651](https://www.augsburger-allgemeine.de/bilder/Laser-Laseraktion-Wald-statt-Stahl-Lohwald-Bannwald-LWS-id59799181.html?aid=59798651)
<br>
[8] [https://lobbypedia.de/wiki/CSU](https://lobbypedia.de/wiki/CSU)
<br>
[9] [https://www.lohwibleibt.de/](https://www.lohwibleibt.de/)

## FOTOS

Angehängt Fotos der Rodungsfläche zur freien Verwendung.

![](/pages/material/images/2022-10-23_lohwald/2022-10-23_093353_lohwald.jpg)
![](/pages/material/images/2022-10-23_lohwald/2022-10-23_093402_lohwald.jpg)
