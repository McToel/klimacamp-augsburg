---
layout: page
title:  "18.08.2022: Nach mehr als vier Monaten: Augsburger „Staatsschutz“ sieht keinen Aufklärungsbedarf"
date:   2022-08-18 08:30:00 +0100
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
permalink: /pressemitteilungen/2022-08-18-polizei-sieht-keinen-aufklaerungsbedarf/
nav_order: 321
---

*Pressemitteilung vom Augsburger Klimacamp am 18.8.2022*

# Nach mehr als vier Monaten: Augsburger „Staatsschutz“ sieht keinen Aufklärungsbedarf

*Pressemitteilung anlässlich Ablauf der in einer Dienstaufsichtsbeschwerde
gestellten Frist*

Mehr als vier Monate ist es nun her, dass die Abteilung „Staatsschutz“
der Augsburger Polizei mit einer Hausdurchsuchung in den intimen
Schutzbereich des ehemaligen Bundestagskandidaten und Klimaaktivisten
Alexander Mai (26) eingriff, seine privaten technischen Geräte sowie
seinen Arbeitslaptop konfiszierte und sich so Zugang zu großen Teilen
der vertraulichen digitalen Kommunikation der Augsburger
Klimagerechtigkeitsbewegung verschaffte. Die Abteilung „Staatsschutz“
begründete ihren Übergriff damit, Mais Urheberschaft an einem
Facebook-Kommentar nachweisen zu wollen – obwohl daran kein Zweifel
bestand, da Mai den Kommentar bereits ein halbes Jahr zuvor unter seinem
vollen Namen veröffentlichte und dies auch nicht bestritt.

Der Fall wurde wegen seines Bezugs zum Hamburger Pimmelgate-Skandal als
„Pimmelgate Süd“ bekannt. In Hamburg hatte vergangenes Jahr ein
Twitter-Nutzer mit der Nachricht „Du bist so 1 Pimmel“ seine Kritik an
dem Verhalten von Hamburgs Innensenator Andy Grote in der
Corona-Pandemie geäußert, woraufhin die Polizei unter den Augen der
verängstigen Tochter eine Hausdurchsuchung vorgenommen und ein
Ermittlungsverfahren wegen Beleidigung eingeleitet hatte. Mai verlinkte
in seinem Kommentar in Reaktion auf einen fremden- und frauenfeindlichen
Post der AfD ein Zeitungsfoto zum Hamburger Pimmelgate.

Wie Ende Juli bekannt wurde, wurde das Hamburger Ermittlungsverfahren
aber schon im März mit Verweis auf mangelndes öffentliches
Strafverfolgungsinteresse eingestellt – die taz titelte [„Zu klein für
öffentliches
Interesse“](https://taz.de/Pimmelgate-Verfahren-eingestellt/!5871270/) – und
die Hausdurchsuchung wurde vom Hamburger Landgericht für illegal erklärt.


AUGSBURGER „STAATSSCHUTZ“ SIEHT OFFENBAR KEINEN AUFKLÄRUNGSBEDARF

Genug Zeit für die Abteilung „Staatsschutz“ der Augsburger Polizei,
findet Mai, um ihn um Entschuldigung für den Übergriff zu bitten und das
Ermittlungsverfahren dem Hamburger Präzedenzfall folgend ebenfalls
einzustellen, zumal der Sachverhalt bei ihm deutlich schwächer ausfalle.
Doch es kam anders: [Wie Anfang August bekannt
wurde](https://netzpolitik.org/2022/pimmelgate-sued-augsburger-polizei-ueberzieht-klimaaktivisten-mit-weiterem-verfahren/),
trat die Augsburger Polizei mit einem neuen Verfahren gegen Mai sogar noch mal
nach. In diesem wirft sie ihm trotz ordentlicher Schwärzung die rechtswidrige
Veröffentlichung von Durchsuchungsbefehl und -protokoll vor.

Nun ließ die Polizei auch eine erste Antwortfrist der gestellten
Dienstaufsichtsbeschwerde verstreichen ([Volltext
verfügbar](https://www.pimmelgate-süd.de/pressemappe/alex-durchsuchung/dienstaufsichtsbeschwerde.pdf)).
Die Dienstaufsichtsbeschwerde wurde Anfang August von Mai und seinen
Mitstreiter\*innen, wie Janika Pondorf, in deren Kinderzimmer die Abteilung
„Staatsschutz“ auch schon stand
([Süddeutsche](https://www.sueddeutsche.de/bayern/klimacamp-augsburg-hausdurchsuchung-1.5589496),
[Stern](https://www.stern.de/gesellschaft/janika-pondorf---ich-war-wegen-dieses-polizeieinsatzes-in-der-klinik--31895656.html)),
gestellt. Hauptvorwurf: Die Beamten der Abteilung „Staatsschutz“ ließen Mai
während der Hausdurchsuchung rechtswidrigerweise nicht mit seiner Anwältin
telefonieren. Gegenüber Netzpolitik.org wies die Polizei diesen Vorwurf zwar
zurück und
[behauptete](https://netzpolitik.org/2022/augsburg-pimmel-kommentar-fuehrt-zu-razzia-bei-klimaaktivisten/),
dem Klimaaktivisten sei ein polizeiliches Smartphone angeboten wurden, und auch
gegenüber dem Bayerischen Rundfunk [behauptete die
Polizei](https://www.br.de/nachrichten/bayern/razzia-bei-augsburger-klimacamp-aktivisten-wegen-beleidigung,T2yqVJq),
es gebe „keinerlei Hinweise auf kritikwürdiges Verhalten“; doch das von Mai
[veröffentlichte polizeiliche
Durchsuchungsprotokoll](https://www.pimmelgate-süd.de/pressemappe/) bestätigt,
dass Mai nicht telefonieren durfte.


BEARBEITUNG DER DIENSTAUFSICHTSBESCHWERDE VERSCHLEPPT

Laut den betroffenen Aktivist\*innen gestand die Polizei auf telefonische
Nachfrage ein, dass sie bislang noch nicht einmal begann, die
Dienstaufsichtsbeschwerde zu bearbeiten. „Wie kann es sein, dass nach
über zwei Wochen die Dienstaufsichtsbeschwerde noch nicht mal dem
zuständigen Dienstvorgesetzten vorgelegt wurde? Wir hatten diesen
benannt und die Beschwerde explizit an ihn adressiert. Im Übrigen hätte
die Polizei in den vier Monaten auch initiativ tätig werden können. Die
Polizei sieht offenbar keinen Aufklärungsbedarf“, schätzt Mai die
Situation ein. „Wenigstens ein Zwischenbericht über die polizeiinternen
Ermittlungen gegen die Beamten der Abteilung ‚Staatsschutz‘ wäre
angebracht gewesen. Wie möchte die Polizei weitere umstrittene Vorstöße
ihrer Abteilung ‚Staatsschutz‘ verhindern?“

Mai stellt diese Frage auch mit Blick auf einen neuen Vorstoß des
„Staatsschutzes“, von dem er Kenntnis erlangte. In der Zielscheibe des
neuen Vorfalls: friedlicher Einsatz für Gleichberechtigung und
Sichtbarmachung von Sorge-Arbeit wie Erziehung und Altenpflege.
