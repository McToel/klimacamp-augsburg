---
layout: page
title:  "15.09.2022: Klimacamp Augsburg freut sich über das „Klima-Regierungscamp“ auf dem Rathausplatz"
date:   2022-09-15 08:30:00 +0100
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
permalink: /pressemitteilungen/2022-09-15-klima-regierungscamp/
nav_order: 323
---

*Pressemitteilung vom Augsburger Klimacamp am 15.9.2022*

# Klimacamp Augsburg freut sich über das „Klima-Regierungscamp“ auf dem Rathausplatz

Diesen Freitag und Samstag hat das Bayerische Staatsministerium für
Umwelt und Verbraucherschutz ein zweites Klimacamp in der Augsburger
Innenstadt aufgebaut (klimawandel-meistern.bayern.de), auf dem
Rathausplatz, in Sichtweite des ersten Klimacamps Augsburgs und
Deutschlands auf dem Fischmarkt.

Klimacamperin Charlotte Lauter (19): „Endlich schönere Zelte, werden
sich die Augsburger\*innen und Augsburger denken. Doch woher nimmt das
Bayerische Staatsministerium für Umwelt und Verbraucherschutz die
Überzeugung, den historischen Rathausplatz belegen zu dürfen, ohne echte
Klimaschutzmaßnahmen im Gepäck zu haben?“ Lauter kritisiert die
Ausrichtung des zweiten Klimacamps, welches Privatpersonen auffordert,
ihren privaten Konsum umzustellen.

Unter dem Slogan „Wir treffen uns bei Null“ möchte die bayerische
Staatsregierung den Bürgerinnen und Bürgern erklären, wie von derzeit
durchschnittlich 7 Tonnen CO₂ pro Jahr und Person auf Null kommen
sollen. Mit Tipps und Anregungen für ihre persönlichen Beiträge zum
Klimaschutz – ganz ohne staatliche Unterstützung in Form geeigneter
Rahmenbedingungen. Dazu stellt die Regierung zehn „Klimamissionen“
zusammen, die es ganz leicht machen sollen, klimabewusster zu leben und
im Allag CO₂ einzusparen.

Für die Klimacamper\*innen kommt das Klimacamp II gelegen. Sie weisen
schon lange daruf hin, dass die wesentlichen Maßnahmen zur Erreichung
der Klimaziele von Politik und Wirtschaft kommen müssen. Die Hebel seien
um ein Vielfaches größer. 

Aus Sicht der Klimacamper\*innen soll die vom Klimacamp II betriebene
Verantwortungsumverteilung auf Bürgerinnen und Bürger von der
Verantwortung der Politikerinnen und Politiker und der Wirtschaft in
Augsburg ablenken. Der Stadtrat werde seiner Verantwortung bei weitem
nicht gerecht. „Die viel größeren Stellschrauben für die
Emissionsreduktion liegen nämlich bei der Politik. Statt den Menschen zu
sagen, sie sollten weniger Bücher kaufen, könnte Bayern etwa endlich
vernünftig in die Windkraft einsteigen oder durch massive Investitionen
Mobilität ohne Auto auch auf dem Land möglich und attraktiv machen“,
erklärt Lauter. Lauter ist zusammen mit ihren Mitstreiter\*innen seit
Gründung des Klimacamps vor mehr als zwei Jahren ausgesprochene
„Konsumkritik-Kritikerin“.

Lauter zu Folge könnten Bürgerinnen und Bürger nicht einfach die Preise
für den öffentlichen Personennahverkehr reduzieren oder Bäume erhalten,
die etwa wie beim Bahnhofsvorplatz von der Stadtregierung für Parkplätze
geopfert werden. Lauter: „Die Politiker\*innen haben die Aufgabe,
Klimaschutz für alle Menschen zu betreiben. Auch diejenigen, die es sich
nicht leisten können, die teuren Tipps des Bayerischen
Staatsministeriums umzusetzen. Die staatlichen Anweisungen an die
Bürgerinnen und Bürger sind sehr konkret, mit Zahlen unterlegt. Bei den
Maßnahmen der bayerischen Staatsregierung hingegen sind die
Informationen ausgesprochen vage, unkonkret und mehrheitlich bloße und
unglaubwürdige Absichtserklärungen.“


## *Nachtrag zu den Hintergründen*

Seit dem 1. Oktober 2022 haben wir
auf der Webseite des Klimacamps auch einen
[Artikel zu Konsumkritikkritik](/informationen/artikel/konsumkritikkritik/).
Darin wird in größerem Detail erklärt,
warum Konsumkritik keinen Lösungweg
für das Erreichen der Klimaziele darstellt
und was stattdessen zu tun ist.
