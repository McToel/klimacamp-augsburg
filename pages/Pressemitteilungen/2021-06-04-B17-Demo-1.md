---
layout: page
title:  "04.06.2021: Bürgerbündnis 'Mobilitätswende Augsburg' veranstaltet Fahrraddemonstration durch ganz Augsburg"
date:   2021-06-04 02:00:00 +0200
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
nav_order: 221
---

*Pressemitteilung vom Bürgerbündnis "Mobilitätswende Augsburg" am 4. Juni 2021*

# Bürgerbündnis "Mobilitätswende Augsburg" veranstaltet Fahrraddemonstration durch ganz Augsburg

Am kommenden Sonntag (6.6.2021) veranstaltet das neu gegründete Bürgerbündnis
"Mobilitätswende Augsburg" eine Fahrraddemonstration durch ganz Augsburg. Die
Route führt sowohl an Verkehrsunfallschwerpunkten vorbei als auch an wichtigen
Orten eines vom Bürgerbündnis vorgeschlagenen Schnellbusliniennetzes, welches
das sternförmige Tramnetz ringförmig verbinden soll. Die Veranstaltung ist Teil
eines deutschlandweiten Verkehrswendewochenendes, zu dem das Bündnis "Wald
statt Asphalt" aufrief
(https://wald-statt-asphalt.net/veranstaltung/mobilitaetswende-jetzt-dezentrale-aktionstage-am-5-6-juni/).

Die Demonstration beginnt um 14:00 Uhr auf dem Plärrergelände. Der
Demonstrationszug wird von der Polizei profesionell begleitet und gesichert. Es
wird immer wieder kleinere Pausen geben, um auch Familien eine
niedrigschwellige Teilnahme zu ermöglichen. Die beantragte Route ist knapp 10
Kilometer lang, führt durch ganz Augsburg und auch über die B17.

Forderungen des Bürgerbündnisses:
- gerechte Mobilitätswende im Ballungsraum Augsburg
- sicheres, gut ausgebautes Radwegenetz
- attraktiver, gut ausgebauter und erschwinglicher ÖPNV
- ganztägiges Tempolimit auf der A8
- Einführung von Schnellbuslinien nach dem Konzept "Verkehr 4.0"
  von Michael Finsinger (verkehr4x0.de)

"Uns Bürgern in Deutschland wurde in den letzten Jahren Klimaschutz immer
wichtiger. Alle führenden Experten und selbst die Bundesregierung sagen klipp
und klar: Klimaschutz ist nur mit einer Mobilitätswende zu schaffen", so
Vorsitzender Uwe Kirchheimer des Bürgerbündnisses.
