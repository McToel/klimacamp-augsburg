---
layout: page
title:  "14.12.2020: Klimacamp bietet Ordnungsamt gütliche Einigung an"
date:   2020-12-14 07:00:00 +0200
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
nav_order: 33
---

*Pressemitteilung vom Augsburger Klimacamp am 14. Dezember 2020*

# Klimacamp bietet Ordnungsamt gütliche Einigung an

Am vergangenen Freitag erließ die Stadt einen neuen Bescheid über das
Klimacamp, der zwischen 21 und 5 Uhr ein Versammlungsverbot anordnet und in
dieser Zeit zu einer Brandwache von genau zwei Personen verpflichtet. Dazu
erklärte ein Mitarbeiter des Ordnungsamts der Augsburger Allgemeinen [1], dass
eine Kundgebung kein triftiger Grund sei, die Ausgangssperre zu umgehen. Das
Klimacamp vertritt die Position, dass die Bekämpfung der Pandemie sehr wichtig,
der Verbotsbescheid allerdings rechtswidrig und nicht verhältnismäßig ist. Denn
dem Ordnungsamt stehen mildere Mittel zur Verfügung, wie beispielsweise eine
Beschränkung der nächtlichen Versammlungsteilnehmer\*innenzahl auf fünf
Personen. Diese Beschränkung erlegten sich die Klimacamper\*innen neben anderen
Einschränkungen auch schon vor Wochen selbst auf.

Schülerin Lara Sylla (16) erklärt: "Wir unterbreiteten heute dem Ordnungsamt
einen Einigungsvorschlag. Das ausgesprochene Versammlungsverbot ist klar
rechtswidrig, weshalb wir die Stadt dazu auffordern, dieses zurückzunehmen. Um
der Behörde den Aufwand eines weiteren verlorenen Rechtsstreits zu ersparen,
versuchen wir es auf dem gütlichen Weg. Kommt sie jedoch unserer Bitte nicht
nach, werden wir unverzüglich Klage vor dem Verwaltungsgericht einreichen, denn
wir kennen unsere Rechte und wünschen uns rechtliche Klarheit."

Die Behörden der Stadt Augsburg haben die rechtliche Pflicht, Gesetze in einer
grundgesetzkonformen Art zu interpretieren. Verordnungen dürfen keine
grundsätzlichen Versammlungsverbote enthalten, wie das Bundesverfassungsgericht
im vergangenen Frühjahr erneut klar bestätigte. Um eine angemeldete Versammlung
verbieten zu können, sind konkrete und schwerwiegende Gründe notwendig, wie
dies etwa am vergangenen Wochenende bei Querdenken in Dresden der Fall war. Die
Versammlungsbehörde ist durch das Gesetz verpflichtet, das geringstmögliche
Mittel zur Abwehr von Gefahren, in diesem Fall der Pandemie, zu verwenden. 

"Unser Motto seit Anfang des Jahres ist 'Fight every crisis' -- wir nehmen die
Pandemie seit dem ersten Tag des Camps, dem 1. Juli, sehr ernst", ergänzt
Syllas Mitstreiterin Paula Stoffels (18). "So trugen wir etwa schon von Anfang
an Masken an der frischen Luft. Damals tat das an öffentlichen Plätzen sonst
keine Person. Verschärfte Auflagen sind bei der besorgniserregenden
Inzidenzrate wie auch in anderen Städten Bayerns sinnvoll und verhältnismäßig."
Beispielsweise erließ die Versammlungsbehörde der Stadt Nürnberg dem dortigen
Klimacamp eine nächtliche Verringerung der maximalen Teilnehmer\*innenzahl auf
10 Menschen. "Wir erlegten uns schon vor Wochen selbst die Auflage, nachts
höchstens zu fünft zu sein. Ein Versammlungsverbot ist jedoch auf keinen Fall
verhältnismäßig. Wegen Schule, Ausbildung oder Beruf kann keine\*r von uns
tagsüber schlafen, jedoch könnten wir uns bei nur zwei Personen bei der
Nachtwache nicht abwechseln."

Die Klimagerechtigkeitsaktivist\*innen sind erneut erstaunt über das Fehlen
grundlegenden Rechtsverständnisses bei der Versammlungsbehörde der Stadt
Augsburg. "Es wäre besser, wenn sich die Stadt auf die Umsetzung des seit
Samstag fünf Jahre alten Pariser Klimaabkommens konzentrieren würde, statt
Energie in unnötige Rechtsstreitigkeiten zu stecken, die sie schon mehrmals
verloren hat. Wir würden gerne allen Beteiligten den Aufwand ersparen, sehen
uns aber durch das Handeln der Stadt dazu gezwungen."

Die Bekämpfung der Pandemie ist für alle Aktiven im Klimacamp sehr wichtig.
"Wir distanzieren uns ausdrücklich von Corona-Rebell\*innen, die ein großes
Problem mit Verschwörungsideologien und Antisemitismus haben. Solidarisches
Handeln zum Infektionsschutz hat eine hohe Priorität". Die Absicht der Klage
ist, rechtliche Klarheit über den formal mangelhaften Bescheid der Stadt
einerseits sowie die Wichtigkeit der Versammlungsfreiheit andererseits zu
schaffen.

[1] https://www.augsburger-allgemeine.de/augsburg/Ausgangssperre-Diese-Ausreden-hoeren-Kontrolleure-in-Augsburg-id58719721.html

