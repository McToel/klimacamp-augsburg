---
layout: page
title:  "25.11.2020: CSU erkennt rechtlichen Rahmen und demokratische Entscheidungen nicht an -- Klimacamp spricht Augsburger CSU gesundes Urteilsvermögen ab"
date:   2020-11-25 19:00:00 +0200
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
nav_order: 30
---

*Pressemitteilung vom Augsburger Klimacamp am 25. November 2020*

# CSU erkennt rechtlichen Rahmen und demokratische Entscheidungen nicht an -- Klimacamp spricht Augsburger CSU gesundes Urteilsvermögen ab

"Wir sind erneut enttäuscht, wie wenig wir uns auf die Aussagen der
aktuellen Stadtregierung verlassen können." So lautet das Urteil der
Klimacamper\*innen nach der heutigen Pressemitteilung der Stadt Augsburg.
"Aus Reihen der CSU hören wir in persönlichen Gesprächen immer, wir
müssten demokratische Entscheidungen akzeptieren. Demokratie sei kein
Wunschkonzert und Klimapolitik müsse sich den rechtlichen Vorgaben
anpassen. Natürlich sehen wir das ein und versuchen deswegen,
demokratisch und im rechtlichen Rahmen auf eine der Klimakrise
angemessenen Klimapolitik zu bestehen", meint Janika Pondorf (16). "Aber
die zweimalige Niederlage vor Gericht möchte unsere Oberbürgermeisterin
einfach nicht akzeptieren."

Seit die Klimacamper\*innen am 1. Juli ihr Klimacamp errichteten,
verbrauchte Augsburg schon über 8% des CO2-Budgets, das Augsburg noch
bleibt, um seinen Teil am 1,5-Grad-Ziel einzuhalten. "Konsequenterweise
nennt Oberbürgermeisterin Weber den Klimaschutz 'dringlich'", so
Camperin Anouk Zenetti (13). "Das hören wir von ihr nicht zum ersten
Mal. Wieso ergriff Frau Weber dann aber seit ihrem Amtsantritt keine
einzige konkrete Klimaschutzmaßnahme -- von der Umwandlung einer
einzigen Straße in eine Fahrradstraße abgesehen?" Vorschläge gab es
dabei genügend, unter anderem reichten die Oppositionsfraktionen
Augsburg in Bürgerhand und die Bürgerliche Mitte mehrere fundierte
Anträge ein. "Doch diese ließ Frau Weber im Stadtrat nicht mal zur
Diskussion zu, sondern machte von ihrem Vertagungsrecht Gebrauch. Wir
würden so gerne sehen, dass Frau Weber Klimagerechtigkeit zur
Chefinnensache macht. In ihrem Handeln ist das leider nicht erkennbar."

Dass Oberbürgermeisterin Weber das Klimacamp als 'unbefristetes
Campieren' bezeichnet, zeige den jungen Aktivist\*innen deutlich, dass
die Stadtspitze das Anliegen nicht versteht. "Wir sind nicht zum Spaß
hier. Wir wollen alle nach Hause gehen und die Frist ist klar. Die Stadt
muss uns zeigen, dass sie Verantwortung übernimmt und echte Maßnahmen
einleitet", erinnert Zenetti. "Konkrete Vorschläge präsentieren wir seit
Monaten auf unserem vorderen Banner. Zum Beispiel den städtischen
Ausstieg aus der Kohle oder die Verwirklichung der schon 2012
beschlossenen Fahrradstadt Augsburg."

Für das Verwaltungsgericht war auch bei genauer Betrachtung völlig klar,
dass es sich beim Klimacamp um eine Versammlung handelt, weshalb es auch
keine Berufung zuließ. "Heute ergibt sich erneut der Eindruck, dass die
CSU-Fraktion uns unbedingt weghaben will, da wir unbequem sind und sie
den Fischmarkt lieber wieder als Parkplatz verwenden wollen", so Zenetti
weiter. Statt weiter Energie in den verlorenen Rechtsstreit zu stecken,
solle die Stadt besser die eigenen Versprechen zu Klimaschutz ernst
nehmen.

Die Oberbürgermeisterin bezieht sich gerne auf den PR-Begriff der "Blue
City", etwa schrieb sie in ihrer heutigen Pressemitteilung: 'Blue City
Augsburg steht für eine klimafreundliche Stadt, in der der nachhaltige
Umgang sowie die Nutzung von Energie und Ressourcen unser Wohlergehen,
unsere Wirtschaft und unsere Lebensqualität sichern'. "In dieser Vision
ist Klimagerechtigkeit weiterhin keine Priorität", erklärt Klimacamperin
Janika Pondorf (16). "Stattdessen befeuert unser weiterhin viel zu hoher
Treibhausgasausstoß im Globalen Süden Krisen und zerstört die
Lebensgrundlage von vielen Millionen Menschen. Aus diesem Grund ist
unser bald fünfmonatiger Protest für Klimagerechtigkeit so wichtig." Die
von der Stadt geschaffenen Wege der Bürger\*innenbeteiligung, allen voran
der von Frau Weber vielzitierte Klimabeirat, können nur unverbindliche
Empfehlungen aussprechen und "sind definitiv keine echten Wege der
Beteiligung", so Pondorfs Urteil.

Das Klimacamp lädt natürlich weiterhin zu aufklärenden Gesprächen ein.
"Interessierte Menschen und CSU-Stadträt\*innen finden uns ganztags von 0:00 bis
23:59 auf dem Fischmarkt. Auch sonntags und feiertags", so Zenetti. Termine für
konkrete Gespräche mit passenden Akteur\*innen aus dem Klimacamp könnten
jederzeit gerne vereinbart werden — "wir sind zeitlich äußerst flexibel!".
