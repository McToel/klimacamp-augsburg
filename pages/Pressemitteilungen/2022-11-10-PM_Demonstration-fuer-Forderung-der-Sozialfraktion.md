---
layout: page
title: "10.11.2022: Demonstration für Forderung der Sozialfraktion"
date: 2022-11-10 01:30:00 +0100
categories: jekyll update
parent: "Pressemitteilungen"
grand_parent: "Presse"
permalink: /pressemitteilungen/2022-11-10-demonstration-fuer-forderung-der-sozialfraktion/
nav_order: 352
---

*Pressemitteilung vom 10.11.2022*

# Verkehrswendeaktivist\*innen unterstützen mit Demonstration der besonderen Art Forderung der Sozialfraktion

Am heutigen Freitag (11.11.2022) ließen unabhängige Augsburger
Verkehrswendeaktivist\*innen von 11:15 Uhr bis 12:00 Uhr mittels einer
besonderen Ausübung des Versammlungsrechts die Hallstraße bei
Holbein-Gymnasium und Ulrich-Schule für den Autoverkehr sperren. Eine
Sperrung der Hallstraße ist möglich und nötig für mehr Sicherheit und
Aufenthaltsqualität für Schüler\*innen, erklärt Florian Lenz (21), einer
der Organisator\*innen. „Durch unsere Aktion können sich die
Schüler\*innen frei auf der Straße bewegen, ohne Sorge haben zu müssen,
angefahren zu werden.“ Ihre Meinung tun die Schüler\*innen auch mit
Straßenkreide kund. „Es entstehen jedes Mal kleine Kunstwerke“, freut
sich Ute Grathwohl (49).

Mit der Aktion unterstützten die Verkehrswendeaktivist\*innen einen
Antrag der Sozialfraktion (SPD, Linke, ÖDP). Diese hatte schon im Juli
dieses Jahres eine testweise Sperrung der Hallstraße beantragt [2].
CSU-Baureferent Gerd Merkle wies diesen jedoch unter Verweis auf ein
mehr als zehn jahre altes Dokument zurück [2, Ende erster Absatz] – nur
formell befindet er sich noch in Bearbeitung. Am 16.5.2022 wurde durch
die weiterhin ungezügelte Freigabe der Hallstraße ein Kind verletzt [1].
Allerdings wurde auch ein erneuter Dringlichkeitsantrag der
Sozialfraktion für die Stadtratssitzung am 27.10. von der Stadtregierung
verworfen und auf den Bauausschuss verschoben; entgegen ihrer
Ankündigung sucht man den Antrag auf der Tagesordnung der nächsten
Sitzung am 17.11. allerdings vergebens [3].

Im Zuge der geplanten autofreien Maximilianstraße bietet es sich nur an,
auch weitere Straßen für den automobilen Durchgangsverkehr zu sperren,
da sich der Autoverkehr im Gebiet dadurch ja ohnehin reduzieren werde,
so die Aktivisten, und spielen dabei auf die Autoposer an, die sich für
die Schulen neben den Unfallgefahren für Schüler\*innen auch durch Lärm
negativ auswirken. 

Die Aktivist\*innen, viele von ihnen ehemalige Schüler\*innen des
Holbein-Gymnasiums, fordern nicht nur, dass die ihnen persönlich
wichtige Hallstraße autofrei wird. Sie fordern stattdessen eine
autofreie Zone um alle Schulen und Kindergärten in Augsburg. Dabei
nennen sie exemplarisch die Schülestraße vor der Grundschule am Roten
Tor, für die eine Elterninitiative eine Sperrung für den Autoverkehr
ebenfalls fordert, nicht zuletzt da dort im Mai ebenfalls ein 8-jähriger
Junge vor der Schule von einem Auto erfasst wurde [4].

[1] [https://www.klimacamp-augsburg.de/pressemitteilungen/2022-05-17-protest-nach-vorfall-in-schulstrasse/](https://www.klimacamp-augsburg.de/pressemitteilungen/2022-05-17-protest-nach-vorfall-in-schulstrasse/) (nicht zu verwechseln mit dem anderem Unfall ohne Fremdverschulden am selben Tag)

[2] [https://spd-dielinke-augsburg.de/dringlichkeitsantrag-temporaere-sperrung-der-hallstrasse/](https://spd-dielinke-augsburg.de/dringlichkeitsantrag-temporaere-sperrung-der-hallstrasse/)

[3] [https://ratsinfo.augsburg.de/bi/to010.asp?SILFDNR=2134](https://ratsinfo.augsburg.de/bi/to010.asp?SILFDNR=2134)

[4] [https://www.augsburger-allgemeine.de/augsburg/augsburg-innenstadt-unfall-am-roten-tor-autofahrerin-erfasst-zweitklaessler-id62664011.html](https://www.augsburger-allgemeine.de/augsburg/augsburg-innenstadt-unfall-am-roten-tor-autofahrerin-erfasst-zweitklaessler-id62664011.html)
