---
layout: page
title: Tagebuch
permalink: /tagebuch/
nav_order: 60
has_children: true
has_toc: false
keywords:
  - Augsburg
  - Augsburger
  - Camp
  - CO₂
  - Demonstration
  - Energiewende
  - erneuerbar
  - FFF
  - Fischmarkt
  - Fridays for Future
  - Globalstreik
  - Klimacamp
  - Klimagerechtigkeit
  - Klimaschutz
  - Kohleausstiegsgesetz
  - Kohleeinstiegsgesetz
  - Lokalpolitik
  - Mobilität
  - Mobilitätswende
  - Moritzplatz
  - nachhaltig
  - Pariser Klimaschutzabkommen
  - Übereinkommen von Paris
  - Protest
  - Rathaus
  - Rathausplatz
  - Restbudget
  - Stadtrat
  - Streik
  - Tagebuch
  - Verkehrswende
  - Versammlung
  - Wissenschaft
  - Vortrag
  - Workshop
  - 2022
---

# Tagebuch

*Derzeit gibt es sehr viele Ereignisse und Aktionen.
Wir sind mit dem Tagebuch aktuell ein wenig ins Hintertreffen geraten.
Daher gibt es für einige Ereignisse ab dem 17. Oktober
hier derzeit nur die Terminankündigung,
aber (noch) keine Fotos und keinen Bericht darüber,
wie das Event tatsächlich ablief.*


## Freitag 11.11.2022 -- **Tag 864**

### Demonstration: Sperrung der Hallstraße für Autos

**Ort:** Hallstraße
<br>
**Zeit:** 11:15 Uhr bis 12:00 Uhr


### Demonstration gegen die bayerische Klimapolitik im München

Unter dem Motto
„Schluss mit dem Politzirkus – es ist Zeit für echte Klimapolitik!“
organisieren *Fridays for Future Bayern* und *BUNDjugend Bayern*
gemeinsam einen zentralen Klimastreik in München.
Protestiert wird gegen die äußerst mangelhafte Klimapolitik
der bayerischen Landesregierung unter Markus Söder.

Weitere Informationen:
[https://www.jbn.de/termine/zentraler-klimastreik-mit-fridays-for-future-bayern](https://www.jbn.de/termine/zentraler-klimastreik-mit-fridays-for-future-bayern)

**Ort:** München (am Königsplatz)
<br>
**Zeit:** 12:00 Uhr
<br>
**Optional:** Zirkuskostüm

**Nachtrag:**

Die Demonstration richtete sich gegen
das neue bayerische sogenannte „Klimaschutzgesetz“.
Kritisiert wird weniger, was in dem Gesetz steht,
als viel mehr das, was alles nicht in dem Gesetz steht.
(„Too little, too late.“)
In seiner jetzigen Form hätte das bayerische Klimaschutzgesetz
allenfalls noch in den 90er Jahren als gut gegolten.
Die politische Realität in Bayern sieht 2022 traurig aus:

* Die Energiewende wird über die 10H-Regel behindert.
* Eine Mobilitätswende ist nicht in Sicht.
* Mit Ausgleichsflächen wird regelmäßig Schindluder getrieben.
* Söder lässt sich beim Pflanzen von Baumsetzlingen fotografieren,
  die im nächsten Sommer aufgrund von Trockenheit wieder absterben.

Kritisiert wird unter anderem auch,
dass das Klimaschutzgesetz die Kommunen nicht einbindet.
Es ist nicht bekannt, ob Augsburgs Oberbürgermeisterin Eva Weber
an der Demonstration teilnahm.
Einen Grund dazu hätte sie.
Weber hatte sich wiederholt damit herausgeredet,
dass für viele Klimagerechtigkeitsmaßnahmen in Augsburg,
wie beispielsweise einen kostengünstigen öffentlichen Personennahverkehr,
erst die nötigen Voraussetzungen auf Landesebene geschaffen werden müssten.
Mit der Überarbeitung des Klimaschutzgesetzes ist dies nicht geschehen.

Auch der ein oder andere Mensch aus Augsburg verirrte sich auf die Demo.
Nach einer kurzen Eröffnungsrede sorgte *Jacob Leo*
für gute Musik und damit auch gute Stimmung.
(Leider erinnere ich mich nicht mehr an den Namen des Trommlers.)
Aufgeführt wurde auch ein bislang unveröffentlichter Song.

Es folgte ein Auftritt von *Clowns Sind Unterwegs* (kurz: *CSU*),
angeführt von Klimaclown Söder.
Die Truppe wurde schließlich mit Rufen
(„10H muss weg!“, sofern mich mein Gedächtnis nicht trügt)
von der Bühne vertrieben.

Gegen 13 Uhr zog der Demonstrationszug los.
Der an der Spitze vorausfahrende Lautsprecherwagen
– ein umgebautes Feuerwehrauto –
wurde mit Muskelkraft gezogen.
In Augsburg verwenden wir oft Lastenfahrräder als Lautsprecherwagen.
Diese sind schneller, flexibler und benötigen weniger Menschen.
Sie fassen aber auch nicht so viel und so lautstarke Tontechnik.

![Der Lautsprecherwagen wird auf der Maximilianstraße
von zehn oder mehr Demonstrationsteilnehmer\*innen
an einem dicken langen Seil gezogen.
Der Lautsprecherwagen selbst ist ein umgebautes altes Feuerwehrauto,
auf dessen Ladefläche ein Gerüst mit einem Lautsprecherturm
in jeder Ecke und einem Geländer angebracht wurde.
Auf der Ladefläche selbst stehen zwei junge Aktivistinnen.](/pages/material/images/2022-11-11_muenchen/2022-11-11_Lautsprechwagen.jpg)

Vom Königsplatz ging es erst in Richtung Karolinenplatz
und weiter die Brienner Straße entlang.
Die Akustik hier war recht gut und bald hallte
„No more BLA BLA BLA!“ von den Häusern wieder.
Über verschiedene andere Straßen verlief die Demo zur Maximilianstraße
und kam gegen 14 Uhr am Maxmonument an.

![Der Karolinenplatz ist ein Kreisverkehr
mit etwa hundert Metern Durchmesser.
Das Bild wurde von Vornen im Demonstrationszug aufgenommen
und zeigt den hinteren Teil der Demonstration,
wie er gerade im Kreisverkehr unterwegs ist.
Links im Bild trägt eine Person eine ‚Klimaclown Söder‘-Maske.](/pages/material/images/2022-11-11_muenchen/2022-11-11_Karolinenplatz.jpg)

Am Maxmonument gab es noch einige Abschlussreden,
darunter auch durch eine Vertreterin des Radentscheides
und einen Vertreter vom BUND Naturschutz.

Am Maxmonument entschloss sich die Polizei
noch zu einer unnötigen Störung des Straßenbahnverkehrs.
Während die Demonstration auf dem Platz vor dem Maxmonument stand,
war der Straßenbahnringlinie um den Platz
mit Absicht von den Demonstrationsteilnehmer\*innen freigehalten worden.
Die Straßenbahnen hätten also
ungestört um die Demonstration herumfahren können.
Die Polizei entschied sich jedoch,
die Straßenbahnen an der Zufahrt zur Ringline zu hindern.

![Das Bild zeigt eine Landkarte des Platzes um das Maxmonument.
Von allen vier Himmelsrichtungen verlaufen Straßenbahnlinien zum Platz.
Eine große Kreisverbindung um den Platz verbindet alle vier Linien.
Von Westen nach Osten gibt es noch eine direkte Verbindung
quer über den Platz.
Diese wird als einziges von den Demonstrationsteilnehmer\*innen benutzt.
Die Ringline ist frei.
Polizeisperren an allen vier Seiten hindern die Straßenbahnen
an der Befahrung der Ringlinie.](/pages/material/images/2022-11-11_muenchen/2022-11-11_map-Maxmonument.jpg)
*(Kartenmaterial von openstreetmap.org)*

Bevor die Demonstration gegen 14:20 schließlich endete,
wurde noch ein paar Mal laut „10H muss weg!“
in Richtung des Landtages gerufen.


## Donnerstag 10.11.2022 -- **Tag 863**

Heute kam in *quer*, einer Sendung des Bayerischen Fernsehens,
ein fünfminütiger Artikel über den Lohwald bei Meitingen,
der zur Erweiterung der Lech-Stahlwerke von Rodung bedroht ist
und zum Teil bereits gerodet wurde.
Der Beitrag erwähnt leider nicht das Klimacamp.
Auch wird nicht erwähnt,
dass die Max-Aicher-Gruppe als Eigentümerin der Lech-Stahlwerke
zu den größten Spender\*innen der CSU gehört.
Das macht aber nichts.
Dafür zeigt der Beitrag umso schöner,
welches Schindluder mit Ausgleichsflächen betrieben wird.
Mit dem Lohwald wird ein besonders geschützter Bannwald
mit einem großen Artenreichtum gerodet.
Die Ausgleichsflächen bestehen aus halb vertrockneten Baumsetzlingen
und liegen unmittelbar an der lauten Bundesstraße B2.

**Link zum Beitrag:** [Bannwald muss Stahlwerk weichen](https://www.br.de/mediathek/video/av:636d6b6ba33ce10008f188c7)


## Montag 07.11.2022 bis Freitag 11.11.2022

In dieser Woche findet zum dritten(?) Mal
in Augsburg die *Public Climate School* statt.
Hier wird in öffentlichen Vorträgen über
verschiedene Aspekte der Klimakrise informiert.
Wie stets ist der Eintritt frei.

Weitere Informationen und Details zum Vortragsprogramm:

* [https://www.klimauni-augsburg.de/](https://www.klimauni-augsburg.de/)
* [https://www.hs-augsburg.de/Kommunikation/Public-Climate-School.html](https://www.hs-augsburg.de/Kommunikation/Public-Climate-School.html)


## Montag 07.11.2022 -- **Tag 860**

Heute haben wir uns mit einer Pressemitteilung einen kleinen Spaß
mit der Stadtratsfraktion „Bürgerliche Mitte“ erlaubt.
Bitte hier kurz inne halten und erstmal
[die Pressemitteilung](/pressemitteilungen/2022-11-07-besorgnis-wegen-radikaler-buergerlichen-mitte/)
lesen.

Vertreter\*innen der Stadtratsfraktion „Bürgerliche Mitte“
wie auch der freien Wähler versuchten schon zum wiederholten Male
abstrakte Ängste vor dem Klimacamp zu schüren.
Zuletzt geschah dies Ende letzter Woche
in einer Stellungnahme durch Peter Hummel
(sowohl Freie Wähler als auch Bürgerliche Mitte).
Das entzürnte auch einen Klimacamper.
Er stellte sich daraufhin die Frage,
wie es denn aussähe,
wenn man in dem gleichen Ton,
in dem durch die Bürgerliche Mitte und andere Akteure
Kritik am Klimacamp geübt wird,
Kritik an der Bürgerlichen Mitte geübt werden würde.
So entstand am Wochenende der Text,
der an diesem Montag zu unserer Pressemitteilung wurde.

Erstmal gab es keine Pläne,
den so entstandenen Text als Pressemitteilung herauszugeben.
Eigentlich wollten wir die Aussagen von Peter Hummel
keiner Antwort würdigen.
Dann baten uns aber Vertreter\*innen der Presse um eine Stellungnahme.
Also wurde der Text spontan mit leichten Änderungen
zur Pressemitteilung umfunktioniert.

Die Kritik an der Bürgerlichen Mitte und den Freien Wählern
ist absichtlich überspitzt formuliert und
ahmt den Duktus aktueller Kritiken am Klimacamp nach.
Dennoch bleibt sie inhaltlich korrekt.
Sie besitzt sogar mehr Substanz als die Kritik der Freien Wähler an uns.

* Die Bürgerliche Mitte teilt Positionen mit der AfD.
* Die Bürgerliche Mitte sprach sich dafür aus,
  (in einem Punkt) den Vertrag mit „Fahrradstadt jetzt“ zu brechen.
* Die Bürgerliche Mitte bietet in Sachen Klimagerechtigkeit
  bis heute keine tragfähige Alternative zu unseren Forderungen.
* Vertreter\*innen der Bürgerlichen Mitte fallen immer mal wieder
  in ein Schema
  [tumber Konsumkritik](/informationen/artikel/konsumkritikkritik/)
  zurück.
* Die Freien Wähler
  (wenn auch die der Landtagsfraktion und nicht die aus unserem Stadtrat)
  verteidigen die Rodung des Lohwaldes.

Was wir absichtlich nicht gemacht haben, war Differenzierung.

* Wir hätten zwischen den Freien Wählern im Stadtrat
  und den Freien Wählern auf Landesebene differenzieren können.
* Wir hätten erkären können,
  dass die Bürgerliche Mitte aus anderen Gründen als die AfD
  ihre Probleme mit der neuen Stellplatzsatzung hat.

Nachdem wir aber wiederholt derart platt
von Leuten wie Mehring (schon
[wiederholt unangenehm aufgefallen](/pressespiegel/#27102022))
und Hummel angegangen worden waren,
bestand der Zweck der Übung nicht in Differenzierung, sondern darin,
ihnen einen Spiegel vor zu halten.
Ob die Presse die harten Formulierungen aus der Pressemitteilung,
wie beispielsweise
„Den Unwillen sich an geschlossene Verträge zu halten
und diese inhaltliche Nähe zur AfD sind es auch,
die man am Klimacamp mit großer Sorge beobachtet.“
auch aufgreifen wird,
können wir noch nicht sagen.
Eine verbale Watschen dieser Art
haben sie sich durch ihr Schüren von Angst
vor friedlichem Aktivismus schon mal verdient.


## Freitag 04.11.2022 -- **Tag 857**

Heute wurden die Umbaumaßnahmen am Klimacamp abgeschlossen.
Das Klimacamp nutzt den ihm zur Verfügung stehenden Platz
nun etwas effizienter.
Im vorderen Bereich wurden mehrere Platzparks aufgestellt.
Diese tragen zur Begrünung und
einer angenehmen Aufenthaltsatmosphäre bei.
Zwar war das Klimacamp schon immer
eine optimische Bereicherung für den Fischmarkt.
– *Viele erinnern sich schon gar nicht mehr,
aber vor Gründung des Camps war der Fischmarkt ein trostloser Parkplatz.*
– Nun ist es aber noch etwas optisch einladender als vor dem Umbau.

![Das Bild zeigt das Klimacamp aus Richtung des Rathausplatzes.
Links stehen einige Platzparks sowie Schilder mit politischen Aussagen.
Dahinter befindet sich ein überdachter
nach Vornen offener Aufenthaltsbereich.
Dahinter befinden sich die übrigen funktionalen Aufbauten des Camps.
In der Mitte ist eine Zufahrt zum Fischmarkt freigehalten worden.
Auf der rechten Seite hängt das Banner mit den Forderungen des Camps.
Davor liegt ein Bodenbanner mit Informationen
über Wind- und Solarenergie.
Das Foto enstand am Abend des 05.11.2022.](/pages/material/images/2022-11-05/2022-11-05_klimacamp_front.jpg)


## Dienstag 01.11.2022 -- **Tag 854**

Der heutige Feiertag wurde fleißig für Umbaumaßnahmen genutzt.
Die Umbaumaßnahmen erstrecken sich über mehrere Tage.
Das primäre Ziel des Umbaus besteht darin,
dass Klimacamp funktionaler und winterfest zu machen.


## Freitag 28.10.2022 -- **Tag 850**

### 18 Uhr Criticial Mass

Wie gewohnt wird es am letzten Freitag des Monats um 18 Uhr
wieder eine Critical Mass am Rathausplatz geben.

Link: [https://criticalmass-augsburg.de/termine](https://criticalmass-augsburg.de/termine)

**Ort:** Rathausplatz
<br>
**Zeit:** 18:00 Uhr


**Nachtrag:**

Die Critical Mass heute war ein voller Erfolg.
Die Stimmung war super.
Das Wetter und die Temperaturen waren ideal.
Mit wenigen Ausnahmen verhielten sich die Autofahrer\*innen angemessen.
Vereinzelt winkten Passant\*innen dem Verband
und Menschen im Verband winkten zurück.

Die Zusammenarbeit der Teilnehmer\*innen ist einfach fabelhaft.
Manchmal kommt es vor,
dass sich Autofahrer\*innen auf das Recht des Stärken berufen,
welches sie irgendwo in der Straßenverkehrsordnung
aufgeschrieben zu wissen glauben.
Wie gewohnt stellten sich auch heute
an kritischen Kreuzungen und Einfahrten
immer wieder erfahrene Fahrradfahrer\*innen
zwischen die wartenden Autos und den durchfahrenden Verband.
Das soll den Autofahrer\*innen klar signalisieren,
dass sie sich nicht in den Verband hineinzudrängeln haben.
Als heute einmal ein einzelner Radfahrer
in so einer Situation angehupt wurde,
brachen sofort zwei weitere Fahrräder
aus dem Verband aus und stellten sich dazu.

Bei zwei unabhängigen Zählungen kurz nach Start am Königsplatz
wurden 79 Fahrräder und 82 Personen gezählt.
Geschuldet war das wohl mitunter dem sonnigen Wetter.
Für den größten Teil der Strecke
blieb die Anzahl der Fahrräder stabil bei über 70.
Erst bei der Rückfahrt in der Innenstadt
kam es zu einem merklichen Schwund.

Gelegentlich riefen uns Autofahrer\*innen irgendetwas zu.
Allerdings ertranken ihre Worte im Lärm der eigenen Motoren.
Es konnte kein einziges Wort verstanden werden.
<br>
Für den Fall, dass einzelne Autofahrer\*innen wieder denken,
dass die Critical Mass den Verkehr blockiere,
sei nochmal darauf hingewiesen,
dass der ganze Verband etwa den Platzbedarf von 8 bis 12 Autos einnimmt.
In den Autos sitzen meist nur ein oder zwei Personen.
Der gleiche Verkehrsraum reicht für
80 Fahrradfahrer\*innen oder 18 Passagiere in Autos.
Wer geht hier wohl ökonomischer mit dem Verkehrsraum um?

Es ist auch immer wieder bezeichnend,
wenn Autofahrer\*innen, nachdem sie an der Critical Mass vorbei sind,
nochmal kurz so richtig Gas geben,
dann aber jeden Zeitvorteil ihrer kurzen Schnellfahrt
direkt an der nächsten Ampel wieder verlieren.

*Zum Ablauf:*

Den einzigen größeren Zwischenfall gab es kurz nach Start
in der Einbahnstraße *Am Pfannenstiel*.
In einer Aktion, die man mit viel gutem Willen
noch als fehlgeleiteten Einparkversuch bezeichnen kann,
drängte sich ein Auto in die Critical Mass
und stellte sich auf der Straße quer.

Die letzten dreißig oder so Fahrräder
wurden so vom Rest des Verbandes abgetrennt.
Es kam zu eine Spaghettisierung.
Anstatt einem geschlossenen Verband
von nebeneinander fahrenden Fahrrädern,
entstand eine lange lose Kette bestehend aus einzelnen Fahrrädern
und kleineren Grüppchen von Fahrrädern
mit größeren Lücken dazwischen.

Der Vorfall führte zu kleineren Verkehrsstörungen
in der Heinrich-von-Buz-Straße und der Riedingerstraße.
In einem verkehrsarmen Seitenarm der Riedingerstraße
gruppierte sich der Verband neu,
was mehrere Minuten in Anspruch nahm.

Weiter ging es über Bärenwirt und den Bahnhof *Augsburg Oberhausen*
in den Westen Augsburg.
Dort verkehrte die Critical Mass
eine ganze Weile durch verschiedene Straßen.

Auf dem Rückweg durch Pfersee
an der Haltestelle *Bürgermeister-Bohl-Straße*
warf ein Halbstarker Kiesel und kleinere Steine
in Richtung des Verbands.
Uns ist nicht bekannt, dass jemand getroffen wurde.
Es schien dem Jugendlichen wohl auch mehr um Aufmerksamkeit zu gehen
als darum tatsächlich jemanden treffen zu wollen.

Kurze Zeit kam es noch zum Sturz einer Fahrradfahrerin.
Größere Verletzung blieben zum Glück aus.

Zurück ging es über die Luitpoldbrücke und den Pferseer Tunnel,
dann am Hauptbahnhof vorbei in die Schießgrabenstraße
und am Theodor-Heuss-Platz die Konrad-Adenauer-Allee
– inzwischen eine Fahrradstraße – hinein.

Hier kam es noch zu einem kurzen Deadlock.
Auf der Kreuzung des Theodor-Heuss-Platzes stehende Autos behinderten
die Einfahrt des Verbandes in die Konrad-Adenauer-Allee.
Die Autos wurden ihrerseits an der Weiterfahrt gehindert,
weil der Verband von Fahrrädern so lang war,
dass das Ende des Verbandes noch die Weiterfahrt von Autos
vom Theodor-Heuss-Platz in die Stettenstraße verhinderte.

Als die Ampel für uns auf Grün umschwenkte,
durchfloss der Verband die Lücken
zwischen den mitten auf der Kreuzung stehenden Autos
und beendete so das Deadlock.

Bei der Einfahrt in den Königsplatz kurz vor Ende der Critical Mass
wurden immer noch 60 Fahrräder gezählt.
Am Königsplatz rief uns noch ein Fußgänger etwas zu,
worin das Wort „Klimaaktivisten“ auszumachen war.
Man darf Sagen,
dass viele von uns die Critical Mass toll finden,
dass einige von uns gerne dort mitfahren und
dass wir die Termine der Critical Mass
gerne auf unserer Webseite ankündigen.
Die Critical Mass ist aber primär
keine Aktion von Klimagerechtigkeitsaktivist\*innen.
Es ist eine Freizeitaktion des gemeinsamen gemütlichen Fahrradfahrens,
frei von dem Stress und den Gefahren,
denen man als einzelne\*r Fahrradfahrer\*in ausgesetzt ist.
Die Critical Mass gibt es in Augsburg
schon seit mindestens 2014 oder länger
– viel länger als das Klimacamp oder *Fridays for Future*.

Die Critical Mass endete um kurz vor 20 Uhr am Rathausplatz.
Einige Teilnehmer\*innen folgten anschließend der Einladung zum Klimacamp.


### <span style="background-color: #80FF80">ab etwa 19:30 Tee, Kinderpunsch und Crêpes am Klimacamp</span>

![Das Bild zeigt eine krakelige aber kunstvolle Zeichnung
einer zu einer Teekanne verformten Erdkugel.
Rund herum stehen Datum und Zeit sowie die Stichpunkte
„Tee“, „Punsch“, „Crepes“ und „Freunde“
und die Sätze „Tee Plausch im Klimacamp“ und
„komm nicht zu spät zum Tee!“](/pages/material/images/sharepics/2022-10-28_teerunde.png)

Wir wollen uns zum gemütlichen Kennenlernen,
Austausch und direkten Kontakt bei warmen Getränken und Crêpes treffen.
Die Veranstaltung ist als Kennenlernveranstaltung ausgelegt.
Wer mehr über die Klimagerechtigkeitsbewegung erfahren möchte und
verschiedene Klimagerechtigkeitsaktivist\*innen kennen lernen möchte,
hat hier die Gelegenheit dazu.
Auch die Teilnehmer\*innen der Critical Mass sind eingeladen,
den Abend mit uns am Klimacamp ausklingen zu lassen.

**Ort:** Klimacamp
<br>
**Zeit:** 19:30 Uhr

**Nachtrag:**

Auch dieses Event war ein voller Erfolg.
Neben warmen Getränken gab es auch Lebkuchen, Muffins, Waffeln und mehr.
Einige Menschen der Critical Mass kamen auch noch vorbei.
Es gab interessante Gespräche zwischen Klimacamper\*innen
und Menschen, die noch nie zuvor am Klimacamp gewesen waren.
Daher ist der Plan,
das Event im Anschluss an die nächste Critical Mass
im November zu wiederholen
und eventuell sogar regelmäßig stattfinden zu lassen.


## Donnerstag 27.10.2022 -- **Tag 849**

Heute war ein ereignisreicher Tag.
Zunächst einmal hatten wir Probleme mit der CI/CD-Pipeline der Webseite.
Das verhinderte leider,
dass wir auf der Webseite live von der heutigen Aktion, der
[Besetzung der Regierung Schwaben](/pressemitteilungen/2022-10-27-besetzung-der-regierung-von-schwaben/),
berichten konnten.


### Die erste Besetzung der Regierung von Schwaben

Es war tatsächlich nicht die ganze Regierung von Schwaben,
die besetzt wurde, sondern lediglich das Büro
des schwäbischen Regierungspräsidenten Erwin Lohner.


#### Hintergründe der Besetzung

Grund der Aktion war der Zorn über eine Ausnahmegenehmigung,
die es den Lech-Stahlwerken erlaubte,
mehrere Hektar des Lohwaldes bei Meitingen zu roden.
Eigentlich war mit der Rodung frühestens im Herbst 2023 gerechnet worden,
wenn mit der Stadt Meitingen vereinbarte Ausgleichsmaßnahmen
eine gewisse Wirkung entfaltet haben sollen.
Außerdem laufen gegen die Rodung mehrere Klagen,
darunter auch eine Klage des *Bund Naturschutz* und des Klimacamps
vor dem Bayerischen Verwaltungsgerichtshof.

Die Ausnahmegenehmigung hatte die schwäbische Regierung ausgestellt.
Die klagenden Parteien wurden jedoch nicht
über die Ausnahmegenehmigung informiert
und hatten so keine Möglichkeit,
einen einstweiligen Rechtsschutz für den Wald zu beantragen.

Es gibt erhebliche Zweifel an der Rechtmäßigkeit der Rodung.
Regierungspräsidenten Erwin Lohner (CSU)
trägt die politische Verantwortung
für das Handeln der schwäbischen Regierung.
Die Max-Aicher-Gruppe, welcher die Lech-Stahlwerke gehören,
ist eine der größten Spenderinnen der CSU.


#### Ablauf der Besetzung

Fotos der Aktion sind in unserer
[Pressemitteilung vom 27. Oktober 2022](/pressemitteilungen/2022-10-27-besetzung-der-regierung-von-schwaben/)
verlinkt.

Die Aktion verlief weitgehend harmonisch.
Eine Person hing an der Hauswand des Regierungsgebäudes in einem Seil,
welches durch zwei weitere Personen im Büro des Regierungspräsidenten
gesichert war.

![Das Bild zeigt den Eingang zum Regierungsgebäude.
Über dem Eingang am Schild mit der Beschriftung „Regierung von Schwaben“
hängt in etwa 3 bis 4 Metern Höhe ein Banner mit der Aufschrift
„Lohwald-Rodung genehmigen trotz laufender Gerichtsverfahren? Frech!“.
Nochmal ein wenig höher rechts von der Tür sitzt eine Aktivistin
an einem Seil hängend,
welches aus einem Fenster des zweiten Stockes hängt.](/pages/material/images/2022-10-27_schwaebische_regierung/2022-10-27_besetzung_schwaebische_regierung.jpg)

Für Unmut sorgte gegen 10 Uhr die Polizei,
die die Personalien der meisten Passant\*innen
– darunter auch Vertreter\*innen des Klimacamps – aufnahm,
welche im Park des Fronhofes vor dem Regierungsgebäude
die Aktion verfolgten.
Die Personalien wurden auch von Personen erfasst,
die den Platz gerade erst betreten hatten.
Als Vorwand der Maßnahme diente
„emotionale Unterstützung einer Straftat“.
Diese Aussage überzeugte nicht.
Die Erfassung der Personalien
wurde als planlose Repräsionsmaßnahme wahrgenommen.
Manch eine\*r wurde durch die schwach begründete Polizeimaßnahme an
[Pimmelgate Süd](https://www.pimmelgate-süd.de/)
und die
[Hausdurchsuchungen wegen Sprühkreide](https://www.pimmelgate-süd.de/kreide/)
erinnert.

Später zogen sich die Beamt\*innen,
welche die Personalienerfassung vorgenommen hatten,
in den Hintergrund zurück.
Stattdessen gesellten sich Polizist\*innen in mit „Presse“
und „Kommunikation“ beschrifteten Uniformen zu den Passant\*innen.
Es ergaben sich mehrere sehr nette Gesprächsrunden
zwischen Polizist\*innen, Presse, Aktivist\*innen
und zufällig anwesenden Passant\*innen.

![Links im Bild der Park des Fronhofes,
in dem Passant\*innen, Vertreter\*innen der Presse
und Polizist\*innen für Kommunikation und Pressearbeit.
Rechts im Bild befindet sich das Regierungsgebäude
mit der am Seil sitzenden Aktivistin
und neun in der Gegend herumstehenden Polizist\*innen.](/pages/material/images/2022-10-27_schwaebische_regierung/2022-10-27_schwaebische_regierung_zuschauer.jpg)

Lohner entzog sich während der ganzen Aktion der Öffentlichkeit.
Er sprach weder mit den Aktivist\*innen noch mit der anwesenden Presse.
Ob er sich für die Dauer der Aktion in einem Luftschutzbunker verschanzte
oder lediglich im benachbarten Büro abwartete,
ist nicht bekannt.

Gegen 5 vor 12 folgte die Räumung mittels einer Drehleiter.
Nachdem die Person von der Fassade herunter geholt worden war,
wollten die Aktivisten im Büro dieses freiwillig verlassen,
wurden dann aber durch einen unverhältnismäßigen
und zu diesem Zeitpunkt unnötigen SEK-Einsatz überwältigt.
Details zur Räumung stehen auch in unserer
[Pressemitteilung vom 28. Oktober 2022](/pressemitteilungen/2022-10-28-sek-einsatz-bei-regierung-von-schwaben/).


## Dienstag 25.10.2022 -- **Tag 847**

### Workshop *Konsumkritik-Kritik: von der Mär der angeblichen Macht der Verbraucher\*innen*

Für weitere Informationen zu dem Thema des Workshops
siehe auch unseren Artikel
*[Konsumkritikkritik – Kritik an Konsumkritik ](/informationen/artikel/konsumkritikkritik/)*.

**Ort:** Im Klimacamp
<br>
**Zeit:** 19:30 Uhr


## Sonntag 23.10.2022 -- **Tag 845**

### Bannermalen – Kritik an CO₂-intensivem Neubau bei Centerville Nord

Bei Centerville Nord sollen große Bestandsbauten abgerissen
und durch Neubauten ersetzt werden.
Dabei ist fast immer Sanierung klimafreundlicher
als ein CO₂-intensiver Neubau.
Für Wohnraum sollten wir zunächst Leerstand verwenden
und dann Bestandsgebäude umwidmen.
Dagegen in Mitten der Energiekrise Unmengen Energie
für einen unbenötigten Neubau aufwenden? Das ist grotesk.
Den Auftakt unseres Protests wird ein Banner auf dem Areal machen.

Gemeinsames Bannermalen am Sonntag ab 10:30 Uhr im Klimacamp.

**Ort:** Klimacamp
<br>
**Zeit:** 10:30 Uhr


### Mahnwache „Lützerath lebt — die Grünen sind tot“ der Students for Future

📣 Mahnwache: Die „Grünen“ beschlossen am vergangenen
Wochenende das Abbaggern das Dorfes Lützerath im Braunkohlerevier
Garzweiler beschlossen. Im selben Atemzug wurde auch der Kurs des
Energieministers bestätigt. Die Atomkraftwerke Neckarwestheim 2 und Isar
2 sollen bis Frühjahr 2023 auf Reserve laufen. Wer es noch nicht getan
hat, ist herzlich eingeladen Austrittformulare auszufüllen und mit uns
den Briefkasten der „Grünen“-Partei zu füllen. Außerpalamentarische
Alternativen werden wir ebenso beleuchten.

**Ort:** Königsplatz
<br>
**Zeit:** 14:00 Uhr


## Samstag 22.10.2022 -- **Tag 844**

### <span style="background-color: #80FF80">Kleine experimentelle Tee-, Kinderpunsch- und Crêperunde am Klimacamp</span>

![Das Bild zeigt eine krakelige aber kunstvolle Zeichnung
einer zu einer Teekanne verformten Erdkugel.
Rund herum stehen Datum und Zeit sowie die Stichpunkte
„Tee“, „Punsch“, „Crepes“ und „Freunde“
und die Sätze „Tee Plausch im Klimacamp“ und
„komm nicht zu spät zum Tee!“](/pages/material/images/sharepics/2022-10-22_teerunde.png)

In den letzten Monaten kamen soziale Events am Klimacamp viel zu kurz.
Das wollen wir nun ändern.
Daher planen wir diesen Samstag um 15:00 im Klimacamp
eine kleine experimentelle Tee-, Kinderpunsch- und Crêperunde
zum gemütlichen Kennenlernen, Austausch und direkten Kontakt.
Wir üben schon mal für die große Teerunde am kommenden Freitag.
Nebenbei diskutieren wir Falschinformationen/Klimamythen
und wie sie am Besten entkräftet werden können.

Eingeladen sind insbesondere Alle,
die unverbindlich Teile der Augsburger Klimagerechtigkeitsbewegung
kennenlernen möchten. 👋

**Ort:** Klimacamp
<br>
**Zeit:** 15 Uhr


## Freitag 21.10.2022 -- **Tag 843**

* **🏫 11:20:**
  Unabhängige Verkehrswendeaktivist\*innen lassen wieder die
  **Hallstraße**, die zwischen den beiden Teilen des Holbein-Gymnasiums
  verläuft, sperren. Pünktlich zur zweiten Pause. Mit einem kurzen
  Formular ist das vor jeder Schule möglich — bei Interesse @iblech
  kontaktieren: Wäre doch nett für den letzten Freitag vor den
  Schulferien, oder?

* **🌐 12:30:**
  An der **Uni Augsburg** findet in **Raum 1006 von Gebäude L** eine
  Live-Demo zu Internetunsicherheit statt. Eine Person loggt sich mit dem
  eigenen Handy auf einer Website ein, die vortragende Person zeigt das
  Passwort am Beamer an. Ist als Anfänger\*innenworkshop zu Netzwerktechnik
  gedacht, für Menschen, die etwas zu dem Thema wissen wollen und
  Fachbegriffe wie IP-Adressen oder DNS-A-Eintrag bislang nicht kennen.

* **🥙 19:15:**
  Gemütliches Beisammensein im **Klimacamp**, leckeres veganes
  warmes Abendessen inklusive. Dabei arbeiten wir mehrere
  Pressemitteilungen aus, die wir in den nächsten Tagen alle verschicken;
  malen für mehrere Aktionen Banner; fertigen eine Website für das Projekt
  „autofreier Freitag vor Schulen“ an; und arbeiten an Klimacamp-Flyern.
  Wir haben dabei die Aktionen von
  [https://t.me/klimacamp_augsburg/851](https://t.me/klimacamp_augsburg/851)
  im Blick, planen aber auch zwei weitere: zur Energiewende und zum
  Walderhalt. Herzliche Einladung, Vorkenntnisse keine erforderlich!


## Donnerstag 20.10.2022 -- **Tag 842**

### Wir stellen E-Scooter auf Parkplätze

**Treffpunkt:** Klimacamp
<br>
**Zeit:** 13:00 Uhr

[Hintergrund der Aktion](/pressemitteilungen/2022-10-19-e-scooter/)


### <span style="background-color: #80FF80">Offener Workshop im Klimacamp zum Einmaleins der Pressearbeit</span>

**Ort:** Klimacamp
<br>
**Zeit:** 19:30 Uhr

* Was ist das Ziel von Pressearbeit? (Impuls+Diskussion)
* Wie sind Pressemitteilungen zu verschicken? (Impuls)
* Wie kann Presse in geheime Aktionen eingebunden werden? (Impuls+Diskussion)
* Wie rahmen wir unsere Aktionen geschickt? Das fundamentale Werkzeug: Framing (Diskussion)
* Analyse von Pressemitteilungen von CSU, Grünen und Klimagerechtigkeitsinitiativen (Diskussion)
* Konkret: Wir verfassen eine oder mehrere Pressemitteilungen,
  die wir im Anschluss auch verwenden (in Gruppen)

Herzliche Einladung!

![Im Bild zu sehen: Interview durch den BR bei einer unserer letzten Aktionen.
Sat.1 kam auch vorbei, ohne dass wir sie einladen mussten.
AZ-Artikel landen regelmäßig auch in den Bussen und Trams.](/pages/material/images/sharepics/2022-10-20_workshop-pressearbeit.jpeg)


## Dienstag 18.10.2022 -- **Tag 840**

* Um 19:00 Uhr gingen mehrere von uns gemeinsam ins Zeughaus,
  um den im Rahmen vom Mondiale-Filmfestival vorgeführten Film
  *Boom und Crash - Wie Spekulation ins Chaos führt* anzuschauen.
* Im Anschluss gab es im Klimacamp warmes veganes Abendessen (indische Küche).
  Dabei arbeiteten wir in gemütlicher Runde
  die gestern geplanten Aktionen weiter aus.
  Außerdem bereiteten wir mögliche weitere Veranstaltungen
  zu den Themen SUVs und autofreie Straßen vor Schulen vor.


## Montag 17.10.2022 -- **Tag 839**

Heute gab es um 18:30 Uhr im Klimacamp
eine kurzfristig einberufene Lagebesprechung und
einen Austausch zu Aktionen und Workshops in den kommenden vier Wochen.

Themen/Eckpunkte:

* Veranstaltungen auch für Aktivismusneulinge 👋
* Überblick über aktuelle lokalpolitische Themen 💥
* mit vielfältigen Erregungskorridoren Augsburg klimagerechter machen 💪
* Nachbesprechung der jüngsten zwei großen Erfolge:
  Bäume beim Hauptbahnhof, Bäume auf dem Reese-Areal 🎉

Bereits am nächsten Tag nahmen wir die ersten hier geplanten Aktionen
in unser [Programm](/programm/) auf.


## Donnerstag 13.10.2022 -- **Tag 835**

Heute haben wir **vier Neue Bodenbanner-Designs** fertiggestellt und bestellt,
nachdem wir bereits seit vergangenem Juli aktiv daran arbeiteten.
In wenigen Tagen sollten sie am Klimacamp genießbar sein.

![Vorschau Bodenbanner Aufheizung Stripes](/bodenbanner/png-vorschau/2022-10-bodenbanner-aufheizung-stripes-klein.png) ![Vorschau Bodenbanner Strom 2](/bodenbanner/png-vorschau/2022-10-bodenbanner-strom2-klein.png)

Nachdem wir bislang schon insgesamt neun verschiedene Bodenbanner erstellt haben,
gibt es jetzt eine Auflistung aller Bodenbanner,
inklusive Download-Links und demnächst auch jeweils mit Erläuterungen,
hier auf [dieser Unterseite: /bodenbanner](/bodenbanner/).

Mit den neuen Bodenbannern im Kasten
können wir uns nun weiter dem neuen Anlauf der Klimacamp-Flyer widmen...
bleibt gespannt!


## Montag 10.10.2022 -- **Tag 832**

### Baumbesetzung auf dem Reese-Areal

Aufgrund eines Hilferufes durch Einwohner\*innen
begann heute in den frühen Morgenstunden (noch vor 6:30)
die Besetzung von Bäumen,
die durch eine Fällung bedroht sind.

Für weitere Informationen siehe auch:

* unsere [Pressemitteilung zur heutigen Baumbesetzung](/pressemitteilungen/2022-10-10-baumbesetzung/)
  (mit Fotos)
* unseren [Pressespiegel von heute](/pressespiegel/#10102022)
  zur Berichterstattung über die Aktion

Leider hatte die Baumbesetzung
von Anfang an wenig Aussicht auf Erfolg.
Alles, was wir tun konnten,
war den Bäumen noch ein letztes Mal die Ehre zu erweisen.
Sie sollten nicht einen sang- und klanglosen Tod sterben müssen.
So schafften sie es nochmal landesweit ins Fernsehen.

Wir nannten das ein „Aufbäumen der Verdammten“.
Das steht im Gegensatz zu dem
vielfach gelebten „Verdammen der Bäume“,
sei es direkt durch Fällung
oder indirekt als Folge der Erderhitzung.

Die Politik hat viel zu lernen.
Dazu gehört auch Folgendes:
Wir leben nicht mehr in den 60er Jahren.
Man kann nicht mehr einen Wald durch eine Asphaltwüste ersetzen
und erwarten dafür gefeiert zu werden.
Die Fällung jedes einzelnen alten, großen, gesunden Baumes
ist ein Skandal.
Die Schaffung eines Ersatzes dauert Jahrzehnte,
falls die jungen Bäume überhaupt so lange überleben.
Daher wird es normal werden,
dass Fällaktionen in Zukunft zunehmend als Skandal gewertet
und dementsprechend medial begleitet werden.
Die Politik und die Wohnbaugruppe täten gut daran,
den Erhalt dieser wertvollen Bestandsbäume
von Anfang an in all ihre Planungen mit einzubeziehen.


## Samstag 08.10.2022 -- **Tag 830**

* Ab 12:00 sammeln wir Unterschriften für das [Bürger\*innenbegehren Augsburg
  Erdgasfrei](https://augsburg-erdgasfrei.de/). Kommt gerne vorbei und helft mit :-)


## Freitag 07.10.2022 -- **Tag 829**

FFF-Augsburg veranstaltet eine Kundgebung
für den Erhalt des Dorfes Lützerath.
Lützerath soll nach dem Willen des Konzerns RWE
und einiger Politiker\*innen demnächst zerstört werden,
um der Erweiterung eines Braunkohletagebaus Platz zu machen.
Dagegen protestiert FFF-Augsburg.

**Ort:** Königsplatz
<br>
**Zeit:** 15:30 bis 17:30


## Donnerstag 6.10.2022 -- **Tag 828**

**Vortrag:**
Angekommen in der Klimakatastrophe.
Was wirst du tun? Hör dir unseren Plan an!
<br>-- Vortrag der Letzten Generation

**Ort:** Grandhotel Cosmopolis, Springergäßchen 5
(etwa 600 Meter vom Klimacamp)
<br>**Zeit:** 18 Uhr

**Nachtrag:**
Beim Vortrag der letzten Generation wurde die Frage gestellt, vor die uns die
Klimakrise stellt: Wollen wir überleben oder wollen wir nicht?
Wenn alle Verhaltensänderung, Petitionen und Demonstrationen die
Bundesregierung aber nur dazu bringen, Klimaneutralität für 2045 zu
versprechen, während die 1,5-Grad-Grenze voraussichtlich bis 2030 überschritten
wird?

Die Aktivist\*innen der letzten Generation setzen auf friedlichen zivilen
Widerstand, der mit leicht erfüllbaren Forderungen wie einem
Essen-Retten-Gesetz oder einem Tempolimit und erheblichen Störungen wie
Autobahnblockaden große Aufmerksamkeit und
politischen Druck erzeugt. Die Konsequenzen werden in Kauf genommen, in der
Überzeugung das Richtige zu tun für die Umwelt, die Zukunft und für alle
Menschen die nicht so privilegiert leben wie wir.

Der Vortrag informierte auch
über Aktionstrainings für alle Menschen, die sich dem friedlichen zivilen
Widerstand der letzten Generation anschließen wollen. Weitere Informationen
gibt es auf der [Website der letzten
Generation](https://letztegeneration.de/).


## Samstag 01.10.2022 -- **Tag 823**

Heute haben wir nach längerer Pause
wieder mal einen [Artikel](/informationen/)
auf unserer Webseite veröffentlicht.
Der neue Artikel behandelt das Thema
*[Konsumkritikkritik](/informationen/artikel/konsumkritikkritik/)*.
Das Thema ist essenziell für das Verständnis
der politischen Positionen und der Arbeitsweise
des Augsburger Klimacamps.


## Freitag 30.09.2022 -- **Tag 822**

18 Uhr: **Critical Mass** am Rathausplatz gegenüber vom Klimacamp

Die *Critical Mass* ist keine Demonstration.
Stattdessen fährt einfach am letzten Freitag eines Monats
aus Spaß an der Sache gemeinsam im Verband
mit dem Fahrrad durch die Stadt.
Die Route wird spontan festgelegt.<br>
Link: [https://criticalmass-augsburg.de/termine](https://criticalmass-augsburg.de/termine)

**Hintergrund:**
Die Critical Mass gibt es in Augsburg seit 13 Jahren.
Es ist eine Fahrradfahrt durch die Stadt,
fast wie eine Demo, nur ohne Meinungskundgabe.
Keine Plakate. Keine Sprüche. Keine Anmeldung.
Keine Polizeibegleitung. Einfach mitfahren.
Die Person, die zufällig gerade vorne fährt, bestimmt die Route.
Jede Fahrt ist anders.
Lustig sind sie alle.
Wir sind oft um die 60-100 Menschen, je nach Wetter und Laune.
Wir bleiben  immer zusammen als Gruppe.
Wenn die Spitze bei grün auf die Kreuzung fährt,
folgen alle, bis alle durch sind.
Das ist alles von § 27 der Straßenverkehrsordnung gedeckt:
§ 27 Verbände
> (1) Für geschlossene Verbände
> gelten die für den gesamten Fahrverkehr
> einheitlich bestehenden Verkehrsregeln und Anordnungen sinngemäß.
> Mehr als 15 Rad Fahrende dürfen einen geschlossenen Verband bilden.
> Dann dürfen sie zu zweit nebeneinander auf der Fahrbahn fahren.
> ...

Probier es aus, am Freitag, 30.09. um 18 Uhr.
Start ist am Rathausplatz.
Wir sind längstens bis 20 Uhr unterwegs und wieder zurück.

**Nachtrag:**
Die heutige Critical Mass war mit Ausnahme von ein paar sehr
aggressiven Autofahrer\*innen und Motorradfahrer\*innen recht nett.
Es regnete nicht und die Temperaturen waren auch noch ganz angenehm.
Tatsächlich gab es sogar zwei Critical Masses.

### Erste Critical Mass

Die erste Critical Mass fuhr wie geplant
nach 18 Uhr am Rathausplatz los.
Die Anzahl der Teilnehmer\*innen lag zwischen 60 und 70.
Bei verschiedenen Zählungen während der Fahrt
wurden „etwa 70“, 61 und 65 Fahrräder gezählt.

Beeindruckend war es anzusehen,
als beim Erscheinen eines Fahrzeugs mit Sirene und Blaulicht
der ganze Verband rechts an den Rand fuhr
und innerhalb von Sekunden vielleicht 100 Meter an Straße freigab.
Das Fahrzeug wählte aber einen anderen Weg.

Diese erste Critical Mass endete gegen 19:12 wieder am Rathausplatz.


### Zweite Critical Mass

Aufgrund der kurzen Dauer der ersten Critical Mass
ergab sich spontan eine zweite Critical Mass,
die mit etwa 20 Fahrrädern gegen 19:19 am Rathausplatz startete.
Eine weitere Gruppe schloss sich am Theodor-Heuss-Platz an,
so dass die Anzahl kurzzeitig auf Mitte 20 bis 30 anstieg.

Bereits in der Schaetzlerstraße
meldete sich wieder von hinten ein Fahrzeug mit Blaulicht und Sirene.
Der Verband fuhr zügig an den Rand und gab die Straße frei,
bevor das Fahrzeug überhaupt in der Nähe des Verbandes war.

In der Schießgrabenstraße
wurde der Verband dann trotz Überholverbot
von zwei Motorrädern mit sehr hohen Geschwindigkeiten
– es würde mich nicht wundern, wenn diese schneller als 100km/h fuhren –
und einem Auto mit ebenfalls hoher Geschwindigkeit überholt.
Ein paar Straßen weiter überholte ein Fahrzeug
den Verband rechtswidrigerweise auf der Linksabbiegerspur.
Wenn wir ein Fahrrad mit einer Dashcam dabei gehabt hätten,
wären ihre Nummernschilder nun an die Polizei übergeben worden.

In einem anderen Fall wurde die Polizei informiert.
Ein sehr aggressiver Autofahrer
war mit einem Fahrradfahrer aus unserem Verband aneinander geraten.
Der Autofahrer folgte dem Verband anschließend über Kilometer.
Erst am Königsplatz,
als der Verband die Fußgänger- und Fahrradzone durchquerte,
verlor der Autofahrer den Anschluss.

Mindestens zwei Diskussionen mit Autofahrern fanden statt.

In einem Fall versuchte ein Autofahrer
auf einer benachbarten Spur beim gemeinsamen Warten
vor einer roten Ampel
eine unverständliche Argumention dadurch zu begründen,
dass er ein Steuerzahler sei.
Das brachte ihm einen großen Steuerzahlerapplaus
von den teilnehmenden Fahrradfahrer\*innen,
die ihrerseits zum großen Teil ebenfalls Steuerzahler\*innen sind
und sich fragen,
warum ihre Steuergelder für eine Autoinfrastruktur verschwendet werden,
die zum einem dem Klima schadet
und zum anderen von ihnen gar nicht benutzt wird.

Kurz vor Ende versuchte uns in der Konrad-Adenauer-Allee
der Fahrer eines großen breiten SUVs zu erklären,
dass wir den Verkehr blockieren würden.
Fahrradfahrer\*innen sind aber auch Teil des Verkehrs.
Ein Verband von Fahrrädern ist sogar
mit die effizienteste Nutzung des Straßenraums.
Der Straßenraum, der von 60 Fahrrädern eingenommen wird,
reicht höchstens für 5 bis 10 Autos,
die dann meist sogar nur mit ein bis zwei Personen befüllt sind.
Allenfalls mit gut gefüllten Bussen
oder als Gruppe von Fußgänger\*innen
kann man den Straßenraum effizienter Nutzen
als mit einem Verband von Fahrrädern.
Als ob die Kritik noch nicht absurd genug sei,
handelt es sich bei der Konrad-Adenauer-Allee um eine Fahrradstraße.
Der Fahrer eines SUVs erklärte uns also in einer Fahrradstraße,
dass wir mit unseren Fahrrädern den Verkehr blockieren würden.

Der Verband löste sich schließlich
mit noch 18 oder 19 teilnehmenden Rädern
kurz nach 19:50
irgendwo zwischen dem Moritzplatz und dem Rathausplatz auf.


## Mittwoch 28.09.2022 -- **Tag 820**

Um 19:30 findet im Augustana-Saal (Im Annahof 4)
eine Podiumsdiskussion zm Thema
„Wissenschaft und Politik – wie passt das zusammen?“ statt.
Mit auf dem Podium werden auch Vertreter\*innen des Klimacamps sein.

[Link zur Veranstaltung](https://www.augsburg.de/presse-kommunikation/pressemitteilungen/detail?tx_news_pi1%5Baction%5D=detail&tx_news_pi1%5Bcontroller%5D=News&tx_news_pi1%5Bnews%5D=13093&cHash=5dd0200bd8f706b1fb2cff925125f0cd)

**Nachtrag:**
Die Veranstaltung hatte 30 Teilnehmer\*innen
(Moderation, Podium und zu spät Kommende mit eingerechnet).
Darunter waren mindestens 5 Personen aus dem Umfeld des Klimacamps.

### Impulsvortrag

Nach einer kurzen Einleitung durch Moderator Dr. Norbert Stamm,
folgte ein Impulsvortrag über evidenzbasierte Politik
durch Dr. Olivia Mitscherlich-Schönherr.
Der Vortrag behandelte das Konzept der *politischen Klugheit*
und die Förderung des demokratischen pluralistischen Austausches.
Dafür als Beispiel stellte sie das Konzept der Bürger\*innenräte
als die repräsentative Demokratie ergänzendes Werkzeuge vor.

### Podiumsdiskussion

Anschließend fand die eigentliche Podiumsdiskussion statt.
Daran nahmen teil:

* Dr. Norbert Stamm (Moderation)
* Lars Vollmar (FDP / Bürgerliche Mitte)
* Christine Kamm (Grüne)
* Dr. Florian Freund (SPD / Soziale Fraktion)
* Matthias Fink (CSU)
* Stefan (Klimacamp)
* Alex (Klimacamp)
* Dr. Olivia Mitscherlich-Schönherr (Hochschule für Philosophie in München)
* Prof. Dr. Christoph Weller (Lehrstuhl Politikwissenschaft, Friedens- und Konfliktforschung der Uni Augsburg)

![Das Bild zeigt die aufgezählten Teilnehmer\*innen von links nach rechts.
Stefan blickt in die Kamera,
Prof. Dr. Weller ist in das Schreiben von Notizen vertieft,
Alex hält ein Mikrofon und spricht gerade.
Die übrigen Teilnehmer\*innen blicken auf Alex.](/pages/material/images/2022-09-28_podiumsdiskussion/2022-09-28_panel-discussion-participants_small.jpg)

Ohne Aufzeichnung der Podiumsdiskussion ist es schwer bis unmöglich,
die verschiedenen Themen und Positionen alle akurat wiederzugeben.
Daher folgt eine stark verkürzte Zusammenfassung der Podiumsdiskussion
mit einzelnen als besonders wichtig oder interessant erachteten Punkten.

Ein interessantes Beispiel dafür,
wie wissenschaftliche Erkenntnisse
in der Stadtpolitik genutzt werden können,
nannte Dr. Florian Freund.
Eine Studie der Stadt hatte Erkenntnisse
über Kinderarmut in Augsburg erbracht.
Die Studie ergab, dass die Kinderarmut in Augsburg rückläufig sei,
nicht jedoch, wie man erwarten könnte,
aufgrund eines wirtschaftlichen Aufschwungs,
sondern aufgrund des Ausbaus der Kindertagesstätten
und der damit einhergehenden Erleichterung für Eltern
berufstätig zu sein.

Eigentlich sollte die Diskussion
auf einem höheren Abstraktionsniveau erfolgen,
aber zwei Mal verloren sich die Stadträt\*innen
dann doch in sehr speziellen Diskussionen.
Zum einen trugen sie die Diskussion
um den Stellplatzschlüssel für Lastenfahrräder,
die gerade im Stadtrat im Rahmen der neuen Stellplatzordnung verläuft,
aus dem Stadtrat in die Podiumsdiskussion.
Zum anderen gab es Uneinigkeit
bezüglich der Sinnhaftigkeit der Solarpflicht.

Die Stadträt\*innen waren sich einig,
dass alle demokratischen Parteien Klimaschutz
als wichtigste Aufgabe unserer Zeit erkannt hätten
und sie sich auch bei vielen Maßnahmen einig seien.
Uneinigkeit bestände allenfalls
bezüglich der Sinnhaftigkeit einzelner Klimaschutzmaßnahmen.

Dr. Olivia Mitscherlich-Schönherr betonte noch einmal,
dass die Frage „Nachhaltig? Ja oder nein?“
in der Gesellschaft unmissverständlich mit „Ja“ beantwortet sei.
Als Beispiel für die aktuelle Fragestellung nach der Strategie
wurde „Grünes Wachstum oder Postwachstum?“ genannt.

Prof. Weller meinte,
dass den unterschiedlichen Strategien für Klimaschutz
wahrscheinlich unterschiedliche Wertevorstellungen zu Grund lägen.
Wissenschaft könne bei der Klärung
der anzuwendenden Wertevorstellung nicht helfen.
Hierfür brauche es politischen Streit.
Dem Klimacamp müsse man dafür dankbar sein,
dass es diesen Streit in der Augsburger Lokalpolitik
immer wieder in den Fokus rückt.

Alex betonte wie wichtig Transparenz
für die Bürger\*innenbeteiligung in der Lokalpolitik sei.
Die Möglichkeit Stadtratssitzungen auch online zu verfolgen,
sei ein guter Schritt in die richtige Richtung.

Stefan streute noch unabsichtlich
einen inhaltlichen Fehler in die Diskussion ein.
Er sagte, dass die KlimaKom-Studie
eine Halbierung des motorisierten Individualverkehrs (MIV)
bis 2030 empfähle.
Die Umsetzbarkeit dieser Empfehlung war für mehrere Minuten
ein heiß diskutiertes Gesprächsthema.
Tatsächlich lautet die Empfehlung der Studie aber auf 2040.
Den Stadträt\*innen,
die mit den Empfehlungen der KlimaKom-Studie
zumindest grob vertraut seien sollten,
war der Fehler nicht aufgefallen. :-D

Hier nochmal der entsprechende Absatz aus der Studie,
welcher von einer zentralen Bedeutung für die Verkehrsplanung
in Augsburgs seien sollte:

> Deshalb empfehlen wir zur Substitution des fossilen MIV (MIVfossil)
> eine Strategie, die nicht nur auf Elektrifizierung setzt,
> sondern durch einen breiten Strategieansatz die Fahrleistungen
> des gesamten MIV bis 2040 um 50% reduziert und schon vor 2030
> durch Verkehrsverlagerung zum Umweltverbund und der
> Vermeidung von Fahrten im MIVfossil zu wirken beginnt.

-- Quelle: Seite 47 der [KlimaKom-Studie](https://www.augsburg.de/fileadmin/user_upload/umwelt_soziales/umwelt/klima%20und%20energie/Studie_Klimaschutz_2030_mit_allen_anlagen.pdf)

Die Podiumsdiskussion endete schließlich gegen 21:30.
Anschließend gab es noch in kleinen Runden
eine ganze Reihe persönlicher Gespräche
zwischen den verschiedenen Teilnehmer\*innen.

![Stefan und Alex blicken nach der Podiumsdiskussion
gut gelaunt in die Kamera.](/pages/material/images/2022-09-28_podiumsdiskussion/2022-09-28_kc-after-panel-discussion_small.jpg)


## Sonntag 25.09.2022 -- **Tag 817**

14:30 Uhr: **[Kidical Mass](https://augsburg.adfc.de/artikel/kidicalmass)** ab Königsplatz

Die Kidical Mass ist eine kinderfreundliche Fahrraddemo für alle.
Gefordert werden kinder- und fahrradfreundliche Orte.
Dazu gingen bereits am 14./15. Mai 40.000 Menschen
in über 200 deutschen Orten und 14 weiteren Ländern auf die Straße.

Siehe:
* [https://augsburg.adfc.de/artikel/kidicalmass](https://augsburg.adfc.de/artikel/kidicalmass)
* [https://kinderaufsrad.org/](https://kinderaufsrad.org/)

### Nachtrag:

Die Demo war ein voller Erfolg.
Bei guter Laune und guter Musik fuhr man gegen 14:42
vom Königsplatz erst Richtung Norden in die Fuggerstraße
und drehte dort einige Schlenker
– Prinzregentenstraße, Holbeinstraße, Schaezlerstraße.
Anschließend ging es nochmal am Königsplatz vorbei
in die Schießgrabenstraße,
dann die Eserwallstraße runter zum Roten Tor
und weiter in die Haunstetter Straße.
Dort, zwischen dem Roten Tor und dem Bahnhof Haunstetterstraße,
pausierte die Demonstration dann bis 15:30 auf der Straße.
Klimacamp- und Verkehrswendeaktivist Alexander Mai hielt eine Rede.
Anschließend ging es die Schertlinstraße hoch,
dann in die Hochfeldstraße und über verschiedene weitere Straßen
zum Wittelsbacher Park,
wo die Demo auf einer großen Wiese kurz nach 16 Uhr endete.

Zu Beginn wurden 186 Teilnehmer\*innen oder vielmehr Fahrräder gezählt.
Bei einer weiteren Zählung wenig später lag die Zahl bereits bei 241.
Ein Versuch während der Pause in der Haunstetter Straße
die Fahrräder und fahrradähnlichen Konstruktionen zu zählen,
musste bei 200 oder 220 aufgrund von Verwirrung abgebrochen werden.
Bei der Einfahrt in den Wittelsbacher Park wurden noch 183 gezählt.
Die tatsächliche Anzahl an Teilnehmer\*innen liegt höher,
da viele Kinder in Fahrradanhängern versteckt mitfuhren.

#### Fotostrecke:

Die Demonstration durchquert die Schießgrabenstraße.
![Die Fahrraddemo fährt die Schießgrabenstraße entlang nach Süden.
Die Gegenfahrbahn wird für den Gegenverkehr frei gehalten.
Ein paar Demonstrationsteilnehmer\*innen
fahren auch auf dem Radstreifen neben der Straße.](/pages/material/images/2022-09-25_kidical-mass_schießgrabenstraße.jpg)
Im Gegensatz zu anderen Fahrraddemos
lief die Kidical Mass mit etwas weniger Disziplin ab.
So gelang es leider nicht
den Fahrradweg neben der Demonstration frei zu halten.
Gefährlich war jedoch der Gegenverkehr.
Manche Autos bretternden mit hoher Geschwindigkeit
mit nur einem Meter Abstand an der Demonstration vorbei.

Das nördliche Ende der Haunstetter Straße
wird für einige Minuten zu einem Rastplatz.
Hierfür wurde auch der Verkehr in Gegenrichtung gesperrt.
![Das nördliche Ende der Haunstetter Straße
ist voller herumtollender Kinder, herumgehender Erwachsener
und abgestellter Fahrräder.
Anb einigen Fahrrädern sind Luftballons oder Fahnen angebracht.
Das Bild ist von der vorderen linken Ecke der Demo aufgenommen worden.
Im Hintergrund sieht man die mit Bäumen flankierte Straßenbahnstrecke
der Linien 2 und 3.
Am linken Bildrand sieht man noch
das Heck eines der beiden Polizeiautos,
welche der Demo voran fuhren.](/pages/material/images/2022-09-25_kidical-mass_haunstetter-straße.jpg)
In diesem wilden durcheinander fiel es schwer
die Teilnehmer\*innenzahl zu ermitteln.

Indem es auf dem Radweg parkt,
erinnert ein Auto wenige Meter weiter daran,
warum wir diese Demonstration durchführen.
![Das Foto zeigt einen schwarzen Kleinbus,
möglicherweise von Opel,
der auf dem Wartestreifen für linksabiegende Fahrradfahrer*innen
unmittelbar vor einer Ampel parkt.
Das Nummernschild ist verpixelt.](/pages/material/images/2022-09-25_kidical-mass_auto-auf-radweg.jpg)
Die Linksabbiegespur für Radfahrer\*innen in der Haunstetter Straße
zwischen Brunnenlechgäßchen und Schülestraße
ist leider ein beliebter Parkplatz.
Wir haben schon eine ganze Fotosammlung von Autos,
die mit deutlich erkennbaren Nummernschildern
genau an dieser Stelle parken.

Während der Beginn der Demo oben bereits in die Hochfeldstraße einbiegt,
sind am Ende der Demo noch nicht mal alle Teilnehmer\*innen
aus der Haunstetter Straße in die Schertlinstraße eingebogen.
![Das Foto ist vom Gehweg nahe der Einmündung der Hochfeldstraße
in die Schertlinstraße aus aufgenommen.
Von dort fällt die Schertlinstraße nach Osten hin ab.
Das Foto zeigt einen langen Strom von Fahrradfahrer\*innen,
die wie sie die Schertlinstraße hinauf fahren.
Viele der kleinsten Fahrradfahrer\*innen
fahren auf der rechten Seite der Straße nahe am Gehweg,
während es vor allem erwachsene Fahrradfahrer\*innen sind,
die nahe des Gegenverkehrs fahren.](/pages/material/images/2022-09-25_kidical-mass_schertlinstraße.jpg)
Die Kidical Mass ist eine polizeilich abgesicherte,
kinderfreundliche Fahrraddemo.
Trotzdem sieht man hier,
wie vor allem Erwachsene die Grenze
zwischen Demonstration und Gegenverkehr bilden
und die jüngsten Radfahrer\*innen in die Mitte nehmen.


## <span style="background-color: #80FF80">Freitag 23.09.2022 -- **Globaler Klimastreik** -- **Tag 815**</span>

**Ort:** Königsplatz<br>
**Zeit:** 16 Uhr

Trotz aller Wahlkampfversprechen bleibt die neue Bundesregierung
mit ihren Maßnahmen weit hinter dem zurück,
was notwendig wäre, damit Deutschland
seinen Anteil an der Einhaltung der 1,5°C-Grenze beiträgt.
Damit verspielt die Bundesregierung wertvolle Zeit.
Dabei ist klar, dass die Kosten der Folgen unzureichenden Klimaschutzes
um ein Vielfaches höher seien werden
als es wirkungsvolle und gerechte Klimaschutzmaßnahmen sind.
Am 23. September finden weltweit Proteste gegen
die unzureichenden Maßnahmen der jeweiligen Regierungen statt.
Auch Augsburg nimmt wieder daran Teil.<br>
In Augsburg beginnt die (Zu-Fuß-geh-)Demonstration
um 16 Uhr am Königsplatz.
Bitte verbreitet diese Nachricht!

Du bist an dem Tag nicht in Augsburg? Kein Problem! Siehe:

* [Deutschlandkarte der Streiks](https://fridaysforfuture.de/klimastreik/)
* [Weltkarte der Streiks](https://fridaysforfuture.org/action-map/map/) (enthält nicht alle deutschen Streiks)

Siehe auch:

* [https://www.fff-augsburg.de/](https://www.fff-augsburg.de/)
* [https://fridaysforfuture.de/](https://fridaysforfuture.de/)

**Persönlicher Nachtrag:**
*Dieser Nachtrag stellt weder die offizielle Position
des Klimacamps noch die von FFF-Augsburg dar,
sondern gibt die Beobachtungen von Ordner\*innen,
die vor Ort auf der Demo waren, wieder.*

*Die Demonstration war an sich ganz schön,
wenn man mal von einem unschönen Vorfall
– dazu später mehr –
und einem (nach persönlicher Meinung) mangelhaften Polizeieinsatz
– auch dazu später mehr – absieht.
Zu Beginn wurden bei zwei unabhängigen Zählungen in der Schaezlerstraße
über tausend Teilnehmer\*innen (ca. 1040 bzw. 1050) gezählt.
Wir gehen daher davon aus,
dass die Demo zu diesem Zeitpunkt
zwischen 1000 und 1100 Teilnehmer\*innen hatte.
Damit zählt sie nicht ganz zu den
aller größten FFF-Demonstrationen Augsburgs,
wie es sie vor der Pandemie gab,
aber man kann mit der Zahl zufrieden sein.
Angemeldet waren nur 700 Teilnehmer\*innen.
Also hat sie auf jeden Fall die Erwartungen mindestens erfüllt.

Später wirkte es so als wäre die Teilnehmer\*innenzahl angewachsen.
Zumindest war es ein massiver Demonstrationszug,
der sich über etwa einen halben Kilometer Länge erstreckte.
Leider hatten wir keine Kapazitäten für eine weitere Zählung frei.
Zu den Gründen später mehr.
<br>
Nicht alle Teilnehmer\*innen blieben bis ganz zum Ende.
Beispielsweise zweigten an der Kreuzung Hoher Weg und Karlstraße
mehrere kleinere Gruppen ab.*

*Die Demonstration begann am Königsplatz.
Nach einigen kurzen Ansprachen brach der Demozug
durch die Schaezlerstraße auf.
Vor dem Demozug fuhr ein Polizeiauto.
Es folgten einzelne Polizist\*innen zu Fuß links und rechts der Demo.
Vielleicht nach etwa dem ersten Drittel des Demozuges
gab es einen linken antikapitalistischen Block
von einigen dutzend Demonstrant\*innen.
Dieser wurde links und rechts
durch ein massives Aufgebot an Polizist\*innen flankiert.
Sowohl die Präsenz eines antikapitalistischen Blocks
von Demonstrant\*innen als auch ein stärkeres Aufgebot an Polizei
in dessen Nähe sind nichts Ungewöhnliches.*

*Ein sehr unschöner Vorfall ereignete sich
als der Demozug durch eine enge Straße ging,
an dessen Seite die Ausfahrt eines Parkhauses liegt.
Die folgende Schilderung basiert zum Teil auf Hörensagen,
zum Teil wurde sie aber auch selbst beobachtet.
Anscheinend hatte ein Autofahrer versucht
aus der Ausfahrt herauszufahren
und sich durch die Demonstration zu drängen.
(Die Augsburger Allgemeine berichtet,
dass ein Teilnehmer der Demonstration vom Auto gestreift wurde.)
Darauf stellten sich Demonstrationsteilnehmer\*innen
dann dem Auto demonstrativ in den Weg und
versuchten dem Fahrer über Klopfen auf die Motorhaube zu signalisieren,
dass es hier derzeit nicht weiter geht.
Der Wagen stand nun schon ein Stück auf der Straße,
sorgte für eine merkliche Verengung und
bildete damit ein Hindernis für den Demonstrationszug.
Die Berührung des Autos störte anscheinend den Fahrer,
der dann Ausstieg und laut und „handgreiflich“ wurde.
Eine Person schilderte,
dass mindestens ein\*e Demonstrationsteilnehmer\*in
anschließend blutete.
Zwar waren Ordner\*innen recht schnell vor Ort,
aber es dauert eine ganze Weile (evtl. 1 Minute?)
bis Polizist\*innen vom antikapitalistischen Block
nach Vorne gerannt kamen und den Fahrer von der Demonstration trennten.
Ein paar Polizist\*innen, ein paar Ordner\*innen
und ein paar Augenzeug\*innen
blieben anschließend zur weiteren Klärung am Ort des Vorfalls zurück,
während die Demo ihren Weg fortsetzte.<br>
Das war übrigens nicht der erste Vorfall bei einer FFF-Demo
an genau dem selben Parkhaus.*

*Es dauerte eine ganze Weile,
nämlich bis die Demonstration wieder auf dem Rückweg zum Königsplatz war
und an der großen Kreuzung oben am Leonhardsberg
von der Straße* Hoher Weg *in die Karlstraße abbog,
dass uns ein gravierendes Problem auffiel.
Die Polizei sicherte den Verkehr nicht.
Zwischen dem antikapitalistischen Block und dem Ende der Demo
lagen vielleicht 400 oder 500 Meter,
in denen kein\*e einzige\*r Polizist\*in zu sehen war.
Sobald das Problem bemerkt wurde,
wurde es über Walkie-Talkies an die Versammlungsleitung gemeldet
und so etwa vier oder fünf Ordner\*innen
stellten sich zwischen den Verkehr und die Demonstration
auf die Kreuzung.
Die Autofahrer\*innen blieben aber auch ohne Polizeipräsenz ruhig
und machten keinerlei Anstalten sich durch eine Lücke in der Demo
zu drängeln.
Als man in einem Einsatzwagen am Ende der Demo
den Zustand der Kreuzung bemerkte,
fuhr der Einsatzwagen schnell vor und stellte sich zur Absicherung
der letzten vielleicht fünfzig Teilnehmer\*innen des Demozuges
noch auf die Kreuzung.<br>
Auch alle Seitenstraßen in der Karlstraße und Grottenau
waren ungesichert.
Wieder übernahmen Ordner\*innen notdürftig die Absicherung.
Erst oben am Staatstheater sicherte wieder
ein Motorradpolizist die Kreuzung.*

*Nun sollte sich die Polizei mal fragen,
ob ihre Fixierung auf den antikapitalistischen Block
so angebracht ist.
Zwar gab es beim antikapitalistischen Block
einer anderen Demo auch schon mal den Vorfall,
das ein Bengalo (=Pyrotechnik mit starker Rauchentwicklung)
angezündet und auf dem Boden liegen gelassen wurde.
Allerdings geht durch den antikapitalistischen Block
im Allgemeinen keine Gefahr für Personen aus,
während Autofahrer\*innen, welche sich in die Demo drängen,
oder Passant\*innen,
die handgreiflich werden oder Demoteilnehmer\*innen bedrängen,
immer wieder präsente Probleme sind.
Wir wissen, dass die Polizei eine bessere Arbeit leisten könnte,
weil sie es bei vergangenen Demos und
so auch verschiedenen Fahrraddemos diese Woche
bereits bewiesen hat.<br>
Waren die Polizist\*innen vielleicht kurzfristig
zu einem anderen Einsatz abgezogen worden?
Wir wissen es nicht und kennen die Hintergründe nicht.*

*Sonstige Ereignisse:
Während des Zuges durch die Karlstraße und Grottenau,
überholte ein Krankenwagen mit Blaulicht die Demo.
Da die Demo die Gegenverkehrsfahrbahnen frei gelassen hatte
und der Gegenverkehr durch den forderen Teil der Demo
– mit Ausnahme der* Kleinen Grottenau *und
vielleicht der Fuggerstraße – abgeblock war,
kam der Krankenwagen mindestens genauso gut wenn nicht gar besser
als bei normaler Verkehrslage durch die Straßen.
Gleichzeitig war der fordere Teil des Demo über Walkie-Talkies
über das Herannahen des Krankenwagens informiert worden.*

*Zurück am Königsplatz gab es dann noch weitere Reden.
Die Demo endete dann zwischen 17:30 und 18:00.*

**Nachtrag:**
Am Abend gegen 20:45 gab es
am Klimacamp spontan einen Kletterkurs.

**Sonstiges:**
[*Pimmelgate Süd*](https://www.pimmelgate-süd.de/)
hat es heute zu einer Erwähnung in der New York Times geschafft.
Siehe auch unseren
[Pressespiegel vom 23.09.2022](/pressespiegel/#23092022).


## Donnerstag 22.09.2022 -- Fahrraddemo für besseren ÖPNV und sichere Radwege -- **Tag 814**

Zeit: **12:00**<br>
Start: Gögginger Wochenmarkt<br>
Ziel: Zwölf-Apostel-Platz<br>
Derzeit gibt es passabele Straßenbahnlinien in die Innenstadt.
Die Verbindungen zwischen benachbarten Stadtteilen
sind dagegen stellenweise sehr schlecht.
Mit der Fahrraddemo wollen wir nicht nur unsere Forderung
nach besserem ÖPNV und sicheren Radwegen bekräftigen,
sondern auch auf vorgeschlagene Straßenbahnringverbindungen hinweisen.
* Eine vorgeschlagene Ringverbindung verläuft
  vom südlichen Universitätsviertel
  über den Bahnhof *Augsburg Messe*, Göggingen und die Rosenaustraße
  zur neuen Straßenbahnhaltestelle unterhalb des Hauptbahnhofs
  und von dort über das Theater
  und eine neu einzurichtende Ost-West-Achse durch die Karlstraße
  zum Jakobertor.
* Eine andere vorgeschlagene Ringverbindung
  soll die bestehende Straßenbahnlinie 1
  von der Haltestelle *Neuer Ostfriedhof*
  und den Bahnhof *Augsburg Hochzoll*
  in den Süden von Hochzoll verlängern.

Die Stadt führt im Rahmen ihres
„[2. Mobilitätsforum on Tour](https://www.augsburg.de/buergerservice-rathaus/verkehr/augsburger-mobilitaetsplan/informieren-mitmachen#c1122171)“
von 9:00 bis 12:00 am Gögginger Wochenmarkt
und von 15:00 bis 18:00 am Zwölf-Apostel-Platz
Veranstaltungen mit dem Titel „Mobilität im Viertel“ durch.
Die Fahrraddemo stellt nicht nur eine klimafreundliche Möglichkeit dar,
um von der einen Veranstaltung zur anderen zu gelangen,
sondern führt auch entlang geforderter ÖPNV-Verbindungen
und zahlreicher Problemstellen,
die Fahrradfahren in Augsburg gefährlich gestalten.

**Nachtrag:**
Auch die heutige Demo war außergewöhnlich.
Die Demo bestand aus lediglich sechs Teilnehmern.
(Es ist ok hier nicht zu gendern,
da es tatsächlich nur männliche Teilnehmer waren.)
Aufgrund der geringen Teilnehmerzahl
sprach man mit der Polizei verschiedene Änderungen ab.
Zum einen sprach man ab,
dass die Demo Fahrradwege anstatt der Straße nutzt,
wo auch immer geeignete Fahrradwege zur Verfügung stehen.
Wo keine Fahrradwege oder
lediglich kombinierte Rad- und Fußwege zur Verfügung standen
oder Radwege nicht sehr breit waren
und lediglich durch einen Streifen
und nicht räumlich vom Fußweg getrennt waren,
konnte die Demo die Straße nutzen.
Des Weiteren wurden die Polizeifahrzeuge abgezogen
und die Demonstration lediglich von zwei Fahrradpolizisten begleitet.
Wenn man mal davon absieht,
dass bei Beginn der Demo vorrübergehend Polizeiautos anwesend waren,
und man davon ausgeht,
dass beteiligte Elektroräder mit Ökostrom geladen wurden,
dann war dies damit eine nahezu klimaneutrale Demonstration.

Die Demo wurde durch eine von einem Fahrrad gezogene
vier Meter lange Modellstraßenbahn begleitet.
Im Gegensatz zu vielen anderen Straßenbahnen der swa
ist diese Modellstraßenbahn übrigens werbefrei.
![Das Bild zeigt die neue Modellstraßenbahn des Klimacamps.
Die Straßenbahn besteht aus zwei Fahrradanhängern,
auf denen eine Straßenbahn aus Pape inklusive Stromabnehmer
drauf gebaut wurde.
Gezogen wird sie von einem Fahrrad.
Sie Straßenbahn ist ohne Fahrrad etwa vier Meter lang.](/pages/material/images/2022-09-22_Straßenbahn.jpg)

An der Einmündung der Eichleitner Straße in die Gögginger Straße
pausierte die Demonstration für eine Gedenkminute.
Hier war wenige Wochen zuvor eine Fahrradfahrerin
von einem rechts in die Eichleitner Straße abbiegenden LKW
erfasst und getötet worden.
Als die Demonstration im Anschluss an die Gedenkminute
ihren Weg fortsetzte,
stand auch ein nach rechts blickender LKW an der Kreuzung.
Er wartete jedoch, bis die Demonstration die Straße überquert hatte.

Auch an der Kreuzung von Karlstraße und Karolinenstraße
sollte eine kurze Zwischenkundgebung erfolgen.
Diese wurde kurzfristig in die Länge gezogen,
da ein Passant einen epileptischen Anfall erlitt.
Die beiden Fahrradpolizisten eilten zum Passenten.
So wartete die Demonstation
die Ankunft des Krankenwagens ab,
bevor sie ihre Tour fortsetzte.

Schließlich traf die Demonstration gegen 14:15
mit noch fünf Teilnehmern am Zwölf-Apostel-Platz ein.
Dort wurde man schon von den Mitarbeiter\*innen der Stadt begrüßt.
Die aktuellen, extensiven Erfahrungen
mit dem ständigen Hin und Her zwischen Straße und Radwegen
und dem schlechten Zustand vieler Radwege
wurde dann auch gleich als Feedback weitergegeben.

![Das Bild zeigt die Modellstraßenbahn
auf dem Zwölf-Apostel-Platz.
Das Bild ist hochkant,
damit im Hintergrund noch der Turm
der Kirche „Zu den Heiligen Zwölf Aposteln“
drauf passt.
](/pages/material/images/2022-09-22_Straßenbahn-vor-Zwoelf-Aposteln.jpg)

Kleine Demos wie diese haben ihren eigenen Charme.
Für die Autofahrer\*innen war es sicher etwas angenehmer,
da sie in Anwesenheit geeigneter Radwege die Demonstration
überholen konnten.
So sahen sie auch mehr von der Demonstration
als wenn sie hinter dieser im Stau gestanden hätten.
Das Feedback der Passant\*innen war überaus positiv.
Kritische Stimmen gab es nur vereinzelt.
Beispielsweise rief uns im Pferseer Tunnel jemand
„Ihr blockiert den Verkehr.“ zu.
Vermutlich als Kritik gemeint, so war die Aussage korrekt.
Wie fuhren wie mit der Polizei vorher abgesprochen auf der Straße,
da es im Pferseer Tunnel keinen abgetrennten Radweg gibt.
Der Pferseer Tunnel gilt schon lange als Gefahrenquelle.
Auch hier kam es erst im letzten Jahr zu einem tödlichen Unfall.

Negative Stimmen waren aber die Ausnahme.
Fußgänger\*innen und Fahrradfahrer\*innen
äußerten ihr Einverständnis in verschiedener Form.
Es wurde viel gewunken.
Es ist schon lustig und ermutigend,
dass sich nach fünf Fahrraddemonstranten,
einem Straßenbahnmodell und zwei Fahrradpolizisten
mehr Leute umdrehen und wohlwollender reagieren
als sie es bei so einem teuren tiefergelegten Auto mit lautem Motor
tun würden.
Besonders an Haltestellen stießen die Botschaften der Demo
auf sehr viel Zustimmung.
An einer Stelle brach sogar spontaner Applaus aus,
als die Demonstration vorbei fuhr.

Fazit zur kleinen Demo: Gerne mal wieder machen.
Alle Teilnehmer hatten ihren Spaß.<br>
Sowohl große, mittelgroße als auch kleine Demos
haben ihre eigenen Vor- und Nachteile.
Schade war lediglich,
dass hier eine als mittelgroß geplante Demo
spontan zu einer kleinen Demo umfunktioniert wurde
anstatt von Anfang an als kleine Demo geplant gewesen zu sein.


## Mittwoch 21.09.2022 -- **B17-Raddemo** und **Raddemo für Tramausbau** -- **Tag 813**

* **14:00** am Messezentrum: Fahrraddemonstration über die B17<br>
  Start: Am Messezentrum<br>
  Ziel: Straßenbahnhaltestelle *Oberhausen Nord P+R*<br>
  Die Demonstration ist im Allgemeinen der Mobilitätswende
  und konkret der Einrichtung von Busspuren und Schnellbuslinien
  entlang der groben Route der Demonstration gewidmet.<br>
  Die Fahrraddemo beginnt am Messezentrum
  und endet an der Straßenbahnhaltestelle *Oberhausen Nord P+R*.
  Die geplante Route (ca. 11km)
  verläuft zum Teil über die Bundesstraße B17.
  Details zur Route wollen wir in einigen Tagen bekannt geben.<br>
  Die Stadt führt an der Straßenbahnhaltestelle *Oberhausen Nord P+R*
  von 15 Uhr bis 18 Uhr im Rahmen ihres
  „[2. Mobilitätsforum on Tour](https://www.augsburg.de/buergerservice-rathaus/verkehr/augsburger-mobilitaetsplan/informieren-mitmachen#c1122171)“
  eine Aktion namens „Pendlertag mit dem AVV“ durch.
  Wir bitten darum, diese Veranstaltung nicht zu stören,
  sondern sie zu Nutzen, um der Stadt und dem AVV
  beim Thema *Mobilitätswende* auf die Sprünge zu helfen.
* **16:00** Fahrraddemonstration für den Ausbau der Tram zum Bahnhof Gersthofen<br>
  Start: Oberhausen Nord P+R<br>
  Ziel: Bahnhof Gersthofen<br>
  Mit der Demonstration wollen wir den Zielen der Mobilitätswende,
  dem Ausbau des öffentlichen Personennahverkehrs
  und konkret der Forderung nach einer Erweiterung der Straßenbahn
  bis zum Bahnhof Gersthofen Nachdruck verleihen.

Für Informationen zu Verkehrswendezielen siehe auch:
[https://www.verkehrswende-augsburg.de/](https://www.verkehrswende-augsburg.de/).


**Nachtrag:**
Der Tag war sehr ereignisreich.

Gegen 14 Uhr versammelte man sich an der Messe
für die erste Fahrraddemo des Tages.
Dort ging es dann um kurz nach 14:30
mit erstmal etwa 25 Teilernehmer\*innen los.
Vorbei ging es dann auch an der Einmündung der Eichleitnerstraße
in die Gögginger Straße.
Hier war erst vor wenigen Wochen eine Fahrradfahrerin
von einem LKW erfasst und getötet worden.
An der Bügermeister-Ackermann-Straße führte die Route auf die B17.
Dort wurden dann 43 Teilnehmer\*innen gezählt.
Dass die Zahl während der Demo schwankt, ist nichts Ungewöhnliches.
Da die Route vorher bekannt war und die Position der Demo
in sozialen Netzwerken sogar live kommuniziert wurde,
entschieden sich Einige, erst zur Demo dazuzustoßen,
wenn diese in der Nähe der eigenen Wohnung vorbeifährt.
Andere Fahrradfahrer\*innen sahen die Demo und beschlossen spontan,
sich dieser anzuschließen.
Kurz vor 15:40 traf die Demo dann
an der Haltestelle *Oberhausen Nord P+R* ein.
Die Polizei sperrte ein kleines Stück der Diedorfer Straße ab,
so dass dieses als notdürftiger Fahrradparkplatz dienen konnte.

![Das Bild zeigt Fahrradfahrer auf der B17.
Der Asphalt ist grau, der Himmel ist blau
gesprenkelt mit einzelnen weißen Wolken.
Auf beiden Seiten der Straße sind Lärmschutzwände.
Eine der Lärmschutzwände schirmt auch
den Blick auf die Gegenfahrbahn ab.
Die Radfahrer verteilen sich auf die zwei Fahrspuren.
Zusätzlich gibt es noch eine Abbiegespur.
In der Entfernung sieht man eine Brücke über die B17.](/pages/material/images/2022-09-21_B17.jpg)

Viele Teilnehmer\*innen der Demonstration besuchten anschließend
den *Pendlertag mit dem AVV*.
Warum der AVV ein Event,
welches sich explizit an Pendler\*innen richtet,
ausgerechnet mitten unter der Woche von 15 Uhr bis 18 Uhr ausrichtet,
erscheint nicht nachvollziehbar.
Als Pendler\*in muss man sich entweder Urlaub nehmen,
wie es so manche\*r Demonstrationsteilnehmer\*in getan hat,
oder auf dem Heimweg nach der Arbeit einen Umweg
über *Oberhausen Nord P+R* machen.
Mehr Glück hatten die Polizist\*innen,
die unsere Demonstrationen begleiteten.
Der ein oder andere Polizist wurde dabei beobachtet,
wie er selbst die Infotafeln des *Pendlertags* betrachtete. :-)<br>
Es gab verschiedene Möglichkeiten,
beispielsweise über farbige Klebepunkte,
Feedback zu geben.
An einer Pinnwand konnte man Kärtchen mit eigenen Texten hinterlassen.
Auf einer Infotafel mit dem Titel *Augsburger Mobilitätsplan*
wurde auch schön beschrieben,
wie die Stadt vor hat, die nächsten Jahre
mit Konzeption, Definition und Planung zu verbringen.
([Sir Humphrey Appleby wäre stolz. (Youtube)](https://youtu.be/gmOvEwtDycs?t=55))
Maßnahmen und Handlungen wurden dagegen ausgespart.
Stattdessen gab es noch
ein paar schön klingende Stichpunkte zur Zielsetzung
wie *nachhaltig*, *barrierfrei*, *autoarm* und *vernetzt*.
Die Tatsache, das es 10 Monate nach dem Erscheinen der KlimaKom-Studie
mit ihren Empfehlungen für den Augsburg Mobilitätssektor
noch keine konkreten Maßnahmen gibt,
sorgte für hitzige Diskussionen.
Es ist ein Armutszeugnis von Augsburgs Verkehrspolitik.
Vor Ort verwiesen Mitarbeiter\*innen der Verwaltung
dann auf die Politik,
welche die Entscheidungen über konkrete Maßnahmen zu treffen habe.
Politiker\*innen wie Eva Weber verweisen dagegen gerne
auf Bürger\*innenbeteiligungsmöglichkeiten wie das Mobilitätsforum,
in dessen Rahmen der *Pendlertag mit dem AVV* abgehalten worden war.

Gegen 16:30 begann dann die zweite Fahrraddemo,
die entlang einer vorgeschlagenen Verlängerung der Straßenbahnlinie
von *Oberhausen Nord P+R* zum Bahnhof von Gersthofen verlief.
Mit 28 Teilnehmer\*innen endete die Demo
schließlich an eben jenem Bahnhof.

Nun war man aber draußen in Gersthofen und wollte nach Hause.
18 der Personen entschieden sich gemeinsam im geschlossenen Verband,
wie man es beispielsweise von der
[Critical Mass](https://criticalmass-augsburg.de/info) kennt,
zurück nach Augsburg zu fahren.
Die Regeln sind wie folgt:

* 16 oder mehr Radfahrer\*innen können gemeinsam
  einen geschlossenen Verband bilden.
* Wenn das erste Fahrrad des Verbands bei grün über eine Ampel fährt,
  darf der Rest des Verbands folgen,
  selbst falls die Ampel zwischendrin auf rot umschaltet.
* Der Verband von Fahrradfahrer\*innen
  darf auch dann auf der Straße fahren,
  wenn es nebendran einen Fahrradweg gibt.
* Andere Verkehrsteilnehmer\*innen, wie beispielsweise Autos,
  dürfen sich nicht in den Verband reindrängen oder diesen unterbrechen.

Für weitere Details siehe die Straßenverkehrsordnung
– insbesondere §27.<br>
Da die Fahrt im Verband keine Demonstration war,
war diese auch nicht mehr von der Polizei abgesichert.
Das machte sie nicht weniger ereignisreich
als die beiden Demonstrationen.
Damit ein Krankenwagen mit Blaulicht den Verband überholen konnte,
fuhr man rechts an den Straßenrand und hielt kurz an.
Auf dem Weg durch Gersthofen und den Norden Augsburgs
wurde der Verband mehrfach von aggressiven Autofahrer\*innen
auf der Rechtsabbiegerspur und der Linksabbiegerspur überholt.
Wir nehmen stark an, dass dieses Verhalten
im Gegensatz zu unserem Verband
nicht im Einklang mit der Straßenverkehrsordnung war.
Da die Fahrradfahrer\*innen an der Spitze des Verbands
jedoch keine Dashcam im Einsatz hatten
und auch niemand Lust hatte,
sich die Nummernschilder der Autos zu merken,
kommen die Autofahrer\*innen dieses mal
wohl noch um eine Anzeige herum.

Leider ist rücksichtsloses und lebensgefährendes Verhalten
durch Autofahrer\*innen etwas,
was man als Radfahrer\*in ziemlich oft erleben kann.
Der Eindruck ist, dass die Menschen am Steuer vieler Autos
gerade nicht in einer Verfassung sind,
in der sie ein schnelles tonnenschweres Fahrzeug
in der Anwesenheit von anderen Verkehrsteilnehmer\*innen führen sollten.
Das äußert sich beispielsweise dadurch,
dass diese Autofahrer\*innen bei kurzen Verzögerungen
gleich ungeduldig zur Hupe greifen.
Ungeduld, Zeitdruck, Müdigkeit und Stress sind alles Faktoren,
die sich auf die Konzentration auswirken
und zu Fahrfehlern führen können.
Nun ist man aber häufig müde oder gestresst,
wenn man aus der Arbeit kommt,
und Alternativen zum Auto gibt es für viele Wege nicht.
Daher sehen wir diese Menschen vor allem auch als Opfer
einer jahrzehntelangen schlechten Verkehrspolitik.<br>
Siehe auch:
[Unseren Artikel zur Mobilitätswende](/informationen/artikel/mobilitaetswende/)

In der Nähe der Haltestelle *Wertachbrücke*
löste sich der Verband dann auf
und man fuhr auf den Fahrradwegen
oder in Ermangelung dieser mancherorts auch auf der Straße weiter.
Mehrere Demonstrationsteilnehmer\*innen gelangten so
gegen 18 Uhr zum Klimacamp.

Um 19 Uhr gab es noch ein kleines Konzert zur Klimakrise.
Dort wurden Stücke wie ‚„how dare you?“ 1921-2020‘
und „tipping points“ (ur-)aufgeführt.
Das sorgt für einen sehr netten Ausklang
für diesen sehr anstrengenden Tag.


**Eindrücke:**

Die Polizei leistete bei der Abschirmung der Fahrraddemos
von aggressiven Autofahrer\*innen wieder mal eine gute Arbeit
beziehungsweise verhielten die Autofahrer\*innen sich anständig.
Bei anderen Fahrraddemos kam es bei unzureichender Absicherung
vereinzelt schon vor,
dass Autofahrer\*innen versuchten sich durch Lücken
zwischen den Demonstrationsteilnehmer\*innen durchzuquetschen
und so mitunter sehr gefährliche Situationen herbeiführten.
Kleinere Demos, wie die heutige, sind hier weniger gefährdet.

Das Konzept einer B17-Fahrraddemo an einem Werktag
war ein interessantes Experiment.
An einem Freitagabend, Samstag oder Sonntag
wäre die Teilnehmer\*innenzahl
wahrscheinlich leicht dreistellig geworden.
Denn entgegen den Behauptungen mancher politischer Gegner\*innen
ist ein Großteil der Menschen in der Klimagerechtigkeitsbewegung
berufstätig oder geht zur Schule.
Auf jeden Fall hat die Demo viel Spaß gemacht
und es herrschte eine recht gute Laune.

Mit dem Beginn der Demo am frühen Nachmittag wurde auch bezweckt,
die Sperrung der B17 aus den Berufsverkehrzeiten heraus zu halten.
Wie so ziemlich jede Fahrraddemo,
erfuhren auch die heutigen beiden Demos
sowohl Zuspruch als auch Ablehnung.
Einige Autofahrer\*innen machte wüste Gesten
in Richtung der Demonstration, aber das waren nicht sehr viele.
Viele Autofahrer\*innen äußerten aber auch ihre Zustimmung,
beispielsweise indem sie einen Daumen hoch
in Richtung der Demo zeigten.
Beim Hupen und vielen Rufen war jedoch nicht zu erkennen,
ob mit diesen Zustimmung oder Ablehnung der Demonstration
signalisiert werden sollte
oder ob sie vielleicht sogar gar nicht der Demo,
sondern beispielsweise dem vorausfahrenden Auto gewidmet waren.
Letztendlich sind der große Feind der Autofahrer\*innen
nicht die sporadisch stattfindenden Demonstrationen,
sondern in erster Linie die anderen Autofahrer\*innen
und steigenden PKW-Zahlen,
die vor ihnen die Straßen verstopfen.
Von den Fußgänger\*innen und Fahrradfahrer\*innen,
welche die Demos passierte,
gab es neben verwunderten Blicken vor allem Zuspruch.


## Montag 19.09.2022 -- **Tag 811**

Heute haben wir die Webseite ein wenig umgestaltet.
*Programm & Tagebuch* befinden sich nun
nicht mehr auf der gleichen Seite,
sondern wurden in [Programm](/programm/)
und [Tagebuch](/tagebuch/) aufgetrennt.


## Samstag 17.09.2022 -- **Tag 809**

### Abbau des Klima-Regierungscamps

Diesen Freitag und Samstag hatte das Bayerische Staatsministerium
für Umwelt und Verbraucherschutz auf dem Rathausplatz
in Sichtweite unseres Klimacamps ein zweites Klimacamp aufgebaut.
Diesem Camp hatten wir bereits am 15. September
[eine kritische Pressemitteilung](/pressemitteilungen/2022-09-15-klima-regierungscamp/)
gewidmet.
Die von der Regierung an die Bürger\*innen
propagierten Empfehlungen sind aber
bestenfalls ein Tropfen auf den heißen Stein.
Die Aktion der Regierung kann als Ablenkung
von tatsächlich notwendigen politischen Maßnahmen,
wie der Zurücknahme der 10H-Regel, verstanden werden.

Am frühen Abend war der Spuk dann vorbei
und die Zelte wurden wieder abgebaut.


### Behinderung für Fahrradfahrer\*innen

Fahrradfahrer\*innen in Augsburg müssen sich
durch zahlreiche Hindernisse hindurch navigieren.
Dazu zählen Schlaglöcher, Glasscherben und
auch auf den Radwegen abgestellte Fahrzeuge.
Das folgende Foto entstand am heutigen Tag.
Die Autonummer und der Firmenname
wurden aus Datenschutzgründen verpixelt.
![Das Foto zeigt einen Einsatzwagen der bayerischen Bereitschaftspolizei,
der im Halteverbot steht und so weit in den Radweg hineinreicht,
dass Fahrradfahrer\*innen auf die Straße ausweichen müssen.
Die Autonummer und der Schriftzug „Polizei“ wurden verpixelt.](/pages/material/images/2022-09-17-Einsatzwagen-in-Parkverbot-verpixelt.jpg)


## Freitag 16.09.2022 -- **Parking Day** und **Auto-Frei-Tag** -- **Tag 808**

Der *Parking Day*
ist ein jährlich begangener internationaler Aktionstag.
Er ist der (Rück)Umwandlung von Parkplätzen
zu Grünflächen, Aufenthaltsbereichen, Fahrradstellplätzen
und allgemein öffentlich nutzbarem Raum gewidmet.

Augsburg begeht zeitgleich seinen *Auto-Frei-Tag*.

* **07:00 bis 08:00**:
  Pop-Up-Fahrradstraße vor dem Schulzentum in Neusäß<br>
  Hintergrund der Aktion ist die Bewerbung
  eines zukunftsorientierten Verkehrskonzepts
  für das Schulzentrum und sein Umfeld.
  Zu den Zielen gehören die Ermöglichung sichereres Fahrradfahrens
  und ein sicherer Schulweg vor dem Neusäßer Schulzentrum.
  Erreicht werden soll das über die Forderungen
  der Bahnunterführung als autofreien Fahrradstraße
  und der Rembold- und Landrat-Dr. Frey-Str.
  als autofreien Fahrradstraße
  zwischen Siegfriedstraße und Pallerstraße.
* **09:30 bis 12:00**: Aktion für eine Autofreie Hallstraße<br>
  Die Hallstraße steht seit vielen Jahren in der Kritik,
  da sie mitten durch ein Schulzentrum verläuft.
  In letzter Zeit finden während der Schulpausen
  vermehrt Demonstrationen für die Autofreiheit der Straße statt.
  So auch wieder an diesem Tag.
  Anstatt einer Gefahrenstelle wollen wir hier
  einen sicheren Aufenthaltsraum für Schüler\*innen schaffen.
* **15:00 bis 18:00**: Bahnhofstraße Autofrei<br>
  Während die Stadt Augsburg wie schon seit Jahren
  zum x.ten Mal in Folge, Bürger\*innenbeteiligungsmöglichkeiten
  zu simulieren versucht,
  zeigen wir der Stadt, wie Handeln aussieht.
  Dazu starten wir einen Verkehrsversuch *Bahnhofstraße Autofrei*.
  Wie auch bereits beim
  [Straßenfest auf der Karlstraße](#1400-bis-1700-straßenfest-auf-der-karlstraße)
  seid Ihr dazu eingeladen,
  alternative Nutzungsmöglichkeiten der Straßen- und Parkfläche
  auszuprobieren.<br>
  Mobilität soll umwelt- und klimafreundlich gestaltet werden.
  Wir möchten eine Stadt mit lebenswertem Platz für Menschen
  anstatt für Autos,
  eine Stadt mit einer hohen Aufenthaltsqualität,
  Platz für Kommunikation, Kultur und Regeneration
  und frei von Motorlärm, Feinstaubbelastung und Gestank.<br>
  Die Stadt wird parallel dazu im Rahmen ihres
  „[2. Mobilitätsforum on Tour](https://www.augsburg.de/buergerservice-rathaus/verkehr/augsburger-mobilitaetsplan/informieren-mitmachen#c1122171)“
  wenige hundert Meter entfernt am Königsplatz
  unwissend die Frage stellen,
  was es für eine lebenswerte und gut erreichbare Innenstadt braucht.
  Fühlt euch frei der Stadt diese dringend benötigte Nachhilfe zu geben.
  ![Das Bild zeigt die Bahnhofstraße.
  Rechts im Bild hält ein Verkehrswendeaktivist
  ein Plakat mit der Aufschrift „Verkehrswende jetzt“ in die Kamera.
  Links im Bild stehen zwei Aktivist\*innen von Greenpeace,
  die ein großes Banner mit der Aufschrift
  „Städte für Menschen nicht für Autos“
  in die entgegengesetzte Richtung halten.
  Der Schriftzug schimmert durch den Stoff des Banners durch.
  Weiter sieht man mehrere Aktivist\*innen, Passant\*innen,
  einen Pavillion, kleinere Klappstühle- und tische, Lautsprecher
  sowie im Hintergrund ein Polizeiauto.
  Gerade so nicht mehr im Bild erkennbar sind Aktivist\*innen,
  die hinter dem Pavillion auf der Straße Federball spielen.
  Der Boden ist feucht, weil es kurz zuvor geregnet hatte.
  Links und rechts der Straße stehen Bäume,
  deren Blätterdach sich über die Straße spannt.](/pages/material/images/2022-09-16_Auto-Frei-Tag.jpg)

Für Informationen zum Auto-Frei-Tag siehe auch:
[https://www.verkehrswende-augsburg.de/termine/#169-auto-frei-tag](https://www.verkehrswende-augsburg.de/termine/#169-auto-frei-tag).

Auch andere Organisationen planen zum Parking Day ein Programm.
So sind uns beim Stöbern im Internet hierauf aufmerksam geworden:
[https://www.rechts-der-wertach.de/parkingday/](https://www.rechts-der-wertach.de/parkingday/)


### Fahrradseptember

Der Parking Day ist der erste von mehreren Tagen mit Aktionen,
die die Mobilitätswende thematisieren und
sich speziell an Fahrradfahrer richten.
Dies hat dem Monat den Spitznamen
*aktivistischer Fahrradseptember* eingebracht.
Weitere Aktionen für Fahrradfahrer sind:

* 16.09.2022 [Auto-Frei-Tag mit verschiedenen Aktionen](#freitag-16092022--parking-day-und-auto-frei-tag--tag-808)
* 21.09.2022 [Zwei Fahrraddemos](#mittwoch-21092022--b17-raddemo-und-raddemo-für-tramausbau--tag-813)
* 22.09.2022 [Fahrraddemo](#donnerstag-22092022--fahrraddemo-für-besseren-öpnv-und-sichere-radwege--tag-814)
* 25.09.2022 [Kidical Mass](#sonntag-25092022--tag-817)
* 30.09.2022 [Critical Mass](/programm/#freitag-30092022) (monatlich am letzten Freitag)

[Terminplan von Verkehrswende-Augsburg.de](https://www.verkehrswende-augsburg.de/termine/).


## Samstag 10.09.2022 -- **Tag 802**

Die Grüne Jugend plant an diesem Tag
am Rathausplatz gegenüber von Klimacamp
von 12 Uhr bis 18 Uhr eine Klimademonstration durchzuführen.
Die Demo soll künstlerische Installationen beinhalten.<br>
Link zur Veranstaltung:
[https://gj-augsburg.de/events/event/klimademo/](https://gj-augsburg.de/events/event/klimademo/)

**Nachtrag:**
Um 18 Uhr fand am Klimacamp wieder ein längeres Plenum
mit zweitweise bis zu 24 Teilnehmer\*innen statt.<br>
Im Verlauf des Plenums wurde zwei Mal von sehr freundlichen Menschen
Essen vorbei gebracht.
Leider blieb Einiges übrig, da das Angebot unseren Hunger überstieg.<br>
Das Plenum endete kurz vor Mitternacht.
Grund dafür war vor allem,
dass die swa um Mitternacht ihren Straßenbahnbetrieb einstellen
und zahlreiche Teilnehmer\*innen noch die letzte Straßenbahn
nach Hause erwischen wollten.
Damit war das Plenum dann aber nicht mehr beschlussfähig.<br>
Leider ist es in Augsburg oft so,
dass Nachtleben, Kultur, Vereinssitzungen und eben auch Aktivismus
abends mit der letzten Straßenbahn enden
oder auf klimaunfreundlichen Individualverkehr angewiesen sind.


## Sonntag 04.09.2022 -- **Tag 796**

Heute gab es am Camp ein kleines Plenum zu ausgewählten Themen.
Das Plenum begann um 18 Uhr und endete kurz nach 23:30.
Den größten Teil der Zeit waren etwa 15 Teilnehmer\*innen vor Ort,
wobei einige früher gingen und andere später kamen,
was die Gesamtteilnehmer\*innenzahl schwer zu ermitteln macht.

Mindestens ein Passant fragte interessiert,
ob wir hier einen Workshop veranstalten.
Leider sind für die nahe Zukunft jedoch keine Workshops geplant.


## Samstag 03.09.2022 -- **Tag 795**

Heute gegen 15:30 trafen starke Windböen auf das Camp.
Unbefestigtes Zeug wurde herumgewirbelt und eine Fahne umgeknickt.
Es folgte ein kurzer Regenschauer.
Wenig später schien nochmal die Sonne.

Ungewöhnlich ist starker Wind im Klimacamp nicht,
da der Fischmarkt eine schöne Windschneiße bildet.


## Montag 29.08.2022 -- **Tag 790**

Heute fand am Klimacamp ab 19 Uhr ein Plenum
zu verschiedenen internen Themen statt.
Bei einer Zählung gegen 21:30
nahmen knapp über zwanzig Teilnehmer\*innen am Plenum teil.
Das Plenum endetete kurz vor Mitternacht.


## Freitag 26.08.2022 -- **Tag 787**

**Critical Mass, 18:00 Uhr, Start am Rathausplatz gegenüber vom Klimacamp**

Die Critical Mass findet gewöhnlich immer am letzten Freitag
eines jeden Monats statt.
Voraussetzung für ein Stattfinden
ist die Teilnahme von mindestens 16 Fahrradfahrer\*innen.
Gemeinsam fährt man im Verband durch die Stadt.
Dabei verbleibt man nicht nur in der Innenstadt.
Bei geeignetem Wetter
geht es auch in Stadteile,
wie beispielsweise Göggingen oder Oberhausen, hinaus.

Link: [https://criticalmass-augsburg.de/termine](https://criticalmass-augsburg.de/termine)

***Persönlicher Nachtrag:***

*Die heutige Route war genial.
Als wir uns am Rathaus sammelten,
meinten einige Leute,
dass der Wetterbericht Regen vorausgesagt hatte.
Es gab ein paar Wolken, aber nichts, was allzu bedrohlich aussah.
Gegen 18:30 fuhren wir am Rathausplatz los.
Nach einiger Zeit begann es kleine Tropfen zu regnen,
die man kaum spüren konnte und bei Berührung mit Haut oder Asphalt
sofort verdunsteten,
und es hörte auch schnell wieder auf.
Es zeigte sich ein Regenbogen.
Später regnete es dann einige größere Tropfen,
die man zwar deutlich spürte,
die aber nicht zahlreich genug waren,
um nicht auch schnell wieder zu verdunsten.
Inzwischen sah es richtig düster um uns herum aus
und man sah schließlich in der Ferne auch Blitze und hörte Donner.
Aber wir fuhren einfach bei guter Laune weiter.
Auf dem Rückweg etwa ab der Blauen Kappe
war der Asphalt plötzlich nass
und das Wasser sammelte sich sogar in Pfützen.
Zurück am Klimacamp wurde dann berichtet,
dass es einen lang anhaltenden starken Regenschauer gegeben hatte.
Wir hatten davon nichts mitbekommen.
Während es um uns herum geregnet hatte,
sind wir eineinhalb Stunden durch die Stadt gefahren
ohne wirklich nass zu werden. :-)*

*Auch gab es ein paar spannende Ereignisse.
An einer Engstelle vor uns
standen sich plötzlich drei entgegenkommende Fahrzeuge
und ein in unsere Richtung fahrendes Fahrzeug gegenüber.
Mit Unterstützung eines Radfahrers aus unserem Verband
gelang es dem vor uns fahrenden Fahrzeug zurück zu setzen
und den Weg für die entgegenkommenden Fahrzeuge freizugeben.*

*Wenig später fanden wir uns zwischen zwei Engstellen wieder.
Das Fahrzeug vor uns konnte nicht weiterfahren,
weil in der Engstelle vor uns
noch Fahrzeuge aus dem Gegenverkehr standen,
und der Gegenverkehr konnte nicht weiter fahren,
weil die Engstelle hinter uns mit zahlreichen Radfahrer\*innen
aus unserem Verband belegt war.
Wir umflossen das Fahrzeug vor uns
und gaben so die Straße hinter uns
für die entgegenkommenden Fahrzeuge frei.*

*Im Stadtverkehr kommen einem Autos oft so hilflos vor
als wären sie Fische auf dem Land.*

*Die Teilnehmerzahl war dieses Mal etwas geringer.
Wahrscheinlich haben sich nach zwei sehr verregneten
Critical Masses in den letzten beiden Monaten
mehrere Leute durch den Wetterbericht
und die düsteren Wolken abschrecken lassen.
Zu Beginn am Rathausplatz wurde die Zahl der anwesenden Fahrräder
auf etwa vierzig bis fünfzig geschätzt.
Mitten während der Fahrt wurden dann etwa 36 gezählt.
Zurück am Rathausplatz waren es immer noch 22 Fahrräder.
Die Anzahl der Teilnehmer\*innen ist meist höher
als die Anzahl der Fahrräder,
da manche Fahrräder oder fahrradähnlichen Konstruktionen
zwei oder drei Personen transportieren.
Es ist auch nicht ungewöhnlich,
dass die Teilnehmer\*innenzahl unterwegs schwankt,
da sich Fahrradfahrer\*innen spontan anschließen
und Teilnehmer\*innen die Möglichkeit wahrnehmen,
den Verband vorzeitig zu verlassen,
falls dieser in der Nähe ihrer Wohnung vorbei fährt.*


## Freitag 19.08.2022 -- **Tag 780**

* 19:00 Uhr: Offene politische Runde vom Klimacamp

![Das Bild zeigt unter dem Schriftzug „Politische Runde Freitag, 19.08. – 18 Uhr“
  und dem Logo des Klimacamps eine Zeichnung dreier Menschen,
  die in einem Wohnzimmer beisammen sitzen, sich unterhalten
  und dabei Tassen mit Tee oder Kaffee in den Händen halten.](/sharepics/2022-08-19-politische-runde.webp)

Herzliche Einladung zur offenen politischen Runde diesen Freitag (19.8.) um
19:00 Uhr im Klimacamp! 👋

In gemütlicher Atmosphäre sprechen wir bei gutem Essen über:

🌳 Die aktuellen Pläne der Stadt, alle Bäume am Bahnhofsvorplatz zu fällen.
Neue Entwicklungen seit unserem Kletterworkshop und zusätzliche Baumgefährdung
in Augsburg-Centerville.

🚊 Erfahrungen aus einer Aktion in Nürnberg, an der sich Jesuitenpater Jörg Alt
beteiligte.

🏕 Unsere Pläne zum Schutz des Auwalds bei Bobingen, den Wehringens
Bürgermeister roden lassen möchte.

🚉 Wirksame Aktionen, die zu einer Neuauflage des 9-Euro-Tickets beitragen
sollen.

Wir bearbeiten in dem Treffen auch unsere Strategie für die nächsten Wochen. Es
soll wieder Verkehrswendeaktionen bei Schulen geben, zudem gibt es
Überlegungen, dass wir auch zur Bauwende etwas machen.

Im Anschluss knüpfen wir Pride-Armbänder 🏳️‍🌈 und holen noch leckeres Eis
🍦 Schaut gerne vorbei, wenn ihr möchtet :-)


## Montag 15.08.2022 -- **Tag 776**

Heute Morgen startete in Augsburg
die Südtour von *Ohne Kerosin nach Berlin* (kurz: *OKNB*).
Augsburger Klimacamper\*innen rollten den Radfahrer\*innen
den roten Teppich aus eröffneten die Tour symbolisch
durch das Zerschneiden eines roten Bandes.

Über 30 Radfahrer\*innen starteten vor dem Augsburger Klimacamp.
Weitere Teilnehmer\*innen werden unterwegs dazustoßen.
Am 27. August trifft man dann mit den
[anderen Touren](https://ohnekerosinnachberlin.com/oknb-2022/)
in Leipzig zusammen.
Anschließend geht es gemeinsam nach Berlin.
Dort ist am 2. September ein Aktionstag geplant.

Siehe:
* [Ohne Kerosin nach Berlin – Südtour 2022](https://ohnekerosinnachberlin.com/suedtour-22/)
* [Ohne Kerosin nach Berlin – Tourblog](https://ohnekerosinnachberlin.com/tourblog-2022/)

![Am Rathausplatz vor dem Klimacamp halten zwei Klimaaktivist\*innen
  ein rotes Band über einen dort ausgelegten Teppich,
  während ein\* Dritte\*r das Band gerade durchschnitten hat.
  Eine Hälfte des Bandes flattert bereits davon.
  Im Hintergrund warten über dreißig Radfahrer\*innen darauf
  ihre Tour mit einer Fahrt über den Teppich zu beginnen.](/pages/material/images/2022-08-15-oknb_start_band_flattert.jpg)


## Samstag 06.08.2022 -- **Tag 767**

Wir bauen um.
Heute findet eine große Aufräum- und Umbauaktion statt.
Ziel ist es das Camp kompakter und funktionaler zu gestalten.


## Donnerstag 04.08.2022 -- **Tag 765**

* 18:00 Uhr: Workshop: Greenwashing – wie Konzerne und Parteien ihr Image
  aufpolieren


## Montag 01.08.2022 -- **Tag 762**

Für diesen Tag hatten wir einen Kletterworkshop angekündigt.
Geplant war,
dass interessierten Menschen unter professioneller Anleitung
das Beklettern von Bäumen beigebracht wird.
Stattfinden sollte der Workshop an den Bäumen des Bahnhofsvorplatzes,
deren Fällung gerade im Gespräch ist.
Das Beklettern von Bäumen stellt weder eine Ordnungswidrigkeit
noch eine Straftat dar.
(Gleiches gilt übrigens für Straßenlaternen und Fahnenmasten.)
Dementsprechend weit musste sich die Polizei aus dem Fenster lehnen
und „Gefahrenabwehr nach Polizeiaufgabengesetz“
als Vorwand der Maßnahme heranziehen.

Noch in der Nacht vom 1. August auf dem 2. August
gab das Klimacamp eine Pressemitteilungen zu den Vorkommnissen heraus.
Siehe: [Polizeiaufgebot bei Kletterworkshop vom Augsburger Klimacamp: Fällen erlaubt, Beklettern verboten](/pressemitteilungen/2022-08-01-staatsschutz-bei-kletterworkshop/)

**Update:**
Die Entscheidung über eine Fällung der Bäume am Bahnhofsvorplatz
wurde durch den Stadtrat auf nach die Sommerpause verschoben.


## Samstag 30.07.2022 -- **Tag 760**

* 13:00 Uhr: Öffentliche Information zur bevorstehenden friedlichen
  Massenaktion zivilen Ungehorsams für Neulinge und Einsteiger\*innen: Ende
  Gelände, zu tausendst fossile Infrastruktur blockieren. Inklusive "Was tun
  wenn's brennt"-Vortrag.
* 15:00 Uhr: Feministische Demo, Start Ulrichsplatz. Für Gleichberechtigung und
  das Recht auf freie Oberkörper für alle; gegen Sexualisierung,
  Stigmatisierung und Zensur von Brüsten. Männer werden gebeten, aus
  Solidarität ihren Oberkörper zu verhüllen, und sind eingeladen, "Banner gegen
  Spanner" zu halten. Details dazu gibt's beim Ordner\*innentreffen um 14:30
  Uhr. [Zum Vorabartikel in der AZ](https://www.augsburger-allgemeine.de/augsburg/augsburg-frauen-protestieren-auf-oben-ohne-demo-mit-nacktem-oberkoerper-id63442341.html)


## Freitag 29.07.2022 -- **Tag 759**

* Critical Mass, 18:00 Uhr, Start am Klimacamp

**Nachtrag:**
An diesem Tag gab es um 17 Uhr am Klimacamp ein großes Plenum.
Etwa 40 Personen nahmen daran Teil,
selbst als dann irgendwann nach 18 Uhr die Critical Mass
mit über 30 Personen den Radhausplatz verließ.
Die Radtour verlief überwiegend im Regen.
Das hatte zur Folge,
dass dann gegen 19:30 nur noch etwa 17 oder 18 Teilnehmer,
damit nur etwas mehr als das absolute Minimum für eine Critical Mass,
zum Rathausplatz zurückkehrten.
Das Plenum hatte mit weniger Teilnehmerschwund zu kämpfen
und dauerte bis kurz vor 20 Uhr an.

Störend auf das Plenum wirkte sich die Beschallung
durch die Bühne auf dem Rathausplatz aus.
Es ist schwierig sich über ernsthafte Themen zu unterhalten,
wenn auf der anderen Straßenseite
unterstützt durch gigantische Lautsprecher
„Nasty Bitches“(?) besungen werden.
Andererseits sorgte das Programm am Rathausplatz auch dafür,
dass dort mehr Leute unterwegs waren
und mehr Leute das Klimacamp wahrnahmen.

*Persönliche Anmerkung:
Auf meinem Weg zum Start der Critical Mass
parkte ein Auto auf dem Fußweg,
für den es zu breit war und auf den Radweg hineinragte,
ein weiteres Auto kam mir auf Fuß-/Radweg entgegen gefahren
und ein drittes Auto parkte schamlos auf dem Radweg.
Sobald man 10 Minuten lang mit dem Fahrrad in Augsburg unterwegs ist,
weiß man, warum man auf Fahrraddemos geht
und an der Critical Mass teilnimmt.*


## Freitag 22.07.2022 -- **Tag 752**

Raddemo um 18:00 Uhr ab Klimacamp zum Abschluss des Stadtradelns
entlang einer geforderten Fahrradstraße durch das Hochfeld vom
Theodor-Heuss-Platz Richtung Messe/Universität, dann entlang der
geforderten Tramverbindung Richtung Göggingen und weiter entlang von
Fahrradstraßen und kritschen Stellen für Fahrradfahrer\*innen an der
Stadionstraße zur Rosenaustraße, wo die Tramverbindung Hauptbahnhof über
Kongress und Göggingen zur Uni erneut angesprochen werden soll und über
die Hermannstr und Karlstr. zurück zum Rathausplatz.


## Freitag 15.07.2022 -- **Tag 745**

Verkehrswendeplan-Workshop um 16:00 Uhr im Klimacamp: Was ist der
Verkehrswendeplan, welche Ideen für Augsburg fehlen noch und wie setzen
wir den Plan um?

Raddemo um 18:00 Uhr ab dem Klimacamp durch Augsburg entlang geforderter
Fahrradstraßen Richtung Oberhausen, über das Georgsviertel zum Oberen
Graben und über das Rote Tor und Hallstraße zurück zum Rathausplatz.


## Mittwoch 13.07.2022 -- **Tag 743**

Raddemo über die B17, 20:00 Uhr, Beginn am Klimacamp. Route über eine
geforderte Fahrradstraße durch Pfersee, über den Verlauf einer möglichen
Tramlinie Richtung Leitershofen, über die B17 zur B300, dort entlang des
geforderten verlaufes der geplanten Tramlinie 5 und der geforderten
Tramlinie durch die Karlstraße zum Rathausplatz.

**Nachtrag:**
Um 18:30 Uhr gab es am Klimacamp ein sehr konstruktives Gespräch
mit Vertretern der Grünen.
Das Gespräch dauerte bis zum Beginn der Raddemo.
An dieser nahmen etwa 100 Teilnehmer teil.
Der Abschnitt über die B17 war mal wieder das große Highlight.
Als Radfahrer in Augsburg ist man es gewohnt,
pro Minute mehreren Schlaglöchern oder Glasscherben auszuweichen
und trotzdem durch die schlechten Wege durchgeschüttelt zu werden.
Nicht so auf der B17:
Der Asphalt ist gut gepflegt und die frei von störenden Objekten.

*Persönliche Anmerkung:
Ich wurde zwei Mal von sehr interessierten Passanten gefragt,
worum es sich bei dieser Fahrraddemo handelt.
Das Interesse in Augsburg scheint deutlich größer zu sein
als die Reichweite unserer Bewerbung der Veranstaltungen.*


## Freitag 01.07.2022 -- **Tag 731** -- Zweijähriges Jubiläum

Das Klimacamp wurde am 01.07.2020 gegründet.
Seitdem hat das Klimacamp mehrere Phasen durchlaufen
und dient heute verschiedenen Zwecken:

* Das Klimacamp macht auf die drohende Klimakatastrophe aufmerksam
  und informiert die Bevölkerung.
  Besonders wertvoll ist, dass das Klimacamp
  nicht nur diejenigen Menschen erreicht,
  die sich sowieso bereits für das Thema interessieren,
  sondern alle Menschen,
  die absichtlich oder zufällig am Camp vorbeikommen.
* Das Klimacamp übt Druck auf die Politik aus.
  Dreißig Jahre nach dem ersten Weltklimabericht
  sind Teile der Politik nun endlich so weit,
  dass sie bei der Fahrt in Richtung Abgrund
  vorsichtig den Fuß vom Gaspedal nehmen.
  Jetzt müssen sie noch überzeugt werden, die Bremse zu betätigen.
  Wir sind überzeugt, dass Augsburg mehr tun kann und mehr tun sollte.
* Das Klimacamp erleichtert die Vernetzung
  zwischen Klimagerechtigkeitsaktiven.
  Auch dient es als Anlaufpunkt für Menschen,
  die sich für Klimagerechtigkeitsaktivismus interessieren.
* Das Augsburger Klimacamp dient Klimagerechtigkeitsaktiven
  weit über Schwaben hinaus als Vorbild.

Inzwischen wird das Klimacamp sogar von Münchner Merkur / tz
als *Schatz in Bayern* bezeichnet,
den man mit dem 9-Euro-Ticket besuchen sollte.
<br> Quellen:
[Münchner Merkuer](https://www.merkur.de/bayern/oepnv-tipps-bayern-9-euro-ticket-ziele-reise-ausfluege-fahrten-91576981.html)
/
[tz](https://www.tz.de/muenchen/stadt/reise-deutsche-bahn-muenchen-9-euro-ticket-bayern-tipps-erkundung-deutschland-91576562.html?itm_source=story_detail&itm_medium=interaction_bar&itm_campaign=share)

Diese Errungenschaften gilt es zu feiern.
Das zweijährige Jubiläum werden wir voraussichtlich
nicht so sang- und klanglos verstreichen lassen,
wie beispielsweise das sechshunderttägige Jubiläum
oder das siebenhundertägige Jubiläum.
Weitere Informationen werden wir zu gegebener Zeit veröffentlichen.

* 11—16: Straßenkreidemalen (bei gutem Wetter) (Vorsicht [überzogene
  Hausdurchsuchungen](https://www.xn--pimmelgate-sd-7ob.de/kreidebleich/))
* 11—16: Aktivistisches Kinder- und Erwachsenenschminken
* 14—15: Energiewende-Workshop
* 15—16: Physikalische Grundlagen der Klimakrise: Fakten und Mythen
* 15—16: Fahrradschrotttrommeln für Anfänger\*innen
* 15—17: Kleidertausch-Party
* **16—17:30: Demo für Klimagerechtigkeit**
* 18—19: Rundgang durchs Camp: Forderungen, Rechtliches, klimaaktivistischer
  Ansatz und bisherige Erfolge (für Einsteiger*innen und Passant*innen) —
  insbesondere: verschiedene Formen von Klimaaktivismus
* 19—20: Konsumkritik-Kritik: von der Mär der angeblichen Macht der
  Verbraucher\*innen
* 20—21: Kletterworkshop in Vorbereitung der Besetzung des [Auwalds bei
  Bobingen](https://www.bobinger-auwald-bleibt.de/)
* 21—22: Einführung in die Augsburger Lokalpolitik
* 22—23: Reflexionsrunde zwei Jahre Klimacamp Augsburg
* 23—24: Aktivistisches Sterne schauen

Mit veganem Abendessen und musikalischer Umrahmung durch
[Wollstiefel](https://wollstiefel.bandcamp.com/)!


## Donnerstag 30.6.2022 -- **Tag 730**

`20:00 Uhr` **Erfahrungen einer Ackerbesetzung**

Palû berichtet, wie sie in Neu Eichenberg eine 80ha große Ackerfläche besetzten, und so den Bau eines gigantischen Logistikzentrums verhinderten.

Dazu ein kleiner Exkurs zu emotionalen Dynamiken in der Klimabewegung.

`20:30 Uhr` **Gesellschaftsformenvergleich aus Klimagerechtigkeitsperspektive —
Podiumsdiskussion**

Demokratie, Kommunismus und Anarchie im direkten Vergleich.


## Freitag 24.06.2022 -- **Tag 724**

### 18 Uhr – Critical Mass

**Nachtrag:**
Etwa dreißig Teilnehmer nahmen trotz regnerischen Wetters teil.
Während die Critical Mass gerade durch das Univiertel vor,
wurde sie von einem kräftigen Platzregen überrascht.
Trotzdem war die Stimmung gut.


## Mittwoch 01.06.2022 -- **Tag 701**

### Ortsbegehung des Bobinger Auwalds

Die Grünen vom Augsburger Land veranstalten
eine Ortsbegehung des Bobinger Auwalds.<br>
Link zur Veranstaltung: [https://gruene-augsburgland.de/ortsbegehung-wehringer-auwald/](https://gruene-augsburgland.de/ortsbegehung-wehringer-auwald/)

Auch vom Klimacamp bricht eine Gruppe gegen 17:30
mit öffentlichen Verkehrsmittel zur Ortsbegehung auf.

Weitere Informationen zum Bobinger Auwald gibt es auf
[https://www.bobinger-auwald-bleibt.de/](https://www.bobinger-auwald-bleibt.de/).


## Dienstag 31.05.2022 -- **Tag 700**

Heute war der siebenhunderste Tag des Klimacamps.
Das Klimacamp besteht nun seit einhundert Wochen.
(Da das Klimacamp am ersten Tag erst um 19 Uhr gegründet wurde,
sind es streng genommen erst morgen (= Mittwoch) um 19 Uhr
genau einhundert Wochen.)

Ist das ein Grund zur Feier,
da es die Entschlossenheit, Organisation und den Durchhaltewillen
der Klimagerechtigkeitsbewegung in Augsburg zeigt,
oder ein Grund zur Trauer,
da wir nach so langer Zeit immer noch den Eindruck haben,
dass die Stadt auch weiterhin Druck von Außen braucht,
um das Richtige zu tun?


## Sonntag 29.05.2022 -- **Tag 698**

### ~~Vormittags: Fahrraddemo?~~

~~Wir planen eine Fahrraddemo.
Leider sind wir uns mit dem Ordnungsamt
noch nicht so ganz über die Route einig.
Eingeplant ist auch ein Abschnitt über die A8.
Weitere Informationen gibt es hier,
sobald sich das Gericht mit unserer Klage befasst hat.~~

Die Fahrraddemo findet nicht so wie geplant statt.
Doch kein Grund zur Sorge:
Die nächste spannende Fahrraddemo kommt bestimmt.


### 14:00 bis 17:00 Straßenfest auf der Karlstraße

Unter dem Motto *Tag der neuen Energie*
findet von 14 Uhr bis 17 Uhr ein Straßenfest
auf der Karlstraße statt.
Bei dieser angemeldeten Demonstration wird die Karlstraße gesperrt.
Dann ist Platz für Bänke, Pavillons, Stände, Musik, Pflanzen,
Spielzeug, Getränke und Essen.
Das Straßenfest ist eine Gemeinschaftsaktion eines Bündnisses
aus zahlreichen Gruppen – darunter das Klimacamp,
Fridays for Future, Students For Future, Extinction Rebellion,
Rhythms of Resistance, Gemeinwohl-Ökonomie,
Ohne Kerosin Nach Berlin, Augsburg erdgasfrei und
Psychologists for Future.
Die Aktion verbindet die notwendigen Veränderungen
in den beiden Bereichen *Mobilität* und *Energie*
mit den Forderungen nach Klimagerechtigkeit.

Programm:

* 14:00 Uhr  Aufbau
* 14:40 Uhr  Aktionen: T-Shirt Druck, Kreidebild, Spiele
* 15:00 Uhr  Redebeiträge
* 15:45 Uhr  Musik „Wollstiefel“
* 16:15 Uhr  Vorträge
* 16:45 Uhr  Tanz mit Rhythms of Resistance

<img src="/pages/material/images/karlstrasse.jpeg"
     alt="Das Bild zeigt ein Foto einer Straße.
     Darauf eingezeichnet sind ein Baum und zahlreiche Menschen,
     die die Straße für sinnvolle Dinge nutzen,
     wie beispielsweise Radfahren, sitzen, liegen, sich unterhalten,
     musizieren und spielen.
     Oben steht mittig der Text
     „TAG DER NEUEN ENERGIE Aktionstag Karlstraße
     Sonntag, der 29. Mai 14 - 17 Uhr“.
     Am unteren Rand sind die Logos der unterstützenden Organisationen
     eingezeichnet.
     Dazu zählen das Augsburger Klimacamp,
     Students for Future Augsburg,
     Rhythms of Resistance,
     Extinction Rebellion Augsburg,
     Gemeinwohl Ökonomie Augsburg,
     Ohne Kerosin nach Berlin,
     Fridays for Future Augsburg,
     Augsburg Erdgasfrei
     und die Psychologists for Future Augsburg."
     width="640" height="640" style="width: 100%">

**Nachtrag:**

Die Stimmung war ausgezeichnet.
Mitten auf der Karlstraße wurde Federball und Frisbee gespielt.
Es gab leckeres Essen und Informationsstände.
Man konnte sich auf Bierbänken, Stühlen und
sogar dem ein oder anderen Liegestuhl ausruhen
und der Musik lauschen.
Zur Verzierung hatte man einige Pflanzen und ein Beet aufgestellt.

Selbst als ein starker Regenschauer hereinbrach
und man sich in aufgebauten Zelten unterstellte,
blieb die Stimmung positiv.
Einige ignorierten den Regen vollkommen
und setzten ihr Frisbeespiel fort.

Das Straßenfest setzte ein deutlich Zeichen dafür,
wie öffentlicher Raum in der Innenstadt genutzt werden kann,
wenn er nicht für Autos reserviert wird.

Während unseres Straßenfests sank
laut der in der Karlstraße stehenden Messstation
die Konzentration des Schadstoffes NO₂ auf 8 µg/m³.
Zum Teil war dies sicher auch dem Regen zu verdanken.
Trotzdem, dieser Wert war zuletzt
am Samstag (28.05.2022) um 5 Uhr in der Früh erreicht worden.<br>
Quelle: [https://www.lfu.bayern.de/luft/immissionsmessungen/messwerte/stationen/detail/1401/172/](https://www.lfu.bayern.de/luft/immissionsmessungen/messwerte/stationen/detail/1401/172/)


## Freitag 27.05.2022 -- **Tag 696**

### 9:30 – Demonstration für autofreie Zonen um Schulen

Um die Pause der Schüler\*innen der Ulrichsschule
und des Holbein-Gymnasiums sicherer zu gestalten,
findet diesen Freitag während der großen Pause
wieder eine Demonstration in der Hallstraße statt.
Ähnliche Aktionen hatte es dort schon am 13. Mai
und am 17. Mai dieses Jahres gegeben.

**Ort:** Hallstraße<br>
**Zeit:** ab 9:30

Unter dem Motto „Auto-Frei-Tag“
soll dies in Zukunft regemäßig geschehen.


### 18:00 – Critical Mass

Wie an jedem letzten Freitag im Monat
findet heute wieder eine Critical Mass statt.
Start is um 18 Uhr am Rathausplatz / Klimacamp.

**Ort:** Rathausplatz / Klimacamp<br>
**Zeit:** 18:00


## Mittwoch 25.05.2022 -- **Tag 694**

Um 18 Uhr veranstalten die Students for Future Augsburg
im Dompark ein Neuentreffen für alle Interessierten.
Der Plan ist sich kennen zu lernen, zusammen etwas zu trinken
und sich auszutauschen.
Dort kann man unter anderem erfahren,
welche Projekte nun nach Abschluss
der zweiten Augsburger Public Climate School
in Vorbereitung sind.

**Nachtrag:** An diesem Tag begannen vor Ort
die Vorbereitungen für eine Waldbesetzung im Bobinger Auwald.
Grund sind Rodungsabsichten des Wehringener Bürgermeisters.

Mehr Informationen auf:
[https://www.bobinger-auwald-bleibt.de/](https://www.bobinger-auwald-bleibt.de/)


## 20. Kalenderwoche 2022 (16.05. bis 22.05.)

Diese Woche bietet ein reichhaltiges Programm.

* Montag bis Freitag: [Public Climate School](https://www.klimauni-augsburg.de/)
* Mittwoch: Filmabend mit veganem Essen
* Freitag: gemeinsame Pressekonferenz von FFF-Augsburg und dem Klimcamp
* Freitag: Schulstreik
* Donnerstag bis Sonntag:
  [endlich. Das Augsburger Klimafestival](https://staatstheater-augsburg.de/klimafestival)


### Public Climate School

Von Montag (16.05.2022) bis Freitag (20.05.2022)
findet die von den *Students for Future Augsburg*
organisierte Public Climate School statt.
Mehr Informationen auf können auf der offiziellen Webseite
der Veranstaltung nachgelesen werden:
[https://www.klimauni-augsburg.de/](https://www.klimauni-augsburg.de/).


### Mittwoch 18.05.2022 -- **Tag 687**

Ab 19:30 Uhr Filmabend mit veganem Essen: 📽 Die rote Linie — Widerstand im Hambacher Forst 📽

Die Geschichte eines Walds, der für eine Kohlegrube gerodet werden sollte.
Eines Walds, in den Klimaaktivist\*innen zur Verteidigung einzogen.
Eines Walds, der mit seinen Baumhausdörfern Freiraum und Bildungsraum wurde.
Eines Walds, der von Laschet unter einem Vorwand geräumt werden sollte.
Und vor allem: Die Geschichte eines Walds, der bleibt. 🌱

*Die aktivistische Verteidigung des Hambacher Forsts war erfolgreich:*
Sechs Jahre nach der Initialbesetzung erklärte ein Gericht die Rodung
für rechtswidrig (und letztes Jahr dann auch die Räumung).

Kommt gerne vorbei 👋 Nach dem Film gibt es genügend Möglichkeit zum
gemeinsamen Austausch:
Wie sieht der Alltag in so einer Waldbesetzung aus?
Was macht Waldbesetzungen so schön, egal ob mensch dort zwei Tage,
zwei Wochen oder zwei Monate verbringt?
Wo liegen die nächsten aktiven Waldbesetzungen?


### Freitag 20.05.2022 -- **Tag 689**

#### 10:00 Gemeinsame Pressekonferenz mit FFF-Augsburg

Wie auch im Fall von [Pimmelgate Süd](https://www.pimmelgate-süd.de/)
planen wir an diesem Freitag gemeinsam mit FFF-Augsburg
eine Pressekonferenz abzuhalten.
Während Pimmelgate Süd den bislang letzten Fall in einer Serie
von Übergriffen durch die Abteilung „Staatsschutz“ darstellt,
geht es in dieser Pressekonferenz darum, den ersten Fall aufzuarbeiten.
<br>
Ort: Moritzsaal der Augsburger Moritzkirche<br>
Pressemappe mit Details: [https://www.pimmelgate-süd.de/kreide/](https://www.pimmelgate-süd.de/kreide/)

Eventuell veröffentlichen wir in einiger Zeit noch
eine Aufzeichnung der Pressekonferenz.
Falls dem so seien wird, wollen wir hier auf sie verlinken.

**Nachtrag:**
Der Vortragsteil des Pressekonferenz dauerte etwa eine Stunde.
Es folgte eine etwa 20 Minuten lange Fragerunde.
Im Anschluss gab es dann noch kleinere Gesprächsrunden
zwischen Vertreter\*innen von FFF-Augsburg,
dem Klimacamp und der Presse.

Bereits bei Pimmelgate Süd war angedeutet worden,
dass es kein Einzelfall gewesen sei,
sondern lediglich der Tropfen,
der das Fass zum überlaufen brachte.
Nun wurde ein kleiner Einblick in den Inhalt des Fasses gegeben.
Das Thema der Pressekonferenz waren zwei Hausdurchsuchungen,
die auf den Tag genau zwei Jahre zuvor stattgefunden hatten.
Die Umstände der Hausdurchsuchungen muten ähnlich unverständlich an
wie im Fall von [Pimmelgate Süd](https://www.pimmelgate-süd.de/).
Pimmelgate Süd war der Anlass,
nun auch diese alten Fälle medial aufzuarbeiten.

Vorwand für die Hausdurchsuchungen war eine Sprühkreideaktion
in der Nacht auf den 29.11.2019.
Richtig gelesen:
Die Hausdurchsuchung sollte Beweismittel
für eine Sprühkreideaktion sicherstellen,
die fast sechs Monate zuvor stattgefunden hatte.
Begründet wurde dies auch mit „Gefahr im Verzug“.
Die Albernheiten hören hier nicht auf.

Bei der Sprühkreideaktion waren mit wasserabwaschbarer Sprühkreide
konsumkritische Sprüche vor Läden gesprüht worden.
Weiter hatte sich Greenpeace Augsburg zu der Sprühkreideaktion bekannt.
Die Medien hatten darüber berichtet.
Die Polizei hätte also einfach an Greenpeace Augsburg
eine Rechnung für die Reinigung schicken können.
Stattdessen entschied sie sich für den massiven Grundrechtseingriff
einer Hausdruchsuchung.
Die beiden hausdurchsuchten Aktivist\*innen, Ingo und Janika,
waren und sind jedoch nicht Mitglieder von Greenpeace Augsburg.
Bekannt waren sie vor allem für ihre Tätigkeit bei FFF-Augsburg,
beispielsweise durch das Halten von Reden bei Demonstrationen,
und in städtischen Beiräten.
Hinzu kommt, dass Janika als Kritikerin von Konsumkritik bekannt ist.
Sie stand schon damals für politische Lösungen,
nicht für eine Schuldzuweisung an einzelne Konsument\*innen.

Im Verlauf der Pressekonferenz schilderte Janika,
wie in ihrem Fall der Tatverdacht dadurch begründet wurde,
dass ein Überwachungsvideo ein Mädchen mit ähnlicher Körpergröße
und ähnlicher Jackenfarbe zeigen solle.
Das Video durften die Beschuldigten nie sehen,
auch nicht, als das Verfahren nach eineinhalb Jahren
ohne Anklageerhebung eingestellt wurde.
Die Aussage der Mutter,
dass ihre Tochter in der Tatnacht zu Hause war,
wurde einfach abgetan.

Die Augsburger Klimagerechtigkeitsbewegung
traf der Vorfall damals noch unvorbereitet.
Polizist\*innen kannte man bis dahin als Freund\*innen und Helfer\*innen,
sowie als Begleitung von Demonstrationszügen.

Ingo beschrieb in der Pressekonferenz,
welcher Wandel durch die Hausdurchsuchungen
in Gang gesetzt wurde.
Hätten die Hausdurchsuchungen nicht stattgefunden,
wäre es bei friedlichen Demonstrationen als Protestform geblieben.
Stattdessen wurde durch die Hausdurchsuchungen innerhalb der
Augsburger Klimagerechtigkeitsbewegung ein Reflektionsprozess
und eine Analyse der politischen Verhältnisse angestoßen.
Friedliche Demonstrationen
wurden ergänzt um weitere Aktionsformen.
Etwas Trost findet Ingo in dem Gedanken,
dass die Gründung des Klimacamps als eine indirekte Folge
der Hausdurchsuchungen sowohl in Augsburg viel bewirkte
als auch deutschlandweit nachgeahmt wurde und wird.
<br>
*(So nicht gesagt:
Damals hatte man noch Respekt und Ehrfurcht vor amtlichen Bescheiden.
Heute schreibt man mal eben selbst eine erfolgreiche Klage
gegen so einen Bescheid, ohne wegen dieser Lappalie
die eigene Anwältin zu behelligen.
Folge: Das Ordnungsamt lässt man ohne rechtsgültigen Bescheid
dafür aber mit den Gerichtskosten zurück.)*

Alex, welcher im Kontext von Pimmelgate Süd erst vor wenigen Wochen
Opfer einer Hausdurchsuchung geworden war,
erzählte, wie die Hausdurchsuchungen bei Ingo und Janika dazu führten,
dass man sich innerhalb der Augsburger Klimagerechtigkeitsbewegung
intensiv mit den eigenen Rechten und dem korrekten Verhalten
bei Hausdurchsuchungen auseinandersetzte.
Dieses Wissen half ihm selbst bei seiner eigenen Hausdurchsuchung,
derart professionell zu reagieren
und Rechtsverstöße von Seiten der Polizei,
wie die Verweigerung eines Anrufs bei seiner Anwältin,
schriftlich und von der Polizei unterschrieben
im Durchsuchungsprotokoll festhalten zu lassen.

Ingo und Janika trafen die Hausdurchsuchungen noch unvorbereitet.
So erzählte Janika, wie sie zulies,
wie Polizist\*innen ihre Wäsche durchsuchten und
ihr Tagebuch und andere Notizen lasen und abfotografierten,
obwohl sich der Durchsuchungsbeschluss nur auf Handys,
Sprühkreide und Schablonen bezog.
Sie erzählte, wie sie in Unwissenheit ihrer eigenen Rechte
der Aufforderung der Polizei nachkam,
und ihre Passwörter herausgab und so den Beamt\*innen
Inhalte privater Kommunikation preisgab.
Heute weiß man in der Augsburger Klimagerechtigkeitsbewegung,
dass Passwörter niemals herausgegeben werden müssen
und hält am Klimacamp Verschlüsselungsworkshops ab.

Ebenfalls auf der Pressekonferenz sprach Birgit Zech,
welche Janika nach der Hausdurchsuchung psychologisch
betreute, bei den *Psychologists for Future* aktiv ist
und auch schon am 17.09.2020 mit einer Kollegin zusammen
einen [Vortrag am Klimacamp](/tagebuch/2020/#1930-uhr-gespr%C3%A4chsrunde-zur-emotionalen-verarbeitung-der-klimakrise-von-zwei-psychologists-for-future)
gehalten hatte.
Sie erklärte typische psychologische Folgen einer Hausdurchsuchung,
die vergleichbar mit denen eines Einbruchs seien.

Janika und Ingo erzählten weiter,
wie sie auf's Polizeirevier mitgenommen wurden.
Sie mussten sich halbnackt ausziehen und Fingerabdrücke abgeben.
Die damals erst 15-jährige Janika erzählte in der Pressekonferenz,
wie ihre Hand auf den Fingerabdruckscanner gedrückt wurde.
Die erfassten personenbezogenen Daten
sind aller Wahrscheinlichkeit noch heute bei der Polizei gespeichert.

Vor zwei Jahren lies man der Polizei
eine Überschreitung ihrer eigenen Befugnisse noch durchgehen.
Wie der Fall Pimmelgate Süd gezeigt hat,
entwickelt man innerhalb der Augsburger Klimagerechtigkeitsbewegung
eine Sensibilität und Nulltoleranzgrenze gegenüber Rechtsverstößen
durch Beamt\*innen im Dienst.
Die meisten Übergriffe lassen sich auf die selbe
kleine Gruppe von Beamten zurückführen.
Ingo nahm sich auf der Pressekonferenz die Zeit um zu erklären,
dass man keinesfalls einen Generalverdacht gegen alle
Polizeibeamt\*innen ausprechen wolle.
Bei vielen Anlässen arbeite man mit der Polizei gut zusammen.
Als Beispiel kann hier die Organisation
von Demonstrationen genommen werden.

Sowohl Ingo als auch Janikas Mutter bedankten
sich für die Unterstützung durch Greenpeace,
welches die juristische Verteidigung in dem Fall übernahm,
obwohl es sich bei den Betroffenen nicht um Mitglieder handelte.

Es kann mit Spannung erwartet werden,
welche alten Fälle man in Zukunft ebenfalls noch publik machen wird
und wie sich die Polizei und insbesondere
die Augsburger Abteilung „Staatsschutz“ in Zukunft
gegenüber Vertreter\*innen der Klimagerechtigkeisbewegung
verhalten wird.


#### 11:00 Schulstreik

Die letzten [Weltklimaberichte](/weltklimaberichte/)
haben noch einmal stark vor Augen geführt,
wie schnell uns die Zeit davon läuft,
wenn wir eines der milderen Erwärmungsszenarien anstreben möchten.
[Fridays for Future Augsburg](https://www.fff-augsburg.de/)
ruft nun wieder zum Schulstreik auf.

**Zeit**: 11 Uhr<br>
**Ort**: Rathausplatz

Siehe auch:
[Pressemitteilung zum rechtswidrigen Bescheid des Ordnungsamtes gegen die Versammlung](/pressemitteilungen/2022-05-19-stadt-verliert-vor-gericht-gegen-schulstreik/)

**Nachtrag:**
Die Demonstration begann gegen 11 Uhr,
während im nahen Moritzsaal noch die Pressekonferenz lief.
Nach ziemlich genau 20 Minuten am Rathausplatz
brach die Demonstration auf und zog ihre Runde
vorbei am Dom, der Ganzen Bäckerei, dem Klinkertor
und dem Landratsamt.
In der Nähe des Landratsamtes
hielt die Demonstration dann für eine kurze Rede von Janika,
die nach der Pressekonferenz zur Demo hinzugestoßen war.
Im Anschluss ging es vorbei am Hauptbahnhof
und über Königsplatz und Moritzplatz zurück zum Rathausplatz.
Hier wurde die Demonstration für beendet erklärt.
Zahlreiche Teilnehmer\*innen nutzten die Gelegenheit
noch für einen Besuch am Klimacamp.

Die Demonstration zählte etwa 150 Teilnehmer\*innen.
Damit war die Teilnehmer\*innenzahl ziemlich genau dort,
wo wir sie uns gewünscht hatten.
Wären deutlich mehr Teilnehmer\*innen gekommen,
hätte es sogar Probleme geben können.

Hintergrund:
Im Vorfeld der Demonstration hatte es einen Rechtstreit mit der Stadt gegeben.
Die Stadt hatte versucht, den Beginn und das Ende
der Demonstration auf den deutlich abgelegeneren
Elias-Holl-Platz zu verbannen.
Als Grund wurde eine befürchtete Störung der Veranstaltung
„Frühschoppen mit Fuggerei-BewohnerInnen“
durch eine Großdemonstration am Rathausplatz angegeben.
Gegen diese rechtswidrige Versammlungsauflage hatte FFF-Augsburg
geklagt und Recht bekommen.
Dabei war von Seiten von FFF-Augsburg auch argumentiert worden,
dass man mit maximal 200 Teilnehmer\*innen rechnet.

Siehe auch nochmal
[Pressemitteilung vom 19.05.2022](/pressemitteilungen/2022-05-19-stadt-verliert-vor-gericht-gegen-schulstreik/).

Diese Demonstration war als Schulstreik ausgelegt
und richtete sich überwiegend an Schüler\*innen.
Das klappte hervorragend.
Tatsächlich stellten junge Kinder
den überwiegenden Teil der Teilnehmer\*innen.
Die Demosprüche schallten lautstark durch die Straßen.
Die Stimmung auf der Demo war überaus positiv
und von einem jungen Elan getrieben,
der uns auf den größeren Demos machmal fehlt.

Im Gespräch ist, ob FFF-Demonstrationen in der näheren Zukunft,
vielleicht abwechselnd vormittags, als Schulstreik,
und spät nachmittags, zur Erleichterung der Teilnahme für Berufstätige,
stattfinden sollten.


### endlich. Das Augsburger Klimafestival

„*endlich. Das Augsburger Klimafestival*“ des Augsburger Staatstheaters
findet vom Donnerstag 19.05.2022 bis Sonntag 22.05.2022 statt.
Weitere Informationen, inklusive des Programms des Festivals, können
auf der offiziellen Webseite der Veranstaltung nachgelesen werden:
[https://staatstheater-augsburg.de/klimafestival](https://staatstheater-augsburg.de/klimafestival)

Wir waren am *Marktplatz der Möglichkeiten*,
(Samstag, den 21.05.2022, von 11 Uhr bis 18 Uhr und
Sonntag, den 22.05.2022, von 12 Uhr bis 16 Uhr)
auf der Theaterwiese im Martini-Park mit einem eigenen Stand vertreten.

Auch die Theatervorführungen auf der Brechtbühne
wurden von Einigen von uns besucht.

Vielen Dank an das Augsburger Staatstheater
für die Organisation eines so schönen Events.


## Sonntag 15.05.2022 14:30 – Kidical Mass – **Tag 684**

Die Kidical Mass ist eine Fahrraddemo für alle,
bei der besonderer Wert auf ihre Kinderfreundlichkeit gelegt wird.
Start ist um 14:30 auf dem Platz vor der City Galerie.

Mehr Infos auf [https://kidical-mass-augsburg.de/](https://kidical-mass-augsburg.de/).


## Freitag 13.05.2022 -- **Tag 682**

### 10:00

Um 10:00 Uhr stellen in der Hallstraße
Verkehrswendeaktivist\*innen ein
über die letzten Monate gemeinsam erarbeitetes Konzept
für eine [Verkehrswende](/informationen/artikel/mobilitaetswende/)
in Augsburg vor.

Links:
* [https://www.verkehrswende-augsburg.de/](https://www.verkehrswende-augsburg.de/)
* [Flyer (Stand 1. Mai)](https://www.verkehrswende-augsburg.de/assets/Flyer%20Verkehrswendeplan%20Augsburg.pdf)
* [Artikel des Klimacamps zur Verkehrswende](/informationen/artikel/mobilitaetswende/)
* [Pressemitteilung zum Verkehrswendeplan Augsburg](/pressemitteilungen/2022-05-11-verkehrswende-augsburg/)


### 16:00

Die Stadt Augsburg lädt zum 1. Mobilitätsforum.
Am Event soll es neben Vorträgen auch
Möglichkeiten zur Bürgerbeteiligung geben.<br>
**Ort**: Kongress am Park<br>
**Zeit**: 16:00 bis 20:00<br>
Weitere Informationen zur Veranstaltung:

* [https://www.augsburg.de/aktuelles-aus-der-stadt/detail/augsburger-mobilitaetsplan-wird-neu-aufgestellt](https://www.augsburg.de/aktuelles-aus-der-stadt/detail/augsburger-mobilitaetsplan-wird-neu-aufgestellt)
* [https://www.augsburg.de/buergerservice-rathaus/verkehr/augsburger-mobilitaetsplan](https://www.augsburg.de/buergerservice-rathaus/verkehr/augsburger-mobilitaetsplan)

#### Nachtrag

Es waren einige hundert Menschen gekommen.
Etwa sechzig weitere Teilnehmer waren online zugeschaltet.
Positiv war, dass sich Vertreter der Stadt zumindest mündlich
zu einer echten [Verkehrswende](/informationen/artikel/klimaaktivismus/)
bekannten.

Zum Ablauf: Es gab mehrere Vortragsrunden, gefolgt von Pausen
für jeweils einige Fragen aus dem Publikum (online wie offline).
Es gab hohes Interesse an Beteiligung und
nicht alle Personen mit Fragen konnten dran genommen werden.

In den Vorträgen wurden viele interessante Fakten aufgezählt.
So wurde benannt, dass Autos im Durchschnitt 23 Stunden am Tag
nutzlos in der Gegend herum stehen.
Auch wurden Zahlen genannt, wie viele Pendler täglich nach und aus
Augsburg pendeln und
wie die Verteilung der Benutzung verschiedener Verkehrsmittel
in verschiedenen Stadtteilen ist.
Viele dieser Informationen können auch
auf verschiedenen Webseiten der Stadt eingesehen werden.

Auch die Fragen der Teilnehmer\*innen waren interessant.
Enttäuschend war, dass Baureferent Gerd Merkle das Konzept
*[Verkehr 4.0 für den Ballungsraum Augsburg](https://www.verkehr4x0.de/)*,
welches schon seit 2019 in Augsburg diskutiert wird, nicht kannte.
Viele der Ideen aus dem Konzept fanden auch Eingang in
den am Mittwoch zuvor vorgestellten und am Vormittag beworbenen
*[Verkehrswendeplan Augsburg](https://www.verkehrswende-augsburg.de/)*.

Interessant war, dass unter den hunderten Teilnehmern
kein einziger Fürsprecher für einen Ausbau des Autoverkehrs war.
Konsens unter den Teilnehmer\*innen und
den Vertreter\*innen der Stadt schien zu sein,
dass man eine echte Verkehrswende herbeiführen wolle
und den motorisierten Individualverkehr (MIV),
wie in der städtischen Studie ein halbes Jahr zuvor empfohlen,
mindestens Halbieren will.
Auch deutete sich an, dass die Mehrheit der Teilnehmer\*innen
eine autoarme, aber nicht eine vollständig autofreie Stadt wünsche.

Nach Ende des Vortragsteils gab es mehrere Stände,
an denen Bürger\*innen Kommentare und Vorschläge
für verschiedene Schwerpunkte des Mobilitätsplans
diskutieren und in Form von Zetteln an Pinnwänden abgeben konnten.
Hier eine kurze Zusammenfassung der Ergebnisse.

##### **Autoarme Innenstadt**

52 Zettel wurden zum Thema einer autoarmen Innenstadt abgegeben.
Dazu zählen unter anderem:

* Geschwindigkeitsreduzierung
* mehr und große Park-and-Ride-Parkplatze am Stadtrand
* emissionsfreie Zonen
* mutige Experimente und Pilotprojekte
* Reduzierung der Parkflächen
* mehr Grünflächen und Bäume
* stärkere Priorisierung des Radverkehrs
  * bei Ampelschaltung
  * durch Verbesserung des Radwegenetzes

##### **Stadtteile und Anschluss an die Region**

46 Zettel wurden zum Schwerpunkt des Anschlusses
der Stadtteile und der Region abgegeben.

Dazu zählen unter anderem:

* Einführung von Ringlinien und
  Querverbindungen zwischen Randstadtteilen
* Verbesserung der Taktung
* bessere ÖPNV-Anbindung an Start und Zielort
  (erster und letzter Kilometer)
* S-Bahntaktung im Zugverkehr
* eigene ÖPNV-Spuren auf B17 und A8
* besser Verknüpfung verschiedener Verkehrsmittel (Bahn, Bus, Tram usw.)
* Vergünstigung des ÖPNV
* schnelles Handeln
  <br>Viele Personen schien die Sorge zu umtreiben,
  dass ein Mobilitätsplan bis 2038 bedeutet,
  dass die Stadt erst kurz vor 2038 mit der Umsetzung anfängt.

Auch wurden verschiedene konkrete neue ÖPNV-Strecken,
Haltestellen und Fahrradbrücken vorgeschlagen.

##### **Mobilität für Familien, Jugendliche und junge Erwachsene**

48 Zettel wurden zum Thema der Mobilität für junge Menschen abgegeben.

* Ein großer Schwerpunkt mit zahlreichen Städten
  lag auf dem Ausbau des Radwegenetzes.
* Ein zweiter Schwerpunkt lag darauf,
  die Innenstadt sicherer zu gestalten,
  beispielsweise durch eine Reduzierung
  der erlaubten Höchstgeschwindigkeit und breitere Fahrradwege.

##### **Mobilität für ältere Erwachsene, Seniorinnen & Senioren und Mobilitätseingeschränkte**

32 Zettel waren der Mobilität für ältere Menschen
und Mobilitätseingeschränkte gewidmet.
Dazu zählen unter anderem:

* bessere ÖPNV-Anbindung an Start und Zielort
  (erster und letzter Kilometer)
* Geschwindkeitsreduzierung, evtl. vermehrte Geschwindigkeitskontrollen
* durchgängige Barrierefreiheit
* breitere Gehwege und Radwege,
  durchgängig ohne unnötige Unterbrechungen
* Preisreduzierung für den ÖPNV, beispielsweise durch
  kostenlose Jahrestickets für Ältere, die ihren Führerschein abgeben
* kurze Wege, mehr Angebote und Geschäfte im Quartier vor Ort


## Donnerstag 12.05.2022 -- **Tag 681** -- Heimkehr

Nach 154 Tagen am Moritzplatz
begann das Klimacamp heute damit zum Fischmarkt zurückgekehren.

Am Perlachturm hatte Gefahr durch herabfallende Steine bestanden.
Daher war das Klimacamp am 9. Dezember 2021 zum Moritzplatz ausgewichen.

In der Nacht von Donnerstag auf Freitag
übernachteten Aktivist\*innen wieder am Fischmarkt.
Trotzdem ist der Umzug noch nicht abgeschlossen.
Dies soll am Wochenende geschehen.


## Mittwoch 11.05.2022 -- **Tag 680**

Heute ging ein Verkehrswendekonzept, der *Verkehrswendeplan Augsburg*,
an die Öffentlichkeit.
Der Plan war über die letzten Monate
durch Verkehrswendeaktivist\*innen erarbeitetet worden.

Es wird ausdrücklich um Bürger\*innenbeteiligung
für die weitere Verbesserung des Vorschlags gebeten.

* [https://www.verkehrswende-augsburg.de/](https://www.verkehrswende-augsburg.de/)
* [Flyer (Stand 1. Mai)](https://www.verkehrswende-augsburg.de/assets/Flyer%20Verkehrswendeplan%20Augsburg.pdf)
* [Artikel des Klimacamps zur Verkehrswende](/informationen/artikel/mobilitaetswende/)
* [Pressemitteilung zum Verkehrswendeplan Augsburg](/pressemitteilungen/2022-05-11-verkehrswende-augsburg/)


## Samstag 07.05.2022 -- **Tag 676** -- Willkommenstag

Herzliche Einladung zum ersten großen Willkommenstag
am Klimacamp diesen Samstag. 👋

Auch andere Initiativen wie Greenpeace und Students sind dabei 👋

Unzählige Plakate und Flyer hatten
auf den Willkommenstag aufmerksam gemacht.
Er richtete sich vor allem an Personen,
die sich für Klimagerechtigkeit interessieren,
bislang aber nicht so richtig wussten,
wie sie den Einstieg finden sollten,
um sich für Klimagerechtigkeit zu engagieren.

### 13:00 Uhr
Wie geht eigentlich Klimaaktivismus?
Wieso heizt die Politik die Klimakrise weiter an,
obwohl doch alle Fakten bekannt sind?
Unsere Analyse und ein Blick hinter die Kulissen von Klimaaktivismus. 💥

### 13:40 Uhr
Campführung und gemütliche Frage- und Austauschrunde
bei leckerem veganem Essen,
Überblick über die vielfältigen Formen von Aktivismus 🥙

### 17:00 Uhr
**Nachtrag:**
Gegen 17:00 Uhr befestigten drei Aktivist\*innen des Klimacamps
unterhalb des Vordaches der City-Galerie ein Plakat.
Die Aktivist\*innen brachten enorme Klettererfahrung mit sich.
Einige von ihnen hatten beispielsweise bereits
das Brandenburger Tor oder die Basilika in Weingarten erklettert.

Während der Aktion erschienen auch Mitarbeiter des Sicherheitsdienstes
der City-Galerie auf dem Vordach.
Nach mehreren Ohrenzeug\*innenberichten
sagte einer der Männer etwas wie:
„Ich schneid sie ab/runter. Ich hab ein Messer dabei.“
Angesichts von 14 Metern Höhe hätte das mit hoher Wahrscheinlichkeit
den Tod der „Heruntergeschnittenen“ bedeutet.
Diese Drohung löste Panik aus und
führte zu einer unnötigen Gefährdung der Beteiligten.
Später am Boden wurde versucht zu relativieren,
dass es sich bei dieser Aussage um einen Scherz gehandelt haben soll.
Gelacht hat soweit wir wissen aber niemand.
Es war in diesem Moment einfach nur eine dumme da gefährliche Ansage.

Schließlich traf die Polizei ein
– mit drei Einsatzwagen und mindestens elf Beamt\*innen.
Sie filmte und wartete ab,
bis das Plakat fertig angebracht worden war
und sich die Aktivist\*innen abgeseilt hatten.
Damit verhielt sich die Polizei korrekt.
Bei derartigen Kletteraktionen ist den Beamt\*innen
das Eingreifen untersagt.
Nur speziell im Klettern ausgebildete Beamte des SEK
hätten hier eingreifen dürfen.
Während Aktivist\*innen die Rechtslage
und das Kletterhandwerk beherrschen,
ist es für Polizeibeamt\*innen oft eine ungewohnte Situation.
So passiert es, dass sie eine Straftat oder Ordnungswidrigkeit
zu erkennen glauben, wo in Wahrheit keine vorliegt.
Manchmal kann es passieren,
dass sie dann in einen Aktionismus verfallen und glauben,
jetzt irgendwie eingreifen zu müssen.

> Beispiel:
> Das Erklettern einer Straßenlaterne
> ist weder Straftat noch Ordnungswidrigkeit.
> Gerade in der Anfangsphase,
> als Klimaaktivismus gerade erst aufgeblüht ist,
> behandelten Polizeibeamt\*innen Klimagerechtigkeitsaktivist\*innen,
> die sie beim Anbringen von Bannern auf Straßenlaternen antraf,
> als hätten sie diese in flagranti
> bei einem schweren Verbrechen erwischt.
> Inzwischen scheinen die Polizeibeamt\*innen
> hier besser sensibilisiert zu sein.
> Das Klimacamp gibt vereinzelt auch Kletterkurse für Straßenlaternen.

Zwischen 18:00 und 18:30 konnten die Aktivist\*innen dann gehen.
Das Banner hing zu dieser Zeit immer noch.

Für weitere Details siehe auch die
[Pressemitteilung der Aktion](/pressemitteilungen/2022-05-07-banner-an-city-galerie/).

Hire noch einige Bilder:

![Bildinhalt: Zwei Aktivisten hängen bereits unterhalb des Vordaches
knapp oberhalb des noch nicht richtig angebrachten Plakats.
Eine Aktivistin befindet sich angeseilt direkt an der Kante.
Zwei Männer vom Sicherheitsdienst stehen auf dem Vordach.](/pages/material/images/2022-05-07-banner/sicherheitsdienst-klein.png)
![Bildinhalt: Menschen sammeln sich vor der City-Galerie.
Einige Filmen die Aktion mit ihren Handys.](/pages/material/images/2022-05-07-banner/zuschauer-klein.png)
![Bildinhalt: Das Plaket mit Aufschrift „Hausverbot für Klimakatastrophe?“
hängt nun unterhalb des Vordaches.
Die Klimagerechtigkeitsaktivist\*innen beginnen damit sich abzuseilen.](/pages/material/images/2022-05-07-banner/plakat-haengt-klein.png)

### 19:00 Uhr
Tobi Rosswog, einer der Autor\*innen
des kürzlich veröffentlichten spendenvorfinanzierten
[Aktionsbuchs Verkehrswende](https://cycling-the-change.de/),
kommt für eine Lesung und Diskussion ins Camp! 🛣

**Nachtrag:**
Der Vortrag erfolgt in einem großen Stuhlkreis vor dem Camp.
Neben etwa 15 eher festen Teilnehmer\*innen
gesellten sich auch immer wieder Passant\*innen
für einige Minuten dazu und lauschten oder stellten Fragen.
Nach etwa eineinhalb bis zwei Stunden
ging der Vortrag in ein geselliges Beisammensein
mit leckerem veganem Essen über,
welches noch über das Verfassen dieses Tagebucheintrags hinausging.

Besprochen wurden vor allem Erfahrungsberichte und Aktionsformen,
mit denen man die
[Verkehrswende](/informationen/artikel/mobilitaetswende/)
voranbringen kann.
Viel des besprochenen Inhalts kann auch
in dem Buch „Aktionsbuch Verkehrswende“ nachgelesen werden.
Die PDF-Version des Buches unterliegt einer „CC BY-NC-ND 4.0“-Lizenz
und kann kostenlos auf der Webseite des Verlags heruntergeladen werden:
[https://www.oekom.de/buch/aktionsbuch-verkehrswende-9783962383541](https://www.oekom.de/buch/aktionsbuch-verkehrswende-9783962383541)


## Freitag 06.05.2022 -- **Tag 675**

Diesen Freitag (6.5.2022, ~~12:15-13:30~~ 12:30-13:15
passend zum Schulschluss)
veranstalten wir auf der Kreuzung Karlstraße/Karolinenstraße
eine Popup-Rundum-Grün-Demo.
Alle Fußgänger\*innen und Radler\*innen bekommen gleichzeitig Grün,
während alle Autos warten.
Damit ist es zu Fuß und mit dem Rad deutlich bequemer und
Unfälle beim Rechtsabbiegen gehören der Vergangenheit an.
Wir demonstrieren damit für eine Umwandlung von Kreuzungen
nach dem Rundum-Grün-System und weiter gedacht eine Mobilitätswende,
die über eine unsoziale Antriebswende deutlich hinausgeht.<br>
Hintergrund:
[https://klimafreunde.koeln/rundum-grun-macht-rundum-glucklich/](https://klimafreunde.koeln/rundum-grun-macht-rundum-glucklich/)

**Nachtrag:**
Das vorgeschlagene Konzept von Rundum-Grün-Phasen
unterbrochen von Phasen für die Autofahrer\*innen
war so leider nicht genehmigt worden.
Es erschien nicht umsetzbar,
die Ampelschaltung anzupassen.
Den Verkehr entsprechend durch die Polizei regeln zu lassen,
wurde mit der Begründung abgewiesen,
dass man die Autofahrer\*innen
für zu unkonzentriert und zu unvorsichtig hielte
und so eine zu große Gefahr für an der Kreuzung
den Verkehr regelnde Polizeikräfte bestehen würde.
Diese Sorge können vor allem die Radfahrer\*innen
unter uns nachvollziehen,
weshalb wir eine Herabsetzung des Tempolimits
in der gesamten Innenstadt befürworten.
Es klingt zumindest in der Begründung so,
als würde man das bei der Polizei ähnlich sehen.

Der Zeitraum der Veranstaltung
wurde für die Abänderung unseres Konzepts
durch das Ordnungsamt auf 12:30 bis 13:15 verkürzt.
An Stelle unseres Rundum-Grün-Konzepts
sperrte die Polizei den Bereich um die Kreuzung
in größerem Abstand für Autos ab.
Nur ÖPNV (also Straßenbahnen und Busse) wurden durchgelassen.
Dadurch wurde die Kreuzung de facto zur Fußgängerzone.
Das sorgte für zusätzlichen Frust bei Autofahrer\*innen.
Diese durchbrachen mitunter
die unzureichend abgesicherten Absperrungen der Polizei
und wurden dadurch zur Gefahr für Fußgänger\*innen
im Demonstrationsbereich.

Gegen 13:05 wurde die Veranstaltung
– auch aus Rücksicht auf die Polizei – leicht vorzeitig beendet.
Es war anscheinend außerhalb der Sichtweite der Demonstration
im Bereich der Absperrungen zu Chaos gekommen.

Bei der nächsten Popup-Rundum-Grün-Demo
wird das hoffentlich besser organisiert werden.


## Dienstag 03.05.2022 -- **Tag 672**

### 15:30: Essen verschenken
Um ~~17:00~~ 15:30 Uhr verschenken Aktivist\*innen gerettete Lebensmittel.
Beim sogenannten „Containern“ werden noch genießbare Lebensmittel
aus den Mülltonnen von Supermärkten geholt.
Siehe auch unsere Pressemitteilung
„[Klimaaktivist\*innen verschenken gestohlenes Essen: Essen-Retten-Aktion vor Klimacamp](/pressemitteilungen/2022-05-01-gestohlenes-essen-verschenken/)“.

Vorbild ist die Aktion von Pater Jörg Alt in Nürnberg.<br>
Siehe auch Bayerischer Rundfunk:
[Bis die Polizei kommt: Pater aus Nürnberg rettet Lebensmittel](https://www.br.de/nachrichten/bayern/bis-die-polizei-kommt-pater-aus-nuernberg-rettet-lebensmittel,SsFhNiM)

*Anmerkung:
Das Event wurde ursprünglich für 17 Uhr beworben,
später aber auf 15:30 Uhr vorverlegt.
Wir bitten eventuell damit verbundene Unannehmlichkeiten
zu entschuldigen.*

### 18:00: ‼️DEMO AUFRUF‼️ (nicht vom Klimacamp organisiert)

Am 2. Mai ist in Mannheim ein Mensch gestorben,
nachdem dieser mitten in der Innenstadt Opfer von Polizeigewalt wurde.

Die Polizei fühlte sich offenbar sicher genug,
am helllichten Tag in aller Öffentlichkeit
Gewalt an einem Menschen auszuüben, der bereits am Boden liegt.
Dieses Verhalten ist inakzeptabel!

Wir rufen zu einer Eilversammlung auf,
um laut zu sein gegen Machtmissbrauch und Polizeigewalt.

**🗓 Wann?** 3. Mai um **18:00 Uhr**<br>
**📍Wo?** Treffpunkt ist der **Rathausplatz** in Augsburg.
Nach einer Startkundgebung wird wahrscheinlich eine Laufdemo stattfinden.

Kommt zahlreich, lasst uns wütend sein und gemeinsam zeigen,
dass Polizeigewalt keinen Platz in unserer Gesellschaft haben darf! ✊

Hier das Video. Triggerwarnung: extreme Polizeigewalt.
https://twitter.com/Ezgi_Guyildar/status/1521175986066399233

### 19:00: Podiumsdiskussion des Presseclubs Augsburg
Der Presseclub Augsburg veranstaltet eine Podiumsdiskussion.
Mit dabei ist auch ein Vertreter des Klimacamps.
Für Ort und Anmeldeformalitäten verweisen wir auf die offizielle
[Seite des Veranstalters](https://presseclub-augsburg.de/2022/04/mitgliederversammlung-und-club-gespraech-vom-gaswerk-zum-kulturland/).

**Nachtrag:**
![Links eine Person am Podium,
rechts ein Tisch mit vier Stühlen und drei
Personen.](/pages/material/images/2022-05-03-podiumsdiskussion/diskussion-19-37-klein.png)
Von links nach rechts:<br>
Alfred Schmidt, stellv. Vorsitzender des *Presseclub Augsburg*
und Moderator der Diskussion,<br>
Alex Mai, Vertreter des Klimacamps und *Fahrradstadt jetzt*,<br>
Beate Schabert-Zeidler,
ehemalige Richterin und für *Pro Augsburg* im Stadtrat,<br>
Peter Rauscher, Fraktionsvorsitzender der Grünen im Stadtrat,<br>
und Volker Ullrich, welcher das Klimacamp kürzlich aufgefordert hatte,
sich vielmehr der politischen Debatte zu stellen.<br>
Das Foto wurde gegen 19:35 vor der Ankunft Volker Ullrichs aufgenommen.


## Sonntag 01.05.2022 -- **Tag 670** -- Müllsammeln und Plenum

### Müllsammelaktion

Greenpeace organisierte von 11 Uhr bis 15 Uhr
eine Müllsammelaktion am Vogeltor.
Es wurde um die Mitnahme eigener Arbeitshandschuhe gebeten.<br>
Treffpunkt: Vogeltor,
an der kleinen Brücke von der Jakoberwallstraße
über den Äußeren Stadtgraben

***Nachtrag:***
Bereits gegen 13:30 waren genügend Restmüll für etwa vier Müllsäcke,
zahlreiche Flaschen sowie ein ganzer Klappstuhl gefunden worden.
Besonders widerlich waren die Mengen an Zigarettenkippen.

![Ein Eimer mit Müll. Darüber befindet sich ein Schild mit der Aufschrift
„Ist das deins?“](/pages/material/images/2022-05-01-muellsammlung/eigentuemer-gesucht-klein.png)
![Zwei große Eimer, einer mit Restmüll, einer mit Flaschen.
Am linken Bildrand sieht man einen blauen Müllsack.
Im Hintergrund sieht man zahlreiche kleinere Eimer,
mit denen der Müll gesammelt werden kann.](/pages/material/images/2022-05-01-muellsammlung/restmuell-klein.png)
![Weiter Flaschen und Dosen, teilweise bereits sortiert auf verschiedene Eimer](/pages/material/images/2022-05-01-muellsammlung/flaschen-klein.png)

Greenpeace organisiert derartige Müllsammelaktionen immer wieder.
Die Nächste ist für den 7. Mai geplant.
Treffpunkt und Sammelpunkt wird dann von 11 Uhr bis 15 Uhr
an der Freien Waldorfschule Augsburg in der Dr.-Schmelzing-Straße 52
im Stadtteil Hammerschmiede sein.<br>
[Mehr Informationen zum Programm von Greenpeace Augsburg](https://greenwire.greenpeace.de/group/greenpeace-augsburg)


### Plenum zum Standort

Zeitgleich zur Müllsammelaktion fand auch ein Plenum
über den zukünftigen Standort des Klimacamps statt.
Das Klimacamp befand sich von seiner Gründung am 1. Juli 2020
bis in den Dezember 2021 hinein
am Fischmarkt unmittelbar neben dem Rathaus.
Der Fischmarkt war zwischenzeitlich als „Klimacampplatz“
bzw. „Am Klimacamp“ deutlich bekannter
als unter seinem offiziellen Namen.
Aufgrund eines Gutachtens zur Gefahr
des Herabfallens von Steinen vom renovierungsbedürften Perlachturm,
musste das Camp dann zum Moritzplatz umziehen.
Demnächst wird der Perlachturm voraussichtlich soweit gesichert sein,
dass das Klimacamp zu seinem alten Standort zurückkehren könnte.
Für den Fischmarkt sprechen die Nähe zur Augsburger Stadtpolitik.
Der Moritzplatz ist gemütlicher.
Außerdem kommt man hier leichter mit Passanten ins Gespräch.

Bereits am 26. April hatte man seitenweise Argumente
für und gegen die beiden Standorte besprochen.
Heute fiel nun die Entscheidung,
den Rückumzug zum Fischmarkt vorzubereiten.
Die Nähe zur Lokalpolitik ist uns wichtig.
Gerade erst vor wenigen Tagen haben uns
sowohl Oberbürgermeisterin Eva Weber
als auch Stadtrat Peter Hummel
jeweils eine eigene Stellungnahme gewidmet.


## Samstag 30.04.2022 -- **Tag 669**

### 15:00: Fahrraddemo „Fossil free for Peace Ride“

Greenpeace organsiert unter dem Titel „Fossil free for Peace Ride“
eine Fahrraddemo für eine Verkehrswende, Tempolimit, Frieden
und autofreie Sonntage.
Geplant ist eine entspannte Radtour die nach etwa 10km
mit einer Abschlusskundgebung am Provino endet.<br>
Zeit: 15:00 Uhr<br>
Ort: Konrad Adenauer Allee / Königsplatz


## Freitag 29.04.2022 -- **Tag 668**

### 🚴‍♀️ 18:00 Rathausplatz: Critical Mass — entspannt Fahrrad fahren durch die Stadt

Wie jeden letzten Freitag im Monat trifft man sich um 18:00
am Rathausplatz und fährt gemeinsam
über für Autos vorgesehene Straßenspuren.
Das ist keine angemeldete Demo und muss es auch nicht sein.
Ab 16 Radfahrenden ist es durch §27 StVO legal.
Bei dem aktuellen Wetter nehmen meistens mit 100–200 Leuten
an der entspannten Spazierfahrt durch die Stadt teil.
Am 15.5. gibt's auch wieder die
[Kidical Mass](https://augsburg.adfc.de/artikel/kidicalmass),
die bunte Fahrraddemo für alle und insbesondere Kinder!


## Donnerstag 28.04.2022 17:00 – Offenes Kennenlerntreffen – **Tag 667**

Die Parents for Future Augsburg treffen sich im Camp für ein offenes
Kennenlerntreffen. Bei den Parents können sich alle engagieren, auch Menschen,
die nicht Eltern sind. Die Parents gründeten sich zur Unterstützung der Arbeit
der Schüler\*innen und verfolgen zudem eigene Projekte für Klimagerechtigkeit.


## Donnerstag 21.04.2022 -- **Tag 660**

Heute gab der Bayerische Verwaltungsgerichtshof
die schriftliche Begründung seines Urteils
gegen die Stadt ab.
Dies fand ein breites Medienecho,
wie man auch unserem [Pressespiegel](/pressespiegel/#21042022)
entnehmen kann.


## Mittwoch 20.04.2022 -- **Tag 659**

Der heutige Tag ist ein ganz gutes Beispiel,
um zu veranschaulichen,
dass nicht das gesamte Programm des Augsburger Klimacamps
hier auf der Webseite angekündigt wird.
Eine ganze Reihe von Workshops
wird mit relativ kurzer Vorlaufzeit spontan organisiert.
Die Ankündigung erfolgt manchmal einige Stunden vorher
per Aushang direkt am Klimacamp.

Das stand heute auf dem Aushang:

* 18 Uhr: Erste – Hilfe – Skillshare
* 20 Uhr: Plenum Students 4 Future
* 21 Uhr: AG Öffentlichkeitsarbeit


## Samstag 16.04.2022 -- **Tag 655**

In den frühen Morgenstunden gegen 5:30 Uhr
war am Klimacamp ein Feuer gelegt worden.
Angezündet wurde eine Regenbogenfahne.

Das Feuer wurde durch die Aktivist\*innen,
die in dieser Nacht im Camp übernachteten, gelöscht,
bevor es größeren Schaden anrichteten konnte.

Der Fall taucht im Polizeibericht auf.
In Folge dessen berichteten die Medien.
Ein ähnlicher Vorfall am 4. März war nicht erwähnt worden.

Die Polizei nahm Ermittlungen auf und bittet Zeugen,
die sachdienliche Hinweise geben können, darum,
sich bei der Kriminalpolizei Augsburg
unter der Telefonnummer 0821/323 3810 zu melden.


## Donnerstag 14.04.2022 – **Tag 653**

### Gemeinsame Pressekonferenz mit Fridays for Future Augsburg

*Fridays for Future Augsburg* und wir vom *Klimacamp Augsburg*
haben etwas Wichtiges zu sagen.<br>
Zeit: 10 Uhr<br>
Ort: Klimacamp (am Moritzplatz)

Thema: [Pimmelgate Süd](https://www.pimmelgate-süd.de/)

#### Nachtrag

Das Thema der heutigen Pressekonferenz war die Hausdurchsuchung
vom 05. April, der auch die Webseite
[https://www.pimmelgate-süd.de/](https://www.pimmelgate-süd.de/)
gewidmet ist.
Verschiedene Presseberichte über dieses Ereignis
wie auch Presseberichte über andere Ereignisse
rund um das Augsburger Klimacamp können in unserem
[Pressespiegel](/pressespiegel) nachgelesen werden.

Einige Punkte, die uns wichtig sind, jedoch nicht in der
Berichterstattung aller Medien erwähnt werden,
sind die Folgenden.

1. Alex wurde sowohl das Hinzuziehen eigener Zeugen
   als auch ein Anruf bei seiner Anwältin verweigert.
   Das Polizeipräsidium Schwaben Nord hatte auf Medienanfrage behauptet,
   dass bei der Hausdurchsuchung die geltenden Rechtsvorschriften
   beachtet wurden und Alex für ein Telefonat ein Polizeihandy
   angeboten worden sei.
   Dem widerspricht das von den Beamten bei der Durchsuchung
   angefertigte Durchsuchungsprotokoll, in dem klar festgehalten ist:
   „Der Betroffene war mit der DUSU-Zeugin nicht einverstanden
   und ist unzufrieden damit, dass er für die Dauer der Maßnahme
   nicht telefonieren darf.“<br>
   Die [Süddeutsche Zeitung](https://www.sueddeutsche.de/bayern/augsburg-pimmelgate-klimacamp-polizei-1.5566562?reduced=true)
   sowie [Netzpolitik.org](https://netzpolitik.org/2022/augsburg-pimmel-kommentar-fuehrt-zu-razzia-bei-klimaaktivisten/)
   haben diesen zentralen Aspekt
   in ihrer Berichterstattung berücksichtigt.

2. Das ist nicht der erste Fall,
   sondern nur der bislang letzte Fall in einer Kette von Fällen,
   die bis in die Zeit vor die Gründung des Klimacamps zurück reicht.
   In diesem Fall waren sowohl wir als lokale Gruppe
   der Klimagerechtigkeitsbewegung als auch der Betroffene bereit,
   damit an die Öffentlichkeit zu gehen.
   Etwas unglücklich ist, dass im aktuellen Fall
   als Grund der Hausdurchsuchung die Anzeige eines AfD-Stadtrat
   angegeben wurde.
   Manche Medien erliegen dadurch der Versuchung,
   das Ganze als Konflikt zwischen dem Augsburger Klimacamp
   und der lokalen AfD zu inszenieren.
   Diese Darstellung stimmt nicht.
   Die Beteiligung der AfD ist unerheblich für den Missstand,
   der uns dazu brachte damit an die Öffentlichkeit zu gehen.
   In den letzten zwei Jahren gab es andere Vorfälle,
   darunter auch Hausdurchsuchungen,
   in denen andere teilweise noch absurdere Vorwände geschaffen wurden.<br>
   Bislang trugen wir diese Fälle aus verschiedenen Gründen
   nicht an die Medien.
   Wir wollten den Betroffenen den zusätzlichen Stress ersparen.
   Wir dachten, dass dies von unseren Kernthemen ablenken würde.
   Zu den Kernthemen zählen günstigere Busse und Trams,
   Flächenentsiegelung statt -versiegelung
   und allgemein Klimagerechtigkeit.
   Allerdings bereiteten wir uns in Folge
   auf mögliche zukünftige Vorfälle vor,
   beispielsweise indem wir allen interessierten Menschen,
   egal ob Klimagerechtigkeitsaktivist\*in oder nicht,
   Workshops wie „Aktivistische Rechtskunde“
   (siehe auch Tagebucheintrag vom 12.01.2022) anboten.

In der etwa einstündigen Pressekonferenz wurden noch viele andere
Aspekte der Hausdurchsuchung besprochen,
aber dies hier ist nur ein Tagebucheintrag,
kein Protokoll der Pressekonferenz.

Eventuell veröffentlichen wir in einiger Zeit noch
eine Aufzeichnung der Pressekonferenz.
Falls dem so seien wird, wollen wir hier auf sie verlinken.


### Filmabend

20:30 im Klimacamp schauen wir: *Fiese Tricks von Polizei und Justiz*


#### Nachtrag

Der Film behandelt einen hessischen Polizei- und Justizskandal
mit politischen Verstrickungen bis hin zum damaligen Ministerpräsidenten.

Wer neugierig geworden ist, aber den Filmabend verpasst hat,
findet den Film auch hier:
[https://www.youtube.com/watch?v=-N8sRA0ITPk](https://www.youtube.com/watch?v=-N8sRA0ITPk)


## Montag 04.04.2022 -- **Tag 643**

Heute erschien der dritte und letzte Teil des sechsten
Sachstandsberichts zum Weltklima.
Wer Hineinlesen will, findet Links zum Download
der verschiedenen Teile der letzten Klimaberichte
bei uns unter [Weltklimaberichte](/weltklimaberichte/).

Zu den Hauptaussagen des Berichts existieren
die folgenden offiziellen deutschen Übersetzungen:

* [Hauptaussagen von Arbeitsgruppe I](https://www.de-ipcc.de/media/content/Hauptaussagen_AR6-WGI.pdf)
* [Hauptaussagen von Arbeitsgruppe II](https://www.de-ipcc.de/media/content/Hauptaussagen_AR6-WGII.pdf)
* [Hauptaussagen von Arbeitsgruppe III](https://www.de-ipcc.de/media/content/Hauptaussagen_AR6-WGIII.pdf)

Uno-Generalsekretär António Guterres sprach angesichts der
Veröffentlichung von einem „Dossier der Schande,
welches die leeren Zusagen katalogisiert,
die uns auf einen fest Kurs hin zu einer unbewohnbaren Welt gebracht
haben“.<br>
Quelle: [„World on 'fast track to climate disaster', say UN secretary general“ auf Youtube](https://www.youtube.com/watch?v=FD2BGCA6x6Y&ab_channel=GuardianNews)


## Freitag 01.04.2022 15:00 - 19:00 – Müllsammelaktion im Wittelsbacher Park

Greenpeace organisiert an diesem Tag
ein gemeinsames Müllsammeln im Wittelsbacher Park.<br>
Treffpunkt ist an der Ecke Göggingener Straße / Rosenaustraße.

Es wird empfohlen feste Kleidung und Schuhe zu tragen
und gebeten sofern möglich eigene Arbeitshandschuhe mitzubringen.


## 🕊 Sonntag 27.03.2022 – Fahrraddemo über die B17 -- **Tag 635**

**14:00 am Königsplatz:**
Die erste kombinierte Fahrraddemo über die B17 für
die eng verknüpften Themen Frieden und Klimagerechtigkeit.
Tempolimit auf A8 und B17 und Investitionen in Pflege, Bildung
und öffentlichen Nahverkehr und den Ausbau von Erneuerbaren
statt Aufrüstung und fossile Energien.
Mobilitätswende jetzt! Autofreier Sonntag jetzt!

![Die Route der Fahrraddemo führte vom Königsplatz
über die Gögginger Straße und die Eichleitnerstraße auf die B17.
Nachdem die Demo der B17 etwa fünf Kilometer nach Nordwesten folgte,
ging es über die Bürgermeister-Ackermann-Straße,
die Pferseer Unterführung, Frölichstraße und Schaetzlerstraße
zurück zum Königsplatz.](/pages/material/images/b17-tour_2022-03-27.png)
Das Kartenmaterial stammt von [https://www.openstreetmap.org](https://www.openstreetmap.org).

### Nachtrag

Wie bereits am Freitag herrschte bei der Demo
eine ausgesprochen gute Stimmung.
Kinder wie auch Erwachsene entlang der Strecke winkten
uns Teilnehmer\*innen zu.
Wir Teilnehmer\*innen winkten Passant\*innen zu
und drückten durch Klingeln unsere Begeisterung aus.
Auch unter den Autofahrer\*innen,
die aufgrund der Demo einige Minuten warten mussten,
drückten Einige ihre Zustimmung aus,
Andere wirkten gestresst und
auch ein paar Mittelfinger wurden uns gezeigt.

Die Demoroute selbst bildete für viele Teilnehmer\*innnen
einen starken Kontrast zu dem Erlebnis,
welches Radfahrer\*innen in Augsburg sonst so erwartet.
Es gab keine roten Ampeln,
an denen aufgrund der autofreundlichen Schaltung
Minuten gewartet werden musste.
Autos wurden von der Polizei abgeblockt,
bevor sie den Teilnehmer\*innen gefährlich Nahe kamen.
Auf der Route fehlten die üblichen zerschlagenen Glasflaschen,
– *Persönliche Anmerkung:
Außerhalb der Demo führte mich am selben Tag
der Radweg an zwei verschiedenen Stellen
durch zerschlagene Glasflaschen.* –
Schlaglöcher und Kopfsteinpflaster.
Besonders angenehm zu Fahren war der Abschnitt auf der B17.
Kurz vor Ende passierte die Demo noch die bei Radfahrer\*innen
gefürchtete Pferseer Unterführung,
die auch schon im Vertrag zum Radbegehren eigens erwähnt wird.

Eine Zählung entlang der Strecke ergab 362 Teilnehmer\*innen.
Ganz genau lässt es sich nicht sagen,
weil sich Personen entlang der Strecke anschlossen,
manchmal sogar spontan,
während andere die Demo nicht komplett bis zum Königsplatz
zurück fuhren.

#### Im Nachgang

Kurz nach Ende der Demo gab es noch einen Eklat mit der Polizei,
als diese die Identität einer Teilnehmerin feststellte,
die an der Demo oben ohne mit überklebten Nippeln teilgenommen hatte.
Wir gehen davon aus, dass die Handlung der Teilnehmerin rechtmäßig war.
Nach unserem Rechtsverständnis, welches nicht perfekt ist,
sich aber immerhin bereits gegen das Rechtsverständnis
von Augsburgs Bürgermeisterin, einer studierten Juristin,
behaupten konnte, ist die Polizei eigentlich nicht berechtigt,
die Identität von Teilnehmer\*innen einer politischen Versammlung
festzustellen,
sofern keine Bedrohung für ein polizeiliches Schutzgut
oder eine Straftat vorliegt.
Das umfasst nach gängigen Gerichtsurteilen auch den Weg zur
und von der Versammlung.

In Folge der Polizeiaktion bildete sich eine Spontandemo
gegen die Sexualisierung des weiblichen Körpers,
die nach einer kurzen Fahrradphase auf der Karlstraße Platz nahm
und später auch noch zum Rathausplatz weiterzog.


## 🏘 Samstag 26.03.2022 13:00 – Demonstration für Wohnraum

Rathausplatz:
Demonstrationszug für Wohnraum für alle mit verschiedenen
sinnbildlichen Orten Augsburgs als Zwischenstationen


## Freitag 25.03.2022 -- **Globaler Klimastreik** -- **Tag 633**

### 🏘 14:00 vor der Patrizia AG in der Fuggerstraße:

24-Stunden-Mahnwache (https://augsburgfueralle.noblogs.org/)
für fairen Wohnraum und gegen Immobilienspekulation


### Großdemonstration um 16:00 am Ulrichsplatz:<br>

Auch Augsburg wird sich wird sich mit eigenen Veranstaltungen
am globalen Klimastreik beteiligen.
Um 16 Uhr am Ulrichsplatz eine große Demonstration geplant.

Mehr Infos zum globalen Klimastreik:
[Fridays for Future Deutschland zum Globalen Klimastreik am 25. März 2022](https://fridaysforfuture.de/reichthaltnicht/)
<br>
In English: [Fridays for Future on the Global Climate Strike on the 25. March 2022](https://fridaysforfuture.org/march25/)

💒 Das Bistum Augsburg veranstaltet extra für den Globalstreik
eine zuvorgehende Schöpfungsandacht um 13:30 Uhr
in der Kirche Heilig Kreuz und ruft zur Streikteilnahme auf:
[Informationen auf bistum-augsburg.de](https://bistum-augsburg.de/Hauptabteilung-II/Kirche-und-Umwelt/Aktuelles/Globaler-Klimastreik-am-25.-Maerz_id_0)

#### Andere Klimastreiks in der Nachbarschaft

Falls die Anfahrt nach Augsburg für dich zu weit ist,
erkundige dich doch, ob eine andere Demonstration
für dich nicht näher gelegen ist.
An diesem Tag werden an vielen Orten Demos stattfinden.
[Hier](https://fridaysforfuture.de/reichthaltnicht/)
gibt es eine Übersicht über die in Deutschland
an diesem Tag geplaneten Demos.

Hier eine Liste von Demonstrationen in unserer Nachbarschaft.

##### Nördlich von Augsburg:
* Donauwörth: 15:00 an der Westspange 1
* Ingolstadt: 14:00 am Theaterplatz
* Neuburg an der Donau: 13:30 am Schrannenplatz

##### Östlich von Augsburg:
* München: 12 Uhr am Königsplatz

##### Südlich von Augsburg:
* Füssen: 18:00 am Rathaus Füssen
* Kempten: 13:00 am Forum Allgäu
* Schongau: 14:30 am Marienplatz

##### Westlich von Augsburg:
* Memmingen: 11:00 am Marktplatz
* Ulm: 15:00 am Münsterplatz

Alle Angaben ohne Gewähr.
Kurzfristige Planänderungen durch die jeweiligen Veranstalter
werden hier nicht aktualisiert.<br>
Quelle: [https://fridaysforfuture.de/reichthaltnicht/](https://fridaysforfuture.de/reichthaltnicht/)

#### Nachtrag

Die Stimmung bei der Demo war ausgesprochen positiv.
Im Anschluss wurde auch wieder getanzt. 🙂
Die Teilnehmerzahl war nicht so hoch
wie bei den Demos vor der Pandemie
oder der Demo vor der letzten Bundestagswahl.
An verschiedenen Stellen entlang der Strecke wurden
zwischen 950 und 1000 Menschen gezählt.


### 🚴‍♀️ 18:10 Rathausplatz: Critical Mass — entspannt Fahrrad fahren durch die Stadt

Wie jeden letzten Freitag im Monat trifft man sich um 18:00
am Rathausplatz und fährt meistens ab ca. 18:15 gemeinsam
über für Autos vorgesehene Straßenspuren.
Das ist keine angemeldete Demo und muss es auch nicht sein.
Ab 16 Radfahrenden ist es durch §27 StVO legal.
Bei dem aktuellen Wetter nehmen meistens mit 100–200 Leuten
an der entspannten Spazierfahrt durch die Stadt teil.
Am 15.5. gibt's auch wieder die
[Kidical Mass](https://augsburg.adfc.de/artikel/kidicalmass),
die bunte Fahrraddemo für alle und insbesondere Kinder!


## Samstag 12.03.2022

Wir planen wie bereits am Vortag eine Fahrraddemo abzuhalten.
Beginn ist um 16 Uhr am Klimacamp.<br>
Bereits für 15:30 ist ein Workshop
zur Gestaltung von Fahrrädern mit politischen Botschaften geplant.


## Freitag 11.03.2022

Wir planen um 16 Uhr eine Fahrraddemo abzuhalten.
Beginn ist um 16 Uhr am Klimacamp.<br>
Bereits für 15:30 ist ein Workshop
zur Gestaltung von Fahrrädern mit politischen Botschaften geplant.


## Dienstag 08.03.2022 -- **Tag 616**

Der Bayerischen Verwaltungsgerichtshof (VGH)
informierte uns telefonisch,
dass der Räumungsbescheid der Stadt Augsburg vom 10. Juli 2020
auch weiter rechtswidrig bleibt.
Dazu gaben wir im Verlauf des Vormittags
[eine Pressemitteilung](/pages/Pressemitteilungen/2022-03-08-Raeumungsbescheid-rechtswidrig.html)
heraus.

## Montag 07.03.2022 -- **Tag 615**

Heute fand die Verhandlung vor dem Bayerischen
Verwaltungsgerichtshof (VGH) statt.
Schon im Vorfeld hatte man in der Presse
zahlreiche [Berichte](/pressespiegel/)
über die anstehende Verhandlung gesehen,
die mit Spannung erwartet wurde.
Doch warum verhandeln wir überhaupt vor dem VGH?
<br>
Zur Erinnerung:

1. Vertreter der Stadt hatten uns an Tag 10 einen Räumungsbescheid
   überreicht.
2. Wir gingen per Eilverfahren vor dem Verwaltungsgericht Augsburg
   gegen den Räumungsbescheid vor und konnten bleiben.
3. Im Hauptverfahren erklärte das Verwaltungsgericht Augsburg den
   Räumungsbescheid für rechtswidrig.
4. Die Stadt stellte vor dem Bayerischen Verwaltungsgerichtshof den
   Antrag in Berufung gehen zu dürfen. Diesem wurde stattgegeben.<br>
   Siehe: [Pressemitteilung der Stadt Augsburg zur Berufung](https://www.augsburg.de/aktuelles-aus-der-stadt/detail/vgh-laesst-antrag-der-stadt-auf-berufung-gegen-vg-urteil-zum-klimacamp-zu)

So befinden wir uns nun an Tag 615
vor dem Bayerischen Verwaltungsgerichtshof.

Für Details, die über diese trockene Auflistung an Fakten hinausgehen,
planen wir morgen im Verlauf des Tages
[eine Pressemitteilung](/pages/Pressemitteilungen/2022-03-08-Raeumungsbescheid-rechtswidrig.html)
herauszugeben.


## Freitag 04.03.2022 -- **Tag 612**

Heute gab es um 18 Uhr am benachbarten Rathausplatz
eine große Friedenskundgebung.
Anlass war der russische Angriff auf die Ukraine.

In den Abendstunden gab es im Klimacamp einen Brand.
Der Brand brach am Moritzplatz etwa gegen 20 Uhr
an einem Zelt in der südwestlichen Ecke des Camp aus,
sprich der rückseitigen Ecke in Richtung Zeughausplatz.

Nachdem es bemerkt worden war,
konnte das Feuer zügig durch unsere Aktivist\*innen gelöscht werden.

Der Schaden hält sich in Grenzen und
konnte innerhalb weniger Tage ersetzt werden.
Ein Zelt und eine Matratze wurden zerstört.
Ein Schlafsack wurde beschädigt.
Personen waren zu keinem Zeitpunkt in Gefahr.

Die Polizei sah sich den Brandort an.
Wir vermuten Brandstiftung,
können aber auch ein Versehen,
wie eine weggeworfene Zigarette,
nicht komplett ausschließen.

Wir haben den Fall nicht an die Medien gegeben
und auch erst mit über einem Monat Verzögerung
hier in unserem Tagebuch erwähnt.


## Montag 28.02.2022 -- **Tag 608**

Heute wurde der zweite Teil des sechsten Sachstandsberichts zum
Weltklima veröffentlicht.
Die verschiedenen Teile des Berichts sind bereits bei uns unter
[Weltklimaberichte](/weltklimaberichte/) verlinkt.


## Sonntag 20.02.2022 -- **Tag 600**

Heute war der 600. Tag des Klimacamps.
Ist das ein Grund zum Feiern?
Wir sind uns nicht sicher.

Auf der einen Seite machen die Entschlossenheit und der Einsatz
so vieler Menschen für Klimagerechtigkeit Hoffnung und Mut.
Das bekommen wir auch immer wieder von Passant\*innen gesagt.
Die Stadt Augsburg kann stolz sein auf ihr Klimacamp.
Sein 600-tägiges Bestehen ist ein Monument.
Als solches ist das Klimacamp vielleicht
noch nicht ganz so bekannt wie der Perlachturm,
im Gegensatz zu [diesem](https://www.augsburg.de/aktuelles-aus-der-stadt/detail/gefahr-durch-steinschlag-am-perlachturm-fischmarkt-gesperrt)
bröckelt
[es aber auch nicht](/pages/Pressemitteilungen/2022-01-29-Durchhalten.html). 😄

Auf der anderen Seite zeigt es,
wie schnell uns die Zeit davon läuft
und wie langsam Klimagerechtigkeitsmaßnahmen beschlossen,
geschweige denn umgesetzt werden.
Das geringe CO₂-Restbudget,
das uns für die überlebenswichtige 1,5°C-Grenze
oder auch nur die 2°C-Grenze noch verbleibt,
wird verschleudert.
CO₂-Neutralität ist die Zukunft.
Daran führt kein Weg vorbei.
Eine entscheidende Frage ist,
wie viel Schaden wir dadurch anrichten,
dass wir den Schritt in diese Zukunft verschleppen.
Beim Klimacamp lautet die Antwort auf diese Frage:
„So wenig wie möglich.“

Daher blicken wir mit gemischten Gefühlen auf die letzten 600 Tage zurück
und mit ebenso gemischten Gefühlen in die Zukunft.
<br>
Die Stadt muss sich an ihren Vertrag mit
„[Fahrradstadt jetzt](https://www.fahrradstadt-jetzt.de/)“ halten.<br>
Die Stadt muss den Empfehlungen der
[KlimaKom-Studie](https://www.augsburg.de/fileadmin/user_upload/umwelt_soziales/umwelt/klima%20und%20energie/Studie_Klimaschutz_2030_mit_allen_anlagen.pdf)
folgen.<br>
Die Stadt sollte aus ihrem eigenen Interesse
[unsere Forderungen](/forderungen/) erfüllen.<br>
Es gibt Vieles im Mobilitätssektor zu tun.<br>
Es gibt Vieles im Heizsektor zu tun.<br>
Es gibt Vieles im Energiesektor zu tun.<br>
Am 25. März steht der nächste globale Klimastreik an.

Wir (das Klimacamp und andere Klimagerechtigkeitsorganisationen)
freuen uns auf Eure Mithilfe.

![Foto des Augsburger Klimacamps an Tag 600:
Die Zahl 600 ist auf einem Lastenfahrrad angebracht,
welches vor dem Klimacamp steht.](/pages/material/images/tag_600.jpeg)


## Donnerstag 17.02.2022 -- **Tag 597**

Gegen 19 Uhr ist erneut, wie bereits am 03. Februar,
ein Vortrag von „Aufstand der letzten Generation“ am Klimacamp geplant.<br>
Für Vorträge und Aktionstrainings von „Aufstand der letzten Generation“ in anderen Städten siehe:
[https://letztegeneration.de/vortraege/](https://letztegeneration.de/vortraege/)


## Samstag 12.02.2022 -- **Tag 592**

„Augsburg Solidarisch“ organisiert unter dem Titel
„Solidarisch durch die Krise“ eine Laufdemo.
Mit der Demonstration soll ein Zeichen für
einen evidenzbasierten und sozialen Umgang mit Klimakrise und Pandemie
gesetzt werden.<br>
Beginn um 12 Uhr am Rathausplatz<br>


## Donnerstag 03.02.2022 -- **Tag 583**

Gegen 19 Uhr ist am Klimacamp ein Vortrag von „Aufstand der letzten Generation“ geplant:

> Hey Bundesregierung!
>
> Die bösen Aktivist\*innen blockieren schon wieder Autobahnen? Haben die denn
> nichts besseres zu tun?
>
> Doch! Haben sie.
>
> Und trotzdem gehen viele junge wie alte Menschen auf die Straße, um so sichtbar
> wie kaum woanders für ein Stück bessere Zukunft zu protestieren.
> Der "Aufstand der letzten Generation" fordert im langem Atem einen dringend
> benötigten politischen Kurswechsel, um die 1,5-Grad-Grenze nicht zu
> überschreiten. Dafür haben sie sich ein leicht umsetzbares Ziel gesetzt:
> ein Gesetz gegen Lebensmittelverschwendung durch Großkonzerne.
>
> Ein Vertreter der "letzten Generation" wird am Donnerstag, den 3.2. um 19
> Uhr im Klimacamp am Moritzplatz von der Kampagne und den strategischen
> Überlegungen hinter den Aktionen sprechen. Neben Möglichkeiten, sich selber zu
> engagieren, wird auch auf die Dringlichkeit des Aktionsgrundes eingegangen: die
> Klimakrise und die Zeit, die uns noch zum Handeln bleibt.
>
> Wir freuen uns auf einen regen Austausch und einen interessanten Vortrag.


## Workshops in der 5. Kalenderwoche 2022 (31.01. bis 02.02.)

 * 🏭 (Montag 31.01. 20:00) **Ende Gelände —
   wenn tausende Menschen friedlich Kohleinfrastruktur blockieren**<br>
   Jedes Jahr bietet das Ende-Gelände-Aktionswochenende
   eine ermächtigende Erfahrung, die wie wenige andere Aktionen zeigt:
   Es gibt Hoffnung. Wandel ist möglich. Wir sind nicht allein!<br>
   Im Rahmen von Ende Gelände blockieren mehrere Tausend Aktivist\*innen
   friedlich Kohleinfrastruktur — indem sie auf den speziellen Gleisen
   übernachten, die Kohlegrube und Kohlekraftwerk verbinden,
   oder sich in die Kohlegruben begeben.
   Sind dort Passant\*innen, dürfen die Bagger nämlich nicht mehr arbeiten.<br>
   Wie läuft eine solche Blockade ab? Was muss mensch mitbringen?
   Was besser zu Hause lassen? Wie sieht die rechtliche Einschätzung aus?<br>
   Der Workshop richtet sich, wie Ende Gelände selbst,
   insbesondere an Menschen mit wenig Aktionsvorerfahrung.

 * 👋 (Dienstag 01.02. 17:00) **Einsteiger\*innentreffen**
 * 🎦 (Dienstag 01.02. 19:00) **Filmvorführung mit anschließender Diskussion:
   Beyond the red lines — Systemwandel statt Klimawandel**<br>
   Ob im rheinischen Braunkohlegebiet, am Hafen von Amsterdam
   oder auf den Straßen von Paris während des Weltklimagipfels,
   die Kämpfe für Klimagerechtigkeit werden an immer mehr Fronten geführt.
   Beyond the red lines (Jenseits der roten Linien)
   ist die Geschichte einer wachsenden Bewegung,
   die „Es reicht! Ende Gelände!“ sagt, zivilen Ungehorsam leistet
   und die Transformation hin zu einer klimagerechten Gesellschaft
   selber in die Hand nimmt.

 * 👋 (Mittwoch 02.02. 14:00) **Einsteiger\*innentreffen**
 * ~~🎻 (Mittwoch 02.02. 18:30) **4. Mahnung spielt Erträgliche Musik aus Augsburg. VivA la Akustikpunk!**~~
   <span style="color:rgb(255,32,0)">Das Event fällt aufgrund eines COVID-Falls aus. ☹</span><br>
   Wir wünschen allen Betroffenen eine gute Besserung.<br>
   [4. Mahnung auf Facebook](https://www.facebook.com/VierteMahnung/)


## Workshops in der 4. Kalenderwoche 2022 (24.01. bis 26.01.)

 * 🌐 (Montag 24.01. 20:00) **Livedemonstration Internetunsicherheit**<br>
   Eine Freiwillige loggt sich auf dem eigenen Handy in eine Website ein,
   das Passwort wird dann am Beamer angezeigt. Einstieg in die Grundlagen
   von Netzwerktechnik: Was passiert eigentlich hinter den Kulissen, wenn
   wir im Internet surfen? Ein Workshop für Menschen ohne
   Technikvorkenntnisse, die ihr Sicherheitsbewusstsein schärfen möchten.<br>
   Konsequenzen für Aktivismus und Journalismus.

 * 🔐 (Dienstag 25.01. 18:00) **Verschlüsselungsworkshop**<br>
   (Warum) brauche ich Verschlüsselung auf meinen Geräten? Was kann sie
   leisten, wo liegen ihre Grenzen? Wer möchte, kann gerne Handy oder
   Laptop zum Verschlüsseln mitbringen; dafür vorab ein Backup anlegen,
   Akku voll laden, und Netzteil/Ladekabel nicht vergessen.

 * 🏪 (Mittwoch 26.01. 20:00) **Workshop Konsumkritik-Kritik: von der Mär der
   angeblichen Macht der Verbraucher\*innen**<br>
   Fünf überraschende Sachverhalte, wieso die Erzählung „Dein Kassenbon
   ist ein Stimmzettel“ fehlerhaft ist, in die Irre führt und echte
   gesellschaftliche Veränderung blockiert. Eine grundlegende
   Situationsanalyse und wirksame Alternativen.

Diese Workshops fanden im Klimacamp am Moritzplatz statt.


## Sonntag 23.01.2022 -- **Tag 572**

### 10:00 am Königsplatz -- **Fahrraddemo**
Die Stadt wird sich nur dann
an ihre vertragliche Verpflichtung zum Radbegehren halten,
wenn wir sie daran immer und immer wieder erinnern.
Wer für sichere Radwege und sinnvolle Tempobeschränkungen ist,
kann das am Sonntag auf der Fahrraddemo ausdrücken.

Die geplante Route beginnt und endet am Königsplatz.
Vorgesehen ist auch eine Durchfahrt
des vierspurigen Schleifenstraßen-Tunnels der Nagahama-Allee.
![Route der Fahrraddemo als PNG](/pages/material/images/fahrraddemo_2022-01-23.png)
Das Kartenmaterial stammt von [https://www.openstreetmap.org](https://www.openstreetmap.org).

Fazit:
Die Stimmung war überaus ausgelassen und fröhlich.
Über sechzig Radfahrer\*innen hatten bei kalter Witterung
an der relativ kurzfristig als Ersatzveranstaltung
angekündigten Demonstration teilgenommen.

### 18:00 am Königsplatz -- **Augsburg Solidarisch**

+++ Heute seid ihr gefragt, für Solidarität in Zeiten der Krise auf die Straße
zu gehen! +++

📣 Ab 18:00 Uhr: Kundgebung am Königsplatz von Augsburg Solidarisch

Lasst uns gemeinsam zeigen, dass wir da sind, dass wir laut sind und dass wir
den politischen Diskurs nicht den Schwurbelnden überlassen werden!

Am Samstag, den 22.01. um 18 Uhr wollen wir wieder am Königsplatz gemeinsam auf
die Straße gehen und uns für einen solidarischen Weg durch die Krise stark
machen!

Wir möchten die Demonstrationen der selbsternannten „Coronarebell\*innen“, die
auch diesen Samstag wieder durch Augsburg ziehen werden, nicht unbeantwortet
lassen. Stattdessen möchten wir deutlich machen, dass Verschwörungsglaube,
Wissenschaftsfeindlichkeit und egoistische Freiheitsforderungen nicht in der
Mitte der Gesellschaft stattfinden, keine Mehrheitsmeinung repräsentieren!

Wir möchten zeigen: wir, die sich für einen besonnenen, wissenschaftsfundierten
und solidarischen Weg durch die Krise einsetzen, sind viele!

Und wir möchten zeigen, dass – auch diesen Weg beschreitend – fundierte,
konstruktive Kritik am Krisenmanagement der Bundesregierung möglich und wichtig
ist.

Lasst uns also gemeinsam auf die Straße gehen: für eine gerechtere
Impfstoffverteilung, für faire Löhne im Pflege- und Gesundheitsberufen, für
eine solidarische Krisenpolitik, die nicht auf Kosten der Verwundbarsten in
unserer Gesellschaft stattfindet. Und gegen Verschwörungsglaube,
Wissenschaftsfeindlichkeit und Egoismus!

Alerta!


## Freitag 21.01.2022 15:00 am Rathausplatz -- **Geburtstagskundgebung von Fridays for Future Augsburg**
Neben Redebeiträgen und Musik gibt es auch genügend Möglichkeit
zum informellen Austausch:
Was ist die Vision für die Zukunft, wie geht es weiter?
Herzliche Einladung dazu!


## Workshops in der 3. Kalenderwoche 2022 (17.01. bis 19.01.)

 * 📈 (Montag 17.01. 20:00) **Physikalische Grundlagen der Klimakrise: Mythen und Fakten**<br>
   Wieso hat so wenig CO₂ so große Auswirkungen?
   Woher wissen wir, wie das Klima früher war?
   Wie können wir aus den langsamen Klimaveränderungen der Vergangenheit
   lernen?
   Woher rührt die große Verlässlichkeit unserer Klimamodelle?
   Was sind Konsequenzen für Augsburg, Deutschland und den globalen Süden?
   Zusammenhänge zur Klimakrise allgemeinverständlich erklärt

 * 👩‍🔬 (Dienstag 18.01. 20:00) **Klimagerechtigkeit und Feminismus**<br>
   Zwei Themen, die auf den ersten Blick überhaupt nichts miteinander zu
   tun haben und auf den zweiten untrennbar verknüpft sind. Versteckte
   Zusammenhänge, Konsequenzen für unseren Alltag und unseren Aktivismus
   und eine Einladung zum 8. März.

 * ⚖️ (Mittwoch 19.01. 20:00) **Über die vier prinzipiellen Grundsatzprobleme der
   Demokratie, die systematisch die Klimakrise befeuern**<br>
   Demokratie: Kaum ein Wort ist so positiv besetzt wie dieses. Inwiefern
   legen demokratische Strukturen kontinuierlich einer Lösung der
   Klimakrise Steine in den Weg? Wieso können wir CO₂-Emissionen nicht
   einfach an €SU-Umfragewerte koppeln? Über vier Aspekte, die zu selten
   unser kollektives Bewusstsein erreichen. Eine Analyse aus
   herrschaftstheoretischer Sicht unter besonderer Betrachtung der Vision,
   dass Betroffene von Entscheidungen auf sie Einfluss nehmen könnten.

All diese Workshops fanden im Klimacamp am Moritzplatz statt.


## Workshops in der 2. Kalenderwoche 2022 (10.01. bis 13.01.)

Die aktuelle Woche ist ein besonders guter Zeitpunkt,
in die Augsburger Klimagerechtigkeitsbewegung einzusteigen,
da wir viele Einstiegsworkshops anbieten.
Ideal, um auf den aktuellen Stand zu kommen!

 * (Montag 10.01. 20:00) **Augsburger Lokalpolitik**:<br>
   Wer sind die Akteure und welchen Zwängen unterliegen sie?
   Was sind stabile Forderungen? Wieso fordern wir X und nicht Y?
   Was sind unsere bisherigen Erfolge? Was macht die Stadt schon?
 * (Dienstag 11.01. 20:00) **Das Einmaleins der Pressearbeit**:<br>
   Pressemitteilungen, Begriffsrahmung, Interviews.
   Analyse von Pressemitteilungen der Stadt Augsburg,
   der CSU Augsburg und der Grünen.
 * (Mittwoch 12.01. 20:00) **Aktivistische Rechtskunde**:<br>
   Ordnungswidrigkeiten, Straftaten, Versammlungsrecht;
   aktivistischer Zusammenhalt: unsere Antwort auf ihre Repression
 * (Donnerstag 13.01. 20:00) **Sichere Kommunikation**:<br>
   ein kritischer Vergleich von WhatsApp, Signal & Co.
   Anonymes Surfen im Internet: (Wie) geht das?

All diese Workshops fanden im Klimacamp am Moritzplatz statt.


## [Tagebuch 2021](/tagebuch/2021/)

Das Tagebuch wurde zu lang.
Die Einträge für das Jahr 2021
sind [hierhin](/tagebuch/2021/) umgezogen.

## [Tagebuch 2020](/tagebuch/2020/)

Das Tagebuch wurde zu lang.
Die Einträge des Gründungsjahres 2020
sind [hierhin](/tagebuch/2020/) umgezogen.
