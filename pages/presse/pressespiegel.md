---
layout: page
title: Pressespiegel
permalink: /pressespiegel/
parent: "Presse"
nav_order: 20
---

# Pressespiegel

Das sagen die Anderen über uns (unvollständig):


## 2022

### 10.11.2022

* Quer: [Bannwald muss Stahlwerk weichen](https://www.br.de/mediathek/video/av:636d6b6ba33ce10008f188c7)
  <br>
  Zwar wird das Klimacamp in dem Beitrag nicht namentlich erwähnt,
  allerdings wird der Skandal um die Rodung des Lohwaldes bei Meitingen,
  gegen die wir uns auch engagieren,
  in dem Beitrag sehr gut veranschaulicht.


### 08.11.2022

Für weitere Informationen dazu,
worüber sich die heutige Berichterstattung vorwiegend drehte,
siehe auch unseren
[Tagebucheintrag vom 07.11.2022](/tagebuch/#montag-07112022--tag-860)
und unsere
[Pressemitteilung vom 07.11.2022](/pressemitteilungen/2022-11-07-besorgnis-wegen-radikaler-buergerlichen-mitte/).


* Die Augsburger Zeitung: [Kommentar zur Stellungnahme der Bürgerliche Mitte: Schlimmer geht es kaum](https://www.daz-augsburg.de/kommentar-buergerliche-mitte-von-der-rolle/)
* Augsburger Allgemeine: [Wie viel hat das Klimacamp mit den Aktivisten der "Letzten Generation" zu tun?](https://www.augsburger-allgemeine.de/augsburg/augsburg-wie-viel-hat-das-klimacamp-mit-den-aktivisten-der-letzten-generation-zu-tun-id64483841.html)
* Radio Schwaben: [Augsburg: Fabian Mehring äußert sich zum Klimacamp](https://radioschwaben.de/nachrichten/augsburg-fabian-mehring-aeussert-sich-zum-klimacamp/)
  <br>
  **Persönliche Anmerkung:**
  <br>
  \*seufz\*
  Wenigstens scheint sich Herr Mehring dieses Mal
  zu Herzen genommen haben, was ich in einer
  [persönlichen Anmerkung zum 27.10.2022](/pressespiegel/#27102022)
  kritisiert hatte.


### 07.11.2022

* Die Augsburger Zeitung: [Freie Wähler holzen gegen Augsburger Klimacamp](https://www.daz-augsburg.de/jljljl/)
* Die Augsburger Zeitung: [Kino: Großer Publikumszuspruch beim Dokumentarfestival „Mondiale“](https://www.daz-augsburg.de/91088-2/)
  <br>
  (nur am Rande erwähnt)


### 04.11.2022

* Presse Augsburg: [Enge Verbindung von „Letzte Generation“ zum Augsburger Klimacamp?](https://presse-augsburg.de/enge-verbindung-von-letzte-generation-zum-augsburger-klimacamp/829380/)


### 03.11.2022

* Augsburger Allgemeine: [Aktivisten bauen um: Das Augsburger Klimacamp soll schöner werden](https://www.augsburger-allgemeine.de/augsburg/augsburg-aktivisten-bauen-um-das-augsburger-klimacamp-soll-schoener-werden-id64445116.html)


### 02.11.2022

* Augsburger Allgemeine: [Über eine verirrte Fledermaus und einen Katzentempel - Nachrichtenwecker](https://www.youtube.com/watch?v=9Dp2MepBwVs)


### 01.11.2022

* Augsburger Allgemeine: [Streit um Toilettenhäuschen ärgert Klimacamper in Augsburg](https://www.augsburger-allgemeine.de/augsburg/augsburg-streit-um-toilettenhaeuschen-aergert-klimacamper-in-augsburg-id64422976.html)


### 31.10.2022

* Augsburg.tv: [a.tv kompakt: Weniger Platz für Klimacamp](https://www.augsburg.tv/mediathek/video/a-tv-kompakt-weniger-platz-fuer-klimacamp/)
* Hitradio RT1: [Stadt Augsburg will Klimacamp verlegen](https://www.imsueden.de/hitradio-rt1/stadt-augsburg-will-klimacamp-verlegen-304466/)
* Augsburger Allgemeine: [Klimacamp soll für Christkindlesmarkt-Toiletten Platz machen](https://www.augsburger-allgemeine.de/augsburg/augsburg-christkindlesmarkt-klimacamp-soll-fuer-toiletten-platz-machen-id64397656.html)


### 28.10.2022

* Die Stadtzeitung: [Klimaschützer besetzen Bezirksregierung](https://www.staz.de/region/augsburg/politik/klimaschuetzer-besetzen-bezirksregierung-id245032.html)


### 27.10.2022

* Süddeutsche Zeitung: [Aktivisten besetzen Regierungszentrale](https://www.sueddeutsche.de/bayern/augsburg-lech-stahlwerke-klimacamp-polizei-regierung-von-schwaben-1.5682862)
* Bayerischer Rundfunk: [Wegen Wald-Rodung: Aktivisten besetzen Regierung von Schwaben](https://www.br.de/nachrichten/bayern/wegen-wald-rodung-aktivisten-besetzen-regierung-von-schwaben,TLSe1eQ)
* Zeit: [Ermittlungen nach Protestaktion bei Regierungsbehörde](https://www.zeit.de/news/2022-10/27/ermittlungen-nach-protestaktion-bei-regierungsbehoerde?utm_referrer=https%3A%2F%2Fwww.startpage.com%2F)
* Augsburger Allgemeine: [Klima-Aktivisten demonstrieren mit Abseil-Aktion gegen Lohwald-Rodung](https://www.augsburger-allgemeine.de/augsburg/augsburg-klima-aktivisten-demonstrieren-mit-abseil-aktion-gegen-rodung-des-lohwalds-id64379686.html)
* Hitradio RT1 [Klimacamp-Protest bei Regierung von Schwaben geräumt](https://www.imsueden.de/hitradio-rt1/klimacamper-protestieren-bei-regierung-von-schwaben-301921/)
* Augsburger Allgemeine: [Ermittlungen nach Protestaktion bei Regierungsbehörde](https://www.augsburger-allgemeine.de/bayern/klimacamp-ermittlungen-nach-protestaktion-bei-regierungsbehoerde-id64383336-amp.html)
* Die Stadtzeitung: [Mehring verurteilt Besetzung der schwäbischen Bezirksregierung](https://www.staz.de/region/meitingen/politik/mehring-verurteilt-besetzung-schwaebischen-bezirksregierung-id244983.html)
* Freie Wähler Landesfraktion: [„Klimacamp“-Proteste wegen Lohwald-Teilrodung](https://fw-landtag.de/aktuelles/presse/pressemitteilungen-details/klimacamp-proteste-wegen-lohwald-teilrodung)
  <br><br>
  **Persönliche Anmerkung:**
  <br>
  Fabian Mehring ist ein Troll, wie er im Buche steht.
  Hier drei persönliche Anmerkungen zu seiner neusten Stellungnahme:
  * „Selbst ernannte Klimarebellen“ ist so eine Phrase,
    die er zu lieben scheint.
    Ich bin mir nicht sicher,
    ob sich in der 28-monatigen Geschichte des Klimacamps
    auch nur eine einzige Person hier zu einem\*r „Klimarebellen“
    erklärt hat.
    Wir haben keine Ahnung, wo er den Begriff aufgeschnappt hat.
  * „Berufsdemonstranten“ ist ein weiterer Begriff,
    den er zur Diffamierung verwendet.
    Der Begriff „Berufsdemonstranten“ erinnert an Vorwürfe
    von rechten Verschwörungstheoretikern,
    die sich in ihrem Weltbild
    Großdemonstrationen für verschiedene Gerechtigkeitsthemen
    nur dadurch erklären können,
    dass die Demonstrant\*innen bezahlt seien müssen.
    <br>
    Tatsächlich haben die meisten von uns einen Beruf,
    ein Studium oder eine (Schul-)Ausbildung und
    zweigen jedlichen Klimagerechtigkeitsaktivismus
    von unserer spärlichen Freizeit ab
    oder nutzen sogar unseren Urlaub.
    Wir fragen uns natürlich trotzdem,
    welche Zielgruppe Herr Mehring mit dieser Wortwahl
    anzusprechen versucht.
  * Eine weitere ständig wiederholte Falschaussage von Herrn Mehring ist,
    dass das Klimacamp Bürgerinitiativen vor den Karren gespannt habe.
    Dabei hatten wir den Lohwald anfangs gar nicht auf dem Schirm.
    Es waren Bürgerinitiativen zum Schutz des Lohwaldes,
    welche das Klimacamp vor über einem Jahr
    um unterstützen gebeten hatten.
    Es war ein Anwohner,
    der nach Beginn der Rodungen ins Klimacamp geradelt kam,
    um uns davon zu berichten.
    Trotzdem wiederholt Herr Mehring diese Falschbehauptung
    bereits seit Wochen mantraartig in seinen Stellungnahmen.


### 26.10.2022

* Die Augsburger Zeitung: [Teilrodung Lohwald: Naturschützer greifen Regierung von Schwaben an](https://www.daz-augsburg.de/teilrodung-lohwald-naturschuetzer-greifen-regierung-von-schwaben-an/)


### 25.10.2022

* Die Augsburger Zeitung: [Baurecht vor Klimaschutz: Rodung des Lohwalds gestartet](https://www.daz-augsburg.de/90948-2/)
* Radio Schwaben: *[Ohne Titel](https://radioschwaben.de/nachrichten/45587/)*
* B4B Schwaben: [Lech-Stahlwerke roden Lohwald bei Meitingen: Naturschützer wollen dagegen klagen](https://www.b4bschwaben.de/b4b-nachrichten/augsburg_artikel,-lechstahlwerke-roden-lohwald-bei-meitingen-naturschuetzer-wollen-dagegen-klagen-_arid,268208.html)


### 24.10.2022

* Augsburger Allgemeine: [Rodung des Lohwalds bei Meitingen für Lechstahl hat offenbar begonnen](https://www.augsburger-allgemeine.de/augsburg-land/meitingen-rodung-des-bannwalds-bei-meitingen-fuer-lechstahl-hat-offenbar-begonnen-id64345391.html)
* Bayerischer Rundfunk: [Lech-Stahlwerke sorgen mit Bannwald-Rodung für Kritik](https://www.br.de/nachrichten/bayern/lech-stahlwerke-sorgen-mit-bannwald-rodung-fuer-kritik,TLBZdnV)
* Hitradio RT1: [Erste Lohwald-Rodung bei Meitinger Lechstahlwerken](https://www.imsueden.de/hitradio-rt1/erste-lohwald-rodung-bei-meitinger-lechstahlwerken-299452/)


### 20.10.2022

* Augsburger Allgemeine: [Augsburger Klimacamper räumen E-Scooter von Gehwegen auf Parkplätze](https://www.augsburger-allgemeine.de/augsburg/augsburg-augsburger-klimacamper-raeumen-e-scooter-von-gehwegen-id64310516.html)


### 17.10.2022

* Süddeutsche Zeitung: [Augsburg will mehr Bäume erhalten](https://www.sueddeutsche.de/bayern/augsburg-bahnhof-deutsche-bahn-baeume-klimaschutz-1.5676612)
* chilli – das freiburger stadtmagazin: [14 Jahre Zelten: Freiburger Klimacamp richtet sich auf Rathausplatz ein](https://www.chilli-freiburg.de/stadtgeplauder/14-jahre-zelten-freiburger-klimacamp-richtet-sich-auf-rathausplatz-ein/)
  <br>Hier wird das Klimacamp in Augsburg nur am Rande erwähnt.


### 16.10.2022

* Baumallianz: [Pressemitteilung zum Kommentar „Die Stadt ist kein Wald“](https://baumallianz-augsburg.de/archive/2800)


### 15.10.2022

* Augsburger Allgemeine: [Heimat-Check: Wo hakt es beim Klimaschutz?](https://www.augsburger-allgemeine.de/bayern/augsburg-heimat-check-wo-hakt-es-beim-klimaschutz-id64194231.html)
* Augsburger Allgemeine: [Diskussion um Bäume in Augsburg: Die Stadt ist kein Wald](https://www.augsburger-allgemeine.de/augsburg/kommentar-diskussion-um-baeume-in-augsburg-die-stadt-ist-kein-wald-id64247896.html)


### 13.10.2022

* Augsburger Allgemeine: [Augsburger Regierungskoalition will Baumfällungen am Bahnhofsvorplatz vermeiden](https://www.augsburger-allgemeine.de/augsburg/augsburg-augsburger-stadtregierung-will-baumfaellungen-am-bahnhofsvorplatz-vermeiden-id64237046.html)
  <br>
  (Das Klimacamp wird hier nicht namentlich erwähnt,
  wohl aber Aktionen von uns.)
* Die Augsburger Zeitung: [Bahnhofsvorplatz: Regierungskoalition prüft Planungsänderung](https://www.daz-augsburg.de/nach-protesten-seitens-der-buergerschaft-der-baumallianz-und-des-klimacamps-lassen-die-csu-und-die-gruenen-pruefen-inwieweit-das-aus-dem-realisierungswettbewerb-2015-hervorgegangene-konzept-zu/)

In der DAZ gab es heute auch einen detaillierten Artikel
über die Podiumsdiksussion vom 28. September 2022.
Mit weniger Details gibt es auch einen
[Eintrag in unserem Tagebuch](/tagebuch/#mittwoch-28092022--tag-820)
über die Podiumsdiskussion.

* Die Augsburg Zeitung: [Debatte: Woran kann man kluge politische Entscheidungen erkennen?](https://www.daz-augsburg.de/debatte-woran-kann-man-kluge-politische-entscheidungen-erkennen/)


### 10.10.2022

#### Zur heutigen Baumbesetzung:

* Bayerischer Rundfunk: [Klimacamp-Aktivisten besetzen Bäume in Augsburg ](https://www.br.de/nachrichten/bayern/klimacamp-aktivisten-besetzen-baeume-in-augsburg,TJprUnj)
* Augsburger Allgemeine: [Reese-Areal: Klimacamp protestiert mit Baumbesetzungen gegen Fällungen](https://www.augsburger-allgemeine.de/augsburg/augsburg-reese-areal-klimacamp-protestiert-mit-baumbesetzungen-gegen-faellungen-id64200876.html)
* Die Augsburger Zeitung: [Klimacamp stemmt sich gegen Baumfällungen auf Reese-Gelände](https://www.daz-augsburg.de/klimacamp-stemmt-sich-gegen-baumfaellungen-auf-reese-gelaende/)
* Die Augsburger Zeitung: [Update: Fällungen auf Reese-Gelände verlaufen störungsfrei](https://www.daz-augsburg.de/90798-2/)
* Augsburger Allgemeine: [Baumbesetzung auf Reese-Areal beendet – Klimacamper geben Blockade auf](https://www.augsburger-allgemeine.de/augsburg/augsburg-baumbesetzung-auf-reese-areal-beendet-klimacamper-geben-blockade-auf-id64200876.html)
* Radio Schwaben: [Augsburg: Aktivisten besetzen Bäume](https://radioschwaben.de/nachrichten/augsburg-aktivisten-besetzen-baeume/)
* Hitradio RT1: [Reese-Areal: Aktivisten protestieren gegen Baumfällungen](https://www.imsueden.de/hitradio-rt1/reese-areal-aktivisten-protestieren-gegen-baumfaellungen-290658/)
* SAT1: [Augsburg: Klimacamp-Aktivisten besetzen Bäume](https://www.sat1.de/serien/17-30-sat1-bayern/videos/augsburg-klimacamp-aktivisten-besetzen-baeume)
* Augsburg.tv: [a.tv kompakt](https://www.augsburg.tv/mediathek/video/a-tv-kompakt-messerattacke-vor-bar-in-augsburg/)
* Augsburger Allgemeine: [Widerstand gegen Fällungen: Deshalb haben die Baum-Besetzer aufgegeben](https://www.augsburger-allgemeine.de/augsburg/augsburg-widerstand-gegen-faellungen-deshalb-haben-die-baum-besetzer-aufgegeben-id64205311.html)

Für weitere Informationen zur Baumbesetzung siehe auch unseren
[Tagebucheintrag](/tagebuch/#montag-10102022--tag-832)
und unsere
[Pressemitteilung](/pressemitteilungen/2022-10-10-baumbesetzung/).


#### Zu anderen Themen:

* Radio Schwaben: [Kreis Augsburg: Naturschützer klagen gegen Lechstahlwerke-Erweiterung](https://radioschwaben.de/nachrichten/kreis-augsburg-naturschuetzer-klagen-gegen-lechstahlwerke-erweiterung/)
* Augsburg.tv: [BUND-Naturschutz klagt gegen Erweiterung der Lech-Stahlwerke](https://www.augsburg.tv/mediathek/video/bund-naturschutz-klagt-gegen-erweiterung-der-lech-stahlwerke/)

**Anmerkung:**
Was haben der Lohwald bei Meitingen,
die Bäume auf dem Augsburger Bahnhofsvorplatz,
die Bäume im Reese-Park,
der Bobingener Auwald
und der Eichenwald bei Ulm gemeinsam?
Bei all diesen Fällen wurden das Augsburger Klimacamp oder
mit uns befreunde Klimagerechtigkeitsaktivist\*innen
durch vor Ort lebende Bürger\*innen oder Bürgerinitiativen
auf die Bedrohung aufmerksam gemacht
und um Unterstützung gebeten.

Der in der heutigen Berichterstattung
mehrfach zu Wort gekommene Herr Mehring irrt, wenn er denkt,
dass sich Aktionsgemeinschafen vom Augsburger Klimacamp
„vor den Karren spannen lassen“.
Das Gegenteil ist eher der Fall.
<br>
Nebenbei ist das Klimacamp selbst auch „nur“
eine Versammlung von Bürger\*innen.


### 09.10.2022

* Die Augsburger Zeitung: [Kommentar: Der lange Schatten der Theatersanierung](https://www.daz-augsburg.de/kommentar-der-lange-schatten-des-perlach/)


### 07.10.2022

* Bund Naturschutz: [BN klagt gegen Bannwaldrodung zur Erweiterung der Lechstahlwerke](https://www.bund-naturschutz.de/pressemitteilungen/bn-klagt-gegen-bannwaldrodung-zur-erweiterung-der-lechstahlwerke)
* Bayerischer Rundfunk: [Naturschützer klagen gegen Bannwaldrodung bei Lech-Stahlwerken](https://www.br.de/nachrichten/bayern/naturschuetzer-klagen-gegen-bannwaldrodung-bei-lech-stahlwerken,TJa04mR)
* Süddeutsche Zeitung: [Klage wegen Lechstahlwerken](https://www.sueddeutsche.de/bayern/bannwald-lechstahlwerke-bund-naturschutz-meitingen-1.5670583)
* Augsburger Allgemeine: [Naturschützer wollen Lechstahl-Erweiterung vor Gericht verhindern](https://www.augsburger-allgemeine.de/augsburg-land/meitingen-naturschuetzer-wollen-lechstahl-erweiterung-vor-gericht-verhindern-id64180256.html)
* Die Augsburger Zeitung: [Klage gegen geplante Waldrodung](https://www.daz-augsburg.de/90727-2/)


## 06.10.2022

* FlossenTV - Geschichte erleben!: [Klimaaktivisten im Fokus: Waren früher alle Menschen Klimasünder? \| Klimacamp Augsburg](https://www.youtube.com/watch?v=8aZfEU_Alds)


## 05.10.2022

* Augsburger Allgemeine: [Streit unter Aktivisten des Augsburger Klimacamps: "Das ist eine Orgie"](https://www.augsburger-allgemeine.de/augsburg/augsburg-streit-unter-aktivisten-des-augsburger-klimacamps-das-ist-eine-orgie-id64144546.html)
* Augsburger Allgemeine: [Streitereien im Klimacamp: Das steckt dahinter - Nachrichtenwecker](https://www.youtube.com/watch?v=CU86k4MMazg)

**Anmerkung:**
Die Berichterstattung des heutigen Tages
wirkt auf den ersten Blick nicht sehr schmeielhaft.
In Summe sagt sie aber nur aus,
dass das Klimacamp von einem bunt gemischten Haufen
normaler Menschen betrieben wird.
Menschen sind nicht perfekt, machen Fehler,
benehmen sich daneben und geraten in Streit.
Das wissen wir nicht erst seit letzter Woche,
sondern wurde bereits kurz nach der Gründung des Camps klar.

Der beschriebene Vorfall vom 28.09.2022
und auch andere Kritikpunkte waren bereits am 30.09.2022
in einem internen Plenum thematisiert worden.
Entgegen der reißerischen Titel
sind die genauen Hintergründe den Medien nicht bekannt.
Das ist auch gut so.
In erster Linie handelt es sich
um persönliche Probleme und Streits,
die in der Öffentlichkeit nichts zu suchen haben.
Das Klimacamp versucht hier zu unterstützen und zu vermitteln,
schreckt aber auch vor harten Maßnahmen,
wie temporären oder permanenten Ausschlüssen, nicht zurück.

Das Klimacamp will ein inklusiver und kinderfreundlicher Ort sein.
Leider schaffen es nicht immer alle Menschen,
die dem Camp angehören wollen,
den besonderen Ansprüchen,
die dem Klimacamp von Außen aber auch von Innen
durch das eigene Plenum auferlegt werden,
gerecht zu werden.

**Nachtrag vom 12. Oktober:**
Zwei der federführenden Streitenden beider Seiten lachen inzwischen,
wenn sie gemeinsam anderen Campteilnehmer\*innen
die Geschichte erzählen,
was an dem Tag zwischen ihnen tatsächlich vorgefallen war
und wie die Augsburger Allgemeine
einige Äußerungen in verfälschtem Kontext widergegeben hat.
Für die Außenwirkung war es leider trotzdem eine blöde Aktion.


### 30.09.2022

Die Interviews in der folgenden Radiosendung
scheinen schon ein paar Monate älter zu sein.
Mindestens ein davon Teil erweckt den Eindruck,
dass er schon im März 2022 aufgenommen wurde.

* Mach Dein Radio: [Magazinsendung des P-Seminars MWG on Air](https://www.machdeinradio.de/radiobeitrag/sendung-mwg-schulradio-2022_final-mp3/)


### 29.09.2022

Nur eine kleine Erwähnung:

* Augsburger Allgemeine: [Kränzle macht weiter: "Solange ich fit bin, gibt es keine Gedanken ans Aufhören"](https://www.augsburger-allgemeine.de/augsburg/augsburg-kraenzle-macht-weiter-solange-ich-fit-bin-gibt-es-keine-gedanken-ans-aufhoeren-id64088101.html)


### 28.09.2022

* a3kultur: [Wissenschaft und Politik: Kluge Entscheidungen finden](https://www.a3kultur.de/termine/wissenschaft-und-politik-kluge-entscheidungen-finden)


### 27.09.2022

* Die Augsburger Zeitung: [Vortrag im Rahmen der Reihe: „Was tun“ – Wissenschaft und Politik – wie passt das zusammen?](https://www.daz-augsburg.de/vortrag-im-rahmen-der-reihe-was-tun-wissenschaft-und-politik-wie-passt-das-zusammen/)


### 25.09.2022

Augsburg Allgemeine: [Menschenkette für den Erhalt des Wehringer Auwalds](https://www.augsburger-allgemeine.de/schwabmuenchen/wehringen-menschenkette-fuer-den-erhalt-des-wehringer-auwalds-id64049876.html)


### 24.09.2022

* Die Augsburger Zeitung: [„Es muss fundamentale Veränderungen geben“ – Demonstrationen für mehr Klimaschutz](https://www.daz-augsburg.de/90542-2/)
* Johann-Michael-Sailer-Gymnasium in Dillingen: [AK Klimaschutz](https://sailer-gymnasium.de/index.php/faecher-wahlfaecher/wahlfaecher/klimaschutz)
  <br>
  Zwar haben wir kein Datum zu der Erwähnung,
  aber Internetsuchmaschinen haben den Eintrag am 24. September entdeckt.
* Augsburger Allgemeine: [Menschenkette für den Erhalt des Wehringer Auwalds](https://www.augsburger-allgemeine.de/schwabmuenchen/wehringen-menschenkette-fuer-den-erhalt-des-wehringer-auwalds-id64049876.html)


### 23.09.2022

Heute gab es viele Artikel zum globalen Klimastreik
sowie auch zur geplanten Kidical Mass.
Für Informationen dazu,
wie der Globalstreik in Augsburg ablief,
siehe auch den
[Tagebucheintrag vom 23.09.2022](/tagebuch/#freitag-23092022--globaler-klimastreik--tag-815).

* Augsburger Allgemeine: [Klimastreik in Augsburg: "Die Politik muss die Klimakrise endlich als Notfall behandeln"](https://www.augsburger-allgemeine.de/augsburg/augsburg-klimastreik-in-augsburg-die-politik-muss-die-klimakrise-endlich-als-notfall-behandeln-id64035926.html)
  <br>
  **Anmerkung:**
  Der Autor dieses Artikels beweist zum wiederholten Male
  seine Fähigkeit zur phantasievollen Berichterstattung
  und seinen subtilen Humor.
  Er schreibt: „950 Teilnehmerinnen und Teilnehmer kamen in Augsburg.
  **Die Veranstalter hatten eine höhere Resonanz erwartet.**“<br>
  Tatsächlich war von den Veranstaltern
  eine Demonstration mit 700 Teilnehmer\*innen angemeldet worden.
  Insofern hätten sogar 950
  unsere Erwartungen bereits übertroffen.
  Wir gehen aufgrund von zwei Zählungen davon aus,
  dass es tatsächlich sogar zwischen 1.000 und 1.100 waren.
  Mehr dazu im
  [Tagebucheintrag vom 23.09.2022](/tagebuch/#freitag-23092022--globaler-klimastreik--tag-815).
  <br>
  Wir finden diese phantasievolle Berichterstattung
  – wie auch die vom [29.01.2022](#29012022),
  die vom [16.05.2022](#16052022) oder die vom [20.05.2022](#20052022) –
  zwar recht unterhaltsam.
  Wahrscheinlich lacht aber außer uns Insidern niemand sonst darüber.
  Auch sorgen wir uns,
  dass Leser\*innen das für bare Münze nehmen
  und so einen falschen Eindruck gewinnen könnten.
* Bayerischer Rundfunk: [Weltweiter Klimastreik: Fridays for Future wieder auf der Straße](https://www.br.de/nachrichten/deutschland-welt/fridays-for-future-ruft-zu-weltweitem-klimastreik-auf,TI9VAog)
* Bayerischer Rundfunk: [Fridays for Future: Tausende demonstrieren für mehr Klimaschutz](https://www.br.de/nachrichten/bayern/fridays-for-future-tausende-demonstrieren-fuer-mehr-klimaschutz,TIGkEwv)
* Bayerischer Rundfunk: [Klimastreik: Demos in Augsburg, Lindau, Kempten und Sonthofen](https://www.br.de/nachrichten/bayern/klimastreik-demos-in-augsburg-lindau-kempten-und-sonthofen,TIAqWTS)
  <br>Dieser Artikel benennt korrekt,
  dass in Augsburg eine Demonstration mit 700 Teilnehmer\*innen
  angemeldet worden war.
* Neue Szene Augsburg: [4. Augsburger Kidical Mass am Sonntag den 25.09.](https://www.neue-szene.de/magazin/news/4-augsburger-kidical-mass-sonntag-den-2509)
* Stadtzeitung: [Fahrraddemo am Sonntag in Augsburg: Familien fordern mehr Tempo-30-Zonen](https://www.staz.de/region/augsburg/politik/fahrraddemo-sonntag-augsburg-familien-fordern-mehr-tempo-30-zonen-id243935.html)

Am heutigen Tag hat es eine Aktion von Augsburgs
sogenanntem „Staatsschutz“ gegen Klimagerechtigkeitsaktivist\*innen,
um genau zu sein [Pimmelgate Süd](https://www.pimmelgate-süd.de/),
in die New York Times geschafft.
Der Fall wird als eines von meheren Beispielen
kurz mit mehreren Absätzen in einem Artikel
über die Einschränkung der Redefreiheit im Internet erläutert.
Der Artikel wurde bereits von einem anderen
englischsprachigen Magazin aufgegriffen:

* New York Times: [Where Online Hate Speech Can Bring the Police to Your Door](https://www.nytimes.com/2022/09/23/technology/germany-internet-speech-arrest.html)
* Reason: [Germany's Criminalization of Online Offensiveness Shows the Perils of Weakening the First Amendment](https://reason.com/2022/09/23/germanys-criminalization-of-online-offensiveness-shows-the-perils-of-weakening-the-first-amendment/)

Außerdem haben wir noch eine Erwähnung
in einem Interview mit dem König von Augsburg.

* Hallo Augsburg: [Der König von Augsburg im Interview](https://www.hallo-augsburg.de/koenig-von-augsburg-der-koenig-von-augsburg-im-interview_Cia)


### 21.09.2022

* Radio Schwaben: [Augsburg: Verkehrsbeeinträchtigungen wegen Fahrraddemos](https://radioschwaben.de/nachrichten/augsburg-verkehrsbeeintraechtigungen-wegen-fahrraddemos/)
* Augsburger Allgemeine: [Rad-Demo auf der B17: Für welche Ziele sich die Aktivisten einsetzen](https://www.augsburger-allgemeine.de/augsburg/augsburg-rad-demo-auf-der-b17-fuer-welche-ziele-sich-die-aktivisten-einsetzen-id64020586.html)


### 20.09.2022

* Stadtzeitung: [Fahrraddemos am Mittwoch und Donnerstag: Augsburger Aktivisten fordern Ausbau des Tramnetzes](https://www.staz.de/region/augsburg/politik/fahrraddemos-mittwoch-donnerstag-augsburger-aktivisten-fordern-ausbau-tramnetzes-id243802.html)


### 19.09.2022

Am Rande erwähnt:

* Hallo Augsburg: [Ab nach Wien: Warum Jonas Riegel den Stadtjugendring verlassen hat](https://www.hallo-augsburg.de/stadtjugendring-augsburg-ab-nach-wien-warum-jonas-riegel-den-stadtjugendring-verlassen-hat_Z7a)


### 16.09.2022

#### Verkehrswende:

* Augsburg.tv: [Augsburger Mobilitätsplan - Bürgerbeteiligung für die Verkehrswende](https://www.augsburg.tv/mediathek/video/augsburger-mobilitaetsplan-buergerbeteiligung-fuer-die-verkehrswende/)


#### Übersicht über globale Klimastreiks in Bayern:

* Sonntagsblatt: [Globaler Klimastreik auch in bayerischen Städten](https://www.sonntagsblatt.de/artikel/epd/globaler-klimastreik-auch-bayerischen-staedten)


### 14.09.2022

Nicht direkt über uns, aber über das Aktionsprogramm
rund um das Thema *Mobilität* in diesem September:

* Die Augsburg Zeitung: [Mehrere Demonstrationen für besseren ÖPNV und sichere Fahrradwege in Augsburg angekündigt](https://www.daz-augsburg.de/mehrere-demonstrationen-fuer-besseren-oepnv-und-sichere-fahrradwege-in-augsburg-angekuendigt-2/)


### 08.09.2022

* Augsburger Allgemeine: [Grünen-Politiker fordert runden Tisch zum Auwald](https://www.augsburger-allgemeine.de/schwabmuenchen/wehringen-gruenen-politiker-fordert-runden-tisch-zum-auwald-id63851271.html)


### 07.09.2022

* Kontext: Wochenzeitung: [Lebensmittelretter:innen in Ravensburg: Diebe oder Wohltäterinnen?](https://www.kontextwochenzeitung.de/gesellschaft/597/diebe-oder-wohltaeterinnen-8401.html)


### 30.08.2022

* Neue Szene: [Ciao Sommer - Hallo Herbst - Die September Szene ist da](https://www.neue-szene.de/magazin/news/ciao-sommer-hallo-herbst-die-september-szene-da)


### 28.08.2022

Zwar wird hier nicht direkt das Klimacamp erwähnt,
aber die am [15. August](/tagebuch/#montag-15082022--tag-776)
am Klimacamp gestartete
von Students for Future organisierte Fahrraddemo nach Berlin.
* Leipziger Zeitung: [Kundgebung am Kohlekraftwerk: Augsburger Students for Future machten Zwischenstopp in Lippendorf](https://www.l-iz.de/politik/engagement/2022/08/kundgebung-am-kohlekraftwerk-augsburger-students-for-future-machten-zwischenstopp-in-lippendorf-467795)


### 23.08.2022

* uniCROSS: [Klimacamp Freiburg: Wir campen bis ihr handelt](https://www.unicross.uni-freiburg.de/was-passiert-im-klimacamp/)


### 15.08.2022

* Augsburger Allgemeine: [Bahn frei: Radler fahren im Protestzug von Augsburg nach Berlin](https://www.augsburger-allgemeine.de/augsburg/augsburg-bahn-frei-radler-fahren-im-protestzug-von-augsburg-nach-berlin-id63635166.html)


### 12.08.2022

* junge Welt: [»Hausdurchsuchungen sind bei uns auf der Tagesordnung«](https://www.jungewelt.de/artikel/432407.polizeiaufgabengesetz-hausdurchsuchungen-sind-bei-uns-auf-der-tagesordnung.html)


### 10.08.2022

* juraforum.de: [OVG Hamburg: Zelte des Klimacamps unterliegen Versammlungsfreiheit](https://www.juraforum.de/news/ovg-hamburg-zelte-des-klimacamps-unterliegen-versammlungsfreiheit_258264)


### 06.08.2022

* Bayerischer Rundfunk: [Kritik an Polizei: "Klimacamp keine terroristische Vereinigung"](https://www.br.de/nachrichten/bayern/kritik-an-polizei-klimacamp-keine-terroristische-vereinigung,TDjOVcS)


### 05.08.2022

* Netzpolitik.org: [Augsburger Polizei überzieht Klimaaktivisten mit weiterem Verfahren](https://netzpolitik.org/2022/pimmelgate-sued-augsburger-polizei-ueberzieht-klimaaktivisten-mit-weiterem-verfahren/)


### 02.08.2022

* Stadtzeitung: [Kritik an geplanten Baumfällungen: Polizei stoppt Kletteraktion des Klimacamps](https://www.staz.de/region/augsburg/politik/kritik-geplanten-baumfaellungen-polizei-stoppt-kletteraktion-klimacamps-id242135.html)
* Augsburger Allgemeine: [Bäume am Bahnhofsvorplatz sollen gefällt werden: Kritik vom Klimacamp](https://www.augsburger-allgemeine.de/augsburg/augsburg-baeume-am-bahnhofsvorplatz-sollen-gefaellt-werden-kritik-vom-klimacamp-id63507481.html)


### 01.08.2022

Die originale Pimmelgate-Affäre in Hamburg
wurde durch Eistellung des Verfahrens
vor Monaten klammheimlich beendet.
Derweil kriegt die Polizei in Augsburg das Memo nicht
und verdoppelt ihren Einsatz.

* Netzpolitik.org: [Hamburger Staatsanwaltschaft erklärt #Pimmelgate-Affäre für beendet](https://netzpolitik.org/2022/verfahren-eingestellt-hamburger-staatsanwaltschaft-erklaert-pimmelgate-affaere-fuer-beendet/)


### 31.07.2022

* Aichacher Zeitung: [Streit um Bäume am Hauptbahnhof](https://www.aichacher-zeitung.de/vorort/augsburg/streit-um-baeume-am-hauptbahnhof;art21,191150)


### 30.07.2022

* Stadtzeitung: [Fallen auch die übrigen Bäume am Augsburger Hauptbahnhof? Stadtrat verschiebt die Entscheidung](https://www.staz.de/region/augsburg/politik/fallen-uebrigen-baeume-augsburger-hauptbahnhof-stadtrat-verschiebt-entscheidung-id242000.html)


### 12.07.2022

Hier wird nicht explizit das Klimacamp erwähnt,
aber die von *[Verkehrswende Augsburg](https://www.verkehrswende-augsburg.de/)*
organisierten Fahrraddemos.

* Die Augsburger Zeitung: [Fahrraddemostrationen für ein sicheres Radwegenetz und den ÖPNV-Ausbau in Augsburg](https://www.daz-augsburg.de/fahrraddemostrationen-fuer-ein-sicheres-radwegenetz-und-den-oepnv-ausbau-in-augsburg/)


### 07.07.2022

* Youtube: [BR24: Wegen Energiekrise: Augsburg will sogar die Ampeln ausschalten \| Abendschau \| BR24](https://www.youtube.com/watch?v=KK5QxM2IssE)


#### Freiburg:

* StadtWandler.org: [Klimacamp auf dem Rathausplatz für die sozial-ökologische Wende ](https://www.stadtwandler.org/de/articles/klimacamp-auf-dem-rathausplatz-fuer-die-sozial-oekologische-wende)


### 06.07.2022

* Bayerischer Rundfunk: [Brunnen aus, Ampeln aus: Augsburg will Signal fürs Energiesparen](https://www.br.de/nachrichten/bayern/brunnen-aus-ampeln-aus-augsburg-will-signal-fuers-energiesparen,TApSKBV)
* Youtube: [reporter: Hausdurchsuchung bei 15-jähriger Fridays for Future Aktivistin - warum? \| reporter](https://www.youtube.com/watch?v=Q7J8bW0EkVA)


### 01.07.2022

* Bayerische Staatszeitung: ["Pimmelgate Süd": Hausdurchsuchung nach Kritik an Politiker](https://www.bayerische-staatszeitung.de/staatszeitung/landtag/detailansicht-landtag/artikel/pimmelgate-sued-hausdurchsuchung-nach-kritik-an-politiker.html#topPosition)


### 30.06.2022

* Hitradio RT1: [Zwei Jahre Klima-Camp: Aktivisten wollen weitermachen](https://www.imsueden.de/hitradio-rt1/zwei-jahre-klima-camp-aktivisten-wollen-weitermachen-231754/)

#### Zu Freiburg:

* Freiburg.de: [Klimacamp auf dem Rathausplatz](https://www.freiburg.de/pb/1913508.html)
* SWR: [Klimacamp Freiburg: "Wir campen, bis ihr handelt"](https://www.swr.de/swraktuell/baden-wuerttemberg/suedbaden/klima-camp-freiburger-rathausplatz-100.html)


### 25.06.2022

* [Musik und Menschen aus Augsburg mit Achim 60 Bogdahn](https://www.br.de/radio/bayern2/sendungen/zuendfunk/stadtwerke-augsburg-achim-bogdahn-100.html)


### 22.06.2022

* Youtube: [Assyria TV: Das Klimacamp](https://www.youtube.com/watch?v=KtSzaEinLyA)


### 08.06.2022

* Welt: [Wenn Klima-Aktivisten mit Schichtplan in der Innenstadt kampieren](https://www.welt.de/politik/deutschland/plus239169115/Augsburger-Klima-Camp-Aktivisten-und-Politik-im-Dauerstreit.html)


### 07.06.2022

* Augsburger Allgemeine: [Nachrichtenwecker – Parkplatznot am Augsburger Zoo - Nachrichtenwecker](https://www.youtube.com/watch?v=xQ09x_XdiYg&ab_channel=AugsburgerAllgemeine)


### 06.06.2022

* Augsburger Allgemeine: [Klimacamp: Das umstrittene Zeltlager soll aufgehübscht werden](https://www.augsburger-allgemeine.de/augsburg/augsburg-klimacamp-das-umstrittene-zeltlager-soll-aufgehuebscht-werden-id62895971.html)


### 04.06.2022

* Augsburger Allgemeine: [Lauter Protest vor dem Klimacamp ruft mehrfach die Polizei auf den Plan](https://www.augsburger-allgemeine.de/augsburg/augsburg-lauter-protest-vor-dem-klimacamp-ruft-mehrfach-die-polizei-auf-den-plan-id62894251.html)


### 02.06.2022

Die folgenden Artikel handeln nicht direkt über uns,
aber vom Schutz des Bobinger Auwaldes (siehe
[https://www.bobinger-auwald-bleibt.de/](https://www.bobinger-auwald-bleibt.de/),
an dessen Schutz wir uns beteiligen möchten.

* Hitradio RT1: [Abholzung: Experte widerspricht Wehringens Bürgermeister](https://www.imsueden.de/hitradio-rt1/abholzung-experte-widerspricht-wehringens-buergermeister-218016/)
* Augsburger Allgemeine: [Gewerbegebiet im Auwald: Förster widerspricht Darstellung des Bürgermeisters](https://www.augsburger-allgemeine.de/schwabmuenchen/wehringen-bobingen-gewerbegebiet-foerster-widerspricht-darstellung-des-buergermeisters-id62858761.html)
* Augsburger Allgemeine: [Gewerbegebiet im Auwald: Friedlicher Protest beim Ortstermin](https://www.augsburger-allgemeine.de/schwabmuenchen/wehringen-bobingen-gewerbegebiet-im-auwald-friedlicher-protest-beim-ortstermin-id62876616.html)


### 01.06.2022

* Augsburger Allgemeine: [Der Augsburger Kurt Späth führt eine Ein-Mann-Demo gegen das Klimacamp](https://www.augsburger-allgemeine.de/augsburg/augsburg-der-augsburger-kurt-spaeth-fuehrt-eine-ein-mann-demo-gegen-das-klimacamp-id62858336.html)
* Hitradio RT1: [Rentner protestiert gegen Augsburger Klimacamp](https://www.imsueden.de/hitradio-rt1/rentner-protestiert-gegen-augsburger-klimacamp-217206/)


### 31.05.2022

* Die Augsburger Zeitung: [Interview mit Ingo Blechschmidt: „Für Kompromisse fehlt uns die Zeit“](https://www.daz-augsburg.de/interview-mit-ingo-blechschmidt-fuer-kompromisse-fehlt-uns-die-zeit/)


### 30.05.2022

* Die Augsburger Zeitung: [Stadtrat relativiert politische Bedeutung des Klimacamps](https://www.daz-augsburg.de/89327-2/)


### 29.05.2022

* Münchner Merkur: [Klimawandel im Allgäu: Das kommt auf uns zu](https://www.merkur.de/bayern/schwaben/kempten-westallgaeu-kreisbote/klimawandel-im-allgaeu-das-kommt-auf-uns-zu-91575808.html)
* tz: [9-Euro-Ticket: tz-Redakteure verraten, welche Schätze Sie von München aus erkunden sollten](https://www.tz.de/muenchen/stadt/deutsche-bahn-muenchen-9-euro-ticket-bayern-tipps-erkundung-deutschland-reise-91576562.html?itm_source=story_detail&itm_medium=interaction_bar&itm_campaign=share)
  <br>
  *Ist das die erste Erwähnung des Augsburger Klimacamps
  in einem Reiseführer?*
* Videos auf Youtube:
  * BR24: [BR24live: Streit um die Energiewende - Wie viel Windkraft verträgt Bayern? | jetzt red i | BR24](https://www.youtube.com/watch?v=jyyq3_O-mx8&ab_channel=BR24)
    <br> *Wortbeitrag ab 33:25*
  * Gans Normal Vegan: [Folge 81 - Janika](https://www.youtube.com/watch?v=m_8_lappyHM&ab_channel=GansNormalVegan)


### 28.05.2022

* Bayerischer Rundfunk: [Beispiel Holzheim: Windkraft sorgt in Bayern für Zündstoff](https://www.br.de/nachrichten/bayern/beispiel-holzheim-windkraft-sorgt-in-bayern-fuer-zuendstoff,T732AEb)


### 27.05.2022

* Stadtzeitung: [Protest an Auwald-Rodung in Wehringen: Klimaaktivisten hängen Banner auf](https://www.staz.de/region/bobingen/politik/protest-auwald-rodung-wehringen-klimaaktivisten-haengen-banner-id238937.html)
* Augsburger Allgemeine: [Augsburger Stadtrat streitet über den Umgang mit dem Klimacamp](https://www.augsburger-allgemeine.de/augsburg/augsburg-augsburgs-stadtrat-streitet-ueber-den-umgang-mit-dem-klimacamp-id62808596.html)
* Radio Schwaben: [Landkreis Augsburg: Klimacamp-Aktivisten gehen gegen geplantes Gewerbegebiet vor](https://radioschwaben.de/nachrichten/landkreis-augsburg-klimacamp-aktivisten-gehen-gegen-geplantes-gewerbegebiet-vor/)


### 26.05.2022

* Bayerischer Rundfunk: [Klimaaktivisten wollen Bobinger Auwald besetzen](https://www.br.de/nachrichten/bayern/klima-aktivisten-bereiten-besetzung-des-bobinger-auwalds-vor,T6w78bd)
* Die Augsburger Zeitung: [Bobinger Auwald: Klimaaktivisten kündigen massiven Widerstand an](https://www.daz-augsburg.de/89294-2/)
* Augsburger Allgemeine: [Klimacamp hängt Banner im Wald bei Wehringen auf](https://www.augsburger-allgemeine.de/augsburg/augsburg-wehringen-klimacamp-haengt-banner-im-wald-bei-wehringen-auf-id62805346.html)
* Augsburger Allgemeine: [Neues Gewerbegebiet in Wehringen: Klima-Aktivisten planen Waldbesetzung](https://www.augsburger-allgemeine.de/schwabmuenchen/wehringen-neues-gewerbegebiet-in-wehringen-klima-aktivisten-planen-waldbesetzung-id62804651.html)


### 25.05.2022

* Stadtzeitung: [Rodung des Bobinger Auwaldes: Klimaaktivisten wollen Forst besetzen](https://www.staz.de/region/bobingen/politik/rodung-bobinger-auwaldes-klimaaktivisten-wollen-forst-besetzen-id238884.html)
* Augsburger Allgemeine: [Schwarz-Grün will Klimaaktivisten städtischen Raum zur Verfügung stellen](https://www.augsburger-allgemeine.de/augsburg/augsburg-schwarz-gruen-will-klimaaktivisten-staedtischen-raum-zur-verfuegung-stellen-id62777496.html)
* Augsburger Allgemeine: [Geplantes Gewerbegebiet: Klimaaktivisten wollen Wald besetzen](https://www.augsburger-allgemeine.de/schwabmuenchen/wehringen-geplantes-gewerbegebiet-klimaaktivisten-wollen-wald-bei-wehringen-besetzen-id62799216.html)
* Stern: [Fridays For Future-Aktivistin Janika Pondorf: "Ich war in der Klinik. Und ich war dort wegen dieses Polizeieinsatzes"](https://www.stern.de/gesellschaft/janika-pondorf---ich-war-wegen-dieses-polizeieinsatzes-in-der-klinik--31895656.html)
* Die Augsburger Zeitung: [Positiver Schnelltest: Heutige Stadtratssitzung ohne OB Weber](https://www.daz-augsburg.de/positiver-schnelltest-heutige-stadtratssitzung-ohne-ob-weber/)

#### International:

* Prudent Press Agency: [Augsburg: Black-Green wants to make urban space for climate activists](https://www.prudentpressagency.com/augsburg-black-green-wants-to-make-urban-space-for-climate-activists/)

#### Verwandte Themen:

Die folgenden Artikel erwähnen uns nicht direkt,
handeln aber von Projekten, an denen Viele von uns beteiligt sind:

* myheimat.de: [Vision klimaneutrale Stadt- Gemeinsam für Verkehrs- und Energiewende](https://www.myheimat.de/augsburg/politik/vision-klimaneutrale-stadt-gemeinsam-fuer-verkehrs-und-energiewende-d3386112.html)
* Radio Schwaben: [Landkreis Augsburg: Klimaschützer wollen Bobinger Auwald besetzen](https://radioschwaben.de/nachrichten/landkreis-augsburg-klimaschuetzer-wollen-bobinger-auwald-besetzen/)


### 24.05.2022

* Die Augsburger Zeitung: [Kommentar: Staatsschutz im Kinderzimmer oder warum die Feuerwehr zum Brandstifter wird](https://www.daz-augsburg.de/jkjlkj/)


### 22.05.2022

* Bayerischer Rundfunk: [Wenn der Augsburger Staatsschutz im Kinderzimmer steht](https://www.br.de/nachrichten/bayern/wenn-der-augsburger-staatsschutz-im-kinderzimmer-steht,T6NGbJ1)
* Süddeutsche Zeitung: [Warum der Ton zwischen den Klimaaktivisten und der Stadt schärfer wird](https://www.sueddeutsche.de/bayern/klimacamp-augsburg-hausdurchsuchung-1.5589496)
* Augsburger Allgemeine: [Kuratorin des Klimafestivals: „Es braucht noch viel für klimaneutrale Events“](https://www.augsburger-allgemeine.de/augsburg/kultur/interview-es-braucht-noch-viel-fuer-klimaneutrale-events-id62764856.html)
* Aichacher Zeitung: [Aktivisten erheben erneut Vorwürfe gegen die Abteilung "Staatsschutz"](https://www.aichacher-zeitung.de/vorort/augsburg/aktivisten-erheben-erneut-vorwuerfe-gegen-die-abteilung-staatsschutz;art21,174345)

#### International:

* All News Press: [Augsburg: the tone between climate activists and the city is getting sharper – Bavaria](https://allnewspress.com/augsburg-the-tone-between-climate-activists-and-the-city-is-getting-sharper-bavaria/)


### 20.05.2022

* Augsburger Allgemeine: [Durchsuchung nach Spray-Aktion: Klima-Aktivisten erheben Vorwürfe](https://www.augsburger-allgemeine.de/augsburg/augsburg-durchsuchung-nach-spray-aktion-klima-aktivisten-erheben-vorwuerfe-id62737271.html)
* Stadtzeitung: [Klimaaktivisten erheben erneut Vorwürfe gegen die Abteilung "Staatsschutz" der Augsburger Polizei](https://www.staz.de/region/augsburg/politik/klimaaktivisten-erheben-erneut-vorwuerfe-gegen-abteilung-staatsschutz-augsburger-polizei-id238729.html)
* Die Augsburger Zeitung: [Klimacamp: Augsburger Justiz ist politisch](https://www.daz-augsburg.de/klimacamp-augsburger-justiz-ist-politisch/)
* Augsburger Allgemeine: [FFF-Klimademo: Schüler protestieren wieder, es kommt fast keiner](https://www.augsburger-allgemeine.de/augsburg/augsburg-fff-klimademo-schueler-gehen-wieder-auf-die-strasse-es-kommt-fast-keiner-id62745321.html)
  <br>*Persönliche Anmerkung:
  Der Artikel erschien gegen 11:29 und behauptet,
  dass gegen 11:15 nur 30 Teilnehmer\*innen anwesend waren.
  Nun kann ich das weder bestätigen noch wiederlegen,
  da ich selbst erst wenig später hinzukam.
  Allerdings waren es bereits gegen 11:20 über 100 Teilnehmer\*innen
  und im weiteren Verlauf der Demo wurden etwa 150 gezählt.
  Das ist genau die Größenordnung,
  die angepeilt war.
  Von mangelnder Resonanz kann keine Rede sein.
  <br>
  Für weitere Informationen,
  unter anderem der Erklärung dazu,
  warum wir bei mehr als 200 Teilnehmer\*innen Probleme bekommen hätten,
  siehe auch den
  [Tagebucheintrag zum Schulstreik vom 20.05.2022](/tagebuch/#1100-schulstreik).*


### 19.05.2022

Die folgenden beiden Beitrage handeln streng genommen
nicht vom Augsburger Klimacamp, sondern von FFF-Augsburg.
Allerdings sind bei den angesprochenen Themen die Übergänge fließend.

* Augsburger Allgemeine: [Fridays for Future demonstriert in Augsburg wieder zur Schulzeit](https://www.augsburger-allgemeine.de/augsburg/augsburg-fridays-for-future-demonstriert-in-augsburg-wieder-zur-schulzeit-id62733781.html)
* Stadtzeitung: [Fridays for Future gegen Stadt Augsburg: Gericht erlaubt Schulstreik auf dem Rathausplatz](https://www.staz.de/region/augsburg/politik/fridays-future-gegen-stadt-augsburg-gericht-erlaubt-schulstreik-rathausplatz-id238672.html)


### 18.05.2022

* a3kultur: [Standpauke statt Laudatio](https://a3kultur.de/positionen/standpauke-statt-laudatio)
* Stadtzeitung: [Das Klimacamp und OB Eva Weber: Künftige Zusammenarbeit oder weitere Konfrontation?](https://www.staz.de/region/augsburg/politik/klimacamp-ob-eva-weber-kuenftige-zusammenarbeit-konfrontation-id238592.html)
* Augsburger Allgemeine: [Klimafestival am Staatstheater: Wenn Klima das Theater wandelt](https://www.augsburger-allgemeine.de/augsburg/kultur/aktion-wenn-klima-das-theater-wandelt-klimafestival-am-staatstheater-augsburg-id62716471.html)


### 17.05.2022

* Hallo Augsburg: [So reagiert das Klimacamp Augsburg auf den Zukunftspreis](https://www.hallo-augsburg.de/zukunfspreis-klimacamp-so-reagiert-das-klimacamp-augsburg-auf-den-zukunftspreis_njJ)
* Augsburger Allgemeine: [Das Klimacamp kritisiert OB Eva Weber, geht aber auf Gesprächsangebot ein](https://www.augsburger-allgemeine.de/augsburg/augsburg-das-klimacamp-kritisiert-ob-eva-weber-geht-aber-auf-gespraechsangebot-ein-id62708926.html)
* Presse Augsburg: [Augsburger Zukunftspreise 2021 verliehen – Stadt Augsburg zeichnet sechs Preisträgerinnen und Preisträger aus](https://presse-augsburg.de/augsburger-zukunftspreise-2021-verliehen-stadt-augsburg-zeichnet-sechs-preistraegerinnen-und-preistraeger-aus/793556/)
* B4B Schwaben: [Das sind die Augsburger Zukunftspreisträger 2021](https://www.b4bschwaben.de/b4b-nachrichten/augsburg_artikel,-das-sind-die-augsburger-zukunftspreistraeger-2021-_arid,267043.html)
* Hallo Augsburg: [Stadt Augsburg zeichnet Klimacamp mit dem Zukunftspreis aus](https://www.hallo-augsburg.de/zukunftspreis-klimacamp-augsburg-stadt-augsburg-zeichnet-klimacamp-mit-dem-zukunftspreis-aus_DhJ)


### 16.05.2022

* Augsburger Allgemeine: [Überraschung beim Zukunftspreis: Klimacamp bekommt 1000 Euro von der Stadt](https://www.augsburger-allgemeine.de/augsburg/augsburg-ueberraschung-beim-zukunftspreis-klimacamp-bekommt-1000-euro-von-der-stadt-id62705841.html)
* Augsburger Allgemeine: [Sie haben den Augsburger Zukunftspreis 2021 bekommen](https://www.augsburger-allgemeine.de/augsburg/augsburg-sie-haben-den-augsburger-zukunftspreis-2021-bekommen-id62700361.html)
* Augsburger Allgemeine: [Alarm im Augsburger Rathaus: Rettungskräfte rücken aus](https://www.augsburger-allgemeine.de/augsburg/augsburg-alarm-im-augsburger-rathaus-rettungskraefte-ruecken-aus-id62664096.html)
  <br>
  *Es ist lustig zu sehen,
  dass die Augsburger Allgemeine inzwischen immer öfter explizit erwähnt,
  wenn wir mit einer Sache nichts zu tun haben.*
  <br>• *Pfiffe und Parolen stören Rede der Oberbürgermeisterin.
        → Das Klimacamp hat nichts damit zu tun.*
  <br>• *Rettungskräfte Rücken zum Rathaus aus.
        → Das Klimacamp hat nichts damit zu tun.*
  <br>*Womit werden wir als Nächstes nichts zu tun haben?
  Nur die Augsburger Allgemeine weiß es. :-D*


### 13.05.2022

Heute war die Presse über uns geprägt von zwei Themen –
dem durch Verkehrswendeaktivist\*innen erstellten und am Mittwoch
zum ersten Mal veröffentlichten *Verkehrswendeplan Augsburg*
([https://www.verkehrswende-augsburg.de/](https://www.verkehrswende-augsburg.de/))
sowie dem gerade ablaufenden Umzug des Klimacamps vom Moritzplatz
zurück zum Fischmarkt.

* Augsburg.tv: [Die neue Mobilität - Immer mehr Diskussionen um den Verkehr in Augsburg](https://www.augsburg.tv/mediathek/video/die-neue-mobilitaet-immer-mehr-diskussionen-um-den-verkehr-in-augsburg/)
* Bayerischer Rundfunk: [Augsburger Mobilitätsplan: Kritik an Art der Bürgerbeteiligung](https://www.br.de/nachrichten/bayern/augsburger-mobilitaetsplan-kritik-an-art-der-buergerbeteiligung,T5hHLbO)
* Augsburger Allgemeine: [Das Klimacamp steht wieder neben dem Rathaus in Augsburg](https://www.augsburger-allgemeine.de/augsburg/augsburg-das-klimacamp-steht-jetzt-wieder-neben-dem-rathaus-in-augsburg-id62658256.html)
* Stadtzeitung: [Das Augsburger Klimacamp ist wieder zurück am Rathausplatz](https://www.staz.de/region/augsburg/politik/augsburger-klimacamp-zurueck-rathausplatz-id238426.html)
* Hitradio RT1: [Augsburger Klimacamp ist zurück am Moritzplatz](https://www.imsueden.de/hitradio-rt1/augsburger-klimacamp-ist-zurueck-am-rathausplatz-207437/)
* Augsburger Allgemeine: [Augsburgs Klimacamper im Interview: "Meistens gehe ich unglücklich schlafen"](https://www.augsburger-allgemeine.de/augsburg/augsburg-augsburgs-klimacamper-im-interview-meistens-gehe-ich-ungluecklich-schlafen-id62616941.html)
* Augsburger Allgemeine: [Alter Ort, neues Gesicht? Das Klimacamp kehrt an den Fischmarkt zurück](https://www.augsburger-allgemeine.de/augsburg/augsburg-alter-ort-neues-gesicht-das-klimacamp-kehrt-an-den-fischmarkt-zurueck-id62661676.html)
* Augsburger Allgemeine: [Aktivisten fordern Gratis-Nahverkehr am Wochenende und größere Fußgängerzone](https://www.augsburger-allgemeine.de/augsburg/augsburg-aktivisten-fordern-gratis-nahverkehr-am-wochenende-und-groessere-fussgaengerzone-id62656996.html)


### 11.05.2022

* Bayerischer Rundfunk in ARD Mediathek: [Verlorene Jugend? Corona, Krieg und Klimawandel](https://www.ardmediathek.de/video/Y3JpZDovL2JyLmRlL3ZpZGVvL2ZmYjU5NTk5LTllMjYtNGYyZC1iZGNmLTg0OTE0MzA3ODczNg)
  <br>Der Beitrag zum Klimacamp erfolgt im Video ab 18:25.
  <br>(in der Mediathek verfügbar bis zum 11.05.2023)
* Augsburger Allgemeine: [Fridays for Future rufen in Augsburg wieder zu Schulstreiks auf](https://www.augsburger-allgemeine.de/augsburg/klima-demos-fridays-for-future-rufen-in-augsburg-wieder-zu-schulstreiks-auf-id62644716.html)


### 10.05.2022

* Augsburger Allgemeine: [Die Klimacamper kehren bald zum Rathaus zurück](https://www.augsburger-allgemeine.de/augsburg/augsburg-die-klimacamper-kehren-bald-zum-rathaus-zurueck-id62626616.html)


### 09.05.2022

* Bayerischer Rundfunk: [Verlorene Jugend? Corona, Krieg und Klimawandel](https://www.br.de/themen/religion/verlorene-jugend-corona-krieg-klima-100.html)
* Augsburger Allgemeine: [Klimacamper seilen sich von der City-Galerie ab – nun ermittelt die Polizei](https://www.augsburger-allgemeine.de/augsburg/augsburg-innenstadt-klimacamper-seilen-sich-von-der-city-galerie-ab-nun-ermittelt-die-polizei-id62616491.html)


### 08.05.2022

* Augsburger Allgemeine: [Klimacamper seilen sich von der City-Galerie ab](https://www.augsburger-allgemeine.de/augsburg/augsburg-klimacamper-seilen-sich-von-der-city-galerie-ab-id62605506.html)
  <br>Ein detaillierterer Bericht der Kletteraktion
  findet sich auch in unserem
  [Tagebucheintrag zum 07.05.2022](/tagebuch/#samstag-07052022--tag-676--willkommenstag).
* Augsburger Allgemeine: [Beim Klimacamp in Augsburg droht allen Seiten ein Verlust an Glaubwürdigkeit](https://www.augsburger-allgemeine.de/augsburg/kommentar-beim-klimacamp-in-augsburg-droht-allen-seiten-ein-verlust-an-glaubwuerdigkeit-id62560476.html)

### 07.05.2022

* Augsburg-Journal: [Ernährung aus der Tonne](https://www.augsburg-journal.de/news/ernaehrung-aus-der-tonne/)

### 06.05.2022

* Augsburger Allgemeine: [Klimacamper sperren Kreuzung: Fußgänger haben kurzzeitig Vorfahrt](https://www.augsburger-allgemeine.de/augsburg/augsburg-klimacamper-sperren-kreuzung-fussgaenger-bekommen-kurze-zeit-vorfahrt-id62585461.html)


### 05.05.2022

* Augsburger Allgemeine: [Nachrichtenwecker – Prozesse und Skandale: So tickt das Klimacamp](https://www.youtube.com/watch?v=oXpkaWtiy_o&ab_channel=AugsburgerAllgemeine)
* Augsburger Allgemeine: [Kurze Klimacamp-Demo am Freitagmittag auf einer Innenstadtkreuzung](https://www.augsburger-allgemeine.de/augsburg/augsburg-klimacamp-demo-findet-am-freitagmittag-auf-einer-innenstadtkreuzung-statt-id62573071.html)
* Radio Schwaben: [Augsburg: Geldstrafe wegen unzulässigem Plakat am Rathaus](https://radioschwaben.de/nachrichten/augsburg-geldstrafe-wegen-unzulaessigem-plakat-am-rathaus/)
* Welt: [Geldstrafe wegen unzulässigem Plakat am Augsburger Rathaus](https://www.welt.de/regionales/bayern/article238548157/Geldstrafe-wegen-unzulaessigem-Plakat-am-Augsburger-Rathaus.html)


### 04.05.2022

* Zeit.de: [Geldstrafe wegen unzulässigem Plakat am Augsburger Rathaus](https://www.zeit.de/news/2022-05/04/geldstrafe-wegen-unzulaessigem-plakat-am-augsburger-rathaus?utm_referrer=https%3A%2F%2Fwww.startpage.com%2F)
* Stadtzeitung: [Nach Banner-Aktion vor Gericht: Aktivist des Augsburger Klimacamps soll Geldstrafe zahlen](https://www.staz.de/region/augsburg/blaulicht/banner-aktion-gericht-aktivist-augsburger-klimacamps-geldstrafe-zahlen-id237992.html)
* Augsburger Allgemeine: [Augsburgs CSU-Chef Ullrich legt Klimacampern den Abschied nahe](https://www.augsburger-allgemeine.de/augsburg/klimacamp-augsburg-volker-ullrich-legt-klimacampern-abschied-nahe-id62546131.html)
* Radio Augsburg: [Polizeiaufmarsch im Klimacamp](https://radioaugsburg.de/polizeiaufmarsch-im-klimacamp/)
* Radio Fantasy: [Augsburger Klimaaktivisten stehlen Lebensmittel und verschenken sie an Bedürftige](https://www.fantasy.de/news/augsburger-klimaaktivisten-stehlen-lebensmittel-und-verschenken-sie-an-beduerftige)
* Augsburger Allgemeine: [Klimacamp-Aktivist verurteilt: Vor dem Prozess hängt er erneut ein Plakat auf](https://www.augsburger-allgemeine.de/augsburg/augsburg-klimacamp-aktivist-verurteilt-vor-dem-prozess-haengt-er-erneut-ein-plakat-auf-id62560821.html)
* Stern: [Geldstrafe wegen unzulässigem Plakat am Augsburger Rathaus](https://www.stern.de/gesellschaft/regional/bayern/klimacamp-geldstrafe-wegen-unzulaessigem-plakat-am-augsburger-rathaus-31830156.html)
* Münchner Merkur: [Geldstrafe wegen unzulässigem Plakat am Augsburger Rathaus](https://www.merkur.de/bayern/geldstrafe-wegen-unzulaessigem-plakat-am-augsburger-rathaus-zr-91521315.html)


### 03.05.2022

Der heutige Tag war vor allem geprägt von der Berichterstattung
über die Aktion des Verschenkens von containertem Essen.
Die Aktion folgte dem Vorbild einer ähnlichen Aktion in Nürnberg
durch Pater Jörg Alt.
Für weitere Informationen zur ursprünglichen Aktion siehe den
[Bericht des Bayerischen Rundfunks vom 22.12.2021](https://www.br.de/nachrichten/bayern/bis-die-polizei-kommt-pater-aus-nuernberg-rettet-lebensmittel,SsFhNiM).

* Augsburger Allgemeine: [Polizeieinsatz wegen "Containern": Das Klimacamp steht in der Kritik](https://www.augsburger-allgemeine.de/augsburg/augsburg-polizeieinsatz-wegen-containern-das-klimacamp-steht-in-der-kritik-id62547496.html)
* Augsburger Allgemeine: [Die Klimacamper in Augsburg machen sich angreifbar](https://www.augsburger-allgemeine.de/augsburg/kommentar-die-klimacamper-in-augsburg-machen-sich-angreifbar-id62548846.html?wt_mc=redaktion.escenic-reco.article.desktop.)


### 02.05.2022

Heute antworteten wir in der DAZ
auf die Kritik von Stadrat Peter Hummel.

* Die Augsburger Zeitung: [Debatte: Klimacamp antwortet Peter Hummel](https://www.daz-augsburg.de/debatte-klimacamp-antwortet-peter-hummel/)
* Augsburger Allgemeine: [Klimacamp-Aktivisten wollen "containern" und Lebensmittel verschenken](https://www.augsburger-allgemeine.de/augsburg/augsburg-klimacamp-aktivisten-wollen-containern-und-lebensmittel-verschenken-id62542066.html)
* Neue Szene: [Klimaaktivist*innen verschenken gestohlenes Essen](https://www.neue-szene.de/magazin/news/klimaaktivistinnen-verschenken-gestohlenes-essen)


### 30.04.2022

* Die Augsburger Zeitung: [Klimacamp: Muss OB Weber als Zeugin vor Gericht aussagen?](https://www.daz-augsburg.de/klimacamp-muss-ob-weber-als-zeugin-aussagen/)
* Augsburger Allgemeine: [Klimaplakat am Rathaus: 1600 Euro Geldstrafe gegen Aktivisten](https://www.augsburger-allgemeine.de/augsburg/augsburg-klimaplakat-am-rathaus-1600-euro-geldstrafe-gegen-aktivisten-id62520841.html)


### 29.04.2022

* Hitradio RT1: [Klimacamp darf zurück auf den Augsburger Rathausplatz](https://www.imsueden.de/hitradio-rt1/klimacamp-darf-zurueck-auf-den-augsburger-rathausplatz-191444/)
* Stadtzeitung: [CSU und Grüne wollen Augsburg zur "klimafreundlichsten Kommune Bayerns machen"](https://www.staz.de/region/augsburg/politik/csu-gruene-wollen-augsburg-klimafreundlichsten-kommune-bayerns-id237801.html)


### 28.04.2022

* Augsburger Allgemeine: [Perlach gesichert: Das Klimacamp darf neben das Rathaus zurückkehren](https://www.augsburger-allgemeine.de/augsburg/augsburg-perlach-gesichert-das-klimacamp-darf-neben-das-rathaus-zurueckkehren-id62496831.html)


### 27.04.2022

Heute gab es zwei Pressemitteilungen von politischen Parteien über uns.

* Bundesverband Ökologisch-Demokratische Partei ["Pimmelgate-Süd" weitet sich aus](https://www.oedp.de/aktuelles/pressemitteilungen/newsdetails/news/pimmelgate-sued-weitet-sich-aus)
* Grüne Augsburg: [Klimacamp ist wichtiger Akteur der Klimagerechtigkeitsbewegung](https://gruene-augsburg.de/home/news-detail/article/klimacamp-ist-wichtiger-akteur-der-klimagerechtigkeitsbewegung/)

Presse:

* all-in.de: ["Pimmelgate" zieht in Augsburg weiter Kreise - Student sitzt auf dem digitalen Trockenen](https://www.all-in.de/augsburg/c-boulevard/pimmelgate-zieht-in-augsburg-weiter-kreise-student-sitzt-auf-dem-digitalen-trockenen_a5157734)
* la-rundschau.de: ["Pimmelgate-Süd" verhöhnt den Rechtsstaat](http://www.la-rundschau.de/bayern/59440-pimmelgate-sued-verhoehnt-den-rechtsstaat)


### 26.04.2022

* Die Augsburger Zeitung: [Kritik am Klimacamp: Ideologische Scharmützel statt Klimaschutz](https://www.daz-augsburg.de/kritik-am-klimacamp-ideologische-scharmuetzel-statt-klimaschutz/)
  <br>
  Für diese endlich mal inhaltliche Kritik am Klimacamp
  verdient dieser Artikel heute den ersten Platz
  in unserem Pressespiegel und diese kurze Anmerkung.
* Augsburger Allgemeine: [Augsburg hält durch – das Klimacamp in Nürnberg hört nach 600 Tagen auf](https://www.augsburger-allgemeine.de/augsburg/augsburg-augsburg-haelt-durch-das-klimacamp-in-nuernberg-hoert-nach-600-tagen-auf-id62476416.html)
* Augsburger Allgemeine: [Kritik am Klimacamp: "Trommelkurse fundamentaler als erneuerbarer Strom"](https://www.augsburger-allgemeine.de/augsburg/augsburg-kritik-am-klimacamp-trommelkurse-sind-fundamentaler-als-erneuerbarer-strom-id62471311.html)


### 25.04.2022

* Augsburger Allgemeine: [Zwist mit CSU: Grüne stärken Klimacamp den Rücken](https://www.augsburger-allgemeine.de/augsburg/augsburg-zwist-mit-csu-gruene-staerken-klimacamp-den-ruecken-id62461221.html)
* Die Augsburger Zeitung: [Grüne: Klimacamp ist wichtiger Akteur der Klimagerechtigkeitsbewegung](https://www.daz-augsburg.de/gruene-klimacamp-ist-wichtiger-akteur-der-klimagerechtigkeitsbewegung/)
* Augsburger Allgemeine: ["Dogmatisch und kompromisslos": Eva Weber geht auf Konfrontation zum Klimacamp](https://www.augsburger-allgemeine.de/augsburg/augsburg-dogmatisch-und-kompromisslos-eva-weber-geht-auf-konfrontation-zum-klimacamp-id62434636.html)<br>
  Das sieht nach einer Kopie eines Artikels vom 22.04. aus
  und geht noch in keinster Weise auf unsere Pressemitteilung
  vom selbigen Tag ein.
  Dieses Mal ist er aber nicht hinter einer Paywall verborgen.
* Augsburger Allgemeine: [Krach im Bündnis: Das Klimacamp entzweit CSU und Grüne](https://www.augsburger-allgemeine.de/augsburg/augsburg-krach-im-buendnis-das-klimacamp-entzweit-csu-und-gruene-id62461771.html)


### 23.04.2022

* Augsburger Allgemeine: [CSU-Chef Ullrich und Oberbürgermeisterin Weber attackieren Klimacamp](https://www.augsburger-allgemeine.de/augsburg/augsburg-csu-chef-ullrich-und-oberbuergermeisterin-weber-attackieren-das-klimacamp-id62437371.html)


### 22.04.2022

Am heutigen Tag gaben wir die [Pressemitteilung](/pressemitteilungen/)
[„Augsburger Klimacamp zu Webers Niederlagenstellungnahme“](/pressemitteilungen/2022-04-22-antwort-auf-stellungnahme-webers/)
heraus.
In dieser gehen wir auf Kritik ein,
die Augsburgs Oberbürgermeisterin Eva Weber
in einer schriftlichen Stellungnahme am Vortag geäußert hatte.
Vorausgegangen war dem die schriftliche Begründung
des Bayerischen Verwaltungsgerichtshofs zu seinem Urteil
gegen die Stadt.

#### Links/Quellen:

* 22.04.2022: [Pressemitteilung der Stadt zum Gerichtsurteil](https://www.augsburg.de/aktuelles-aus-der-stadt/detail/stellungnahme-der-stadt-augsburg-zum-klimacamp-urteil-des-bayerischen-verwaltungsgerichtshofs-vom-08-maerz)
* 21.04.2022: [Persönliche Stellungnahme von Eva Weber zum Klimacamp](https://www.augsburg.de/fileadmin/user_upload/footer/presse/downloads/2022/22_04_21_Stellungnahme_von_Oberb%C3%BCrgermeisterin_Eva_Weber_nach_Klimacamp-Urteil.pdf)
* 22.04.2022: [Antwort des Augsburger Klimacamps auf die Stellungnahme von Eva Weber](/pressemitteilungen/2022-04-22-antwort-auf-stellungnahme-webers/)

#### Presse (Deutschsprachig):

* Augsburger Allgemeine: [Aktivisten des Klimacamps kritisieren Oberbürgermeisterin Eva Weber](https://www.augsburger-allgemeine.de/augsburg/augsburg-aktivisten-des-klimacamps-greifen-oberbuergermeisterin-eva-weber-an-id62435321.html)
* Die Augsburger Zeitung: [Klimacamp: Stadt respektiert das VGH-Urteil und verzichtet auf Revision](https://www.daz-augsburg.de/88662-2/)
* Stadtzeitung: ["Verlasst eure Komfortzone": Eva Weber richtet deutliche Worte an Klimacamper](https://www.staz.de/region/augsburg/politik/verlasst-komfortzone-eva-weber-richtet-deutliche-worte-klimacamper-id237548.html)
* Bayerischer Rundfunk: [Stadt Augsburg akzeptiert Klimacamp als politische Versammlung](https://www.br.de/nachrichten/bayern/stadt-augsburg-akzeptiert-klimacamp-als-politische-versammlung,T3iCzto)
* Augsburg.tv: [a.tv kompakt: Klimacamp am Augsburger Moritzplatz bleibt](https://www.augsburg.tv/mediathek/video/a-tv-kompakt-klimacamp-am-augsburger-moritzplatz-bleibt/)
* Die Augsburger Zeitung: [Kommentar: Die Bürgermeisterin und das Klimacamp](https://www.daz-augsburg.de/88671-2/)
* Süddeutssche Zeitung: ["So funktioniert unsere Demokratie nicht"](https://www.sueddeutsche.de/bayern/augsburg-klimacamp-protest-verwaltungsgerichtshof-1.5571003?reduced=true)
* Augsburger Allgemeine: [Die Politik im Rathaus muss endlich Position zum Klimacamp beziehen](https://www.augsburger-allgemeine.de/augsburg/kommentar-die-politik-im-rathaus-muss-endlich-position-zum-klimacamp-beziehen-id62427171.html)
* Augsburger Allgemeine: [Endlich wird in Augsburg über das Klimacamp auch politisch diskutiert](https://www.augsburger-allgemeine.de/augsburg/kommentar-endlich-wird-in-augsburg-ueber-das-klimacamp-auch-politisch-diskutiert-id62436426.html)
* juraforum.de: [Klimacamp Augsburg ist eine geschützte Versammlung](https://www.juraforum.de/recht-gesetz/klimacamp-augsburg-ist-eine-geschuetzte-versammlung-724681)

#### Presse (International):

* All News Press: [Augsburg: Further dispute over the climate camp – Bavaria](https://allnewspress.com/augsburg-further-dispute-over-the-climate-camp-bavaria/)


### 21.04.2022

Heute gab der Bayerische Verwaltungsgerichtshof
die schriftliche Begründung für sein Urteil gegen die Stadt heraus.
Die Begründung fand ein breites Medienecho.

#### Zur letzten Hausdurchsuchung

* Die Augsburger Zeitung: [Augsburger Pimmelgate schlägt in der Politik auf](https://www.daz-augsburg.de/88652-2/)

#### Zum schriftlichen Begründung des Bayerischen Verwaltungsgerichtshofs

* Bayerischer Verwaltungsgerichtshof: [Bayerischer Verwaltungsgerichtshof stuft Augsburger Klimacamp als Versammlung ein](https://www.vgh.bayern.de/media/bayvgh/presse/pm_klimacamp_augsburg.pdf)
* Zeit.de: [Augsburg will Gerichtsstreit um Klimacamp nicht fortsetzen](https://www.zeit.de/news/2022-04/21/augsburg-will-gerichtsstreit-um-klimacamp-nicht-fortsetzen?utm_referrer=https%3A%2F%2Fwww.startpage.com%2F)
* Augsburger Allgemeine: [Schriftliches Urteil: Darum gilt das Klimacamp als Versammlung](https://www.augsburger-allgemeine.de/augsburg/augsburg-schriftliches-gerichtsurteil-darum-gilt-das-klimacamp-als-versammlung-id62426346.html)
* Legal Tribune Online: ["Augs­burger Kli­ma­camp" zählt als Ver­samm­lung](https://www.lto.de/recht/nachrichten/n/10b211694-bayvgh-augsburg-klimaschutz-klimacamp-versammlungsrecht/)
* Radio Augsburg: [Klimacamp darf bleiben – das sagen Eva Weber und die Aktivisten](https://radioaugsburg.de/klimacamp-darf-bleiben-das-sagen-eva-weber-und-die-aktivisten/)
* tz: [Augsburg will Gerichtsstreit um Klimacamp nicht fortsetzen](https://www.tz.de/bayern/augsburg-will-gerichtsstreit-um-klimacamp-nicht-fortsetzen-zr-91493009.html?cmp=defrss)
* beck.de: [Augsburger Klimacamp ist verfassungsrechtlich geschützte Versammlung](https://rsw.beck.de/aktuell/daily/meldung/detail/vgh-muenchen-augsburger-klimacamp-ist-verfassungsrechtlich-geschuetzte-versammlung)
* jura.cc: [Bayerischer Verwaltungsgerichtshof stuft Augsburger Klimacamp als Versammlung ein](https://www.jura.cc/rechtstipps/bayerischer-verwaltungsgerichtshof-stuft-augsburger-klimacamp-als-versammlung-ein/)
* Main-Post: [Augsburg will Gerichtsstreit um Klimacamp nicht fortsetzen](https://www.mainpost.de/ueberregional/bayern/bayern/augsburg-will-gerichtsstreit-um-klimacamp-nicht-fortsetzen-art-10777456)
* Fränkische Landeszeitung: [Augsburg will Gerichtsstreit um Klimacamp nicht fortsetzen](https://www.flz.de/augsburg-will-gerichtsstreit-um-klimacamp-nicht-fortsetzen/cnt-id-ps-b0ddbcc8-1184-4f22-89ad-4e3b8e989ab0)
* Donaukurier: [Augsburg will Gerichtsstreit um Klimacamp nicht fortsetzen](https://www.donaukurier.de/nachrichten/bayern/Prozesse-Urteile-Demonstrationen-Klima-SCHWABEN-Bayern-Augsburg-will-Gerichtsstreit-um-Klimacamp-nicht-fortsetzen;art155371,4867456)


### 20.04.2022

#### Zur letzten Hausdurchsuchung

* Hitradio RT1: ["Pimmelgate" Affäre zieht weitere Kreise ](https://www.imsueden.de/amp/hitradio-rt1/pimmelgate-affaere-zieht-weitere-kreise-175548/)
* Augsburger Allgemeine: [Razzia bei Klimacamp-Aktivisten: Grüne stellen Anfrage an die Staatsregierung](https://www.augsburger-allgemeine.de/augsburg/augsburg-razzia-bei-klimacamp-aktivisten-gruene-stellen-anfrage-an-die-staatsregierung-id62414711.html)

#### Zum Feuer

* Augsburger Allgemeine: [Nachrichtenwecker – Kripo ermittelt: Feuer im Klimacamp - Nachrichtenwecker](https://www.youtube.com/watch?v=rAET-g4hGrM&ab_channel=AugsburgerAllgemeine)
* Presse Augsburg: [Unbekannte legen Feuer im Augsburger Klimacamp](https://presse-augsburg.de/unbekannte-legen-feuer-im-augsburger-klimacamp/788638/)
* Hitradio RT1: [Nach Feuer: Mehr Nachtwachen für Augsburger Klimacamp](https://www.imsueden.de/hitradio-rt1/nach-feuer-mehr-nachtwachen-fuer-augsburger-klimacamp-175392/)


### 19.04.2022

* Augsburg.tv: [a.tv kompakt: Grüne stellen Anfrage im Augsburger Pimmelgate-Fall](https://www.augsburg.tv/mediathek/video/a-tv-kompakt-gruene-stellen-anfrage-im-augsburger-pimmelgate-fall/)
* Augsburger Allgemeine: [Kripo Augsburg ermittelt: Unbekannte legen Feuer im Klimacamp](https://www.augsburger-allgemeine.de/augsburg/augsburg-brandstiftung-im-klimacamp-unbekannte-zuenden-flagge-und-zelt-an-id62407756.html)
* Stadtzeitung: [Mutmaßliche Brandstiftung am Klimacamp – Kripo bittet um Mithilfe](https://www.staz.de/region/augsburg/blaulicht/mutmassliche-brandstiftung-klimacamp-kripo-bittet-um-mithilfe-id237394.html)
* Hitradio RT1: [Unbekannte legen Feuer im Augsburger Klimacamp](https://www.imsueden.de/amp/hitradio-rt1/unbekannte-legen-feuer-im-augsburger-klimacamp-175257/)


### 18.04.2022

* Die Augsburger Zeitung: [Klimacamp und Pimmelgate: Vorwürfe Richtung Staatsanwaltschaft und Staatsschutz](https://www.daz-augsburg.de/jkjk/)


### 15.04.2022

* Netzpolitik.org: [Wochenrückblick KW15: Von Kameras, Kryptocoins und Klimakrise](https://netzpolitik.org/2022/wochenrueckblick-kw15-von-kameras-kryptocoins-und-klimakrise/)
* Augsburger Allgemeine: [Karfreitag: Was Christen zu Putins Krieg und dem Augsburger Klimacamp sagen](https://www.augsburger-allgemeine.de/augsburg/augsburg-karfreitag-was-christen-zu-putins-krieg-und-dem-augsburger-klimacamp-sagen-id62382391.html)


### 14.04.2022

Heute gab es eine offizielle Pressekonferenz
vom Augsburger Klimacamp und
[FFF-Augsburg](https://www.fff-augsburg.de/) zur Hausdurchsuchung.

Vor der Pressekonferenz:

* Hitradio RT1: [Razzia bei Klimacamp: Augsburger "Pimmelgate"?](https://www.imsueden.de/hitradio-rt1/razzia-bei-klimacamp-augsburger-pimmelgate-174099/)

Nach der Pressekonferenz:

* Netzpolitik.org [Pimmel-Kommentar führt zu Razzia bei Klimaaktivisten](https://netzpolitik.org/2022/augsburg-pimmel-kommentar-fuehrt-zu-razzia-bei-klimaaktivisten/)<br>
  *Archyworldys: [Cock comment leads to crackdown on climate activists](https://www.archyworldys.com/cock-comment-leads-to-crackdown-on-climate-activists/)
  Das ist eine englische Übersetzung des Artikels von Netzpolitik.org. (This is a translation of the article from Netzpolitik.org.)*
* augsburg.tv: [Wirbel um Hausdurchsuchung - Klimacamp-Aktivisten kritisieren Polizeieinsatz](https://www.augsburg.tv/mediathek/video/wirbel-um-hausdurchsuchung-klimacamp-aktivisten-kritisieren-polizeieinsatz/)
* Bayerischer Rundfunk: [Razzia bei Augsburger Klimacamp-Aktivisten wegen Beleidigung](https://www.br.de/nachrichten/bayern/razzia-bei-augsburger-klimacamp-aktivisten-wegen-beleidigung,T2yqVJq)
* Stadtzeitung: [Neuauflage des "Pimmelgate" in Augsburg? Klimaaktivisten richten Vorwürfe gegen Polizei und Staatsanwaltschaft](https://www.staz.de/region/augsburg/politik/neuauflage-pimmelgate-augsburg-klimaaktivisten-richten-vorwuerfe-gegen-polizei-staatsanwaltschaft-id237286.html)
* Ausburger Allgemeine: [Klimaschützer kritisieren Augsburger Polizei nach Hausdurchsuchung](https://www.augsburger-allgemeine.de/augsburg/augsburg-klimaschuetzer-kritisieren-augsburger-polizei-nach-durchsuchungsaktion-id62376396.html)
* Youtube – Nik so Wichtig: [Pimmelgate in der AfD-Edition: In Augsburg kommt sogar der Staatsschutz](https://www.youtube.com/watch?v=L6PJo2QIF4k&ab_channel=NiksoWichtig)


### 13.04.2022

Heute drehen sich die Presseberichte
vor allem um die Hausdurchsuchung,
die den Spitznamen „Pimmelgate Süd“ erhalten hat.
Die Details haben wir von der Webseite des Klimacamps ausgelagert.
Siehe: [https://www.pimmelgate-süd.de/](https://www.pimmelgate-süd.de/)

* Fefes Blog: [https://blog.fefe.de/?ts=9ca9d029](https://blog.fefe.de/?ts=9ca9d029)<br>
  Wow, nach nur 652 Tagen Klimacamp hat Fefe
  zum ersten Mal auf eine unserer Seiten verlinkt. ;-)
* Süddeutsche Zeitung: [Das Augsburger "Pimmelgate"](https://www.sueddeutsche.de/bayern/augsburg-pimmelgate-klimacamp-polizei-1.5566562?reduced=true)
* Augsburger Allgemeine: [Staatsschutz durchsucht Wohnung eines Klimacamp-Aktivisten](https://www.augsburger-allgemeine.de/augsburg/augsburg-staatsschutz-durchsucht-wohnung-eines-klimacamp-aktivisten-id62355086.html)
* Augsburger Allgemeine: [Die Razzia bei einem Augsburger Klima-Aktivisten wirkt überzogen](https://www.augsburger-allgemeine.de/augsburg/augsburg-die-razzia-bei-einem-klima-aktivisten-wirkt-ueberzogen-id62366246.html)


### 11.04.2022
* Stadtzeitung: [Elf Augsburger Unternehmen unterzeichnen "Klimapakt" der Stadt – Kritik des Klimacamps](https://www.staz.de/region/augsburg/politik/elf-augsburger-unternehmen-unterzeichnen-klimapakt-stadt-kritik-klimacamps-id237188.html)

### 07.04.2022
* Augsburger Allgemeine: [Nachrichtenwecker](https://www.youtube.com/watch?v=yUopsuNqA7c&ab_channel=AugsburgerAllgemeine)

### 06.04.2022
* augsburg.tv: [Mehr Klimaschutz: "Klimapakt Augsburger Wirtschaft" ist unterzeichnet](https://www.augsburg.tv/mediathek/video/mehr-klimaschutz-klimapakt-augsburger-wirtschaft-ist-unterzeichnet/)
* Augsburger Allgemeine: [OB Eva Weber will beim Klimaschutz in Augsburg einen Weg der Mitte gehen](https://www.augsburger-allgemeine.de/augsburg/augsburg-ob-eva-weber-will-beim-klimaschutz-in-augsburg-einen-weg-der-mitte-gehen-id62260211.html)
* Augsburger Allgemeine: [Die AfD will das Augsburger Klimacamp stündlich kontrollieren lassen](https://www.augsburger-allgemeine.de/augsburg/augsburg-die-afd-will-das-augsburger-klimacamp-stuendlich-kontrollieren-lassen-id62258926.html)
* hitradio rt1: [Klimaaktivisten fordern von Augsburg mehr Anstrengungen](https://www.imsueden.de/hitradio-rt1/klimaaktivisten-fordern-von-augsburg-mehr-anstrengungen-171790/)

### 31.03.2022
* Augsburger Allgemeine: [Der Augsburger Perlach verschwindet bald hinter einem Schutznetz](https://www.augsburger-allgemeine.de/augsburg/augsburg-der-augsburger-perlach-soll-demnaechst-mit-schutznetz-eingewickelt-werden-id62148696.html)

### 27.03.2022
* Bayerischer Rundfunk: [Fahrraddemo für Klimagerechtigkeit](https://www.br.de/mediathek/video/augsburg-fahrraddemo-fuer-klimagerechtigkeit-av:6240788c9c227a000855807e)
  <br>Nur einen Monat lang in der Mediathek verfügbar.
* Augsburger Allgemeine: [Demo in Augsburg: Protest für Frieden, Klima und gegen Sexualisierung](https://www.augsburger-allgemeine.de/augsburg/demo-in-augsburg-protest-fuer-frieden-klima-gegen-sexualisierung-id62179406.html)

### 26.03.2022
* Bayerischer Rundfunk: [Fridays for Future-Demo in Augsburg mit knapp 1.000 Teilnehmern](https://www.br.de/nachrichten/bayern/friday-for-future-demo-in-augsburg-mit-knapp-1-000-teilnehmern,T1Am8Tr)

### 25.03.2022
* B4B Schwaben: [Bundesstraße B17 in Augsburg wird teilgesperrt](https://www.b4bschwaben.de/b4b-nachrichten/augsburg_artikel,-bundesstrasse-b17-in-augsburg-wird-teilgesperrt-_arid,266457.html)
* Augsburger Allgemeine: [Knapp 1000 Menschen ziehen beim Globalen Klimastreik durch Augsburg](https://www.augsburger-allgemeine.de/augsburg/augsburg-knapp-1000-menschen-ziehen-beim-globalen-klimastreik-durch-augsburg-id62167891.html)
* Augsburger Allgemeine: [Dem Protest der Klimaaktivisten in Augsburg geht die Luft aus](https://www.augsburger-allgemeine.de/augsburg/kommentar-dem-protest-der-klimaaktivisten-in-augsburg-geht-die-luft-aus-id62166381.html)
* Die Augsburger Zeitung: [Wohnungsnot: Protest gegen steigende Mietpreise](https://www.daz-augsburg.de/wohnungsnot-protest-gegen-steigende-mietpreise/)

### 24.03.2022
* Augsburger Allgemeine: [Globaler Klimastreik an diesem Freitag findet auch in Augsburg statt](https://www.augsburger-allgemeine.de/augsburg/augsburg-globaler-klimastreik-an-diesem-freitag-findet-auch-in-augsburg-statt-id62154066.html)
* Augsburger Allgemeine: [Augsburgs Citymanager kritisiert Stadtregierung wegen des Klimacamps](https://www.augsburger-allgemeine.de/augsburg/augsburg-augsburgs-citymanager-kritisiert-stadtregierung-wegen-klimacamp-id62149256.html)
* Augsburger Allgemeine: [In der Augsburger CSU wächst der Unmut über das Klimacamp](https://www.augsburger-allgemeine.de/augsburg/klimacamp-augsburg-in-der-augsburger-csu-waechst-der-unmut-id62117171.html)

### 23.03.2022
* junge Welt: [»Versammlungsfreiheit gilt nicht nur für Wohlhabende«](https://www.jungewelt.de/artikel/423175.protest-in-der-brd-versammlungsfreiheit-gilt-nicht-nur-f%C3%BCr-wohlhabende.html)
* Augsburger Allgemeine: [In der Augsburger CSU wächst der Unmut über das Klimacamp](https://www.augsburger-allgemeine.de/augsburg/augsburg-in-der-augsburger-csu-waechst-der-unmut-ueber-das-klimacamp-id62117171.html)
* Augsburger Allgemeine: [Streit um Augsburger Klimacamp: Der Führungsanspruch der CSU geht verloren](https://www.augsburger-allgemeine.de/augsburg/kommentar-der-fuehrungsanspruch-der-csu-geht-verloren-id62129936.html)

### 19.03.2022
* Augsburger Allgemeine: [Stadt Augsburg lässt nächste Schritte gegenüber dem Klimacamp noch offen](https://www.augsburger-allgemeine.de/augsburg/augsburg-stadt-augsburg-laesst-naechste-schritte-gegenueber-dem-klimacamp-noch-offen-id62095756.html)

### 17.03.2022
* Augsburger Allgemeine: [Augsburgs Klimacamp ist das älteste in Deutschland – und hat noch viel vor](https://www.augsburger-allgemeine.de/augsburg/augsburg-augsburgs-klimacamp-das-aelteste-in-deutschland-und-hat-noch-viel-vor-id62049381.html)

### 11.03.2022

* Radio Schwaben: [Augsburg: FDP fordert Klärung zu Dauerkundgebungen](https://radioschwaben.de/nachrichten/augsburg-fdp-fordert-klaerung-zu-dauerkundgebungen/)
* Augsburger Allgemeine: [Klimacamp: Sozialfraktion kritisiert die Stadt Augsburg für den Gang vor Gericht](https://www.augsburger-allgemeine.de/augsburg/augsburg-klimacamp-sozialfraktion-kritisiert-die-stadt-augsburg-fuer-den-gang-vor-gericht-id62015701.html)
* Augsburger Allgemeine: [Die Stadt Augsburg hat beim Klimacamp kaum noch Handlungsspielraum](https://www.augsburger-allgemeine.de/augsburg/augsburg-die-stadt-augsburg-hat-beim-klimacamp-kaum-noch-handlungsspielraum-id61997421.html)

### 10.03.2022

* Die Augsburger Zeitung: [Kommentar zum VHG-Urteil: Ein Ausrufezeichen für die Stadt, ein Armutszeugnis für die Stadtregierung](https://www.daz-augsburg.de/88215-2/)
* Die Augsburger Zeitung: [VGH-Entscheidung zum Klimacamp: Soziale Fraktion meldet sich zu Wort](https://www.daz-augsburg.de/vgh-entscheidung-zum-klimacamp/)

Der folgende Beitrag handelt zwar nicht direkt von uns,
aber von der Diskussion um die Studie „Augsburg 2030“,
die wir interessiert verfolgen und an der wir uns beteiligen.

* Augsburg.tv: [Diskussion um Augsburger Studie "Klimaschutz 2030"](https://www.augsburg.tv/mediathek/video/diskussion-um-augsburger-studie-klimaschutz-2030/)

### 09.03.2022

* Augsburger Allgemeine: [Was bedeutet das Gerichtsurteil fürs Augsburger Klimacamp?](https://www.augsburger-allgemeine.de/augsburg/klimacamp-augsburg-was-bedeutet-das-urteil-id61992116.html)

### 08.03.2022

Siehe auch die heutige Pressemitteilung des Klimacamps:
[Räumungsbescheid rechtswidrig – Stadt Augsburg kassiert zum fünften Mal Schlappe vor Gericht gegen Klimacamper\*innen](/pages/Pressemitteilungen/2022-03-08-Raeumungsbescheid-rechtswidrig.html)

* Traunsteiner Tagblatt: [Augsburg scheitert auch in zweiter Instanz gegen Aktivisten](https://www.traunsteiner-tagblatt.de/startseite_artikel,-augsburg-scheitert-auch-in-zweiter-instanz-gegen-aktivisten-_arid,687138.html)
  <br>Vielen Dank an das Traunsteiner Tagblatt dafür,
  dass sie auf diese Webseite hier und andere Quellen linken.
  Wir schätzen es sehr, wenn Medien ihre Quellen verlinken.
* Die Augsburger Zeitung: [Urteil: Stadt blitzt beim VGH ab – Klimacamp darf weiter protestieren](https://www.daz-augsburg.de/urteil-stadt-blitzt-beim-vgh-ab-klimacamp-darf-weiter-protestieren/)
* Süddeutsche Zeitung: [Klimaaktivisten dürfen weiter campen](https://www.sueddeutsche.de/bayern/augsburg-nuernberg-klimacamp-verwaltungsgerichtshof-1.5543572)
* Bayerischer Rundfunk: [Urteil: Räumungsbescheid zu Augsburgs Klimacamp war rechtswidrig](https://www.br.de/nachrichten/bayern/urteil-raeumungsbescheid-zu-augsburgs-klimacamp-war-rechtswidrig,SzU4DMR)
* Augsburger Allgemeine: [Klimacamp-Bescheid war rechtswidrig: Stadt scheitert auch in der Berufung](https://www.augsburger-allgemeine.de/augsburg/augsburg-klimacamp-bescheid-war-rechtswidrig-stadt-scheitert-auch-in-der-berufung-id61982896.html)
* Stadtzeitung: [Augsburger Klimacamp: Stadt scheitert mit Berufung in zweiter Instanz](https://www.staz.de/region/augsburg/lokales/augsburger-klimacamp-stadt-scheitert-berufung-zweiter-instanz-id235647.html)
* augsburg.tv: [a.tv kompakt: Augsburger Klimacamp darf bleiben](https://www.augsburg.tv/mediathek/video/a-tv-kompakt-augsburger-klimacamp-darf-bleiben/)
* Radio Fantasy: [Klimacamp Augsburg: Laut Gericht rechtmäßig, aber...](https://www.fantasy.de/news/klimacamp-augsburg-laut-gericht-rechtmaessig-aber)
* Allgäuer Zeitung: [Gericht bestätigt: Klimacamp in Augsburg darf weiter stehen bleiben](https://www.allgaeuer-zeitung.de/bayern/augsburg-scheitert-auch-in-zweiter-instanz-gegen-klima-aktivisten_arid-391735)
* Radio Schwaben: [Augsburg: Das Klimacamp bleibt](https://radioschwaben.de/nachrichten/augsburg-das-klimacamp-bleibt/)
* Zeit.de: [Augsburg scheitert auch in zweiter Instanz gegen Aktivisten](https://www.zeit.de/news/2022-03/08/augsburg-scheitert-auch-in-zweiter-instanz-gegen-aktivisten?utm_referrer=https%3A%2F%2Fwww.startpage.com%2F)
* FAZ: [Augsburg scheitert auch in zweiter Instanz gegen Aktivisten](https://www.faz.net/agenturmeldungen/dpa/augsburg-scheitert-auch-in-zweiter-instanz-gegen-aktivisten-17860878.html)
* Main-Post: [Augsburg scheitert auch in zweiter Instanz gegen Aktivisten](https://www.mainpost.de/ueberregional/bayern/bayern/augsburg-scheitert-auch-in-zweiter-instanz-gegen-aktivisten-art-10745817)
* Nordbayern.de: [Augsburg scheitert auch in zweiter Instanz gegen Aktivisten ](https://www.nordbayern.de/politik/augsburg-scheitert-auch-in-zweiter-instanz-gegen-aktivisten-1.11906056)
* Passauer Neue Presse: [Augsburg scheitert auch in zweiter Instanz gegen Aktivisten](https://www.pnp.de/nachrichten/bayern/Augsburg-scheitert-auch-in-zweiter-Instanz-gegen-Aktivisten-4254787.html)
* Münchner Merkur: [Augsburg scheitert auch in zweiter Instanz gegen Aktivisten](https://www.merkur.de/bayern/augsburg-scheitert-auch-in-zweiter-instanz-gegen-aktivisten-zr-91395859.html)

Vielen Dank auch an all jene Medien,
wie die Süddeutsche Zeitung, die Augsburger Zeitung,
den Bayerischen Rundfunk, die Augsburger Allgemeine
und die Stadtzeitung,
die nicht nur dpa-Meldungen abdrucken,
sondern noch selbst etwas schreiben.

### 07.03.2022

#### Nach der Verhandlung

* Bayerischer Rundfunk: [Augsburger Klimacamp muss wohl nicht geräumt werden](https://www.br.de/nachrichten/bayern/augsburger-klimacamp-muss-wohl-nicht-geraeumt-werden,SzOeZRg)
* Süddeutsche Zeitung: [Zelten in der Stadt](https://www.sueddeutsche.de/bayern/augsburg-klimacamp-verwaltungsgericht-1.5543047)
* Augsburger Allgemeine: [Klimacamp-Streit: Vor Gericht zeichnet sich Schlappe für Augsburg ab](https://www.augsburger-allgemeine.de/augsburg/augsburg-klimacamp-streit-vor-gericht-zeichnet-sich-schlappe-fuer-augsburg-ab-id61720111.html)
* Schwäbische Zeitung: [Richter wollen über Zulässigkeit von Dauerdemo entscheiden](https://www.schwaebische.de/sueden/bayern_artikel,-vgh-verhandelt-ueber-zulaessigkeit-von-augsburger-klimacamp-_arid,11480647.html)
* Passauer Neue Presse: [Richter wollen über Zulässigkeit von Dauerdemo entscheiden](https://www.pnp.de/nachrichten/bayern/Richter-wollen-ueber-Zulaessigkeit-von-Dauerdemo-entscheiden-4253739.html)
* idowa (isar • donau • wald): [Richter wollen über Zulässigkeit von Dauerdemo entscheiden](https://www.idowa.de/inhalt.umwelt-vgh-verhandelt-ueber-zulaessigkeit-von-augsburger-klimacamp.bc508ce8-ec93-4b3e-9604-26a95200c65f.html)

#### Vor der Verhandlung

* Bayerischer Rundfunk: [Gericht urteilt über Klimacamp: Ist der Dauer-Protest rechtens?](https://www.br.de/nachrichten/bayern/gericht-urteilt-ueber-klimacamp-ist-der-dauer-protest-rechtens,Syvc6kR)
* RTL: [Richter entscheiden über Zulässigkeit von Klima-Dauerdemo](https://www.rtl.de/cms/richter-entscheiden-ueber-zulaessigkeit-von-klima-dauerdemo-4930903.html)
* Süddeutsche Zeitung: [Richter entscheiden über Zulässigkeit von Klima-Dauerdemo](https://www.sueddeutsche.de/bayern/demonstrationen-muenchen-richter-entscheiden-ueber-zulaessigkeit-von-klima-dauerdemo-dpa.urn-newsml-dpa-com-20090101-220306-99-410267)
* Hallo Augsburg: [Gericht prüft erneut: Darf das Augsburger Klimacamp bleiben?](https://www.hallo-augsburg.de/klimacamp-augsburg-kommt-weg-gericht-prueft-erneut-darf-das-augsburger-klimacamp-bleiben_tDu)
* TAG24: [Streit um Augsburger Klima-Camp: Richter entscheiden über Zulässigkeit](https://www.tag24.de/nachrichten/politik/fridays-for-future/streit-um-augsburger-klima-camp-richter-entscheiden-ueber-zulaessigkeit-2359753)
* Münchner Merkur: [Richter entscheiden über Zulässigkeit von Klima-Dauerdemo](https://www.merkur.de/bayern/richter-entscheiden-ueber-zulaessigkeit-von-klima-dauerdemo-zr-91392479.html)
* Radio Schwaben: [Augsburg: VGH verhandelt über Zulässigkeit von Klimacamp](https://radioschwaben.de/nachrichten/augsburg-vgh-verhandelt-ueber-zulaessigkeit-von-klimacamp/)
* Nordbayern.de: [Richter entscheiden über Zulässigkeit von Klima-Dauerdemo](https://www.nordbayern.de/politik/richter-entscheiden-uber-zulassigkeit-von-klima-dauerdemo-1.11900943?logout_success=true)
* hitradio rt1: [Augsburger Klimacamp erneut Fall fürs Gericht](https://www.imsueden.de/hitradio-rt1/augsburger-klimacamp-erneut-fall-fuers-gericht-163239/)
* brennessel magazin: [Verwaltungsgerichtshof befasst sich mit Augsburger Klimacamp](https://www.brennessel.com/verwaltungsgerichtshof-befasst-sich-mit-augsburger-klimacamp/)
* Ingolstadt Today: [Klimaschutz-Camp in Augsburg](https://www.ingolstadt-today.de/news/klimaschutz-camp-in-augsburg-4465936)
* Bayerischer Rundfunk: [Abendschau: Der Süden – Prozess um Klimacamp Augsburg](https://www.ardmediathek.de/video/Y3JpZDovL2JyLmRlL3ZpZGVvL2U5Y2JlNDU0LTViNjQtNGY3My1hMTJlLTkwNjY5Mjg0NDNlMw)

Heute sind ziemlich viele Kopien der selben DPA-Meldung darunter. 🤣

### 06.03.2022
* Nordbayern.de: [Dauerprotest rechtens? Richter entscheiden über bayerisches Klima-Camp](https://www.nordbayern.de/region/dauerprotest-rechtens-richter-entscheiden-uber-bayerisches-klima-camp-1.11898860)
* Nachrichten Pforzheim: [Dauerprotest legal? Richter entscheiden über bayerische Klimacamp – Region](https://nachrichten-pforzheim.de/dauerprotest-legal-richter-entscheiden-ueber-bayerische-klimacamp-region/)

### 05.03.2022
* Allgäuer Zeitung: [Richter entscheiden über Zulässigkeit von Klima-Dauerdemo](https://www.allgaeuer-zeitung.de/bayern/richter-entscheiden-ueber-zulaessigkeit-von-klima-dauerdemo_arid-390566)

### 04.03.2022
* Augsburger Allgemeine: [Entscheidung fällt vor Gericht: Muss das Klimacamp verschwinden?](https://www.augsburger-allgemeine.de/augsburg/klimacamp-augsburg-vor-dem-aus-gericht-entscheidet-id61924561.html)

### 03.03.2022
* Augsburger Allgemeine: [Das Klimacamp in Augsburg steht vor einer ungewissen Zukunft](https://www.augsburger-allgemeine.de/augsburg/kommentar-das-klimacamp-in-augsburg-steht-vor-einer-ungewissen-zukunft-id61946081.html)

### 29.01.2022
 * Augsburger Allgemeine: [Bewundert und beschimpft: Wie lange hält das Klimacamp in Augsburg noch durch?](https://www.augsburger-allgemeine.de/augsburg/debatte-bewundert-und-beschimpft-wie-lange-haelt-das-klimacamp-in-augsburg-noch-durch-id61582291.html)
   <br>
   **Klimacamp:**
   Die [Antwort auf die in der Überschrift gestellte Frage](/pages/Pressemitteilungen/2022-01-29-Durchhalten.html)
   findet sich in unseren [Pressemitteilungen](/pressemitteilungen/).
   <br>
   **Persönliche Anmerkung:**
   *Beim Lesen des Abschnitts zu den angeblich
   das Klimacamp unterstützenden „Kommunikationsstrategen“
   brach ein Kenner der internen Prozesse
   der Öffentlichkeitsarbeit des Klimacamps
   in so heftiges minutenlanges Lachen aus,
   dass ihm anschließend das Zwerchfell weh tat.*

### 28.01.2022
* Hallo Augsburg: [Klimakonferenz Augsburg 2022 - Das erhoffen sich die AktivistInnen](https://www.hallo-augsburg.de/augsburg-klimakonferenz-klimakonferenz-augsburg-2022-das-erhoffen-sich-die-aktivistinnen_Z9s)

### 24.01.2022
* hitradio rt1: [Mehrere Protest-Aktionen in Augsburg](https://www.imsueden.de/hitradio-rt1/mehrere-protest-aktionen-in-augsburg-151456/)

### 23.01.2022
* Bayerischer Rundfunk: [Radldemo statt Abseilaktion über A8 und B17: Klimaschützer klagt](https://www.br.de/nachrichten/bayern/radldemo-statt-abseilaktion-ueber-a8-und-b17-klimaschuetzer-klagt,SvLXIkk)
* Augsburger Allgemeine: [Statt Abseilaktion: Radler demonstrieren auf der Augsburger Schleifenstraße](https://www.augsburger-allgemeine.de/augsburg/augsburg-statt-abseilaktion-radler-demonstrieren-auf-der-augsburger-schleifenstrasse-id61569151.html)

### 22.01.2022
* Augsburger Allgemeine: [Innenstadt und Schleifenstraße: Raddemo ist auf neuer Route unterwegs](https://www.augsburger-allgemeine.de/augsburg/augsburg-am-sonntag-raddemo-durch-innenstadt-und-ueber-schleifenstrasse-id61565091.html)

### 21.01.2022
* Hallo Augsburg: [Klimacamp Augsburg: So gefällt es den AktivistInnen am Moritzplatz](https://www.hallo-augsburg.de/klimacamp-augsburg-moritzplatz-2022-klimacamp-augsburg-so-gefaellt-es-den-aktivistinnen-am-moritzplatz_4vs)
* Stadtzeitung: [Augsburger "Fridays-for-Future"-Bewegung feiert dreijähriges Bestehen](https://www.staz.de/region/augsburg/politik/augsburger-fridays-future-bewegung-feiert-dreijaehriges-bestehen-id234022.html)
* augsburg.tv: [Kritik am Augsburger Beteiligungsmanagement: deswegen will die Stadt an Bayerngas festhalten](https://www.augsburg.tv/mediathek/video/kritik-am-augsburger-beteiligungsmanagement-deswegen-will-die-stadt-an-bayerngas-festhalten/)

### 12.01.2022
* Augsburger Allgemeine: [Hier wehren sich Bürger gegen den Staat: Verwaltungsgericht in Augsburg wird 75](https://www.augsburger-allgemeine.de/augsburg/justiz-in-augsburg-hier-wehren-sich-buerger-gegen-den-staat-verwaltungsgericht-in-augsburg-wird-75-id61472451.html)

### 08.01.2022
* augsburg.tv: [Zeit zu Reden - Klimacamp Augsburg](https://www.augsburg.tv/mediathek/video/zeit-zu-reden-klimacamp-augsburg/)

### 04.01.2022
* augsburg.tv: [18 Monate Klimacamp in Augsburg: Aktivisten im a.tv Talk](https://www.augsburg.tv/mediathek/video/18-monate-klimacamp-in-augsburg-aktivisten-im-a-tv-talk/)


## 2021

### 22.12.2021
* Die Augsburger Zeitung: [Stadt dementiert „Auflösung des Klimacamps“](https://www.daz-augsburg.de/rubrik/politik/umwelt/)

### 21.12.2021
* Radio Schwaben: [Augsburg: Verwirrung um vermeintliche Klimacamp-Schliessung](https://radioschwaben.de/nachrichten/augsburg-verwirrung-um-vermeintliche-klimacamp-schliessung/)

### 20.12.2021
* presse augsburg: [„Auch das Klimacamp wird regelmäßig kontrolliert“ – Augsburgs Ordnungsreferent widerspricht Auflösungsmeldung](https://presse-augsburg.de/auch-das-klimacamp-wird-regelmaessig-kontrolliert-augsburgs-ordnungsreferent-widerspricht-aufloesungsmeldung/767149/)

### 12.11.2021
* Süddeutsche Zeitung: [Klimacamps – Schandfleck oder berechtigter Protest?](https://www.sueddeutsche.de/bayern/augsburg-nuernberg-klimacamps-protest-aktivisten-1.5463074)

### 13.10.2021
* Augsburger Allgemeine: [Klimaaktivisten gehen mit Stadt Augsburg auf Konfrontationskurs](https://www.augsburger-allgemeine.de/augsburg/Klimaaktivisten-gehen-mit-Stadt-Augsburg-auf-Konfrontationskurs-id60770266.html)
* Augsburger Allgemeine: [Augsburger Stadtregierung wurde bei der Bürgerversammlung vorgeführt](https://www.augsburger-allgemeine.de/augsburg/Augsburg-Augsburger-Stadtregierung-wurde-bei-der-Buergerversammlung-vorgefuehrt-id60771886.html)

### 18.08.2021
* Augsburger Allgemeine: [Aktivisten greifen mit Banner Nüßlein und CSU an: Was steckt dahinter?](https://www.augsburger-allgemeine.de/krumbach/Landkreis-Guenzburg-Aktivisten-greifen-mit-Banner-Nuesslein-und-CSU-an-Was-steckt-dahinter-id60323906.html)

### 29.07.2021
*  katholisch1.tv: [Bischof Meier trifft Klimacamp Aktivisten](https://www.katholisch1.tv/Videos/Bischof_Meier_trifft_Klimacamp_Aktivisten_Donnerstag_29._Juli_2021_14_12_00)

### 22.07.2021
* Forum solidarisches und friedliches Augsburg: [Für die Lechstahlwerke soll geschützter Bannwald gerodet werden](https://www.forumaugsburg.de/s_5region/Bezirk/210722_kampf-gegen-die-rodung-des-lohwalds-durch-die-lechstahlwerke-meitingen/index.htm)

### 14.07.2021
* Augsburger Allgemeine: [Augsburg muss den Verkehr neu regeln, um die Klimaziele zu erreichen](https://augsburger-allgemeine.de/augsburg/Augsburg-Augsburg-muss-den-Verkehr-neu-regeln-um-die-Klimaziele-zu-erreichen-id60075841.html)

### 01.07.2021
* 3sat: [Ein Jahr im Klimacamp Augsburg](https://www.3sat.de/wissen/nano/210701-klimacamp-nano-102.html)
* ARD: [Grüner Wasserstoff aus dem Donaumoos \| Filmfest München \| Klima Camp Augsburg](https://www.ardmediathek.de/video/Y3JpZDovL2JyLmRlL3ZpZGVvL2RiYzI4ZmRlLWUzNDMtNDMyZS1hZGIxLTMyZGMxNTJlYjVmNw)

### 23.06.2021
* hitradio rt1: [Rechtsstreit um Klimacamp geht in die nächste Runde](https://www.imsueden.de/hitradio-rt1/rechtsstreit-um-klimacamp-geht-in-die-naechste-runde-106840/)

### 16.06.2021
* Augsburger Allgemeine: [Klimacamp-Aktivisten kritisieren den Feuerwehreinsatz am Rathaus](https://www.augsburger-allgemeine.de/augsburg/Augsburg-Klimacamp-Aktivisten-kritisieren-den-Feuerwehreinsatz-am-Rathaus-id59895081.html)
* Forum solidarisches und friedliches Augsburg: [Vergeblich versucht die CSU eine Fahrrad-Demo auf der B 17 zu verhindern](https://forumaugsburg.de/s_2kommunal/Kommunalreform/210616_fahrraddemo-fuer-mobilitaetswende-auf-b17/index.htm)

### 27.05.2021
* Süddeutsche Zeitung: [Protest gegen GNTM bei ProSieben in Unterföhring](https://www.sueddeutsche.de/muenchen/landkreismuenchen/gntm-protest-prosieben-unterfoehring-1.5306013)
* Promiflash: [Kurz vor GNTM-Finale: Aktivistinnen demonstrieren halb nackt](https://www.promiflash.de/news/2021/05/27/kurz-vor-gntm-finale-aktivistinnen-demonstrieren-halb-nackt.html)
* Münchner Merkur: [„Germany‘s next Topmodel“-Finale: Protest vor ProSieben-Zentrale - Sender versteht Kritik nicht](https://www.merkur.de/lokales/muenchen-lk/unterfoehring/germany-next-topmodel-finale-prosieben-muenchen-unterfoehring-protest-kritik-demo-frauen-zr-90682436.amp.html)
* BILD: [Aktivistinnen gegen TV-Show: Nackt-Protest gegen Germany‘s Next Topmodel](https://m.bild.de/regional/muenchen/qualityassurancetest/aktivistinnen-gegen-tv-show-nackt-protest-gegen-germanys-next-topmodel-76541540.bildMobile.html)
* Allgäuer Zeitung: [Germany's Next Topmodel": Protest von halbnackten Frauen vor ProSieben-Zentrale](https://www.allgaeuer-zeitung.de/welt/panorama/germanys-next-topmodel-protest-von-halbnackten-frauen-vor-prosieben-zentrale_arid-296503)
* TZ: [Vor GNTM-Finale: Aktivistinnen demonstrieren vor ProSieben-Zentrale](https://www.tz.de/tv/gntm-2021-prosieben-finale-aktivistinnen-demonstration-vor-prosieben-zentrale-90682449.html)
* Stuttgarter Zeitung: [Halbnackte Frauen demonstrieren gegen „Germany’s Next Topmodel“](https://www.stuttgarter-nachrichten.de/inhalt.kurz-vor-dem-finale-halbnackte-frauen-demonstrieren-gegen-germany-s-next-topmodel.e5f0e35a-7501-4d9a-8df9-ff1cdc12dff4.html)
* Tagesspiegel: [Halbnackte Frauen demonstrieren gegen „Germany's Next Topmodel“](https://m.tagesspiegel.de/gesellschaft/medien/vor-finale-der-modelshow-halbnackte-frauen-demonstrieren-gegen-germanys-next-topmodel/27234214.html)

### 15.05.2021
* Augsburger Allgemeine: [Parken in Augsburg: Steht die Semmeltaste vor dem Aus?](https://www.augsburger-allgemeine.de/augsburg/Augsburg-Parken-in-Augsburg-Steht-die-Semmeltaste-vor-dem-Aus-id59688571.html)

### 13.05.2021
* Augsburger Allgemeine: [Meitinger Klima-Aktivisten bleiben diesmal mit beiden Beinen am Boden](https://www.augsburger-allgemeine.de/augsburg-land/Meitingen-Meitinger-Klima-Aktivisten-bleiben-diesmal-mit-beiden-Beinen-am-Boden-id59668431.html)
* Augsburger Allgemeine: [Aus für mögliche Windräder bei Inningen?](https://www.augsburger-allgemeine.de/augsburg/Augsburg-Aus-fuer-moegliche-Windraeder-bei-Inningen-id59678651.html)
* Forum solidarisches und friedliches Augsburg: [Augsburger Ostermarsch, Teil 2](https://www.forumaugsburg.de/s_3themen/Antimil/210512_ostermarsch-2021_2/index.htm)
* Augsburg in Bürgerhand: [Klimaschutzmaßnahmen der Stadtregierung: Ein einziger Flickenteppich ohne Zusammenhang – Erreichen der Klimaschutzziele ist nicht in Sicht](https://www.augsburg-in-buergerhand.de/?p=2642)

### 12.05.2021
* Bayerischer Rundfunk: [Augsburgs Stadtrat diskutiert Klimaschutzprojekte](https://www.br.de/nachrichten/bayern/augsburgs-stadtrat-diskutiert-klimaschutzprojekte,SXCVT3P)

### 11.05.2021
* Augsburger Allgemeine: [Klimaaktivisten blockieren Gögginger Straße vor Stadtratssitzung in Augsburg](https://www.augsburger-allgemeine.de/augsburg/Augsburg-Klimaaktivisten-blockieren-Goegginger-Strasse-vor-Stadtratssitzung-in-Augsburg-id59670526.html)
* Augsburger Allgemeine: [Augsburg muss für die Klimawende den Verkehr massiv umkrempeln](https://www.augsburger-allgemeine.de/augsburg/Augsburg-Augsburg-muss-fuer-die-Klimawende-den-Verkehr-massiv-umkrempeln-id59673876.html)

### 10.05.2021
* Augsburger Allgemeine: [Klimaaktivisten verschärfen ihren Protest](https://www.augsburger-allgemeine.de/augsburg/Augsburg-Klimaaktivisten-verschaerfen-ihren-Protest-id59661851.html)

### 08.05.2021
* Augsburger Allgemeine: [Kommentar: Kann die Stadt Augsburg ihre eigenen Klimaschutz-Vorgaben einhalten?](https://www.augsburger-allgemeine.de/augsburg/Debatte-Kann-die-Stadt-Augsburg-ihre-eigenen-Klimaschutz-Vorgaben-einhalten-id59648996.html)
* augsburg.tv: [Augsburger Sonder-Stadtrat für mehr Klimaschutz](https://www.augsburg.tv/mediathek/video/augsburger-sonder-stadtrat-fuer-mehr-klimaschutz/)

### 03.05.2021
* Radio Augsburg: [Klima-Aktivisten setzen auf Schaukeln](https://radioaugsburg.de/klima-aktivisten-setzen-auf-schaukeln/)

### 24.04.2021
* Augsburger Allgemeine: [Meitinger Aktivisten wollen nicht mit Mehring ü?ber Lohwald reden](https://www.augsburger-allgemeine.de/augsburg-land/Meitinger-Aktivisten-wollen-nicht-mit-Mehring-ueber-Lohwald-reden-id59553261.html)

### 16.04.2021
* Augsburger Allgemeine: [Analyse von Wetterdaten: So zeigt sich in Augsburg schon jetzt der Klimawandel](https://www.augsburger-allgemeine.de/augsburg/Analyse-von-Wetterdaten-So-zeigt-sich-in-Augsburg-schon-jetzt-der-Klimawandel-id59499281.html)
* Augsburger Allgemeine: [Die strengen Klimaziele sind richtig](https://www.augsburger-allgemeine.de/augsburg/Die-strengen-Klimaziele-der-Stadt-Augsburg-sind-richtig-id59504461.html)

### 15.04.2021
* Augsburger Allgemeine: [Warum Klima-Aktivisten Fabian Mehring ein Hochbeet schenken](https://www.augsburger-allgemeine.de/augsburg-land/Warum-Klima-Aktivisten-Fabian-Mehring-ein-Hochbeet-schenken-id59492496.html)

### 13.04.2021
* Augsburger Allgemeine: [So bereiten sich die Klima-Aktivisten auf Aktionen im Meitinger Lohwald vor](https://www.augsburger-allgemeine.de/augsburg-land/So-bereiten-sich-die-Klima-Aktivisten-auf-Aktionen-im-Meitinger-Lohwald-vor-id59477346.html)

### 11.04.2021
* Augsburger Allgemeine: [Aktivisten setzen erneut Zeichen gegen Lohwald-Rodung](https://www.augsburger-allgemeine.de/augsburg-land/Aktivisten-setzen-erneut-Zeichen-gegen-Lohwald-Rodung-id59472581.html)

### 10.04.2021
* Forum solidarisches und friedliches Augsburg: [700 beim Globalen Klimastreik auf dem Plärrer](https://www.forumaugsburg.de/s_2kommunal/Kommunalreform/210410_globaler-klimastreik-fridays-for-future-auf-dem-plaerrer/index.htm)

### 09.04.2021
* Augsburger Allgemeine: [Klima-Aktivisten hängen Banner zum Schutz des Lohwalds auf](https://www.augsburger-allgemeine.de/augsburg-land/Klima-Aktivisten-haengen-Banner-zum-Schutz-des-Lohwalds-auf-id59455471-amp.html)
* Augsburger Allgemeine: [Proteste wie im Hambacher Forst? "Wenn es sein muss, besetzen wir den Wald"](https://www.augsburger-allgemeine.de/augsburg-land/Proteste-wie-im-Hambacher-Forst-Wenn-es-sein-muss-besetzen-wir-den-Wald-id59462481.html)

### 28.03.2021
* Augsburger Allgemeine: [Umwelt & Co.: Menschen protestieren in Augsburg gegen Missstände](https://www.augsburger-allgemeine.de/augsburg/Umwelt-Co-Menschen-protestieren-in-Augsburg-gegen-Missstaende-id59395086.html)

### 27.03.2021
* Augsburger Allgemeine: [Nicht nur wegen Corona: Zahl der Demos in Augsburg schießt nach oben](https://www.augsburger-allgemeine.de/augsburg/Nicht-nur-wegen-Corona-Zahl-der-Demos-in-Augsburg-schiesst-nach-oben-id59361536.html)

### 23.03.2021
* Augsburger Allgemeine: [Stadtwerke Augsburg wollen Haushalte nur noch mit Ökostrom beliefern](https://www.augsburger-allgemeine.de/augsburg/Stadtwerke-Augsburg-wollen-Haushalte-nur-noch-mit-Oekostrom-beliefern-id59358186.html)

### 19.03.2021
* Augsburger Allgemeine: [Fridays for Future: Rund 500 Menschen demonstrieren in Augsburg für das Klima](https://www.augsburger-allgemeine.de/augsburg/Fridays-for-Future-Rund-500-Menschen-demonstrieren-in-Augsburg-fuer-das-Klima-id59345631.html)
* Augsburger Allgemeine: [Augsburger Aktivisten von Fridays for Future demonstrieren am Plärrer](https://www.augsburger-allgemeine.de/augsburg/Augsburger-Aktivisten-von-Fridays-for-Future-demonstrieren-am-Plaerrer-id59341776.html)

### 13.03.2021
* Salzburger Nachrichten: [Campen fürs Klima: Wie eine Stadt in Bayern den Klimaprotest weiterführt](https://www.sn.at/panorama/klimawandel/campen-fuers-klima-wie-eine-stadt-in-bayern-den-klimaprotest-weiterfuehrt-100891069)
* Augsburger Allgemeine: [Klimacamp-Aktivisten protestieren gegen Abriss der "Diesel-Villa"](https://www.augsburger-allgemeine.de/augsburg/Klimacamp-Aktivisten-protestieren-gegen-Abriss-der-Diesel-Villa-id59301026.html)

### 10.03.2021
* Augsburger Allgemeine: [Augsburg will mehr Platz für Radler schaffen -- und damit weniger für Autos](https://www.augsburger-allgemeine.de/augsburg/Augsburg-will-mehr-Platz-fuer-Radler-schaffen-und-damit-weniger-fuer-Autos-id59271501.html)
* Augsburger Allgemeine: [Die Fahrradstadt Augsburg kommt nur langsam ins Rollen](https://www.augsburger-allgemeine.de/augsburg/Die-Fahrradstadt-Augsburg-kommt-nur-langsam-ins-Rollen-id59272996.html)

### 08.03.2021
* augsburg.tv: [Reportage: Ein Tag im Klimacamp](https://www.augsburg.tv/mediathek/video/reportage-ein-tag-im-klimacamp/)

### 05.03.2021
* Augsburger Allgemeine: [Tarifreform im AVV: Wo bleibt die Sicht der Fahrgäste?](https://www.augsburger-allgemeine.de/augsburg/Tarifreform-im-AVV-Wo-bleibt-die-Sicht-der-Fahrgaeste-id59244101.html)

### 03.03.2021
* Augsburger Allgemeine: [Gibt es beim Fahrrad-Bürgerbegehren jetzt eine Einigung mit der Stadt?](https://www.augsburger-allgemeine.de/augsburg/Gibt-es-beim-Fahrrad-Buergerbegehren-jetzt-eine-Einigung-mit-der-Stadt-id59225031.html)
* Augsburger Allgemeine: [Fahrrad-Bürgerbegehren: Verhandlungspartner stehen unter Druck](https://www.augsburger-allgemeine.de/augsburg/Fahrrad-Buergerbegehren-Verhandlungspartner-stehen-unter-Druck-id59225821.html)

### 28.02.2021
* Bayerischer Rundfunk: [Bannwaldrodung für Lech-Stahlwerke: Umweltschützer demonstrieren](https://www.br.de/nachrichten/bayern/bannwaldrodung-fuer-lech-stahlwerke-umweltschuetzer-demonstrieren,SQK6YTp)
* Augsburger Allgemeine: [Hunderte protestieren lautstark für den Erhalt des Lohwalds bei Meitingen](https://www.augsburger-allgemeine.de/augsburg-land/Hunderte-protestieren-lautstark-fuer-den-Erhalt-des-Lohwalds-bei-Meitingen-id59211951.html)
* Augsburger Allgemeine: [Ein deutliches Zeichen für den Erhalt des Lohwalds](https://www.augsburger-allgemeine.de/augsburg-land/Ein-deutliches-Zeichen-fuer-den-Erhalt-des-Lohwalds-id59212741.html)
* Presse Augsburg: [600 Teilnehmer demonstrieren in Meitingen gegen Rodung des Bannwaldes](https://presse-augsburg.de/600-teilnehmer-demonstrieren-in-meitingen-gegen-rodung-des-bannwaldes/701998/)

### 26.02.2021
* Augsburger Allgemeine: [Augsburger Stadtrat plant Sondersitzung zum Thema Klima](https://www.augsburger-allgemeine.de/augsburg/Augsburger-Stadtrat-plant-Sondersitzung-zum-Thema-Klima-id59195866.html)

### 23.02.2021
* Augsburger Allgemeine: [Demonstration für den Erhalt des Bannwalds bei Meitingen](https://www.augsburger-allgemeine.de/augsburg-land/Demonstration-fuer-den-Erhalt-des-Bannwalds-bei-Meitingen-id59180566.html)

### 21.02.2021
* Neue Sonntagspresse: [Durchhalten im Klima-Camp -- egal wie das Wetter ist](https://neuesonntagspresse.de/images/ausgaben/2021/21-02-21.pdf#page=3)

### 13.02.2021
* Augsburger Allgemeine: Wie viel Kommunikation kann sich die Stadt leisten?

### 10.02.2021
* ZDF logo: [Augsburg: Jugendliche campen für's Klima](https://www.zdf.de/kinder/logo/fridays-for-future-corona-camp-100.html)

### 07.02.2021
* BR Schwaben & Altbayern: [Erfolgreicher Dauerprotest -- Das Klimacamp Augsburg](https://www.br.de/mediathek/video/erfolgreicher-dauerprotest-das-klimacamp-augsburg-av:601ef0efdaef33001a2d2d31)

### 05.02.2021
* Augsburger Allgemeine: [Klimaaktivisten wollen mit spektakulärer Aktion Rodung im Stadtwald verhindern](https://www.augsburger-allgemeine.de/augsburg/Klimaaktivisten-wollen-mit-spektakulaerer-Aktion-Rodung-im-Stadtwald-verhindern-id59048881.html)
* Augsburger Allgemeine: [Klimaaktivisten warnen im Augsburger Stadtwald vor Rodungen](https://www.youtube.com/watch?v=n1aOVcHDuXA) (Video auf YouTube)

### 03.02.2021
* Augsburger Allgemeine: Auf dem Gersthofer Festplatz kehrt Ruhe ein

### 01.02.2021
* Augsburger Allgemeine: [Klima-Aktivisten kämpfen um Bäume am Gersthofer Festplatz](https://www.augsburger-allgemeine.de/augsburg-land/Klima-Aktivisten-kaempfen-um-Baeume-am-Gersthofer-Festplatz-id59011896.html)
* Augsburger Allgemeine: [Klima-Aktivisten können Baumfällung in Gersthofen nur aufschieben](https://www.augsburger-allgemeine.de/augsburg-land/Klima-Akivisten-koennen-Baumfaellung-in-Gersthofen-nur-aufschieben-id59016136.html)
* augsburg.tv: [a.tv Aktuell](https://www.augsburg.tv/mediathek/video/a-tv-aktuell-vom-1-02-2021/) (Zeitstempel 12:36)

### 30.01.2021
- Augsburger Allgemeine: [Autoverkehr: Grenzen des Wachstums in Augsburg sind erreicht](https://www.augsburger-allgemeine.de/augsburg/Autoverkehr-Grenzen-des-Wachstums-in-Augsburg-sind-erreicht-id59002156.html)

### 29.01.2021
- ARD Tagesthemen: [Klimacamp in Augsburg](https://youtu.be/94LAR0I_BSY?t=810)

### 26.01.2021
- Augsburger Allgemeine: [CO2-Ausstoß: Klimacamp begrüßt Klimabeschluss der Stadt Augsburg](https://augsburger-allgemeine.de/augsburg/CO2-Ausstoss-Klimacamp-begruesst-Klimabeschluss-der-Stadt-Augsburg-id58976001.html)

### 22.01.2021
- Augsburger Allgemeine: [Augsburgs Umweltreferent Reiner Erben kontert Kritik der Klimaaktivisten](https://www.augsburger-allgemeine.de/augsburg/Augsburgs-Umweltreferent-Reiner-Erben-kontert-Kritik-der-Klimaaktivisten-id58951851.html)
- augsburg.tv: [Augsburgs Umweltreferent wehrt sich gegen Vorwürfe](https://www.augsburg.tv/mediathek/video/augsburgs-umweltreferent-wehrt-sich-gegen-vorwuerfe/)

### 21.01.2021
- Augsburger Allgemeine: [Klimaschutz: Augsburg will mit neuen Maßnahmen mehr CO2 sparen](https://www.augsburger-allgemeine.de/augsburg/Klimaschutz-Augsburg-will-mit-neuen-Massnahmen-mehr-CO2-sparen-id58944391.html)
- Augsburger Allgemeine: [CO2-Einsparungen sind in Augsburg überfällig](https://www.augsburger-allgemeine.de/augsburg/CO2-Einsparungen-sind-in-Augsburg-ueberfaellig-id58946001.html)

### 10.01.2021
- Mandara Music: [Mandàra - Aurora \| for Klimacamp Augsburg (Fridays For Future)](https://www.youtube.com/watch?v=gqSHNj-Wvf4)
  <br>
  Siehe auch Tag 22 in unserem [Adventskalender 2020](/videolog/).

### 02.01.2021
- Augsburger Allgemeine: [Augsburger Klimacamp will Protest ausweiten: "Sprösslingscamps" geplant](https://www.augsburger-allgemeine.de/augsburg/Augsburger-Klimacamp-will-Protest-ausweiten-Sproesslingscamps-geplant-id58838906.html)
- Bayerischer Rundfunk: [Bilanz nach einem halben Jahr](https://www.br.de/mediathek/video/klimacamp-augsburg-bilanz-nach-einem-halben-jahr-av:5ff0918adf6f39001a195b4c)
- Bayerischer Rundfunk: [Augsburger Klimacamp will Druck auf Politik erhöhen](https://www.br.de/nachrichten/bayern/augsburger-klimacamp-will-druck-auf-politik-erhoehen,SKqE68b)


## 2020

### 31.12.
- Augsburger Allgemeine: [Silvester 2020: Eine stille Nacht zum Jahreswechsel in Augsburg](https://m.augsburger-allgemeine.de/augsburg/Silvester-2020-Eine-stille-Nacht-zum-Jahreswechsel-in-Augsburg-id58834246.html)

### 18.12.
- Augsburger Allgemeine: [Weniger CO2-Ausstoß: Augsburg will Druck beim Klimaschutz machen](https://augsburger-allgemeine.de/augsburg/Weniger-CO2-Ausstoss-Augsburg-will-Druck-beim-Klimaschutz-machen-id58754701.html)

### 17.12.
- Bayerischer Rundfunk: [Augsburger Stadtrat beschließt Klimaschutz-Sofortprogramm](https://www.br.de/nachrichten/bayern/klimaschutz-sofortprogramm-fuer-augsburg-stadtrat-stimmt-ab,SJPwNR2)

### 16.12.
- Allgäuer Zeitung: [Zoff ums Klimacamp vor dem Rathaus - Augsburg klagt gegen junge Klimaschützer](https://www.allgaeuer-zeitung.de/bayern/streit-der-stadt-augsburg-mit-klimasch%C3%BCtzern-eskaliert_arid-255652)
- TAG24: ["Das ist kein Protest, das ist Provokation": Klimacamp von "Fridays for Future" sorgt für Wirbel](https://www.tag24.de/nachrichten/politik/fridays-for-future/klimacamp-von-fridays-for-future-vor-dem-rathaus-sorgt-fuer-wirbel-in-augsburg-1763320)
- GMX.de: [Augsburg und Klimaschützer bleiben auf Konfrontationskurs](https://www.gmx.net/magazine/regio/bayern/augsburg-klimaschuetzer-konfrontationskurs-35358310)

### 15.12.
- Augsburger Allgemeine: [Klimacampern reicht das Beschlusspaket der Stadt nicht](https://www.augsburger-allgemeine.de/augsburg/Klimacampern-reicht-das-Beschlusspaket-der-Stadt-nicht-id58734216.html)
- Bayerischer Rundfunk: [Rückblick - Das Jahr 2020 in Schwaben](https://www.br.de/nachrichten/bayern/rueckblick-das-jahr-2020-in-schwaben,SIg2ifq)

### 14.12.
- Augsburger Allgemeine: [Stadt Augsburg will sich beim Klimaschutz mehr Druck machen](https://augsburger-allgemeine.de/augsburg/Stadt-Augsburg-will-sich-beim-Klimaschutz-mehr-Druck-machen-id58721061.html)
- Augsburger Allgemeine: [Was sagen die Klimacamper zum Klimaschutz-Paket der Stadt?](https://www.augsburger-allgemeine.de/augsburg/Was-sagen-die-Klimacamper-zum-Klimaschutz-Paket-der-Stadt-id58730491.html)

### 13.12.
- Augsburger Allgemeine: [Debatte: Die Stadt Augsburg muss beim Klimaschutz Farbe bekennen](https://www.augsburger-allgemeine.de/augsburg/Debatte-Die-Stadt-Augsburg-muss-beim-Klimaschutz-Farbe-bekennen-id58710141.html)

### 12.12.
- stayfm: [doppelstunde klima -- miri und die schweigende mehrheit](https://hearthis.at/stayfm/doppelstunde-klima-vom-12-12-20/)

### 10.12.
- Augsburger Allgemeine: [Klimacamp-Aktivisten bereiten sich auf Winter am Rathaus vor](https://augsburger-allgemeine.de/augsburg/Klimacamp-Aktivisten-bereiten-sich-auf-Winter-am-Rathaus-vor-id58696186.html)
- Augsburger Allgemeine: [Klimacamp: Hut ab vor dem Engagement](https://augsburger-allgemeine.de/augsburg/Klimacamp-Hut-ab-vor-dem-Engagement-id58699526.html)
- Augsburger Allgemeine: [Kreide-Schriftzug am Rathausplatz löst umstrittenen Polizeieinsatz aus](https://www.augsburger-allgemeine.de/augsburg/Kreide-Schriftzug-am-Rathausplatz-loest-umstrittenen-Polizeieinsatz-aus-id58705391.html)
- Die Augsburger Zeitung: [Kommentar zur Catcalls-of-Augsburg-Aktion: Agiert die Augsburger Polizei politisch?](https://www.daz-augsburg.de/kommentar-zur-catcall-of-augsburg-aktion-agiert-die-augsburger-polizei-politisch/)
- AUXPUNKS: [Du widerliches Stück Augsburg!](https://auxpunks.uber.space/du-widerliches-stueck-augsburg/)

### 9.12.
- STADTGRÜN: [Unermüdlich fürs Klima](https://gruene-augsburg.de/userspace/BY/sv_augsburg/Dokumente/STADTGRUEN/20201211_Infobrief_9_2020_final.pdf#page=19)

### 7.12.
- Hallo Augsburg: [Lokale Agenda 21: Klimacamp ist Aufruf und Erinnerung](https://www.hallo-augsburg.de/klimacamp-augsburg-lokale-agenda-21-klimacamp-ist-aufruf-und-erinnerung_mKq)

### 6.12.
- Forum solidarisches und friedliches Augsburg: [Das Klimacamp feiert sein 100-tägiges Jubiläum. Debatte mit Stadträt_innen, Teil 2](https://www.forumaugsburg.de/s_2kommunal/Kommunalreform/201205_100-tage-klimacamp-diskussionsrunde-mit-den-stadtraet_innen-2/index.htm)
- Die Augsburger Zeitung: [Kommentar zum Radentscheid: Es gibt nichts zu verhandeln!](https://www.daz-augsburg.de/kommentar-zum-radentscheid-es-gibt-nichts-zu-verhandeln/)

### 4.12.
- Augsburger Allgemeine: [Fahrrad-Bürgerbegehren: Rad-Aktivisten machen Druck auf die Stadt Augsburg](https://m.augsburger-allgemeine.de/augsburg/Fahrrad-Buergerbegehren-Rad-Aktivisten-machen-Druck-auf-die-Stadt-Augsburg-id58665526.html)
- Lifeguide Augsburg: [Klimacamp Augsburg soll bleiben!](https://www.lifeguide-augsburg.de/magazin/klimacamp-augsburg-soll-bleiben)

### 2.12.
- Süddeutsche Zeitung: [Frieren für das Klima](https://www.sueddeutsche.de/bayern/augsburg-klima-protest-fridays-for-future-1.5134645?reduced=true)
- Augsburger Hochschulmagazin presstige: [„So ein bisschen hatte ich schon immer das Gefühl, dass man da eigentlich was machen müsste“](http://presstige.org/2020/12/interview-klimacamper/)

### 29.11.
- Augsburger Allgemeine: [Das Augsburger Klimacamp lässt keinen kalt – und das ist gut so](https://augsburger-allgemeine.de/augsburg/Das-Augsburger-Klimacamp-laesst-keinen-kalt-und-das-ist-gut-so-id58625521.html)

### 27.11.
- Augsburger Allgemeine: [Geplante Räumung des Klimacamps: Sozialfraktion greift Stadt Augsburg an](https://augsburger-allgemeine.de/augsburg/Geplante-Raeumung-des-Klimacamps-Sozialfraktion-greift-Stadt-Augsburg-an-id58622056.html)

### 26.11.
- Bayerischer Rundfunk: [Klimacamper in Augsburg: "CSU will uns unbedingt weghaben"](https://www.br.de/nachrichten/bayern/klimacamper-in-augsburg-csu-will-uns-unbedingt-weghaben,SHQOFru)

### 25.11.
- Augsburger Allgemeine: [Die Stadt Augsburg will das Klimacamp weiterhin loswerden](https://www.augsburger-allgemeine.de/augsburg/Die-Stadt-Augsburg-will-das-Klimacamp-weiterhin-loswerden-id58610141.html)
- Presse Augsburg: [Klimacamp: Stadt Augsburg beantragt Zulassung von Berufung gegen VWG-Urteil](https://presse-augsburg.de/klimacamp-stadt-augsburg-beantragt-zulassung-von-berufung-gegen-vwg-urteil/668067/)
- Bayerischer Rundfunk: [Klimacamp: Stadt zieht vor den Verwaltungsgerichtshof](https://www.br.de/nachrichten/bayern/klimacamp-stadt-zieht-vor-den-verwaltungsgerichtshof,SHNHEQa)
- Stadtzeitung: [Stadt Augsburg will weiter gegen das Klimacamp vorgehen](https://www.stadtzeitung.de/region/augsburg/politik/stadt-augsburg-will-gegen-klimacamp-vorgehen-id215756.html)
- Die Augsburger Zeitung: [Klimacamp: Stadt geht gegen das Urteil des Augsburger Verwaltungsgerichts in Berufung — Grüne kritisieren Vorgehen der Stadt](https://www.daz-augsburg.de/klimacamp-stadt-geht-gegen-das-urteil-des-augsburger-verwaltungsgerichts-in-berufung-gruene-kritisieren-dieses-vorgehen/)

### 24.11.
- Forum solidarisches und friedliches Augsburg: [Das Verwaltungsgericht Augsburg entscheidet im Rechtsstreit pro Klimacamp, Teil 2](https://www.forumaugsburg.de/s_2kommunal/Kommunalreform/201124_verwaltungsgericht-entscheidet-pro-klimacamp-2-folgen-fuer-bayern/index.htm)

### 23.11.
- Die Augsburger Zeitung: [Zukunftspreis: CSU-Chef Ullrich und Fridays for Future-Bewegung kritisieren Stadt wegen AfD-Stadtrat in Jury](https://www.daz-augsburg.de/zukunftspreis-csu-chef-ullrich-und-fridays-for-future-bewegung-kritisieren-stadt-wegen-afd-stadtrat-in-jury/)

### 20.11.
- Junge Welt: [»Wir schlafen nicht in der Kälte, weil es uns Spaß macht«](https://www.jungewelt.de/artikel/390874.klimacamp-in-augsburg-wir-schlafen-nicht-in-der-k%C3%A4lte-weil-es-uns-spa%C3%9F-macht.html)

### 19.11.
- Forum solidarisches und friedliches Augsburg: [Eine schwere Niederlage für die Stadtverwaltung und die CSU](https://www.forumaugsburg.de/s_2kommunal/Kommunalreform/201119_verwaltungsgericht-entscheidet-pro-klimacamp/index.htm)

### 14.11.
- Augsburger Allgemeine (Lokalteil Friedberg): [Osttangenten-Gegner erhalten Unterstützung vom Augsburger Klimacamp](https://m.augsburger-allgemeine.de/friedberg/Osttangenten-Gegner-erhalten-Unterstuetzung-vom-Augsburger-Klimacamp-id58528891.html)
- Bayerischer Rundfunk: [Fridays-for-Future-Aktivisten erhalten Augsburger Zukunftspreis](https://www.br.de/nachrichten/bayern/augsburger-klimaaktivisten-erhalten-zukunftspreis-der-stadt,SGJM02T)

### 13.11.
- Augsburger Allgemeine: [Soll das Klimacamp weichen? Grüne lehnen weiteren Rechtsstreit ab](https://m.augsburger-allgemeine.de/augsburg/Soll-das-Klimacamp-weichen-Gruene-lehnen-weiteren-Rechtsstreit-ab-id58531696.html)
- Augsburger Allgemeine: [Klima-Aktivisten erhalten Augsburger Zukunftspreis](https://www.augsburger-allgemeine.de/augsburg/Klima-Aktivisten-erhalten-den-Augsburger-Zukunftspreis-id58538326.html)

### 11.11.
- Die Augsburger Zeitung: [Klimapolitik: Stadt will CO2-Emissionen bis zum Jahr 2030 halbieren](https://www.daz-augsburg.de/klimapolitik-stadt-will-co2-emissionen-bis-zum-jahr-2030-halbieren/)
- Die Augsburger Zeitung: [“Das Urteil verschafft uns zusätzliche Legitimität” – Interview mit Klimacamper Ingo Blechschmidt](https://www.daz-augsburg.de/das-urteil-verschafft-uns-zusaetzliche-legitimitaet-interview-mit-klimacamper-ingo-blechschmidt/)
- augsburg.tv: [Klimacamp darf bleiben: Reaktion der Stadt Augsburg](https://www.augsburg.tv/mediathek/video/klimacamp-darf-bleiben-reaktionen-der-stadt-augsburg/)

### 10.11.
- Augsburger Allgemeine: [Das Klimacamp neben dem Augsburger Rathaus darf bleiben](https://www.augsburger-allgemeine.de/augsburg/Das-Klimacamp-neben-dem-Augsburger-Rathaus-darf-bleiben-id58514851.html)
- Augsburger Allgemeine: [Klimacamp: Augsburg hat sich vergaloppiert](https://m.augsburger-allgemeine.de/augsburg/Klimacamp-Augsburg-hat-sich-vergaloppiert-id58517731.html)
- Augsburger Allgemeine: [Experte: So ungleich trifft der Klimawandel die Menschen in Augsburg](https://m.augsburger-allgemeine.de/augsburg/Experte-So-ungleich-trifft-der-Klimawandel-die-Menschen-in-Augsburg-id58473626.html)
- Stadtzeitung: [Gericht entscheidet: Augsburger Klimacamp darf bleiben](https://www.stadtzeitung.de/region/augsburg/politik/gericht-entscheidet-augsburger-klimacamp-darf-bleiben-id215108.html)
- Bayerischer Rundfunk: [Verwaltungsgericht Augsburg: Klimacamp darf endgültig bleiben](https://www.br.de/nachrichten/bayern/verwaltungsgericht-augsburg-klimacamp-darf-endgueltig-bleiben,SFwjPny)
- Süddeutsche Zeitung: [Aktivisten dürfen weiter neben Rathaus campen](https://www.sueddeutsche.de/bayern/augsburg-klimacamp-raeumung-urteil-1.5110839I)
- Schwäbische Zeitung: [Stadt Augsburg muss Klimacamp neben Rathaus hinnehmen](https://www.schwaebische.de/sueden/bayern_artikel,-stadt-augsburg-muss-klimacamp-neben-rathaus-hinnehmen-_arid,11292737.html)
- Zeit: [Stadt Augsburg muss Klimacamp neben Rathaus hinnehmen](zeit.de/news/2020-11/10/stadt-augsburg-muss-klimacamp-neben-rathaus-hinnehmen)
- augsburg.tv: [Klimacamp darf bleiben](https://www.augsburg.tv/mediathek/video/a-tv-kompakt-klimacamp-darf-bleiben/)
- Traunsteiner Tagblatt: [Stadt Augsburg muss Klimacamp neben Rathaus hinnehmen](https://www.traunsteiner-tagblatt.de/region/nachrichten-aus-bayern_artikel,-stadt-augsburg-muss-klimacamp-neben-rathaus-hinnehmen-_arid,599449.html)
- Donaukurier: [Stadt Augsburg muss Klimacamp neben Rathaus hinnehmen](https://www.donaukurier.de/nachrichten/bayern/Demonstrationen-Umwelt-Prozesse-Urteile-SCHWABEN-Bayern-Deutschland-Stadt-Augsburg-muss-Klimacamp-neben-Rathaus-hinnehmen;art155371,4713096)
- Presse Augsburg: [Verwaltungsgericht erklärt „Klimacamp“ am Augsburger Rathaus zu einer Versammlung im rechtlichen Sinne](https://presse-augsburg.de/verwaltungsgericht-erklaert-klimacamp-am-augsburger-rathaus-zu-einer-versammlung-im-rechtlichen-sinne/657077/)
- Frankenpost: [Stadt Augsburg muss Klimacamp neben Rathaus hinnehmen](https://www.frankenpost.de/region/bayern/Stadt-Augsburg-muss-Klimacamp-neben-Rathaus-hinnehmen;art2832,7462672)
- Radio Fantasy: [Augsburger Klimacamp darf vorerst nicht geräumt werden](https://www.fantasy.de/lokalnews/10296-augsburger-klimacamp-darf-vorerst-nicht-geraeumt-werden)
- ntv: [Stadt Augsburg muss Klimacamp neben Rathaus hinnehmen](https://www.n-tv.de/regionales/bayern/Stadt-Augsburg-muss-Klimacamp-neben-Rathaus-hinnehmen-article22160685.html)

### 7.11.
- Aktionsbündnis keine Osttangente: [Klimacamp und AKO – gemeinsam gegen den Klimawandel](https://keine-osttangente.de/?p=2042)
- Forum solidarisches und friedliches Augsburg: [Das Klimacamp feiert sein 100-tägiges Jubiläum. Debatte mit Stadträt_innen, Teil 1](https://forumaugsburg.de/s_2kommunal/Kommunalreform/201107_100-tage-klimacamp-diskussionsrunde-mit-den-stadtraet_innen/index.htm)

### 28.10.
- Augsburger Allgemeine: [Der Ton zwischen Radfahrern und der Stadt Augsburg verschärft sich](https://www.augsburger-allgemeine.de/augsburg/Der-Ton-zwischen-Radfahrern-und-der-Stadt-Augsburg-verschaerft-sich-id58425406.html)

### 23.10.
- On the air - Evangelische Kirche im Radio: kurz vor sechs
- Die Augsburger Zeitung: [Stadt im Schlagabtausch mit Fahrradaktivisten](https://www.daz-augsburg.de/stadt-im-schlagabtausch-mit-fahrradaktivisten-2)
- Augsburger Allgemeine: [Ist das Klimacamp rechtmäßig? Gerichtsurteil lässt auf sich warten](https://www.augsburger-allgemeine.de/augsburg/Ist-das-Klimacamp-rechtmaessig-Gerichtsurteil-laesst-auf-sich-warten-id58402501.html)
- Channel Welcome: [100 Tage Klimacamp Augsburg](https://www.channel-welcome.de/sendungen/beitraege/100-tage-klimacamp-augsburg/)

### 20.10.
- Radio Schwaben: [111 Tage Klimacamp – und was jetzt?](https://radioschwaben.de/nachrichten/augsburg-111-tage-klimacamp-und-was-jetzt/)

### 19.10.
- Lisa Badum: [Klimacamps in Augsburg und Nürnberg](https://www.lisa-badum.de/2020/10/19/klimacamps-in-augsburg-und-nuernberg/)

### 16.10.
- Bayerische Staatszeitung: [Gretas Jünger belagern das Augsburger Rathaus](https://www.bayerische-staatszeitung.de/staatszeitung/kommunales/detailansicht-kommunales/artikel/gretas-juenger-belagern-das-augsburger-rathaus.html#topPosition)

### 12.10.
- Zündfunk des BR: [100 Tage Klimacamp in Augsburg](https://www.br.de/radio/bayern2/sendungen/zuendfunk/zuendfunk-magazin-12102020_x-100.html) (ab Minute 2:55)

### 11.10.
- Augsburger Allgemeine: [Aktivisten des Klimacamps besetzen Baum in der Fuggerstraße](https://m.augsburger-allgemeine.de/augsburg/Aktivisten-des-Klimacamps-besetzen-Baum-in-der-Fuggerstrasse-id58311051.html)

### 10.10.
- Bayerischer Rundfunk: [100 Tage Klima-Camp - Keine Annäherung ans Augsburger Rathaus](https://www.br.de/nachrichten/bayern/100-tage-klima-camp-keine-annaeherung-ans-augsburger-rathaus,SCvXUaO)
- Rundschau des BR: [100 Tage Klimaprotestcamp in Augsburg](https://www.br.de/mediathek/video/rundschau-1830-10102020-corona-massnahmen-kassenaerzte-kritisieren-regelungswut-av:5f4f8abcbff613001b8303f8) (Minute 11:25)

### 9.10.
- Augsburger Allgemeine: [Aus Rücksicht auf Senioren: Klimaschützer besetzen anderen Baum](https://www.augsburger-allgemeine.de/augsburg/Aus-Ruecksicht-auf-Senioren-Klimaschuetzer-besetzen-anderen-Baum-id58302941.html)

### 8.10.
- Augsburger Allgemeine: [Klimacamp-Aktivisten wollen Baum am Kaufbach besetzen](https://www.augsburger-allgemeine.de/augsburg/Klimacamp-Aktivisten-wollen-Baum-am-Kaufbach-besetzen-id58296166.html)

### 7.10.
- Die Augsburger Zeitung: [Kommentar: Augsburgs Stadtregierung leidet an einer inneren Schwäche](https://www.daz-augsburg.de/kommentar-augsburgs-stadtregierung-leidet-an-einer-inneren-schwaeche/)

### 5.10.
- Stadtzeitung: [Am Klimacamp in Augsburg - Tag 96](https://www.stadtzeitung.de/region/augsburg/lokales/klimacamp-augsburg-tag-96-id213335.html)
- Augsburger Allgemeine: [Die Klimaaktivisten werfen der Stadt Augsburg Untätigkeit vor](https://augsburger-allgemeine.de/augsburg/Die-Klimaaktivisten-werfen-der-Stadt-Augsburg-Untaetigkeit-vor-id58264826.html)

### 25.9.
- Augsburger Allgemeine: [Fridays for Future streikt heute in Augsburg wieder fürs Klima](https://www.augsburger-allgemeine.de/augsburg/Fridays-for-Future-streikt-heute-in-Augsburg-wieder-fuers-Klima-id58198211.html)

### 23.9.
- Radio Schwaben: [Augsburg: Klimacamp muss kleiner werden](https://radioschwaben.de/nachrichten/augsburg-klimacamp-muss-kleiner-werden/)

### 23.9.
- Radio rt.1: [Im Oktober fällt die Entscheidung über das Augsburger Klimacamp](https://www.rt1.de/im-oktober-faellt-die-entscheidung-ueber-das-augsburger-klimacamp-188172/)

### 22.9.
- Augsburger Allgemeine: [Stadt Augsburg macht Auflagen: Klimacamper müssen sich einschränken](https://www.augsburger-allgemeine.de/augsburg/Stadt-Augsburg-macht-Auflagen-Klimacamper-muessen-sich-einschraenken-id58181041.html) (siehe auch [unsere inhaltliche Korrektur](/pages/Pressemitteilungen/2020-09-22-PM_Auflagen.html))

### 21.9.
- Augsburger Allgemeine: [Bürgerliche Fraktion treibt Schwarz-Grün beim Klimaschutz in Augsburg an](https://m.augsburger-allgemeine.de/augsburg/Buergerliche-Fraktion-treibt-Schwarz-Gruen-beim-Klimaschutz-in-Augsburg-an-id58162561.html)

### 20.9.
- Augsburger Allgemeine: [Klimawandel: Im Augsburger Rathaus ist mehr Tempo nötig](https://www.augsburger-allgemeine.de/augsburg/Klimawandel-Im-Augsburger-Rathaus-ist-mehr-Tempo-noetig-id58161006.html)
- Augsburger Allgemeine: [Der globale Klimawandel wird auch Augsburg hart treffen](https://www.augsburger-allgemeine.de/augsburg/Der-globale-Klimawandel-wird-auch-Augsburg-hart-treffen-id58144621.html)

### 19.9.
- Augsburger Allgemeine: [Die Deprimierten: Eine Nacht im Augsburger Klima-Camp](https://www.augsburger-allgemeine.de/augsburg/Die-Deprimierten-Eine-Nacht-im-Augsburger-Klima-Camp-id58123941.html)
- Forum solidarisches und friedliches Augsburg: [Anstehende Stadtratssitzung -- Bürgerliche Mitte und Augsburg in Bürgerhand wollen es wissen](https://forumaugsburg.de/s_2kommunal/Kommunalpolitik/200919_stadtratssitzung-24-september-antraege-zur-energiewende/index.htm)

### 18.9.
- Forum solidarisches und friedliches Augsburg: [80 Tage auf dem Fischmarkt -- Berichte aus dem Klimacamp](https://forumaugsburg.de/s_2kommunal/Kommunalreform/200918_berichte-vom-klimacamp/index.htm)

### 17.9.
- Stadtzeitung: [Hochbeet am Rathausplatz entfernt: Klimaaktivisten erstatten Anzeige gegen die Stadt](https://www.stadtzeitung.de/region/augsburg/lokales/hochbeet-rathausplatz-entfernt-klimaaktivisten-erstatten-anzeige-gegen-stadt-id212520.html)

### 16.9.
- Augsburger Allgemeine: [Ärger im Klimacamp: Streit zwischen AfD und Aktivisten geht weiter](https://www.augsburger-allgemeine.de/augsburg/Aerger-im-Klimacamp-Streit-zwischen-AfD-und-Aktivisten-geht-im-Internet-weiter-id58138936.html)
- Radio Schwaben: [Augsburg: Klimacamp sorgt erneut für Aufregung](https://radioschwaben.de/nachrichten/augsburg-klimacamp-sorgt-erneut-fuer-aufregung/)

### 15.9.
- augsburg.tv: [77 Tage Klimacamp in Augsburg](https://www.augsburg.tv/mediathek/video/77-tage-klimacamp-in-augsburg/)
- Augsburger Allgemeine: [Polizei wirft AfD-Stadtrat aus dem Augsburger Klimacamp](https://www.augsburger-allgemeine.de/augsburg/Polizei-wirft-AfD-Stadtrat-aus-dem-Augsburger-Klimacamp-id58129586.html)
- Augsburger Allgemeine: [Ärger um AfD am Klimacamp: Diese Eskalation war unnötig](https://augsburger-allgemeine.de/augsburg/Aerger-um-AfD-am-Klimacamp-Diese-Eskalation-war-unnoetig-id58132096.html)

### 13.9.
- Augsburger Allgemeine: [Moria und Klimaschutz: In Augsburg gehen die Protestaktionen weiter](https://www.augsburger-allgemeine.de/augsburg/Moria-und-Klimaschutz-In-Augsburg-gehen-die-Protestaktionen-weiter-id58114026.html)

### 11.9.
- Augsburger Allgemeine: [Die Hermanstraße bekommt einen „Pop-up-Radweg" – aber nur für eine Stunde](https://www.augsburger-allgemeine.de/augsburg/Die-Hermanstrasse-bekommt-einen-Pop-up-Radweg-aber-nur-fuer-eine-Stunde-id58103426.html)
- Augsburger Allgemeine: [Die Stadt entfernt das Hochbeet der Klimacamp-Aktivisten an der Maxstraße](https://www.augsburger-allgemeine.de/augsburg/Die-Stadt-entfernt-das-Hochbeet-der-Klimacamp-Aktivisten-an-der-Maxstrasse-id58101506.html)
- hochbeet.stangl.eu:
    - [Hochbeet von Klimaaktivisten auf einem Parkplatz](https://hochbeet.stangl.eu/hochbeet-von-klimaaktivisten-auf-einem-parkplatz/)
    - [Hochbeetentfernung](https://hochbeet.stangl.eu/hochbeetentfernung/)

### 10.9.
- Bayerischer Rundfunk: ["Platz-Park": Augsburger Klimaaktivisten blockieren Parkplatz](https://www.br.de/nachrichten/bayern/platz-park-augsburger-klimaaktivisten-blockieren-parkplatz,SAAKXVZ)
- Augsburger Allgemeine: [Klimacamp-Aktivisten machen einen Parkplatz in der Maxstraße zum Beet](https://www.augsburger-allgemeine.de/augsburg/Klimacamp-Aktivisten-machen-einen-Parkplatz-in-der-Maxstrasse-zum-Beet-id58094576.html)

### 2.9.
- Augsburger Allgemeine: [Vier Monate Schwarz-Grün in Augsburg: In Teilen der CSU grummelt es](https://augsburger-allgemeine.de/augsburg/Vier-Monate-Schwarz-Gruen-in-Augsburg-In-Teilen-der-CSU-grummelt-es-id58031906.html)

### 28.8.
- DAV Augsburg: [Nachlese Augsburger Klimacamp & aktueller Klimaschutzbericht](https://www.dav-augsburg.de/aav/verein-berichte/1429-nachlese-augsburger-klimacamp-aktueller-klimaschutzbericht)
- Youtube: [Neue Szene Augsburg: Mit Fridays for Future-Aktivistin Sarah Bauer im Klima-Camp am Augsburger Rathaus](https://www.youtube.com/watch?v=7bHiWn2_tQ0)

### 20.8.
- Augsburger Allgemeine: ["Pop-up-Radweg"-Protest: Hermanstraße könnte dauerhafte Radspur bekommen](https://outline.com/cG6c6E)

### 17.8.
- Augsburger Allgemeine: [Klimaschützer wollen den Winter über im Camp am Augsburger Rathaus bleiben](https://augsburger-allgemeine.de/augsburg/Klimaschuetzer-wollen-den-Winter-ueber-im-Camp-am-Augsburger-Rathaus-bleiben-id57939946.html)

### 15.8.
- Forum solidarisches und friedliches Augsburg: [Blockade von Premium Aerotec Werk III in Haunstetten – das etwas andere Friedensfest](https://www.forumaugsburg.de/s_2kommunal/Friedensstadt/200815_blockade-von-premium-aerotec-werk-iii-zum-friedensfest/index.htm)

### 12.8.
- Augsburger Hochschulmagazin presstige: [43 Tage Klimacamp – Ende offen](https://presstige.org/2020/08/43-tage-klimacamp-ende-offen/)
- Augsburger Allgemeine: [Verkehr, Klimaschutz und mehr: Wie nachhaltig ist Augsburg?](https://www.augsburger-allgemeine.de/augsburg/Verkehr-Klimaschutz-und-mehr-Wie-nachhaltig-ist-Augsburg-id57915371.html)

### 9.8.
- Süddeutsche Zeitung: [Friedenspreis für Kardinal Marx und Bedford-Strohm](https://www.sueddeutsche.de/politik/kommunen-augsburg-friedenspreis-fuer-kardinal-marx-und-bedford-strohm-dpa.urn-newsml-dpa-com-20090101-200808-99-91576)

### 8.8.
- Bayerischer Rundfunk: [Klimacamp protestiert zum Friedensfest vor Premium Aerotec](https://www.br.de/nachrichten/bayern/klimacamp-protestiert-zum-friedensfest-vor-premium-aerotec,S73t481)

### 7.8.
- Bayerischer Rundfunk: [Die Augsburger Hermanstraße wird zum Pop-up-Radweg](https://www.br.de/nachrichten/bayern/die-augsburger-hermanstrasse-wird-zum-pop-up-radweg,S6ypNyy)

### 6.8.
- MUCBOOK: [Demonstrationskultur in Zeiten von Corona: Was macht eigentlich Fridays for Future gerade?](https://www.mucbook.de/demonstrationskultur-in-zeiten-von-corona-was-macht-eigentlich-fridays-for-future-gerade/)

### 2.8.
- Stadtzeitung: [Augsburger Radbegehren hat nötige Unterschriften für Bürgerentscheid erreicht](https://www.stadtzeitung.de/region/augsburg/politik/augsburger-radbegehren-hat-noetige-unterschriften-fuer-buergerentscheid-erreicht-id210100.html) (Yeah!)

### 1.8.
- Die Augsburger Zeitung: [Radentscheid: 12.819 gezählte Unterschriften – Aktivisten fordern Taten von Stadtregierung](https://www.daz-augsburg.de/radentscheid-12-819-gezaehlte-unterschriften-aktivisten-fordern-taten-von-stadtregierung/)

### 31.7.
- Bayerischer Rundfunk: [Klima-Camp in Augsburg noch immer aktiv](https://www.br.de/nachrichten/bayern/klima-camp-in-augsburg-noch-immer-aktiv,S6KtNYf)
- Augsburger Allgemeine: [Umweltaktivisten ziehen vom Klimacamp in Augsburg aus auf die Straße](https://www.augsburger-allgemeine.de/augsburg/Umweltaktivisten-ziehen-vom-Klimacamp-in-Augsburg-aus-auf-die-Strasse-id57847431.html)

### 28.7.
- Die Augsburger Zeitung: [Zuspruch aus ganz Deutschland – auch Luisa Neubauer besucht das Augsburger Klimacamp](https://www.daz-augsburg.de/zuspruch-aus-ganz-deutschland-auch-luisa-neubauer-besucht-das-augsburger-klimacamp/)
- Bayerischer Rundfunk: [Klimacamp Augsburg: Luisa Neubauer kritisiert Stadtverwaltung](https://www.br.de/nachrichten/bayern/luisa-neubauer-klimacamp-augsburg-wahnsinnig-inspirierend,S629lzK)
- Radio rt.1: [Augsburger Klimacamp: Fridays-for-Future-Aktivistin Neubauer ruft Stadt zu mehr Klimaschutz auf](https://www.rt1.de/fridays-for-future-besuch-luisa-neubauer-von-augsburger-klimacamp-beeindruckt-180817/)

### 22.7.
- Stadtzeitung: [Was will das Augsburger Klimacamp erreichen? Die Forderungen der Umweltaktivisten](https://www.stadtzeitung.de/region/augsburg/politik/will-augsburger-klimacamp-erreichen-forderungen-umweltaktivisten-id209634.html)

### 19.7.
- Forum solidarisches und friedliches Augsburg: [Vortrag auf dem Klimacamp von Tobias Walter](https://www.forumaugsburg.de/s_2kommunal/Kommunalreform/200719_vortrag-auf-dem-klimacamp-dezentrale-energiewende-in-augsburg-jetzt/index.htm)
- Die Augsburger Zeitung: [Kommentar: Augsburgs Stadtregierung leidet an einer inneren Schwäche](https://www.daz-augsburg.de/kommentar-augsburgs-stadtregierung-leidet-an-einer-inneren-schwaeche/)

### 18.7.
- Radio Schwaben: [Augsburg: Klimacamp darf bleiben](https://radioschwaben.de/nachrichten/augsburg-klimacamp-darf-bleiben/)

### 17.7.
- Bayerischer Rundfunk: ["Klima-Camp" Augsburg: Aktivisten rechnen mit Eil-Entscheidung](https://www.br.de/nachrichten/bayern/klima-camp-augsburg-aktivisten-rechnen-mit-eil-entscheidung,S4yG3zh)
- Die Augsburger Zeitung: [Verwaltungsgericht Augsburg: Klimacamp ist vom Versammlungsrecht gedeckt](https://www.daz-augsburg.de/verwaltungsgericht-klimacamp-ist-vom-versammlungsrecht-gedeckt/)
- Aichacher Zeitung: [Klimacamp darf bleiben](https://www.aichacher-zeitung.de/vorort/augsburg/art21,157810)
- Bayerisches Verwaltungsgericht Augsburg: [Gericht gibt Eilantrag des „Klima-Camp“ statt](https://www.vgh.bayern.de/media/vgaugsburg/presse/klimacamp-versammlung.pdf)
- Hallo Augsburg: [Nachgefragt: Das passiert im Protestcamp am Rathaus](https://www.hallo-augsburg.de/klimacamp-augsburg-nachgefragt-das-passiert-im-protestcamp-am-rathaus_4W9)

### 15.7.
- Die Augsburger Zeitung: [Causa Klimacamp: Grüne in Not – Verwaltungsgericht soll entscheiden](https://www.daz-augsburg.de/causa-klimascamp-gruene-in/)

### 14.7.
- Stadtzeitung: ["Klimacamp" neben dem Rathaus wird vorerst noch nicht geräumt](https://www.stadtzeitung.de/region/augsburg/politik/klimacamp-neben-rathaus-vorerst-noch-geraeumt-id209199.html)
- Augsburger Allgemeine: [Das Klimacamp am Augsburger Rathaus wird vorerst nicht geräumt](https://www.augsburger-allgemeine.de/augsburg/Das-Klimacamp-am-Augsburger-Rathaus-wird-vorerst-nicht-geraeumt-id57725176.html)
- Radio Schwaben: [Augsburg: Klimacamp am Rathaus wird Fall fürs Gericht](https://radioschwaben.de/nachrichten/augsburg-klimacamp-am-rathaus-wird-fall-fuers-gericht/)

### 13.7.
- Süddeutsche Zeitung: [Wie "Fridays for Future" wieder in die Öffentlichkeit will](https://www.sueddeutsche.de/bayern/augsburg-fridays-for-future-camp-rathaus-1.4964426)
- Bayerischer Rundfunk: ["Klima-Camp" Augsburg: Aktivisten wollen vor Rathaus bleiben](https://www.br.de/nachrichten/bayern/klima-camp-augsburg-aktivisten-wollen-vor-rathaus-bleiben,S4ch3qw)

### 12.7.
* Oberbürgermeisterin Weber (CSU): [Klimacamp nicht von Versammlungsfreiheit gedeckt](https://www.facebook.com/evaweberaugsburg/photos/a.1065950733447370/4072563629452717/?type=3&source=57) (Wir sind entsetzt)
* Forum solidarisches und friedliches Augsburg: [Fridays for Future errichtet Camp neben dem Rathaus](https://www.forumaugsburg.de/s_6kultur/Unterricht/200712_fridays-for-future-errichtet-camp-neben-dem-rathaus-gegen-kohleausstiegsgesetz/index.htm)
* Bayerischer Rundfunk: [Klima-Demonstranten sollen Augsburger Rathausplatz verlassen](https://www.br.de/nachrichten/bayern/klima-demonstranten-muessen-augsburger-rathausplatz-verlassen,S4QK0pR)

### 11.7.
* Augsburger Allgemeine: [Politiker kritisieren angekündigte Räumung des Klimacamps](https://www.augsburger-allgemeine.de/augsburg/Politiker-kritisieren-angekuendigte-Raeumung-des-Klimacamps-id57713976.html)

### 10.7.
* Augsburger Allgemeine: [Die Stadt will das Klimacamp am Augsburger Rathaus räumen lassen](https://www.augsburger-allgemeine.de/augsburg/Die-Stadt-will-das-Klimacamp-am-Augsburger-Rathaus-raeumen-lassen-id57711651.html)
* Augsburger Allgemeine: [Augsburger Klimacamp: Die Stadt agiert kleinkariert](https://www.augsburger-allgemeine.de/augsburg/Augsburger-Klimacamp-Die-Stadt-agiert-kleinkariert-id57712471.html)

### 8.7.
- Stadtzeitung: [„Fridays for Future“ im Umweltausschuss ignoriert](https://www.stadtzeitung.de/region/augsburg/politik/fridays-future-umweltausschuss-ignoriert-id208926.html)

### 7.7.
- Augsburger Allgemeine: [Die Klimaschützer wollen ihr Camp am Rathaus noch lange betreiben](https://www.augsburger-allgemeine.de/augsburg/Die-Klimaschuetzer-wollen-ihr-Camp-am-Rathaus-noch-lange-betreiben-id57689296.html)

#### 6.7.
- AUXPUNKS: [#RATHAUSbeCAMPen – RATHAUS BLOCKADE](https://auxpunks.uber.space/rathausbecampen-rathaus-blockade/)

### 3.7.
- Augsburger Allgemeine: ["Fridays for Future": Klimaschützer melden sich mit Protesten zurück](https://www.augsburger-allgemeine.de/augsburg/Fridays-for-Future-Klimaschuetzer-melden-sich-mit-Protesten-zurueck-id57671656.html)

### 2.7.
- Bayerischer Rundfunk: [Klimaaktivisten besetzen Augsburger Rathaus](https://www.br.de/nachrichten/bayern/klimaaktivisten-besetzen-augsburger-rathaus,S3eOSKj)

### 2.7.
- Augsburger Allgemeine: [Demo: Fridays for Future campen am Augsburger Rathausplatz](https://www.augsburger-allgemeine.de/augsburg/Demo-Fridays-for-Future-campen-am-Augsburger-Rathausplatz-id57665436.html)
- Augsburger Allgemeine: [Polizeiaufgebot am Rathausplatz: Aktivisten dringen in Rathaus ein](https://www.augsburger-allgemeine.de/augsburg/Polizeiaufgebot-am-Rathausplatz-Aktivisten-dringen-in-Rathaus-ein-id57666531.html)

### 1.7.
- Stadtzeitung: [Augsburger "Fridays for Future"-Bewegung ruft zu Rathaus-Blockade gegen das Kohleausstiegsgesetz auf](https://www.stadtzeitung.de/region/augsburg/politik/augsburger-fridays-future-bewegung-ruft-rathaus-blockade-gegen-kohleausstiegsgesetz-id208579.html)
