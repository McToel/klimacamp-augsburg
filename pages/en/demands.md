---
layout: page
title: Demands
permalink: /en/demands/
parent: "English"
has_children: false
has_toc: false
nav_order: 10
keywords:
  - Augsburg
  - Augsburger
  - camp
  - camp for climate action
  - car-free
  - carbon budget
  - CO₂
  - climate justice
  - climate protection
  - coal phase-out
  - demands
  - energy transition
  - Fahrradstadt jetzt
  - IPCC
  - IPCC report
  - Klimacamp
  - local politics
  - mobility transition
  - Paris agreement
  - public transport
  - science
  - town hall
---

# Demands of the Klimacamp Augsburg

See [Forderungen](/forderungen/)
for the German version of our demands.


## 1. Compliance with the 1.5°C agreement

* **Communication of the carbon budget**<br>
  The city should adapt and openly communicate a carbon budget
  that is compatible with keeping global warming below 1.5°C
  according to the
  [SR15 report](https://www.ipcc.ch/site/assets/uploads/sites/2/2019/05/SR15_TS_High_Res.pdf)
  by the IPCC.
  The climate camp suggested 9.7 million tons CO₂
  for the city of Augsburg.<br>
  ✅ *The communal advisory council on climate
  echoed this demand to the city.
  Finally in January 2021
  the intention to stay within the suggested budget
  was adopted by the city.*
* **Socially just implementation of climate protection measures**<br>
  The measures to stay within the carbon budget
  have to be implemented in a socially fair fashion.<br>
  ❌ *The city hasn't adopted sufficient measures as of yet.*
* **Safeguards for local ecosystems**<br>
  ❌ *The local ecosystems are still under threat
  from infrastructure projects that originate
  from an outdated mentality.*<br>
  *Examples:*
  * *The forest and grassland along the river Lech is threatened
    by a proposed highway, the so-called “Osttangente”
    (engl. “east bypass”).*<br>
    *An independent action group called*
    [Aktionsbündnis Keine Osttangente](https://keine-osttangente.de))
    *(engl. “Action Group No East Bypass”)
    has been working against the Osttangente for years.*
  * *The city council is planning
    to cut down old trees within the city area.
    A plan to cut down 44 trees in front of the main station
    has recently achieved media attention and criticism.
    The city was forced to postpone the decision on the plan,
    but hasn't abandoned it as of yet.
    Other plans receive less attention and sometimes get implemented
    before sufficient protest against them can form.*<br>
    *The* [Baumallianz Augsburg e.V.](https://baumallianz-augsburg.de)
    *(engl. “Tree Alliance Augsburg Association”)
    is trying to protect old trees within the city's boundaries.*
  * *(Technically not the city of Augsburg:)
    The* Lohwald, *a forest near Meitingen just north of Augsburg,
    is threatened by the extension of a steel mill.*<br>
    *In the beginning of October 2022 the* Bund Naturschutz,
    *the* Klimacamp *and other initiatives took legal action
    against the planned deforestation.*<br>
    *In a surprise action in the middle of October 2022
    one third of the forest was destroyed.
    We consider it likely that this deforestation has been illegal.*<br>
    *We support the local protest against this project.
    See: [https://www.lohwibleibt.de/](https://www.lohwibleibt.de/)*
  * *(Technically not the city of Augsburg:)
    Parts of the* Bobinger Auwald,
    *an alluvial forest south of Augsburg,
    are threatened by plans of Wehringen to extend an industrial park
    located within the forest.
    Despite the forest being closer
    to a residential area of the town of Bobingen,
    it belongs to the muncipality of Wehringen.*<br>
    *We support the local protest against this project.
    See: [https://www.bobinger-auwald-bleibt.de/](https://www.bobinger-auwald-bleibt.de/)*

For further information on the IPCC reports
see our [overview over the latest IPCC reports](/en/ipcc-reports/).


## 2. Energy transition of Augsburg

* **Augsburg free of coal-based power until 2023**<br>
  The Stadtwerke Augsburg (swa)
  are the city-owned communal utilities company
  that is responsible for electricity, water, heating
  and public transport.
  Parts of their portfolio is coal-based and
  they own shares in fossil gas companies.
  We demand a phase-out of coal-based power from the city
  including the city-owned companies like the *swa*.<br>
  ❌ *While there have been some improvements,
  the Stadtwerke still sell coal-based electricity
  to its business customers.
  The carbon emissions per kWh on their business energy tariff
  far exceed the national average.*
* **Fast extension of solar and wind power**<br>
  The city should extend its efforts
  to generate solar and wind energy
  within the city limits.<br>
  ❌ *There have been a few efforts by the city,
  but they are still in an early stage.
  For example it is working
  on compulsary installation of solar systems
  on the roofs of development areas.
  Efforts to install wind turbines
  within the city limits are stalling.*
* **Positioning against the continued dependence on coal**<br>
  The city has to take a stance against the federal law from  3rd July 2020,
  which prolonges the phase-out of coal in Germany until 2038.<br>
  ❌ *Criticism by city officials on the law has been tame.*


## 3. Mobility transition

We differentiate between a true
“[mobility transition](https://en.wikipedia.org/wiki/Mobility_transition)”,
which improves public transport and
makes the city more friendly towards cycling and walking,
and an insufficent Antriebswende
(engl. “propulsion transition”),
which suggests to keep mobility as is
with merely replacing fossil-fuel driven cars by electric cars.

* **Implementation of the demands of “Fahrradstadt jetzt”**<br>
  “Fahrradstadt jetzt” (engl. “Bicycle City Now”)
  is a group that organized a petition for a referendum
  for a more cycling-friendly Augsburg.
  The name of the group is a play on words with “Fahrradstadt 2020”.
  “Fahrradstadt 2020” was an initiative by the city from 2012
  to make Augsburg more cycling-friendly by 2020.
  The initiative failed totally.<br>
  The petition was successful.
  To avoid a referendum
  the city entered into a contract
  with the organizers of “Fahrradstadt jetzt”.
  The contract is a good first step,
  but to make Augsburg truely cycling-friendly,
  more actions are necessary
  than just the ones that are specified in the contract.<br>
  ❔ *The city has yet to implement the steps
  towards a more cycling-friendly Augsburg
  to which it agreed in the contract.*
* **Expansion of public transport**<br>
  The city has to improve public transport
  both in quantity and quality.<br>
  ❌ *The policy of the city is still very focussed on cars.
  The trams still go less frequent than before the pandemic.
  While the connections from some outer districts
  into the center of the city are acceptable,
  connections between outer districts are inadequate.<br>
  However, there has been an shift
  in the city governments's communication.
  City officials repeat again and again
  that zones with low car traffic,
  a reduction of parking space
  and improvements for cyclists, walkers and public transport
  are going to be the corner stones of the mobility plan,
  which specifies the city's plans
  from the second half of the 2020s until 2038.
  Sadly, implementation is still years ahead.*
* **Free of charge usage of public transport**<br>
  ❌ *The city fails completely in making public transport affordable.
  In 2021 alone there have been two independent increases
  of the ticket prices.*
* **Discontinuation of the project “Osttangente”**<br>
  The project “Osttangente” aims at building a highway
  through Bavaria's largest connected alluvial forest
  – an important and unique ecosystem.<br>
  ❌ *The idea of the Osttangente is still haunting regional politics.*

In autumn 2021 a study commissioned by the city
confirmed our claims that a simple propulsion transition is insufficient
to reach climate neutrality in Augsburg's mobility sector
sufficiently fast to comply with the 1.5°C limit.
The study recommends a 50% reduction of car traffic
in Augsburg till 2040.<br>
Source: study
“[Klimaschutz 2030: Studie für ein Augsburger Klimaschutzprogramm](https://www.augsburg.de/fileadmin/user_upload/umwelt_soziales/umwelt/klima%20und%20energie/Studie_Klimaschutz_2030_mit_allen_anlagen.pdf)”
page 47

To counter the inaction and lack of ideas in city council,
activists created the
“[Verkehrswendeplan Augsburg](https://www.verkehrswende-augsburg.de/)”
(engl. “mobility transition plan Augsburg”).
It provides a vision containing proposed tram routes,
bicycle boulevards, train stations,
bus routes and car-free zones.
The activists created high-resolution maps of this vision
and explanatory texts.


## System change instead of climate change

Our demands aim at providing the foundations
for everyone to have a happy, socially fair,
climatically just, free and good life.
