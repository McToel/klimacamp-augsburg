---
layout: page
title: "Awareness-Glossar"
grand_parent: "Mitmachen"
parent: "Awarenessleitlinien"
permalink: /awareness-glossar/
nav_order: 10
---

# Awareness-Glossar Klimacamp Augsburg

## Awareness

Der Begriff **„Awareness”** kommt aus dem Englischen „to be aware“ und bedeutet (im weiteren Sinne) „sich bewusst sein, sich informieren, für gewisse Probleme sensibilisiert sein“. Wir leben in einer Gesellschaft, die leider von **ungleichen Machtverhältnissen** geprägt ist. Menschen werden aufgrund bestimmter Merkmale bevorteilt (Privilegierung) und benachteiligt (Diskriminierung) – ob absichtlich oder unbewusst ausgeübt. Kein Mensch ist vorurteilsfrei und diskriminierungsfrei im Umgang mit anderen. Deshalb muss eine **bewusste Reflexion** darüber bei jede\*r einzelnen Person stattfinden. Awareness ist ein Konzept, das sich **gegen jede Form von Diskriminierung, Gewalt und Grenzverletzungen** stellt. Verletzendes und grenzüberschreitendes Verhalten, wie z.B. sexistische, rassistische, queer-, transfeindliche, ableistische, antisemitische, klassistische oder vergleichbare Übergriffe tolerieren wir auf dem Camp nicht.
(aus [https://mobilitaetswendecamp.noblogs.org/awareness/](https://mobilitaetswendecamp.noblogs.org/awareness/))


## Diskriminierung

**Diskriminierung** ist eine Benachteiligung von Menschen aufgrund schützenswerter Merkmale ohne sachliche Rechtfertigung. Schützenswerte Merkmale sind Merkmale eines Menschen, die einen wesentlichen Bestandteil der Persönlichkeit bilden, und gar nicht oder nur schwer veränderbar sind. Diese können tatsächlich so sein, oder aber durch andere Menschen der diskriminierten Person einfach zugeschrieben werden. Beispiele sind Religion, Geschlecht, Behinderung, Alter ...
**Entscheidend ob etwas diskriminierend ist, ist dabei die Wirkung** (also wie es bei der Person ankommt oder was für Auswirkungen die Handlung hat) **und nicht die Intention** (also wie es die andere Person gemeint hat) **einer Handlung**.
Diskriminierung ist immer strukturell, also in unserer Gesellschaft verankert. Diese stützt dieses System der Unterdrückung und hält es aufrecht (=reproduzieren), indem es manchen Menschen eine privilegierte (=bevorteilte) Position gibt, und manchen wiederum eine diskriminierte (=benachteiligte/unterdrückte/erniedrigte). Privilegierten Menschen sind diese Verhältnisse oftmals nicht bewusst, und sie reproduzieren sie oft ganz ohne Absicht, weshalb es besonders wichtig ist, sich über diese Tatsachen zu bilden, sich selbst zu reflektieren, und den Betroffenen zuzuhören.
Demnach soll bei einer Benachteiligung aufgrund schützenswerter Merkmale bei normalerweise privilegierten Personen, also z.B. eine Benachteiligung aufgrund der weißen Hautfarbe eines Menschen, von einer ungerechtfertigten Benachteiligung, und nicht von Diskriminierung gesprochen werden.

Beispiele für Diskriminierung:  

* Ein Mensch wird diskriminiert, weil sie\*er Jüd\*in ist. (= **Antisemitismus**)

* Eine Mensch wird diskriminiert, weil sie eine Frau ist. (= **Sexismus**)

* Ein Mensch wird diskriminiert, weil sie\*er homosexuell ist. (= **Queerfeindlichkeit**, queer ist ein Überbegriff für Menschen, die nicht der Heteronormativität entsprechen, siehe Punkt **"Queer"**)

* Eine Mensch wird diskriminiert, weil sie\*er im Rollstuhl sitzt. (= **Behindertenfeindlichkeit/Ableismus**)

* Ein Mensch wird diskriminiert, weil sie\*er eine nicht-binäre Geschlechtsidentität hat (= **Transfeindlichkeit**, mit **trans\*** sind hier alle Menschen gemeint, die sich nicht mit dem ihnen zugeschriebenen Geschlecht identifizieren (= Cis), siehe Punkt "**Queer**")

* Ein Mensch wird diskriminiert, weil sie\*er Rom\*nja ist. (= **Antiromaismus**)

* Eine Mensch wird diskriminiert, weil sie\*er muslimischen Glaubens ist. (= **antimuslimischer Rassismus**)


Es gibt auch gerechtfertigte Benachteiligung von Menschen, z.B. bei einer Frauenquote werden Männer benachteiligt, was aber durch das Entgegenwirken gegen sexistische Ungerechtigkeiten zwischen Männern und Frauen gerechtfertigt ist.
Mehr Infos findest du unter: [https://diskriminierungsschutz.uni-halle.de/howto/diskriminierung/?lang=de](https://diskriminierungsschutz.uni-halle.de/howto/diskriminierung/?lang=de)

## Rassismus


**Rassismus** nennt mensch die Diskriminierung von Menschen aufgrund ihrer ethnischen oder nationalen Abstammung, ihrer Hautfarbe oder anderen äußerlichen Merkmalen.

Strukturell sind seit Jahrhunderten beispielsweise **BIPoCs** (Black, Indigenous, People of Colour - auf Deutsch: Schwarze, Indigene, Menschen von Farbe) stark von Rassismus betroffen. Historische Beispiele sind Sklaverei, Genozide (= Völkermord) oder **Kolonialismus**. Doch Vorurteile, Unterdrückung, Benachteiligung und sogar Mord und Gewalt sind bis heute noch erhalten geblieben. Dabei geht es um Benachteiligung bei der Wohnungssuche, Arbeitssuche, **Racial Profiling/Rassistisches Profiling**, Alltagsrassismus aber auch um offen ausgelebten Hass und Anschläge auf das Leben, wie zum Beispiel das Attentat von Hanau 2020 (Infos dazu: [https://19feb-hanau.org/](https://19feb-hanau.org/) *Inhaltshinweis: Rassismus, Gewalt, Mord*).

Alltagsrassismen sind bei allen nicht-Betroffenen, aber sogar auch bei manchen Betroffenen (aufgrund dem Aufwachsen in einer rassistischen Gesellschaft), weit verbreitet. Diese sind nicht immer leicht zu erkennen, und werden oft schnell mit Verteidigungsmechanismen kleingeredet. Das nennt mensch **"white fragility"** (= weiße Zerbrechlichkeit). 

Alltagsrassismen können zum Beispiel sein: die Frage "Wo kommst du her?" an eine nicht weiße Person, die BIPoC Personen im eigenen Umfeld immer nur zum Thema Rassismus ansprechen, sprachliche und gedankliche Abgrenzung ("wir" und "die", Zuschreibung von bestimmten Attributen zu den Gruppen = **Othering/Anders-Machen**) oder das Reproduzieren von Vorurteilen. Auch hier gilt wieder die Regel, dass es auf die Wirkung des Handelns ankommt, ob etwas rassistisch ist oder nicht: selbst wenn das Vorurteil ein Witz sein sollte, ist es rassistisch, da es rassistische Denkmuster und Vorurteile reproduziert. Auch die Benachteiligung bei Wohnungs- und Arbeitssuche gehört zu Alltagsrassismus - man spricht von strukturellem Rassismus, da diese Verhaltens- und Gedankenmuster über einen langen Zeitraum in unserer Gesellschaft etabliert wurden. Aus diesem Grund müssen nicht-betroffene Personen ihre Privilegien, Verhaltensweisen und Denkmuster stets hinterfragen, und sich proaktiv zu diesem Thema weiterbilden (**das ist nicht die Aufgabe der Betroffenen, nur weil sie sich "damit auskennen"!**). Werden Menschen rassistisch diskriminiert bei Behördengängen, im Bildungssystem, durch Racial Profiling oder anderweitig durch (staatliche) Institutionen, so spricht mensch von *institutionellem Rassimus*, welcher sich auch über lange Zeit in unserer Gesellschaft etabliert hat.


Auch aufgrund ihrer Religion werden Menschen diskriminiert: Jüd\*innen werden ebenfalls schon seit Jahrtausenden diskriminiert, als Sündenböcke für gesellschaftliche Krisen herangezogen und es ranken sich viele Verschwörungsmythen um Jüd\*innen, die diese zu Feindbildern machen, was im Holocaust (= Völkermord an Jüd\*innen während des zweiten Weltkriegs, Infos dazu: [https://www.annefrank.org/de/anne-frank/vertiefung/was-ist-der-holocaust/](https://www.annefrank.org/de/anne-frank/vertiefung/was-ist-der-holocaust/) *Inhaltshinweis: Rassismus, Antisemitismus, Gewalt, Mord*) seine schlimmste Form annahm. Diese Form des Rassismus nennt mensch **Antisemitismus**. 

Seit der Flüchtendenwelle 2015 erstarkte der **antimuslimische Rassismus**, also die Diskriminierung von Anhänger\*innen des Islams, erneut, und wird sogar ganz offen von angeblich demokratischen Parteien wie z.B. der AfD oder der CDU verbreitet.


In Europa wurde und wird speziell die Gruppe der Sinti und Roma (mehr Infos: [https://zentralrat.sintiundroma.de/](https://zentralrat.sintiundroma.de/)) rassistisch diskriminiert, was mensch **Antiromaismus** nennt. Doch auch andere Menschengruppen, beispielweise indigene Völker aus Nordeuropa, oder sehr häufig Menschen aus Osteuropa sind stark von Diskriminierung betroffen. Ganz allgemein wird die Angst vor Fremden, oder auch Fremdenfeindlichkeit, **Xenophobie** genannt.


### BIPoC

BIPoC steht für **Black, Indigenous and People of Color**, also Schwarz, Indigen und Personen of Color.

Darunter werden alle Menschen gefasst, die über ein oder mehrere Elternteile Vorfahren aus Teilen des afrikanischen Kontinents, Asiens und West-Asiens haben. Deren Vorfahren Rom\*nja, Sint\*ezza, indigene Menschen aus Australasien, aus Nord- und Südamerika, aus der Karibik oder aus dem Raum des Indischen Ozeans sind. Nachfahren von Europäer\*innen, welche aus kolonialen und imperialistischen Gründen nach Asien, Afrika oder in die Amerikas migriert sind, zählen nicht dazu.

(aus [https://www.ende-gelaende.org/news/hinweise-von-der-antira-fuer-die-massenaktion-2021/](https://www.ende-gelaende.org/news/hinweise-von-der-antira-fuer-die-massenaktion-2021/))


### weiß (sein)

**weiß** bzw. **weißsein** bezeichnet ebenso wie der Begriff PoC keine biologische Eigenschaft, sondern eine **politische und soziale Konstruktion**. Mit weißsein ist die dominante und privilegierte Position in dem Machtverhältnis Rassismus gemeint. Sie bleibt **häufig unausgesprochen und unbenannt**, obwohl zu jeder Diskriminierung sowohl eine diskriminierte, als auch eine privilegierte Position gehören. Im Gegensatz zu dem Begriff BIPoC ist weiß keine Selbstbezeichnung.

Um deutlich zu machen, dass **weißsein keine ermächtigende Selbstbezeichnung** ist, wird **weiß** oft klein geschrieben, im Gegensatz zu der empowernden **Selbstbezeichnung Schwarz**, welche dann groß geschrieben wird.

(aus [https://www.ende-gelaende.org/news/hinweise-von-der-antira-fuer-die-massenaktion-2021/](https://www.ende-gelaende.org/news/hinweise-von-der-antira-fuer-die-massenaktion-2021/))




### Racial Profiling/Rassistisches Profiling

Racial oder zu deutsch **rassistisches Profiling** bezeichnet Polizeimaßnahmen, welche nicht auf einem konkretem Anlass beruhen, sondern lediglich aufgrund der äußeren Merkmale der betroffenen Person. Dass diese Merkmale als Anlass hergenommen werden, liegt nicht unbedingt immer an einer offen rassistischen Haltung der Ausübenden, sondern oft an erlernten und verinnerlichten Rassismen und Vorurteile, die sich in unserer Gesellschaft etabliert haben. 

Da hierbei schützenswerte Merkmale (siehe Definition **Diskriminierung**) eines Menschen zu einer ungerechtfertigten Benachteiligung führen, ist diese Verhalten diskriminierend. Es sind auch keine Einzelfälle, sondern ein strukturelles Problem der Polizei in allen Ländern dieser Welt. In den USA gipfelte dies in der Ermordung von **BIPoCs** nach sogenannten **"verdachtsunabhängigen Kontrollen"** durch teilweise unerlaubte Schmerzgriffe. Doch auch in Deutschland und Europa ist dies eine übliche Praxis. Da diese Form des Rassimus etabliert in unserer Gesellschaft ist, und von staatlichen Behörden ausgeht, spricht mensch von institutionellem Rassismus. Die Ausübenden dieser Praxis reproduzieren sich selbst: da sie Menschen mit bestimmten Merkmalen verstärkt kontrollieren steigt die Statistik für diese Gruppen, oft durch ungerechtfertigte Vorwürfe, wodurch sie ihre **menschenverachtende** und deshalb **eigentlich verbotene Praxis** rechtfertigen.


Verdachtsunabhängige Kontrollen der Polizei können aber auch in anderer Weise diskriminierend sein, z.B. sexistisch oder klassistisch.


Die Polizei darf in einem Rechtsstaat grundsätzlich nur das, was ihr erlaubt ist, im Gegensatz zu den Bürger\*innen, denen grundsätzlich alles erlaubt ist, was nicht durch Gesetze verboten ist. Doch die Polizei überschreitet oft, sowohl unbewusst als auch bewusst, ihre Befugnisse, weshalb es wichtig ist, seine\*ihre Rechte zu kennen. Infos hierzu findest du zum Beispiel bei der Kampagne für Opfer rassistischer Polizeigewalt KOP ([https://kop-berlin.de/was-darf-die-polizei-was-darf-sie-nicht](https://kop-berlin.de/was-darf-die-polizei-was-darf-sie-nicht)). Da die Polizei aber eine Machtposition innehat, kann auch das Hinweisen auf ihre Befugnisse oft nicht ausreichen, dass die Polizei sich gesetzeswidrig verhält, und darauf folgende Gerichtsverfahren laufen oft ins Leere.




## Klassismus

**Klassismus** bezeichnet die Diskriminierung aufgrund der sozialen Herkunft und/oder der sozialen und ökonomischen Position. Es geht bei Klassismus also nicht nur um die Frage, wie viel Geld jemensch zur Verfügung hat, sondern auch welchen Status sie\*er hat und in welchen finanziellen und sozialen Verhältnissen sie\*er aufgewachsen ist. Klassismus richtet sich mehrheitlich gegen Personen einer „niedrigeren Klasse“.


Der Begriff Klassismus ist ein noch nicht sehr weit verbreiteter Begriff, der „classism" aus dem US-amerikanischen Kontext ins Deutsche transportiert. Er folgt nicht einer bestimmten Definition von Klasse, wie zum Beispiel der von den Philosophen und Soziologen Marx, Bourdieu oder Max Weber (z.B. bei Marx: Arbeiter\*innenklasse (Proletariat), Kleinbürger\*innnentum (= Mittelschicht) und Bourgeoisie (Kapitalist\*innen) , auch wenn es Überschneidungen zu den Definitionen gibt. Vielmehr wurde mit dem Begriff eine eigene Setzung vorgenommen, bei der nicht davon ausgegangen wurde, dass alle die oben genannten Theorien kennen. Der Begriff wurde maßgeblich durch die Erfahrungen von Communitys geprägt, die mehrfachdiskriminiert werden, also zum Beispiel durch Gruppen innerhalb der Frauenbewegung oder der „Black Movements“, die Klassismus erfahren. Mit dem Begriff werden deswegen verschiedene Diskriminierungsdimensionen aus einer **intersektionalen** Perspektive berücksichtigt. Außerdem umfasst der Begriff nicht nur die ökonomische Stellung von Menschen, sondern auch die verschiedenen Abwertungserfahrungen auf kultureller, politischer, institutioneller und individueller Ebene.

(aus [https://diversity-arts-culture.berlin/woerterbuch/klassismus](https://diversity-arts-culture.berlin/woerterbuch/klassismus))


## Ableismus

**Ableismus** ist ein am Englischen Abelism (englisch "able" = "fähig") angelehnter Begriff, der aus der US-amerikanischen Behindertenbewegung stammt. Er beschreibt die Diskriminierung von Menschen mit Behinderung, indem Menschen an bestimmten Fähigkeiten - laufen, sehen, sozial interagieren - gemessen und auf ihre Beeinträchtigung reduziert werden.


Ableismus betont die Ungleichbehandlung, Grenzüberschreitungen und stereotypen Zuweisungen die Menschen wegen ihrer Behinderung erfahren.  Es gibt eine normative Vorstellung davon, was Menschen leisten oder können müssen. Wer von dieser Norm abweicht, wird als behindert gekennzeichnet und als minderwertig wahrgenommen.

Die radikalste Form von Ableismus war die Euthanasie während des deutschen Nationalsozialismus, bei der Menschen mit Behinderung systematisch ermordet wurden.

Im heutigen Alltag bedeutet Ableismus, dass Menschen mit Behinderung immer damit rechnen müssen, die Ausnahme zu sein. Sie müssen sich oftmals speziell anmelden, um eine Kulturveranstaltung zu besuchen, und können zum Beispiel nicht einfach davon ausgehen, dass der Zugang zum Gebäude oder die Übersetzung in Gebärdensprache gewährleistet sind. Menschen mit Behinderung werden durch stereotype Darstellungen in den Medien diskriminiert und auf ihre Behinderung reduziert. Zudem werden die wenigen Rollen, die es für Menschen mit Behinderung im Film oder Theater gibt, oft von nicht-behinderten Darsteller\*innen gespielt, während Schauspieler\*innen mit Behinderung(en) es schwer haben, Rollenangebote zu bekommen, zumal solche, bei denen ihre Person und nicht die Behinderung im Vordergrund steht.

(aus [https://diversity-arts-culture.berlin/en/node/44](https://diversity-arts-culture.berlin/en/node/44))


In den Sozialwissenschaften wird die Bezeichnung "behindert" oder "Mensch mit Behinderung" verwendet. Allerdings nicht aus dem Grund, dass diese Menschen behindert sind, sondern weil sie von der Gesellschaft **behindert werden**.

(weiterführende Infos: [https://diversity-arts-culture.berlin/woerterbuch/behindert-werden](https://diversity-arts-culture.berlin/woerterbuch/behindert-werden))


## Sexismus

**Sexismus** bezeichnet die Diskriminierung aufgrund des Geschlechts. In einer männlich dominierten Gesellschaft (**Patriarchat** = "Herrschaft des Mannes") zeigt sich Sexismus jedoch vor allem in der Abwertung und Marginalisierung von **FINTA**\*s (= Frauen, Inter, Nicht Binäre, Transgender, Agender, \*)  und Weiblichkeit im Allgemeinen. (Cis-)Männlichkeit wird hier als Norm verstanden, an der alles gemessen wird.


Der Begriff Sexismus stammt aus dem Englischen (sexism). Im deutschen Kontext gibt es oft das Missverständnis, Sexismus beziehe sich vor allem auf diskriminierende Handlungen, die auf Sexualität bezogen sind, wie etwa sexuelle Belästigung. Obwohl diese Formen der sexuellen Gewalt auch Ausdruck von Sexismus sind, bezeichnet der Begriff jedoch ein sehr viel weiteres Spektrum geschlechtsbezogener Diskriminierung, etwa, dass Frauen im Durchschnitt weniger verdienen. In Deutschland verdienen Schauspielerinnen beispielsweise 46% weniger als Schauspieler.

Parallel zur Diskussion über Sexismus verbreitete sich in den 1960er Jahren auch der Begriff Rassismus, der die Diskriminierung ethnischer Gruppen beschreibt. In diesem Zusammenhang ist es wichtig zu verstehen, dass verschiedene Diskriminierungsformen sich überschneiden können (**Intersektionalität**). So ist eine **queere**, weiblich identifizierte **Person of Color** auf mehreren Ebenen Diskriminierung ausgesetzt. Die Frauenbewegung der 1960er und der darauffolgenden Jahre war, sowohl in den USA als auch in Deutschland, in der öffentlichen Wahrnehmung weiß dominiert. Schwarze feministische Positionen und Stimmen von Frauen of Color wurden innerhalb der Bewegung zur Seite gedrängt: Ihre Analysen der Machtverhältnisse, auch zwischen Frauen und auf internationaler Ebene, wurden ignoriert oder unterdrückt. Auch heute besteht die Gefahr, feministische Erfolge auf Kosten anderer marginalisierter Gruppen zu erlangen. Lange wurden von der feministischen Bewegung beispielsweise queer- und transfeministische Positionen ausgeblendet oder gar bekämpft. Diese Positionen beziehen die Begriffe Frau/Weiblichkeit und den Umgang mit geschlechtsspezifischer Diskriminierung nicht nur auf heterosexuelle Cis-Frauen.

(aus [https://diversity-arts-culture.berlin/woerterbuch/sexismus](https://diversity-arts-culture.berlin/woerterbuch/sexismus))


Mit Sexismus und dem Patriarchat, sowie der damit verbundenen Frauenfeindlichkeit (= **Misogynie**), gehen beispielsweise auch **toxische Männlichkeit** oder **Rape Culture/Vergewaltigungskultur** (also strukturell verankerte Verhaltensweisen und Prozesse, die sexualisierte Gewalt begünstigen) einher.


## FINTA\*

**FINTA\*** ist ein Überbegriff für Menschen, die nicht Cis-männlich sind. **Cis** bedeutet, dass ein Mensch sich mit dem ihm zugeschriebenen Geschlecht identifiziert. Ein Cis-Mann ist also eine Person, die bei ihrer Geburt oder auch in der gesellschaftlichen Wahrnehmung das Geschlecht "männlich" zugeschrieben wird, und sich damit auch identifizieren kann. Mit Geschlecht sind dabei nicht die anatomischen Merkmale eines Menschen gemeint (die sich übrigens nicht einfach binär in Mann und Frau einteilen lassen), sondern ein soziales Konstrukt, welches einige gesellschaftliche Ansprüche und oft toxische Erwartungen mit sich bringt.

**FINTA\*** steht für **Frauen, Inter, Nicht-Binäre, Transgender, Agender und Andere**, welche sich in diesen Selbstbezeichnungen nicht wiederfinden, gekennzeichnet durch das Sternchen. Dies ist die Gruppe der Menschen, die im Patriarchat diskriminiert werden.

FINTA\* hat zwar Überschneidungen mit dem Begriff "queer", bezeichnet aber nicht das selbe. **Queer** ist ein Sammelbegriff für Menschen, die nicht der **Heteronormativität**, also die sogenannte binäre Aufteilung in zwei Geschlechter (Mann und Frau) und/oder Heterosexualität (= romantische oder sexuelle Beziehung zwischen Mann und Frau) als Norm, entsprechen.


## toxische Männlichkeit

**Toxische Männlichkeit** beschreibt eine in unserer Gesellschaft vorherrschende Vorstellung von Männlichkeit und umfasst das **Verhalten, das Selbstbild und Beziehungskonzepte von Männern sowie kollektive männliche Strukturen**. Männer sollen keine Schwäche zeigen, höchstens Wut, sie sollen hart sein, aggressiv und nicht zärtlich oder liebevoll, schon gar nicht miteinander.

Toxische Männlichkeit findet in der Kindheit ihren Anfang und setzt sich nicht zuletzt in Männerbünden als Organisationsform auf allen Ebenen der Gesellschaft fort. Sie findet aber nicht nur „unter Männern“ statt, sondern richtet sich auch nach außen: In Form von Gewalt gegen andere, vor allem Frauen und Queers, und **sexualisierter Gewalt** gegen Menschen aller Geschlechter. Es geht immer auch um Sexualität: Nach den Vorannahmen von toxischer Männlichkeit muss ein Mann immer (heterosexuellen) Sex haben wollen und können. Dies ist ein wichtiger Baustein der **Vergewaltigungskultur (Rape Culture)** und verstärkt zudem das gefährliche Vorurteil, dass Männer nicht Opfer von sexualisierter Gewalt werden können.

Wer toxische Männlichkeit erlernt hat, lebt mit einem Mangel: Diese Personen haben meist kein gutes Verhältnis zu ihrem Körper, können ihre eigenen Grenzen ebenso wenig respektieren wie die anderer und haben Schwierigkeiten damit, Gefühle zuzulassen, zu zeigen und zu verarbeiten. Konsequenzen hieraus sehen wir etwa im schlechten Umgang heterosexueller cis Männer mit dem eigenen Körper, ihrer Nachlässigkeit gegenüber der eigenen Gesundheit und ihrer Tendenz zu Depressionen, Sucht und Suizid.

 (weiterführende Infos: [https://missy-magazine.de/blog/2018/08/16/hae-was-heisst-toxic-masculinity/](https://missy-magazine.de/blog/2018/08/16/hae-was-heisst-toxic-masculinity/))


## Intersektionalität

**Intersektionalität** ist die Verschränkung und Gleichzeitigkeit verschiedener Diskriminierungskategorien. Die Position eines Menschen innerhalb der gesellschaftlichen Machtstrukturen hängt also von mehreren Faktoren gleichzeitig ab. Eine Schwarze Feministin beispielsweise erfährt ganz andere Diskriminierung als eine weiße Feministin, trotzdem, dass sie die Diskriminierung als FINTA\*s verbindet. Mensch kann aber auch nicht pauschal sagen, dass zwei zutreffende Diskriminierungskategorien eine stärkere Diskriminerung als eine zutreffende Diskriminierungskategorie oder eine gleich starke wie zwei andere zutreffende Kategorien darstellt - die verschiedenen Faktoren addieren sich nämlich nicht, sondern sind viel mehr sehr komplex voneinander abhängig und bedingen sich manchmal auch gegenseitig. Manche Wissenschaftler\*innen sprechen deshalb auch oft von **Interdependenz** (= wechselseitige Abhängigkeit) anstatt von **Intersektionalität** (= Schnittmenge). Ein Begriff, der das Machtkonstrukt, in welchem mehrere Diskriminierungsebenen miteinander interagieren, beschreiben soll, ist **Kyriarchat**.

Deshalb wollen wir **Kämpfe verbinden**: Antirassismus geht nicht ohne Feminismus und umgekehrt, Antiableismus nicht ohne Antiklassismus und so weiter - um soziale Ungerechtigkeiten und toxische Machtverhältnisse zu bekämpfen, muss anerkannt werden, dass die verschiedenen Kämpfe zusammenhängen, und nur zusammen gekämpft Erfolge erzielen können.



## Queer

**Queer** ist ein Sammelbegriff für Personen, deren **geschlechtliche Identität** und/oder **sexuelle Orientierung** (wen sie begehren oder wie sie lieben) nicht der **Heteronormativität** (also die sogenannte binäre Aufteilung in zwei Geschlechter (Mann und Frau) und/oder Heterosexualität (= romantische oder sexuelle Beziehung zwischen Mann und Frau) als Norm) entspricht. Queer wird auch verwendet, um Bewegungen und Dinge zu bezeichnen, die mit queeren Menschen in Verbindung stehen, wie zum Beispiel die queere Szene, Queer Studies oder queere Filmfestivals.

Der Begriff kommt aus dem Englischen und bezeichnet zunächst Dinge oder Personen, die meist im negativen Sinn von der Norm abweichen. Er lässt sich mit „seltsam“, „eigenartig“ oder „sonderbar“ übersetzen. Er wurde benutzt, um abwertend insbesondere über Homosexuelle aber auch andere Personen zu sprechen, deren geschlechtliche Identität und/oder sexuelle Orientierung nicht der heteronormativen Norm entspricht. Im Zuge der Aids-Bewegung gelang es der queeren Community jedoch, den Begriff wieder **positiv zu bewerten (reclaiming)**, sodass für viele Menschen queer heute ein positiver Begriff ist und sie sich gerne queer nennen. Als Sammelbegriff ist das Wort sehr offen und bietet vielen Menschen ein Identifikationsangebot.
Wie bei allen Selbstbezeichnungen möchten sich aber nicht alle Personen, deren geschlechtliche Identität und/oder sexuelle Orientierung nicht der heteronormativen Norm entspricht, mit dem Begriff identifizieren. Manche finden andere Begriffe wie zum Beispiel schwul, lesbisch oder trans für sich besser. Sie haben Bedenken in der großen queeren Community an Sichtbarkeit zu verlieren. Manchen gefällt auch der politische Zusammenhang nicht, in dem queer verwendet wird. Außerdem wird das Wort queer immer noch sowohl als Schimpfwort als auch als Selbstbezeichnung verwendet.

Die vielen unterschiedlichen Begriffe zum Thema **"Queer"** hier in unserem Glossar zu erklären würde den Rahmen komplett sprengen, weshalb wir hier einige **weiterführende Links** zur Verfügung stellen, über die ihr euch weiter informieren könnt:

* [https://rainbow-europe.org/](https://rainbow-europe.org/) (Rechte von Queers in Europa)

* [https://www.aug.nrw/glossar/](https://www.aug.nrw/glossar/) ("Anders und gleich": Informations- und Antidiskriminierungsarbeit zu sexueller und geschlechtlicher Vielfalt, mit Glossar)

* [https://queer-lexikon.net/](https://queer-lexikon.net/) (Deine Online-Anlaufstelle für sexuelle, romantische und geschlechtliche Vielfalt)

Queere Communities und Anlaufstellen in Augsburg:

* [https://queer-augsburg.de/](https://queer-augsburg.de/) (Vernetzung, mit weiterführenden Links)

* [https://www.queerbeet-augsburg.de/](https://www.queerbeet-augsburg.de/) (queere Community, Vernetzung, Treffen)

* [https://www.csd-augsburg.de/](https://www.csd-augsburg.de/) (Christopher Street Day Augsburg)

* [https://augsburg.aidshilfe.de/de](https://augsburg.aidshilfe.de/de) (sexuell übertragbare Krankheiten, Tests, Beratung, Safer Sex)

* [https://www.augsburg.de/buergerservice-rathaus/gleichstellung/netzwerke-fachstellen](https://www.augsburg.de/buergerservice-rathaus/gleichstellung/netzwerke-fachstellen) (städtische Gleichstellungsstelle: weiterführende Links)



## Gendergerechte Sprache

In der deutschen Sprache wird noch immer häufig lediglich die männliche Wortform genutzt, unabhängig davon ob wirklich männliche Personen adressiert sind oder auch andere Geschlechter gemeint sind. 
Da wir uns in unseren Camp-Strukturen bemühen unser Miteinander so inklusiv wie möglich zu gestalten, nutzen wir gendergerechte Sprache sowohl im mündlichen als auch im schriftlichen Gebrauch. 
Es gibt verschiedene Möglichkeiten des **Genderns** im Sprachgebrauch. Weit verbreitet ist die Variante bei der zusätzlich zur männlichen Form eines Wortes auch noch die weibliche Form genannt wird, beispielsweise "Die meisten Radfahrer und Radfahrerinnen tragen einen Helm." Dabei gibt es im Schriftlichen mehrere Möglichkeiten, wie Radfahrer und Radfahrerinnen, RadfahrerInnen, Radfahrer/-innen. Diese Form des Genderns berücksichtigt jedoch leider nur das **binäre Geschlechtermodell** (also Mann und Frau) und denkt Menschen, die sich mit diesem nicht identifizieren nicht mit. 
Wir möchten deshalb in unseren Strukturen unseren Sprachgebrauch so anpassen, dass **alle Menschen mitgedacht werden**.
Dafür gibt es viele verschiedene Möglichkeiten, von denen die folgenden in unseren Strukturen wohl die meistgenutzten sind: 
* Der Genderstern: "Die meisten Radfahrer\*innen tragen einen Helm."
* Der Genderdoppelpunkt: "Die meisten Radfahrer:innen tragen einen Helm."
* Das Gerundium: "Die meisten Radfahrenden tragen einen Helm."
* Die Abkürzung von Wörtern mit der Endung -i : z.B. Schülis, Studis, Aktivistis, Mitbewohnis, etc.
Die ersten beiden Möglichkeiten hören sich im mündlichen Gebrauch gleich an, es wird dabei eine kleine Pause zwischen dem Radfahrer und dem innen eingebaut, sodass deutlich wird, dass nicht nur die weibliche Form damit gemeint ist.


## Barrierearme Sprache

Das Camp ist ein Ort der offen für alle sein soll. Wir wünschen uns einen inklusiven Umgang miteinander und haben auch das Ziel **Hierarchien abzubauen**. Um Wissenshierarchien abzubauen versuchen wir, das unterschiedliche Wissen, welches die verschiedenen Menschen im Camp haben, anderen Menschen **leicht zugänglich** zu machen. Neben Workshops, Skillshares oder einem transparenten Umgang mit Informationen, gehört dazu auch die verwendete Sprache möglichst **barrierearm** zu gestalten, also möglichst potentielle Barrieren für andere Menschen in dem Gesagten oder Geschriebenen zu vermeiden.
Das können zum Beispiel sein:

* Fachbegriffe

* Fremdwörter

* komplizierte, verschachtelte Sätze


Bei manchen Themen lassen sich Fremdwörter oder Fachbegriffe nicht vermeiden - in solchen Situationen lassen sich mögliche Barrieren abbauen, indem mensch die Bedeutung dieser Wörter erklärt.


## toxische Positivität

**Toxische Positivität** bezeichnet eine Lebenseinstellung, nach der mensch sämtliche Dinge und Situationen **ausschließlich positiv** interpretiert. Dabei werden negative Gefühle ignoriert oder erst gar nicht zugelassen. Eine Folge von toxischer Positivität ist eine **mangelnde Auseinandersetzung mit negativen** (*lieber "als negativ wahrgenommenen" oder "unangenehmen"*) **Emotionen** und damit einhergehend eine mangelnde Fähigkeit emotional zu lernen und Probleme zu überwinden.

## Konsens

**Konsens** bezeichnet eine Form der **Entscheidungsfindung**, bei der nicht nach dem Mehrheitsprinzip abgestimmt wird, sondern versucht wird, die **Bedürfnisse aller beteiligten Personen zu berücksichtigen** und in den Entscheidungsprozess mit einfließen zu lassen. Damit soll vermieden werden, dass Personen übergangen werden und sich nicht gehört fühlen. 
In der Praxis wird das Konsensprinzip oftmals mit der Zustimmungsabfrage durchgesetzt. Dabei gibt es verschiedene Konsensstufen, die einzeln nacheinander abgefragt werden, um die Meinung aller zu einem bestimmten Vorschlag einzuholen, bevor dieser als beschlossen gilt:
* **Zustimmung**: wenn mensch völlig hinter dem Vorschlag steht 
* **Leichte Bedenken**: wenn mensch dem Vorschlag prinzipiell zustimmt, jedoch leichte Bedenken hat 
* **Schwere Bedenken**: wenn mensch schwere Bedenken gegenüber dem Vorschlag hat und sich eine andere Lösung wünschen würde
* **Veto**: wenn mensch in keinster Weise mit dem Vorschlag einverstanden ist. Ein Veto bedeutet (wenn nicht vorab von der Gruppe anders beschlossen), dass der Vorschlag in dieser Weise nicht beschlossen werden kann.
* **Enthaltung**: wenn mensch in der Abstimmung über den Vorschlag nicht beteiligt sein möchte, jedoch die Entscheidung, die getroffen wird, mitträgt.

Wenn von mindestens einer Person leichte oder schwere Bedenken im Entscheidungsprozess geäußert werden, sollte in der Gruppe über diese gesprochen werden. Es gilt dann einen Kompromiss zu finden, mit dem sich alle Beteiligten insoweit wohlfühlen, dass sie die Entscheidung mittragen können.

Auch das Prinzip des Konsens kann kritisiert werden. Eine ausführliche Auseinandersetzung mit hierarchiefreier Entscheidungsfindung findest du auf dieser Seite: [https://www.projektwerkstatt.de/index.php?domain_id=1&a=20205](https://www.projektwerkstatt.de/index.php?domain_id=1&a=20205)


## Gewalt/Gewaltfreiheit 
Die Weltgesundheitsorganisation definiert **Gewalt** in dem Bericht "Gewalt und Gesundheit" (2002) wie folgt:
* "*Gewalt ist der tatsächliche oder angedrohte absichtliche Gebrauch von physischer oder psychologischer Kraft oder Macht, die gegen die eigene oder eine andere Person, gegen eine Gruppe oder Gemeinschaft gerichtet ist und die tatsächlich oder mit hoher Wahrscheinlichkeit zu Verletzungen, Tod, psychischen Schäden, Fehlentwicklung oder Deprivation führt.*"[2]
* "*Unter Gewalt verstehen wir jeden Versuch, sich selbst oder andere Menschen zu bestrafen oder die eigenen Bedürfnisse ohne Rücksicht auf die Bedürfnisse anderer Menschen durchzusetzen.*" (GFK Verständnis)
* Manche Menschen sehen auch physische Einwirkungen auf Dinge als Gewalt an, manche nicht. Wir möchten hier folgende Perspektive aufzeigen: wir leben in einer Gesellschaft, in der Dinge meist Privateigentum sind. Ausserdem schreiben wir Menschen Dingen oft Bedeutungen zu, oder sehen sie als Symbol von Zugehörigkeit zu etwas. Wird also mit physischer Kraft auf Dinge eingewirkt, kann es dazu führen, dass Menschen oder Menschengruppen diese Kraftanwendung als Gewalt oder Gewaltandrohung gegen sich selbst sehen, ganz unabhängig davon, ob das so gemeint war, oder diese Kraftanwendung als legitim oder illegitim betrachtet wird.

Wenn wir einen Raum der **Gewaltfreiheit** herstellen wollen, bedeutet das zum einen **Machtstrukturen so niedrig wie möglich** zu halten und zum anderen unsere gemeinsamen **Bedürfnisse** im Blick zu behalten, sowie den Raum zu halten, dass jede Person auf ihr Wohl achten kann. 

Besonders im Blick: Machtmissbrauch durch hierarchische Strukturen vermeiden
Jegliche Formen von Diskriminierung zu vermeiden und uns daran zu erinnern
Schützende Macht setzen wir sofortig ein, wenn Leben, Würde, Eigentum eines Menschen in Gefahr ist.
WoFÜR sind wir? - nicht woGEGEN? 
Feindbilder vermeiden (Ich bin  nicht gegen eine Person - sondern gegen ihr Handeln)

Aggression als wichtige Lebensenergie (von aggredere: ich geh auf etwas zu, ich verfolge ein Ziel und will es erreichen) unterscheiden wir von Aggressivität die sich selbst oder anderen Schaden zufügt)

## sexualisierte Gewalt

### Täterschutz

Wir unterscheiden strukturellen und aktiven Täterschutz. 

Der strukturelle T. hält die "Rape Culture" aufrecht ( z.B. sexistische Witze, Diskriminierendes Verhalten, etc)was sich vor allem auf das Denken und Bewerten von Dingen in einer Gemeinschaft auswirkt. 

Unter aktivem Täterschutz verstehen wir Verhaltensweisen wie  - es passiert gar nichts,  - Menschen schauen weg, - es gibt keine Konsequenzen und die GaP\* (Gewalt ausübende Person) muss sich nicht mit ihrem Handeln Auseinandersetzen, Personen werden geschützt. 

Diese Mechanismen sind so tief in unserer Gesellschaft verwurzelt  - auch oft kaum bemerkbar - dass Menschen sie oft (bewusst oder) unbewusst verstärken und reproduzieren. Dazu gehört z.B. fehlende Konfrontation der gewaltausübenden Person,aktives Vivtim Blaming, Runterspielen oder Anzweifeln des Geschehenen, fehlende Solidarische Parteilichkeit und ein Aufrechthalten der vermeintlich neutralen Zwei-Seiten-Einstellung.


Auf die GaP\* zuzugehen kann mit der Motivation geschehen, dass eine Transformation stattfindet  d.h. die GaP\* hat die Gelegenheit in einer ehrlichen Auseinandersetzugn mit sich selbst, ihre Handlungen zu reflektieren und Verantwortung zu übernehmen und damit längerfristig zu verändern)

 

Achtung: im Schock und der Fassungslosigkeit drehen sich Gedanken oft um die GaP\* mit Fragen wie: "Bevor ich nicht weiß was genau passiert ist, kann ich keine Entscheidung treffen" - oder "was soll mit dieser Person geschehen" ohne die Bedürfnisse der betroffenen Person im Blick zu haben. So wird der Fokus und die Kraft oft weg von der notwendigen Unterstützungsarbeit gelenkt. 


\*GaP = Gewalt ausübende Person


(Quelle: [gegengewalt.blackblogs.org/broschure](gegengewalt.blackblogs.org/broschure))



### Solidarische Parteilichkeit

**Parteilichkeit** wird als eindeutig zu einer Partei des Konflikts zugewandtem Verhalten beschrieben, um diese zu stärken. Im Fall von **sexualisierter Gewalt** wollen wir eine solidarisch parteiliche Haltung mit der Betroffenen Person leben. Im Gegensatz dazu bemüht sich **Allparteilichkeit** um den bewusst ausgeglichenen Rahmen für beide Parteien (z.B. Redezeit, Zugewandtheit, s.a. Deutungshoheit), um der Konfliktlösung Raum zu geben.

In der Parteilichkeit geht es vorrangig um den **Schutz der\*des Betroffenen**, sich sofort sicher und gehört und mit dem Erlebten auch angenommen zu fühlen. 

Parteilichkeit muss nicht über "richtig und falsch" und "wer hat Recht  - und wer ist schuld" entscheiden - hier geht es um eindeutige Positionierung zu der betroffenen Person für ihre emotionale Sicherheit im Sinne von "Ich interessiere mich dafür wie es dir geht - ich glaube dir und erkenne an was du erlebt hast - ich stelle nichts in Frage was du sagst - ich nehme dich in deiner schwierigen Situation an und bleibe da solang du es brauchst."

Um in späteres Schritten einer möglichen Konfliktlösung näher zu kommen, bietet es sich an, andere Menschen (als die Unterstützergruppe für die betroffene Person) als Kontakt für den\*die Täter\*in anzubieten.


### Deutungshoheit (= Definitionsmacht)

**Menschen erleben ihre Grenzen** (manchmal auch je nach Situation und Tagesform) **unterschiedlich** und damit auch die Überschreitung ihrer Grenzen. Dieses Erleben (da ging jemand zu weit, dieses Verhalten hat mich verletzt) ist absolut individuell und wird in der Erzählung der betroffenen Person ernst genommen.

Wenn sich jemensch an die Awareness AG wendet, braucht es diesen unbedingten und **sofortigen Schutzraum für die betroffene Person ohne den Sachverhalt zu diskutieren oder gar anzuzweifeln**.


## Weiterführende Links:

* [https://gender-glossar.de/glossar/alphabetisches-glossar](https://gender-glossar.de/glossar/alphabetisches-glossar) (umfangreiches Glossar zu Geschlecht, Sexualität und anderen Gerechtigkeitsthemen)


* [https://diversity-arts-culture.berlin/](https://diversity-arts-culture.berlin/) (beschäftigt sich mit dem übergeordneten Thema "Diversität" (=Vielfalt) und hat ein umfangreiches Glossar)


* [https://rainbow-europe.org/](https://rainbow-europe.org/) (Rechte von Queers in Europa)


* [https://www.aug.nrw/glossar/](https://www.aug.nrw/glossar/) ("Anders und gleich": Informations- und Antidiskriminierungsarbeit zu sexueller und geschlechtlicher Vielfalt, mit Glossar)


* [https://queer-lexikon.net/](https://queer-lexikon.net/) (Deine Online-Anlaufstelle für sexuelle, romantische und geschlechtliche Vielfalt)


* [https://www.projektwerkstatt.de/index.php](https://www.projektwerkstatt.de/index.php) (sehr ausführliche Seite zu politischer Theorie und Aktivismus. Auch Seiten zu Hierarchien, Gewalt)


* [https://missy-magazine.de/](https://missy-magazine.de/) (Feministisches Magazin)



## Hilfe und Anlaufstellen

### Probleme im Camp, Redebedarf (auch zu nicht Camp-Themen), Konfliktlösung

* Telegram: @AwarenessAux_bot

* Im Camp ist das Awareness-Team an Holz-Buttons mit einem "A" zu erkennen (wir sind aber nicht immer da, und haben auch nicht immer Kapazitäten, um in der Rolle als Awareness-Mensch auftreten zu können)

* Emo-Runden: wird in unseren Gruppen bekanntgegeben. Einfach #awareness in die Suche eingeben (so findest du auch Nachrichten, in denen die einzelnen Menschen von uns stehen, wenn du mit jemensch bestimmtes reden möchtest) ;D


### Gewalterfahrungen

* Hilfetelefon für Frauen: 08000 116 016 [https://www.hilfetelefon.de/aktuelles.html](https://www.hilfetelefon.de/aktuelles.html)
    
* Hilfetelefon für Männer: 0800 1239900 [https://www.maennerhilfetelefon.de/](https://www.maennerhilfetelefon.de/)

* Frauenhaus Augsburg: 0821 650 87 40 - 10 [https://www.awo-augsburg.de/familie-kinder-frauen/frauenhaus-augsburg.html](https://www.awo-augsburg.de/familie-kinder-frauen/frauenhaus-augsburg.html)

* sexualisierte Gewalt Augsburg: [https://www.wildwasser-augsburg.de/home.html](https://www.wildwasser-augsburg.de/home.html)


### Lebenskrisen und/oder akute Hilfe

* Notarzt, Rettungsdienst: 112

* Notaufnahme Josefinum: 0821 2412-0

* Notaufnahme BKH: 0821 4803-4000

* Krisendienst Bayern: 0800 655 3000

* Telefonseelsorge/Krisentelefon: 0800 111 0 111 oder 0800 111 0 222

* Nummer gegen Kummer für Jugendliche: 0800 111 0 333 oder 116111

* Krisenchat: [https://krisenchat.de/?chat=true](https://krisenchat.de/?chat=true)

* keine akute Hilfe! Informationen zu Depression: [https://www.deutsche-depressionshilfe.de/start](https://www.deutsche-depressionshilfe.de/start)

* keine akute Hilfe! sozialpsychiatrische Dienste in Augsburg: [https://www.diakonie-augsburg.de/de/sozialpsychiatrische-dienste](https://www.diakonie-augsburg.de/de/sozialpsychiatrische-dienste)

### Anlaufstellen Augsburg

* AWO: Hilfe für Familien, Behinderte, Suchtkranke und Übergangswohnheime [https://www.awo-augsburg.de/](https://www.awo-augsburg.de/)

* Frauenhaus: 0821 650 87 40 - 10 [https://www.awo-augsburg.de/familie-kinder-frauen/frauenhaus-augsburg.html](https://www.awo-augsburg.de/familie-kinder-frauen/frauenhaus-augsburg.html)

* SOLWODI: Hilfe für Frauen, Schwerpunkte Menschenhandel, Zwangsverheiratung, Prostitution, Gewalterfahrungen, Missbrauch, Migrant\*innen [https://www.solwodi.de/seite/353241/augsburg.html](https://www.solwodi.de/seite/353241/augsburg.html)

* Wärmestube Augsburg [https://www.skm-augsburg.de/hilfe-beratung/wohnungslosenhilfe/waermestube](https://www.skm-augsburg.de/hilfe-beratung/wohnungslosenhilfe/waermestube)

* SKM: Hilfe für Männer, Obdachlosigkeit, Straffälligenhilfe, Schutzhaus, Übergangswohnheim [https://www.skm-augsburg.de/](https://www.skm-augsburg.de/)

* SKF: Hilfe für Frauen, Familie, Kinder, Jugendliche, Frauenhaus, Übergangswohnheim, Gewalterfahrungen [https://www.skf-augsburg.de/](https://www.skf-augsburg.de/)

* Weisser Ring: Hilfe für Betroffene von Kriminalität, Gewalterfahrungen [https://augsburg-bayern-sued.weisser-ring.de/](https://augsburg-bayern-sued.weisser-ring.de/)

* Aidshilfe: sexuell übertragbare Krankheiten, Tests, Beratung, Safer Sex [https://augsburg.aidshilfe.de/de](https://augsburg.aidshilfe.de/de) 

* profamilia: Verhütung, Schwangerschaft, Sexualberatung [https://www.profamilia.de/index.php?id=743](https://www.profamilia.de/index.php?id=743)

* Diakonie: sozialpsychiatrische Dienste in Augsburg: [https://www.diakonie-augsburg.de/de/sozialpsychiatrische-dienste](https://www.diakonie-augsburg.de/de/sozialpsychiatrische-dienste)
