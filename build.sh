#!/usr/bin/env nix-shell
#! nix-shell -I nixpkgs=https://nixos.org/channels/nixos-21.05/nixexprs.tar.xz -i bash -p ruby.devEnv bundix rake jekyll rubyPackages.public_suffix rubyPackages.colorator rubyPackages.concurrent-ruby rubyPackages.eventmachine rubyPackages.rake rubyPackages.httpclient rubyPackages.ffi rubyPackages.i18n rubyPackages.sassc nodejs perl

set -e

[ -e podcast.xml ] || cd /klimacamp-augsburg

#git reset --hard origin/master
#git fetch
#git merge origin/master

bundle install  # --path ./vendor
bundle exec jekyll build

#/home/iblech/.local/share/gem/ruby/2.7.0/gems/jekyll-4.0.1/exe/jekyll build

find _site -name '*.html' -print0 | \
  xargs -0 \
    sed -i \
      -e 's#<script type="text/javascript"#<script async="async"#g' \
      -e 's#<nav role="navigation"#<nav#' \
      -e 's#<title>[^<]*</title>##'

find _site/en -name '*.html' -print0 | \
  xargs -0 \
    sed -i \
      -e 's#lang="de"#lang="en"#'

./relativize-urls.sh

#cd _site
#mkdir -p https:
#[ -e https:/augsburg.klimacamp.eu ] || ln -s .. https:/augsburg.klimacamp.eu
#../node_modules/.bin/inline-css -i index.html
#mv _inlined.html index.html
