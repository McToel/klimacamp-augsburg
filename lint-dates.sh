#!/bin/sh

# SPDX-FileCopyrightText: 2021 Michael Struwe <michaelstruwe@gmx.net>
# SPDX-License-Identifier: LGPL-3.0-or-later

# Usage: Just run the script.  If there's no output, everything is fine.  If
# there is output, it should be self-explanatory how to fix it.

# Looks for all files that match 20??-*.md in the given directories and compares
# the date in the date: field and the (german) date that is (by our convention)
# in the title: field.

# Incorrect dates break the ordering in the RSS feeds.

dirs=${1-pages/Pressemitteilungen/ pages/offeneBriefe/}

err() {
    printf "%s\n" "$1"
    exit 1
}

for file in $(find $dirs -type f -name '20??-*.md'); do
    date="$(grep '^date: ' $file | awk '{print $2}')"
    title_date="$(grep '^title: ' $file | awk '{print $2}' \
        | sed -e 's/^"//' -e 's/:$//')"
    title_date_iso="$(printf "%s\n" "$title_date" \
        | sed 's/\(.\?.\)\.\(.\?.\).\(....\)/\3-\2-\1/' \
        | sed 's/-\([0-9]\)-/-0\1-/' \
        | sed 's/-\([0-9]\)$/-0\1/')"

    if [ "$date" != "$title_date_iso" ]; then
        echo "$file"
        echo "  date: $date"
        echo "  title: $title_date"
    fi
done
