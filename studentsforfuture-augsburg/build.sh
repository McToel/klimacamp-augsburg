#!/usr/bin/env nix-shell
#! nix-shell -i bash -p ruby.devEnv bundix rake jekyll rubyPackages.public_suffix rubyPackages.colorator rubyPackages.concurrent-ruby rubyPackages.eventmachine rubyPackages.rake rubyPackages.httpclient rubyPackages.ffi rubyPackages.i18n rubyPackages.sassc nodejs

set -e

cd /klimacamp-augsburg/studentsforfuture-augsburg

git reset --hard origin/master
git fetch
git merge origin/master

bundle install  # --path ./vendor
bundle exec jekyll build

find _site -name '*.html' -print0 | \
  xargs -0 sed -ie 's#<script type="text/javascript"#<script async="async" type="text/javascript"#g'

exit 0
cd _site
mkdir -p https:
[ -e https:/ravensburg.klimacamp.eu ] || ln -s .. https:/ravensburg.klimacamp.eu
../node_modules/.bin/inline-css -i index.html
mv _inlined.html index.html
